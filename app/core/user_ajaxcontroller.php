<?php	
//ini_set('max_execution_time', 500);	
require_once './../global/global-config.php';
require_once '../config/config.php';
require_once '../app/models/Model.php';
require_once 'classes/PHPMailerAutoload.php';

class Ajaxcontroller extends Model
{		

/*--------------------------------------------- 
				Login Authentication
----------------------------------------------*/ 

 // User login

function userLogin($data){
	$email 		= $data['email'];
	$passw 		= $data['password'];
	$password 	= $this->encryptPassword($passw);
	$check = $this -> check_query(EMPLOYEE,"id"," email ='$email' AND password ='$password' AND status ='1' ");
	if($check == 1){        	
    	$userinfo = $this->getDetails(EMPLOYEE,"id,email,super_admin,role"," email ='$email' AND password ='$password' ");
        $permissioninfo = $this -> getDetails(PERMISSION,"*"," employee_id='".$userinfo['id']."' ");

        $_SESSION["crm_admin_id"]           = $userinfo["id"];  
        $_SESSION["super_admin"]            = $userinfo["super_admin"];
        $_SESSION["employee_role"]          = $userinfo["role"];

        $_SESSION["customers"]              =  $permissioninfo["customers"];
        $_SESSION["manage_customer"]        =  $permissioninfo["manage_customer"];
        $_SESSION["import_customer"]        =  $permissioninfo["import_customer"];

        $_SESSION["lead"]                   =  $permissioninfo["lead"];
        $_SESSION["manage_lead"]            =  $permissioninfo["manage_lead"];
        $_SESSION["lead_type"]              =  $permissioninfo["lead_type"];
        $_SESSION["activity_type"]          =  $permissioninfo["activity_type"];        
        $_SESSION["lead_source"]            =  $permissioninfo["lead_source"];
        $_SESSION["customer_profile"]       =  $permissioninfo["customer_profile"];
        $_SESSION["import_lead"]            =  $permissioninfo["import_lead"]; 

        $_SESSION["employee"]               =  $permissioninfo["employee"];
        $_SESSION["manageemployee"]         =  $permissioninfo["manageemployee"];

        $_SESSION["reports"]                =  $permissioninfo["reports"];
        $_SESSION["lead_report"]            =  $permissioninfo["lead_report"];
        $_SESSION["customer_report"]        =  $permissioninfo["customer_report"];
        
        $_SESSION["settings"]               =  $permissioninfo["settings"];        
        $_SESSION["chit_master"]            =  $permissioninfo["chit_master"]; 
        $_SESSION["deposit_master"]         =  $permissioninfo["deposit_master"];
        $_SESSION["vam_master"]             =  $permissioninfo["vam_master"];
        $_SESSION["branch_master"]          =  $permissioninfo["branch_master"];
        $_SESSION["employee_role_master"]   =  $permissioninfo["employee_role_master"];

     	if($this->sessionIn($user_type="admin",$finance_type="SS CHIT",$_SESSION["crm_admin_id"],"web","browser")){
     		return 1;
     	}   
    }else{
    	return $this->errorMsg("Sorry your account details are wrong.");
    }
}   
    
    /*---------------------------------------------
            Set Branch in sellion 
    ---------------------------------------------*/

    function setSellectedBranch($data)
    {   
        $id     = $data;
        $check  = $this->check_query(BRANCH_MASTER,"*","status='1' AND id='".$id."' ");
            if($check) {
                $b_info = $this->getDetails(BRANCH_MASTER,"branch","id='".$id."'");
                $_SESSION['selected_branch']      = $id;
                $_SESSION['selected_branch_name'] = $b_info['branch'];
                return "1`Success";
            } else {
                $msg = $this->errorMsg("Sorry Unexpected Error Occurred. Please Try Again.");
                return "0`$msg";
            }
        $list   = mysqli_fetch_assoc($exe);
        return $list;
    }

    function branchNotAssigned()
    {   
        $_SESSION['selected_branch']      = "branch_not_assignes";
        $_SESSION['selected_branch_name'] = "Branch not assigned";
        return 1;
    }

    // Change ADMIN_TBL Password

    function changePassword($data)
    {
        if(isset($_SESSION['change_password_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['change_password_key']){
                $curr           = date("Y-m-d H:i:s");
                $check_password = $this->check_query(EMPLOYEE,"id", " id= '".$_SESSION['crm_admin_id']."' AND
                      password= '".$this->encryptPassword($data['password'])."' ");
                if($check_password==1){
                    $query= "UPDATE ".EMPLOYEE." SET
                            password = '". $this->encryptPassword($data['new_password'])."',
                            updated_at='$curr' WHERE id='".$_SESSION['crm_admin_id']."' ";
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['change_password_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";  
                    }
                }else{
                    return $this->errorMsg("Your Current Password is wrong.");
                }
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit Profile 

    function editProfile($data)
    {
        if(isset($_SESSION['edit_profile_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_profile_key']){         
                    //$user_id        = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $token          = $this->generateRandomString("40");
                    $id             = $this->decryptData($data['session_token']);
                   // $dob            = (($data['dob']!="") ? $this->changeDateFormat($data['dob']) : '' ); 
                    $query = "UPDATE ".EMPLOYEE." SET   
                        token       = '".$this->hyphenize($data['name'])."',                 
                        name        = '".$this->cleanString($data['name'])."',
                        email       = '".$this->cleanString($data['email'])."',
                        mobile      = '".$this->cleanString($data['mobile'])."',
                        gender      = '".$this->cleanString($data['gender'])."',
                        address     = '".$this->cleanString($data['address'])."',
                        city        = '".$this->cleanString($data['city'])."',
                        state       = '".$this->cleanString($data['state'])."',
                        pincode     = '".$this->cleanString($data['pincode'])."',
                        updated_at  = '$curr' WHERE id='$id' ";
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['edit_profile_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    /*----------------------------
         Members Customer
    ------------------------------*/

    // Add Customer 

    function addCustomer($data="")
    {
        if(isset($_SESSION['add_customer_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_customer_key']){  
                $e_check = $this -> check_query(CUSTOMER_TBL,"id"," email ='".$data['email']."' ");
                if($e_check == 0){  
                    $m_check = $this -> check_query(CUSTOMER_TBL,"id"," mobile ='".$data['mobile']."' "); 
                    if($m_check == 0){  
                        $admin_id       =$_SESSION["crm_admin_id"];
                        $curr           = date("Y-m-d H:i:s");
                        $token          = $this->generateRandomString("30");
                        $password       = $this->generateRandomStringGenerator("8");
                        $psw            = $this->encryptPassword($password);
                        $query = "INSERT INTO ".CUSTOMER_TBL." SET 
                            branch_id   = '".((isset($_SESSION['selected_branch']))?  $_SESSION['selected_branch'] : '')."',
                            token       = '".$this->hyphenize($data['name'])."',
                            priority    = '".$data['priority']."',
                            name        = '".$this->cleanString($data['name'])."',
                            gender      = '".$this->cleanString($data['gender'])."',
                            mobile      = '".$this->cleanString($data['mobile'])."',
                            email       = '".$this->cleanString($data['email'])."',                            
                            password    = '".$psw."',
                            has_psw     = '".$password."',
                            address     = '".$this->cleanString($data['address'])."',
                            city        = '".$this->cleanString($data['city'])."',
                            state       = '".$this->cleanString($data['state'])."',
                            pincode     = '".$this->cleanString($data['pincode'])."',
                            kyc_status  = '0',
                            assign_status = '0',
                            kyc_remarks = '',
                            status      = '1',
                            added_by    = '$admin_id',  
                            created_at  = '$curr',
                            updated_at  = '$curr' ";    
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            $customer_id = "CU".str_pad($last_id, 4,0, STR_PAD_LEFT);
                            $q =" UPDATE ".CUSTOMER_TBL." SET uid='".$customer_id."' WHERE id='".$last_id."' ";
                            $result = $this->selectQuery($q);
                            

                            $sender_mail    = NO_REPLY;
                            $subject        = ucwords($data['name'])." Account login details";
                            $receiver       = $data['email'];
                            $user_token     = $this->hyphenize($data['name']);
                            $email_temp     = $this->memberLoginInfoTemp($data['name'],$data['email'],$password,$user_token,$last_id,$data['name']);
                            $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                            if($sendemail){
                                unset($_SESSION['add_customer_key']);
                                return 1;
                            }
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                    return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
                }       
                }else{
                return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
            }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit Customer 

    function editCustomer($data="")
    {
        if(isset($_SESSION['edit_customer_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_customer_key']){  
                $id             = $this->decryptData($data['session_token']);
                $admin_id       =$_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $e_check = $this -> check_query(CUSTOMER_TBL,"id"," email ='".$data['email']."' AND id!='".$id."' ");
                if($e_check == 0){  
                    $m_check = $this -> check_query(CUSTOMER_TBL,"id"," mobile ='".$data['mobile']."' AND id!='".$id."'  "); 
                    if($m_check == 0){  
                        $query = " UPDATE ".CUSTOMER_TBL." SET 
                            token       = '".$this->hyphenize($data['name'])."',
                            priority    = '".$data['priority']."',
                            name        = '".$this->cleanString($data['name'])."',
                            gender      = '".$this->cleanString($data['gender'])."',
                            mobile      = '".$this->cleanString($data['mobile'])."',
                            email       = '".$this->cleanString($data['email'])."',   
                            address     = '".$this->cleanString($data['address'])."',
                            city        = '".$this->cleanString($data['city'])."',
                            state       = '".$this->cleanString($data['state'])."',
                            pincode     = '".$this->cleanString($data['pincode'])."',
                            updated_at  = '$curr' WHERE id='$id' ";    
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            unset($_SESSION['edit_customer_key']);
                            return 1;
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                        return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
                    }       
                }else{
                return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
            }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Avtive & Inactive Status 

    function CustomerStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(CUSTOMER_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".CUSTOMER_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".CUSTOMER_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

   
    /*--------------------------------------
                Employee management
    ----------------------------------------*/

    // Add  Employee 

    function addEmployee($data="")
    {
        if(isset($_SESSION['add_employee_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_employee_key']){  
                $e_check = $this -> check_query(EMPLOYEE,"id"," email ='".$data['email']."' ");
                if($e_check == 0){  
                    $m_check = $this -> check_query(EMPLOYEE,"id"," mobile ='".$data['mobile']."' "); 
                    if($m_check == 0){  

                        if($data['role']==2) {
                            $assigned_branch = implode(',', $data['assigned_branch_multi']);
                            $branch_id = implode(',', $data['assigned_branch_multi']);
                        } else if($data['role']==3) {
                            $assigned_branch = $data['assigned_branch'];
                            $branch_id = $data['assigned_branch'];
                        } else if($data['role']!=1 && $data['role']!=2 && $data['role']!=3 ) {
                            $bm_branch = $this->getDetails(EMPLOYEE,"branch_id","id='".$data['assigned_branch_manager']."'");
                            $branch_id = $bm_branch['branch_id'];
                            $assigned_branch = "";
                        } else {
                            $assigned_branch = "";
                            $branch_id       = ((isset($_SESSION['selected_branch']))?  $_SESSION['selected_branch'] : '');
                        }

                        if($_SESSION['super_admin']==1) {
                            if($data['role']==3) {
                                $assigned_employee = $data['assigned_general_manager'];
                            } elseif($data['role']!=1 && $data['role']!=2 && $data['role']!=3) {
                                    $assigned_employee = $data['assigned_branch_manager'];
                            } else {
                                $assigned_employee = "";
                            }       
                        } elseif($_SESSION['employee_role']==2){
                            if($data['role']==3) {
                                $assigned_employee = $_SESSION['crm_admin_id'];
                            } elseif($data['role']!=1 && $data['role']!=2 && $data['role']!=3) {
                                    $assigned_employee = $data['assigned_branch_manager'];
                            } else {
                                $assigned_employee = "";
                            } 
                        } elseif($_SESSION['employee_role']==3) {
                            $assigned_employee = $_SESSION['crm_admin_id'];
                        } else {
                            $assigned_employee = "";
                        }


                        $admin_id       =$_SESSION["crm_admin_id"];
                        $curr           = date("Y-m-d H:i:s");
                        $token          = $this->generateRandomString("30");
                        //$password       = $this->generateRandomStringGenerator("8");
                        //$psw            = $this->encryptPassword($password);
                        $em_password    = $this->encryptPassword($data['password']);
                        $query = "INSERT INTO ".EMPLOYEE." SET 
                            branch_id           = '".$branch_id."',
                            token               = '".$this->hyphenize($data['name'])."',
                            name                = '".$this->cleanString($data['name'])."',
                            role                = '".$this->cleanString($data['role'])."',
                            gender              = '".$this->cleanString($data['gender'])."',
                            password            = '".$em_password."',
                            email               = '".$this->cleanString($data['email'])."',
                            mobile              = '".$this->cleanString($data['mobile'])."',
                            address             = '".$this->cleanString($data['address'])."',
                            city                = '".$this->cleanString($data['city'])."',
                            state               = '".$this->cleanString($data['state'])."',
                            pincode             = '".$this->cleanString($data['pincode'])."',
                            assigned_branch     = '".$assigned_branch."',
                            assigned_employee   = '".$assigned_employee."',
                            status              = '1',
                            super_admin         = '0',
                            added_by            = '$admin_id',   
                            created_at          = '$curr',
                            updated_at          = '$curr' "; 
                        $link    = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe     = mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            $employee_uid = "EMP".str_pad($last_id, 4,0, STR_PAD_LEFT);
                            $q            = " UPDATE ".EMPLOYEE." SET employee_uid='".$employee_uid."' WHERE id='".$last_id."' ";
                            $result       = $this->selectQuery($q);
                            $l_info       = $this->getDetails(EMPLOYEE,"role","id='".$last_id."'");

                            $employee_permission = $this->insertEmployeePermission($last_id,$l_info['role']);

                           /* $sender_mail    = NO_REPLY;
                            $subject        = "Login details for"." - ".ucwords($data['company_name'])." - Account login informatotion.";
                            $receiver       = $data['email'];
                            $user_token     = $this->hyphenize($data['company_name']);
                            $email_temp     = $this->memberLoginInfoTemp($data['member_name'],$data['email'],$password,$user_token,$last_id,$data['company_name']);
                            $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                            if($sendemail){
                                unset($_SESSION['add_members_key']);
                                return 1;
                            }*/

                            unset($_SESSION['add_employee_key']);
                            return 1;
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                    return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
                }       
                }else{
                return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
            }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit  Employee 

    function editEmployee($data="")
    {
        if(isset($_SESSION['edit_employee_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_employee_key']){ 
                $id = $this->decryptData($data['session_token']); 
                $e_check = $this -> check_query(EMPLOYEE,"id"," email ='".$data['email']."' AND id!='".$id."' ");
                if($e_check == 0){  
                    $m_check = $this -> check_query(EMPLOYEE,"id"," mobile ='".$data['mobile']."' AND id!='".$id."' "); 
                    if($m_check == 0){  

                        if($data['role']==2) {
                            $assigned_branch = implode(',', $data['assigned_branch_multi']);
                            $branch_id = implode(',', $data['assigned_branch_multi']);
                        } else if($data['role']==3) {
                            $assigned_branch = $data['assigned_branch'];
                            $branch_id = $data['assigned_branch'];
                        } else if($data['role']!=1 && $data['role']!=2 && $data['role']!=3 ) {
                            $bm_branch = $this->getDetails(EMPLOYEE,"branch_id","id='".$data['assigned_branch_manager']."'");
                            $branch_id = $bm_branch['branch_id'];
                            $assigned_branch = "";
                        } else {
                            $assigned_branch = "";
                            $branch_id       = ((isset($_SESSION['selected_branch']))?  $_SESSION['selected_branch'] : '');
                        }

                        if($_SESSION['super_admin']==1) {
                            if($data['role']==3) {
                                $assigned_employee = $data['assigned_general_manager'];
                            } elseif($data['role']!=1 && $data['role']!=2 && $data['role']!=3) {
                                    $assigned_employee = $data['assigned_branch_manager'];
                            } else {
                                $assigned_employee = "";
                            }       
                        } elseif($_SESSION['employee_role']==2){
                             if($data['role']==3) {
                                $assigned_employee = $_SESSION['crm_admin_id'];
                            } elseif($data['role']!=1 && $data['role']!=2 && $data['role']!=3) {
                                    $assigned_employee = $data['assigned_branch_manager'];
                            } else {
                                $assigned_employee = "";
                            } 
                        }  elseif($_SESSION['employee_role']==3) {
                            $assigned_employee = $_SESSION['crm_admin_id'];
                        } else {
                            $assigned_employee = "";
                        }

                        $admin_id       =$_SESSION["crm_admin_id"];
                        $curr           = date("Y-m-d H:i:s");
                        $token          = $this->generateRandomString("30");
                        $query = "UPDATE ".EMPLOYEE." SET 
                            branch_id        = '".$branch_id."',
                            token            = '".$this->hyphenize($data['name'])."',
                            name             = '".$this->cleanString($data['name'])."',
                            role             = '".$this->cleanString($data['role'])."',
                            gender           = '".$this->cleanString($data['gender'])."',
                            email            = '".$this->cleanString($data['email'])."',
                            mobile           = '".$this->cleanString($data['mobile'])."',
                            address          = '".$this->cleanString($data['address'])."',
                            city             = '".$this->cleanString($data['city'])."',
                            state            = '".$this->cleanString($data['state'])."',
                            pincode          = '".$this->cleanString($data['pincode'])."',
                            assigned_branch  = '".$assigned_branch."',
                            assigned_employee      = '".$assigned_employee."',
                            added_by         = '$admin_id',
                            updated_at       = '$curr' WHERE id='".$id."' ";    
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        if($exe){     
                            unset($_SESSION['edit_employee_key']);
                                return 1;
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                    return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
                }       
                }else{
                return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
            }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Get Employee Info to assign branch

    function assignBranchEmpInfo($id='')
    {   
        $result =  array();
        $_SESSION['add_assign_branch_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(EMPLOYEE,"id","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['add_assign_branch_key'];
        return $result;
    }

    // Assign branch to Gm

    function assignBranch($data)
    {   
        if(isset($_SESSION['add_assign_branch_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_assign_branch_key']){
                $curr            = date("Y-m-d H:i:s");
                $assigned_branch = implode(',', $data['branch_id']);

                $query ="UPDATE ".EMPLOYEE." SET 
                         branch_id        = '".$assigned_branch."',
                         assigned_branch  = '".$assigned_branch."',
                         updated_at       = '".$curr."'
                         WHERE id = '".$data['token']."'
                         ";

                $exe = $this->selectQuery($query);

                if($exe){
                    $result =  array();
                    $result['status']   = 1;
                    unset($_SESSION['add_assign_branch_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = 0;
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
                      
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Get Gm list for assign to bm

    function getGmListForAssign($branch_id="")
    {
        $layout ="";
        $query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role='2' AND E.assigned_branch!='' AND E.super_admin='0' AND E.status='1' ";
        $exe = $this->selectQuery($query);  
        if(mysqli_num_rows($exe) > 0){
            $layout ="<option value=''>Select General Manager</option> ";
            while ($list = mysqli_fetch_array($exe)) {
                $current_ids = explode(',', $list['assigned_branch']);
                if(in_array($branch_id, $current_ids)) {
                    $layout.= "<option value='".$list['id']."' >".ucwords($list['name'])."</option>";
                }
            }
        }
        return json_encode($layout);
    }


    /*------------------------------------------
                Employee Permissions
    -------------------------------------------*/

    function insertEmployeePermission($employee_id,$role="")
    {
        $curr       = date("Y-m-d H:i:s");
        $admin_id   = $_SESSION["crm_admin_id"];

        if($role==2 || $role==3) {
            $permission = 1;
        } else {
            $permission = 0;
        }


        $q = "INSERT INTO ".PERMISSION."    SET
                employee_id             = '$employee_id',
                added_by                = '$admin_id', 
                customers               = '".$permission."',
                manage_customer         = '".$permission."',
                import_customer         = '".$permission."',
                lead                    = '".$permission."',
                manage_lead             = '".$permission."',
                lead_type               = '".$permission."',
                activity_type           = '".$permission."',
                lead_source             = '".$permission."',
                customer_profile        = '".$permission."',
                import_lead             = '".$permission."',
                employee                = '".$permission."',
                manageemployee          = '".$permission."',
                reports                 = '".$permission."',
                lead_report             = '".$permission."',
                customer_report         = '".$permission."',
                settings                = '".$permission."',                
                chit_master             = '".$permission."',
                deposit_master          = '".$permission."',
                vam_master              = '".$permission."',
                branch_master           = '".$permission."',
                employee_role_master    = '".$permission."',
                status                  = '1', 
                created_at              ='$curr ',
                updated_at              ='$curr ' ";
        $exe = $this->selectQuery($q);
        if($exe){
            return 1;
        }   
        
    }

    function employeePermissions($data)
    {       

        $employee_id    = $this->decryptData($data["session_token"]);
        $curr       = date("Y-m-d H:i:s");
        if(isset($_SESSION['create_permission_key'])){
            if($data["fkey"]==$_SESSION['create_permission_key']){              
                $clean_permissions = $this->cleanPermissions($employee_id);
                if(isset($data["permission"])){
                    $permissions_array = $data["permission"];
                    if(count($permissions_array)>0) {
                        $q = "UPDATE ".PERMISSION." SET employee_id='$employee_id'  ";
                        foreach ($permissions_array as $key => $value) {
                            $q .=  ", ".$value." = '1' ";
                        }
                        $q .= ", updated_at='$curr' WHERE employee_id='$employee_id' ";
                        //echo $q; 
                       $exe = $this->selectQuery($q);
                        if($exe){
                            unset($_SESSION['create_permission_key']);
                            return 1;
                        }
                    }else{
                        return "Please Select atleast One permission to the Employee.";
                    }
                }else{
                    return "Please Select atleast One permission to the Employee.";
                }                   
                
            }else{
                return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Clean current permission

    function cleanPermissions($employee_id)
    {
        $q = "UPDATE ".PERMISSION." SET 
                customers               = '0',
                manage_customer         = '0',
                import_customer         = '0',
                lead                    = '0',
                manage_lead             = '0',
                lead_type               = '0',
                activity_type           = '0',
                lead_source             = '0',
                customer_profile        = '0',
                import_lead             = '0',
                employee                = '0',
                manageemployee          = '0',
                reports                 = '0',
                lead_report             = '0',
                customer_report         = '0',
                settings                = '0',                
                chit_master             = '0',
                deposit_master          = '0',
                vam_master              = '0',
                branch_master           = '0',
                employee_role_master    = '0'
                WHERE employee_id='$employee_id'
            ";

        $exe = $this->selectQuery($q);
        if($exe){
            return 1;
        }
    }

    // Active & Inactive Status 

    function employeeStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(EMPLOYEE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".EMPLOYEE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".EMPLOYEE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Change Password Employee

    function changepasswordEmployee($id='')
    {   
        $result =  array();
        $_SESSION['employee_changepassword_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(EMPLOYEE,"id","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['employee_changepassword_key'];
        return $result;
    }

    // Update Change Password

    function empChangePassword($data)
    {
        if(isset($_SESSION['employee_changepassword_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['employee_changepassword_key']){
                $curr           = date("Y-m-d H:i:s");
                $query = "UPDATE ".EMPLOYEE." SET 
                            password= '". $this->encryptPassword($data['confirm_pass'])."',
                            updated_at='$curr' WHERE id='".$data['token']."' "; 
                $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";                  
                    unset($_SESSION['employee_changepassword_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Active & Inactive Status 

    function propertyStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(PROPERTY_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".PROPERTY_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".PROPERTY_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Avtive & Inactive Status 

    function updateLeadToCustomerModel($id)
    {   
        $result =  array();
        $id = $this->decryptData($id);
        $_SESSION['update_completestatus_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_TBL,"id","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['update_completestatus_key'];
        return $result;
    } 

    function updateCompletedStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(LEAD_TBL,"closed_status"," id ='$data' ");
        if($info['closed_status'] ==1){
            $query = "UPDATE ".LEAD_TBL." SET closed_status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".LEAD_TBL." SET closed_status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    function moveLeadToCustomer($data)
    {
        //$lead_id = $this->decryptData($data['lead_id']);
        $info = $this -> getDetails(LEAD_TBL,"*"," id ='".$data['lead_id']."' ");
        $e_check = $this -> check_query(CUSTOMER_TBL,"id"," email ='".$info['email']."' ");
        if($e_check == 0){  
            $m_check = $this -> check_query(CUSTOMER_TBL,"id"," mobile ='".$info['mobile']."' "); 
            if($m_check == 0){  
                $admin_id       =$_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $token          = $this->generateRandomString("30");
                $password       = $this->generateRandomStringGenerator("8");
                $psw            = $this->encryptPassword($password);
                $name           = $info['fname'].' '.$info['lname'];

                $query = "INSERT INTO ".CUSTOMER_TBL." SET 
                    token       = '".$this->hyphenize($name)."',
                    priority    = 'low',
                    lead_id     = '".$info['id']."', 
                    type        = 'moved', 
                    name        = '".$this->cleanString($name)."',
                    gender      = '".$this->cleanString($info['gender'])."',
                    mobile      = '".$this->cleanString($info['mobile'])."',
                    email       = '".$this->cleanString($info['email'])."',                            
                    password    = '".$psw."',
                    has_psw     = '".$password."',
                    address     = '".$this->cleanString($info['address'])."',
                    city        = '".$this->cleanString($info['city'])."',
                    state       = '".$this->cleanString($info['state'])."',
                    pincode     = '".$this->cleanString($info['pincode'])."',
                    branch_id   = '".$this->cleanString($info['branch_id'])."',
                    kyc_status  = '0',
                    assign_status = '0',
                    kyc_remarks = '',
                    status      = '1',
                    added_by    = '$admin_id',  
                    created_at  = '$curr',
                    updated_at  = '$curr' "; 

                //return  $query ;  
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){       
                    $customer_id = "CU".str_pad($last_id, 4,0, STR_PAD_LEFT);
                    $q =" UPDATE ".CUSTOMER_TBL." SET uid='".$customer_id."' WHERE id='".$last_id."' ";
                    $result = $this->selectQuery($q);

                    $qu =" UPDATE ".LEAD_TBL." SET moved_status='1', customer_id='".$last_id."' , move_remarks='".$data['moveremarks']."'  WHERE id='".$info['id']."' ";
                    $result_update = $this->selectQuery($qu);
                    return 1;

                   /* $sender_mail    = NO_REPLY;
                    $subject        = ucwords($name)." Account login details";
                    $receiver       = $info['email'];
                    $user_token     = $this->hyphenize($name);
                    $email_temp     = $this->memberLoginInfoTemp($name,$info['email'],$password,$user_token,$last_id,$name);
                    $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                    if($sendemail){
                        return 1;
                    }*/
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }
            }else{
            return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
        }       
        }else{
            return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
        }   
    }


    /*-------------------------------------------------- 
                Lead Report Filter Functions
    ----------------------------------------------------*/

    function getBMListForGM($gm_id) 
    {
        $layout ="";
        $query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role='3' AND assigned_branch!='' AND assigned_employee='".$gm_id."' AND E.super_admin='0' AND E.status='1' ";
        $exe = $this->selectQuery($query);  
        if(mysqli_num_rows($exe) > 0){
            $layout ="<option value=''>Select Branch Manager</option> ";
            while ($list = mysqli_fetch_array($exe)) {
                    $layout.= "<option value='".$list['id']."' >".ucwords($list['name'])."</option>";
            }
        }
        return json_encode($layout);

    }

    function getEmployeeListForBM($bm_id) 
    {
        $layout ="";
        $query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role!='3' AND E.role!='2' AND E.role!='1' AND assigned_employee='".$bm_id."' AND E.super_admin='0' AND E.status='1' ";
        $exe = $this->selectQuery($query);  
        if(mysqli_num_rows($exe) > 0){
            $layout ="<option value=''>Select Branch Manager</option> ";
            while ($list = mysqli_fetch_array($exe)) {
                    $layout.= "<option value='".$list['id']."' >".ucwords($list['name'])."</option>";
            }
        }
        return json_encode($layout);

    }

    /*-------------------------------------------------- 
            Customer Report Filter Functions
    ----------------------------------------------------*/

    function getCustomerListForBranch($branch_ids) 
    {
       $layout ="";
        $branch_check = ((isset($_SESSION['selected_branch']))? "AND branch_id='".$_SESSION['selected_branch']."'" : "" );
        $query = "SELECT id,token,name,status  FROM ".CUSTOMER_TBL." WHERE status='1' $branch_check ";
        $exe = $this->selectQuery($query);  
        if(mysqli_num_rows($exe) > 0){
            while ($list = mysqli_fetch_array($exe)) {
                $layout.= "<option value='".$list['id']."'>".ucwords($list['name'])."</option>";
            }
        }
        return json_encode($layout);

    }

   

    /*----------------------------
         Activity type
    ------------------------------*/

    // Add Activity type 

    function addActivityType($data)
    {
        $layout = "";
        if(isset($_SESSION['activity_type_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['activity_type_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $check = $this->check_query(ACTIVITY_TYPE,"id"," token ='".$this->masterhyphenize($data['activity_type'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".ACTIVITY_TYPE." SET
                                token           = '".$this->masterhyphenize($data['activity_type'])."',
                                branch_id       = '".((isset($_SESSION['selected_branch']))?  $_SESSION['selected_branch'] : '')."',
                                activity_type   = '".$this->cleanString($data['activity_type'])."',
                                added_by        = '$admin_id',
                                status          = '1',
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                                //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['activity_type_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Activity type is already exist!.");
                }        
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editActivityType($id='')
    {   
        $result =  array();
        $_SESSION['edit_activitytype_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(ACTIVITY_TYPE,"id,activity_type","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['edit_activitytype_key'];
        $result['activity_type'] = $info['activity_type'];
        return $result;
    }   

    // Update 

    function updateActivityType($data)
    {
        if(isset($_SESSION['edit_activitytype_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_activitytype_key']){
                $check = $this->check_query(ACTIVITY_TYPE,"id"," token ='".$this->masterhyphenize($data['activity_type'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".ACTIVITY_TYPE." SET 
                               token          = '".$this->masterhyphenize($data['activity_type'])."',
                               activity_type  = '".$this->cleanString($data['activity_type'])."',
                               added_by       = '$admin_id',  
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        $result['item']     = $data['activity_type'];
                        unset($_SESSION['edit_activitytype_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Activity type is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Avtive & Inactive Status 

    function activityTypeStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(ACTIVITY_TYPE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".ACTIVITY_TYPE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".ACTIVITY_TYPE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
            Lead Type
    ------------------------------*/

    // Add Lead Type

    function addLeadType($data)
    {
        $layout = "";
        if(isset($_SESSION['lead_type_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['lead_type_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $check = $this->check_query(LEAD_TYPE,"id"," token ='".$this->masterhyphenize($data['lead_type'])."'  ");
                if ($check==0) {
                    if (isset($data['default_settings'])) {
                        $default_settings =1 ;
                    }else{
                        $default_settings =0 ;
                    }
                    $query = "INSERT INTO ".LEAD_TYPE." SET
                        branch_id       = '".((isset($_SESSION['selected_branch']))?  $_SESSION['selected_branch'] : '')."',
                        token           = '".$this->masterhyphenize($data['lead_type'])."',
                        lead_type       = '".$this->cleanString($data['lead_type'])."',
                        default_settings= '".$default_settings."', 
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                                //return $query;
                    $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                    $exe =mysqli_query($link,$query);
                    $last_id = mysqli_insert_id($link);
                    if($exe){       
                        $uid = "LT".str_pad($last_id, 4,0, STR_PAD_LEFT);
                        $q =" UPDATE ".LEAD_TYPE." SET uid='".$uid."' WHERE id='".$last_id."' ";
                        $result = $this->selectQuery($q);

                        unset($_SESSION['lead_type_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Lead type is already exist!.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editLeadType($id='')
    {   
        $result =  array();
        $_SESSION['edit_leadtype_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_TYPE,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_leadtype_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='editemail'>Lead Type  <en>*</en></label>
                                        <input type='text' class='form-control form-control-lg' id='lead_type' name='lead_type' value='".$info['lead_type']."' placeholder='Enter Lead Type'>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div>


                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateLeadType($data)
    {
        if(isset($_SESSION['edit_leadtype_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_leadtype_key']){
                $check = $this->check_query(LEAD_TYPE,"id"," token ='".$this->masterhyphenize($data['lead_type'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".LEAD_TYPE." SET 
                               token           = '".$this->masterhyphenize($data['lead_type'])."',
                               lead_type       = '".$this->cleanString($data['lead_type'])."',
                               added_by        = '$admin_id',
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        unset($_SESSION['edit_leadtype_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Lead type is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    // Avtive & Inactive Status 

    function leadTypeStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this->getDetails(LEAD_TYPE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".LEAD_TYPE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".LEAD_TYPE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
            Lead Type
    ------------------------------*/

    // Add Lead Type

    function addLeadSource($data)
    {
        $layout = "";
        if(isset($_SESSION['lead_source_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['lead_source_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $check = $this->check_query(LEAD_SOURCE,"id"," token ='".$this->masterhyphenize($data['lead_source'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".LEAD_SOURCE." SET
                        branch_id       = '".((isset($_SESSION['selected_branch']))?  $_SESSION['selected_branch'] : '')."',
                        token           = '".$this->masterhyphenize($data['lead_source'])."',
                        lead_source     = '".$this->cleanString($data['lead_source'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                                //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['lead_source_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Lead source is already exist!.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editLeadSource($id='')
    {   
        $result =  array();
        $_SESSION['edit_leadsource_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_SOURCE,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_leadsource_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='lead_source'>Lead Source  <en>*</en></label>
                                        <input type='text' class='form-control form-control-lg' id='lead_source' name='lead_source' value='".$info['lead_source']."' placeholder='Enter Lead Source'>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div>


                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateLeadSource($data)
    {
        if(isset($_SESSION['edit_leadsource_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_leadsource_key']){
                $check = $this->check_query(LEAD_SOURCE,"id"," token ='".$this->masterhyphenize($data['lead_source'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".LEAD_SOURCE." SET 
                               token          = '".$this->masterhyphenize($data['lead_source'])."',
                               lead_source    = '".$this->cleanString($data['lead_source'])."',
                               added_by       = '$admin_id',
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        unset($_SESSION['edit_leadsource_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Lead source is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    // Avtive & Inactive Status 

    function leadSourceStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this->getDetails(LEAD_SOURCE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".LEAD_SOURCE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".LEAD_SOURCE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    function addActivityModel($id='')
    {   
        $result =  array();
        $_SESSION['add_activity_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_TBL,"id,fname,lname,mobile,email","id='$id'");
        $result['id'] = $id;
        $result['name'] = ucwords($info['fname'].' '.$info['lname']);
        $result['mobile'] = $info['mobile'];
        $result['email'] = $info['email'];
        $result['fkey'] = $_SESSION['add_activity_key'];
        return $result;
    }   

    /*----------------------------
           Customer Profile
    ------------------------------*/

    // Add Customer Profile

    function addCustomerProfile($data)
    {
        $layout = "";
        if(isset($_SESSION['customer_profile_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['customer_profile_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $check = $this->check_query(CUSTOMER_PROFILE,"id"," customer_profile ='".$data['customer_profile']."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".CUSTOMER_PROFILE." SET
                        branch_id       = '".((isset($_SESSION['selected_branch']))?  $_SESSION['selected_branch'] : '')."',
                        token           = '".$this->hyphenize($data['customer_profile'])."',
                        customer_profile= '".$this->cleanString($data['customer_profile'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                                //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['customer_profile_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Customer Profile is already exist!.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editCustomerProfile($id='')
    {   
        $result =  array();
        $_SESSION['edit_customer_profile_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(CUSTOMER_PROFILE,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_customer_profile_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='customer_profile'>Customer Profile Name  <en>*</en></label>
                                        <input type='text' class='form-control form-control-lg' id='customer_profile' name='customer_profile' value='".$info['customer_profile']."' placeholder='Enter Customer Profile Name'>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div>


                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateCustomerProfile($data)
    {
        if(isset($_SESSION['edit_customer_profile_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_customer_profile_key']){
                $check = $this->check_query(CUSTOMER_PROFILE,"id"," customer_profile ='".$data['customer_profile']."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".CUSTOMER_PROFILE." SET 
                               token          = '".$this->hyphenize($data['customer_profile'])."',
                               customer_profile    = '".$this->cleanString($data['customer_profile'])."',
                               added_by       = '$admin_id',
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        unset($_SESSION['edit_customer_profile_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Customer Profile Name is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    // Avtive & Inactive Status 

    function customerProfileStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this->getDetails(CUSTOMER_PROFILE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".CUSTOMER_PROFILE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".CUSTOMER_PROFILE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
         Members Lead
    ------------------------------*/

    // Add Lead 

    function addLead($data="")
    {
        if(isset($_SESSION['add_lead_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_lead_key']){  
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $token          = $this->generateRandomString("30");
                $today          = date("Y-m-d");

                $echeck = $this->check_query(LEAD_TBL,"id"," email ='".$data['email']."'  ");
                if ($echeck==0) {
                    $m_check = $this -> check_query(LEAD_TBL,"id"," mobile ='".$data['mobile']."' "); 
                    if($m_check == 0){  
                        $leadstatus_info = $this -> getDetails(LEAD_TYPE,"id"," default_settings='1' ");

                        if (isset($_POST['selectchit'])) {
                            $has_chit = 1;
                            $chit_id = $this->cleanString($data['chit_id']);
                        }else{
                            $has_chit = 0;
                            $chit_id = 0;
                        }
                        if (isset($_POST['selectdeposite'])) {
                            $has_deposite = 1;
                            $deposite_id = $this->cleanString($data['deposite_id']);
                        }else{
                            $has_deposite = 0;
                            $deposite_id = 0;
                        }
                        if (isset($_POST['selectvas'])) {
                            $has_vas = 1;
                            $vas_id = $this->cleanString($data['vas_id']);
                        }else{
                            $has_vas = 0;
                            $vas_id = 0;
                        }

                        $query = "INSERT INTO ".LEAD_TBL." SET 
                            branch_id           = '".((isset($_SESSION['selected_branch']))?  $_SESSION['selected_branch'] : '')."',
                            token               = '".$this->hyphenize($data['fname'])."',
                            property_id         = '0',
                            employee_id         = '0',
                            customer_id         = '0',
                            lead_date           = '".$today."',
                            enquiry_date        = '".$today."',
                            fname               = '".$this->cleanString($data['fname'])."',
                            lname               = '',
                            designation         = '".$this->cleanString($data['designation'])."',
                            leadsource          = '".$this->cleanString($data['leadsource'])."',
                            has_chit            = '".$has_chit."',
                            chit_id             = '".$chit_id."',
                            has_deposite        = '".$has_deposite."',
                            deposite_id         = '".$deposite_id."',
                            has_vas             = '".$has_vas."',
                            vas_id              = '".$vas_id."',
                            flattype_id         = '0',
                            site_visit_status   = '',
                            customer_profile_id = '".$this->cleanString($data['customerprofile_id'])."',
                            gender              = '".$this->cleanString($data['gender'])."',
                            mobile              = '".$this->cleanString($data['mobile'])."',
                            email               = '".$this->cleanString($data['email'])."',
                            secondary_mobile    = '".$this->cleanString($data['smobile'])."',
                            secondary_email     = '".$this->cleanString($data['semail'])."',
                            address             = '".$this->cleanString($data['address'])."',
                            city                = '".$this->cleanString($data['city'])."',
                            state               = '".$this->cleanString($data['state'])."',
                            pincode             = '".$this->cleanString($data['pincode'])."',
                            description         = '".$this->cleanString($data['description'])."',
                            lead_status_id      = '".$leadstatus_info['id']."',
                            assign_status       = '0',
                            closed_status       = '0',
                            moved_status        = '0',
                            status              = '1',
                            deleted_status      = '0',
                            deleted_by          = '0',
                            deleted_on          = '',
                            restored_by         = '0',
                            restored_on         = '',
                            restored_remarks    = '',
                            added_by            = '$admin_id',  
                            created_at          = '$curr',
                            updated_at          = '$curr' "; 
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            $uid = "LE".str_pad($last_id, 4,0, STR_PAD_LEFT);
                            $q =" UPDATE ".LEAD_TBL." SET uid='".$uid."' WHERE id='".$last_id."' ";
                            $result = $this->selectQuery($q);
                            
                            $options['ref_id']      = $last_id;
                            $options['created_by']  = $admin_id;
                            $notification           = $this->leadLogs("created",$options);  

                            unset($_SESSION['add_lead_key']);
                            return 1;

                           /* $sender_mail    = NO_REPLY;
                            $subject        = ucwords($data['name'])." Account login details";
                            $receiver       = $data['email'];
                            $user_token     = $this->hyphenize($data['name']);
                            $email_temp     = $this->memberLoginInfoTemp($data['name'],$data['email'],$password,$user_token,$last_id,$data['name']);
                            $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                            if($sendemail){
                                
                            }*/
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                        return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
                    }       
                }else{
                     return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit Customer 

    function editLead($data="")
    {
        if(isset($_SESSION['edit_lead_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_lead_key']){  
                $id             = $this->decryptData($data['session_token']);
                $admin_id       =$_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $e_check = $this -> check_query(LEAD_TBL,"id"," email ='".$data['email']."' AND id!='".$id."' ");
                if($e_check == 0){  
                    $m_check = $this -> check_query(LEAD_TBL,"id"," mobile ='".$data['mobile']."' AND id!='".$id."'  "); 
                    if($m_check == 0){  

                        if (isset($_POST['selectchit'])) {
                            $has_chit = 1;
                            $chit_id = $this->cleanString($data['chit_id']);
                        }else{
                            $has_chit = 0;
                            $chit_id = 0;
                        }
                        if (isset($_POST['selectdeposite'])) {
                            $has_deposite = 1;
                            $deposite_id = $this->cleanString($data['deposite_id']);
                        }else{
                            $has_deposite = 0;
                            $deposite_id = 0;
                        }
                        if (isset($_POST['selectvas'])) {
                            $has_vas = 1;
                            $vas_id = $this->cleanString($data['vas_id']);
                        }else{
                            $has_vas = 0;
                            $vas_id = 0;
                        }

                        $query = " UPDATE ".LEAD_TBL." SET 
                            token           = '".$this->hyphenize($data['fname'])."',                            
                            fname           = '".$this->cleanString($data['fname'])."',
                            designation     = '".$this->cleanString($data['designation'])."',
                            leadsource      = '".$this->cleanString($data['leadsource'])."',
                            has_chit        = '".$has_chit."',
                            chit_id         = '".$chit_id."',
                            has_deposite    = '".$has_deposite."',
                            deposite_id     = '".$deposite_id."',
                            has_vas         = '".$has_vas."',
                            vas_id          = '".$vas_id."',
                            customer_profile_id = '".$this->cleanString($data['customerprofile_id'])."',
                            gender          = '".$this->cleanString($data['gender'])."',
                            mobile          = '".$this->cleanString($data['mobile'])."',
                            email           = '".$this->cleanString($data['email'])."',
                            secondary_mobile= '".$this->cleanString($data['smobile'])."',
                            secondary_email = '".$this->cleanString($data['semail'])."',
                            address         = '".$this->cleanString($data['address'])."',
                            city            = '".$this->cleanString($data['city'])."',
                            state           = '".$this->cleanString($data['state'])."',
                            pincode         = '".$this->cleanString($data['pincode'])."',
                            description     = '".$this->cleanString($data['description'])."',
                            updated_at  = '$curr' WHERE id='$id' ";    
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            unset($_SESSION['edit_lead_key']);
                            return 1;
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                        return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
                    }       
                }else{
                    return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
            }               
                    
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Avtive & Inactive Status 

    function leadStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(CUSTOMER_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".CUSTOMER_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".CUSTOMER_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Assign Call 

    function assignLeadModel($id='')
    {   
        $result =  array();
        $_SESSION['add_assign_lead_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_TBL,"id,fname,lname,mobile,email","id='$id'");
        $result['id'] = $id;
        $result['name'] = ucwords($info['fname'].' '.$info['lname']);
        $result['mobile'] = $info['mobile'];
        $result['email'] = $info['email'];
        $result['fkey'] = $_SESSION['add_assign_lead_key'];
        return $result;
    }  


    function assignLead($data="")
    {
        if(isset($_SESSION['add_assign_lead_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_assign_lead_key']){          
                    $admin_id        = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");

                    $check = $this->check_query(ASSIGN_LEAD,"id"," ref_id='".$data['lead_id']."' ORDER BY id DESC LIMIT 1 ");
                    if($check==1) {
                            $info  = $this->getDetails(ASSIGN_LEAD,"id,created_to"," ref_id='".$data['lead_id']."' ORDER BY id DESC LIMIT 1 ");
                            $update = "UPDATE ".ASSIGN_LEAD." SET 
                                status          = '0',  
                                updated_at      = '$curr' WHERE id='".$info['id']."' ";
                            $update_status = $this->selectQuery($update);
                    }   
                    $query = "INSERT INTO ".ASSIGN_LEAD." SET 
                                type            = 'assign',
                                created_by      = '$admin_id',
                                created_to      = '".$data['employee_id']."',
                                ref_id          = '".$data['lead_id']."',   
                                priority        = '".$this->cleanString($data['priority'])."',
                                remarks         = '".$this->cleanString($data['remarks'])."',
                                status          = '1',
                                remove_status   = '0',  
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                    //return $query; 
                    $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                    $exe =mysqli_query($link,$query);
                    $last = mysqli_insert_id($link);
                    if($exe){
                        $q =" UPDATE ".LEAD_TBL." SET 
                            assign_status ='1',
                            employee_id = '".$data['employee_id']."',
                            closed_status = '0' WHERE id='".$data['lead_id']."' ";
                        $result = $this->selectQuery($q);
                        if ($result) {
                            $lead_info = $this->getDetails(LEAD_TBL,"*"," id='".$data['lead_id']."' ");
                            $emp_info  = $this->getDetails(EMPLOYEE,"*"," id='".$data['employee_id']."' ");

                            $options['ref_id']      = $data['lead_id'];
                            $options['created_by']  = $admin_id;
                            $options['created_to']  = $data['employee_id'];
                            $options['remarks']     = $this->cleanString($data['remarks']);
                            $notification           = $this->leadLogs("assigned",$options); 
                            unset($_SESSION['add_assign_lead_key']);
                            return 1;   
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }                       
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Reassign Call 

    function reassignLeadModel($id='')
    {   
        $result =  array();
        $_SESSION['add_reassign_lead_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_TBL,"*","id='$id'");
        $assigninfo = $this->getDetails(ASSIGN_LEAD,"*","ref_id='$id'");
        $result['id']           = $id;
        $name = ucwords($info['fname'].' '.$info['lname']);
        $result['layout']       = "

            <input type='hidden' name='session_token' id='session_token' value='".$this->encryptData($id)."'>
                <input type='hidden' name='fkey' id='fkey' value='".$_SESSION['add_reassign_lead_key']."'>
                <input type='hidden' name='lead_id' id='token' value='".$id."'>
                <div class='modal-body modal-body-sm'>
                    <h5 class='title'>Reassign Lead to Employee</h5>
                    <h5 class='title name'></h5>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <p >
                                            <em class='icon ni ni-user'></em> <span style='margin-right: 10px;'>".$name."</span> 
                                            <em class='icon ni ni-mobile'></em> <span style='margin-right: 10px;'>".$info['mobile']."</span> 
                                            <em class='icon ni ni-mail'></em> <span >".$info['email']."</span>
                                        </p>
                                    </div>
                                </div>

                                
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label' for=''>Current Owner <en></en></label>
                                        <select class='form-control form-select ' id='' name='' data-placeholder='Select a Flat/Villa' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true' disabled>
                                            ".$this->getEmployeeList($assigninfo['created_to'])."
                                        </select>
                                    </div>
                                </div>
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label' for='employee_id'>Reassign Employee <en>*</en></label>
                                        <select class='form-control form-select ' id='employee_id' name='employee_id' data-placeholder='Select a Flat/Villa' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true'>
                                            ".$this->getEmployeeList()."
                                        </select>
                                    </div>
                                </div>   
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label' for='priority'>Lead Priority <en>*</en></label>
                                        <select class='form-control ' name='priority' id='priority'>
                                           <option value='low'>Low</option>
                                           <option value='medium'>Medium</option>
                                           <option value='high'>High</option>
                                           <option value='urgent'>Urgent</option>
                                        </select> 
                                    </div>
                                </div> 
                                <div class='col-lg-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='company'>Remarks <en>*</en></label>
                                        <textarea class='form-control form-control-sm' id='remarks' name='remarks' placeholder='Write your Remarks'></textarea>
                                    </div>
                                </div>
                                <div class='col-12'>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right'>
                                        <li>
                                            <a href='#' data-dismiss='modal' class='btn btn-lg btn-danger'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        ";
        return $result;
    } 

    function reassignLead($data="")
    {
        if(isset($_SESSION['add_reassign_lead_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_reassign_lead_key']){            
                    $user_id        = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");  
                    $check = $this->check_query(ASSIGN_LEAD,"id"," ref_id='".$data['lead_id']."' ORDER BY id DESC LIMIT 1 ");
                    if($check==1) {
                        $info  = $this->getDetails(ASSIGN_LEAD,"id,created_to"," ref_id='".$data['lead_id']."' ORDER BY id DESC LIMIT 1 ");
                        $call_status = "UPDATE ".LEAD_TBL." SET 
                                assign_status='1',
                                closed_status = '0',
                                employee_id = '".$data['employee_id']."',
                                updated_at      = '$curr' WHERE id='".$data['lead_id']."' ";
                        $call_status = $this->selectQuery($call_status);
                        if ($call_status) {
                            $update = "UPDATE ".ASSIGN_LEAD." SET 
                                status          = '0',  
                                updated_at      = '$curr' WHERE id='".$info['id']."' ";
                            $update_status = $this->selectQuery($update);
                            if($update_status){
                                $query = "INSERT INTO ".ASSIGN_LEAD." SET 
                                    type            = 'reassign',
                                    created_by      = '$user_id',
                                    created_to      = '".$data['employee_id']."',
                                    ref_id          = '".$data['lead_id']."',
                                    priority        = '".$this->cleanString($data['priority'])."',
                                    remarks         = '".$this->cleanString($data['remarks'])."',
                                    status          = '1',  
                                    remove_status   = '0',
                                    created_at      = '$curr',
                                    updated_at      = '$curr' ";
                                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                                $exe =mysqli_query($link,$query);
                                $last = mysqli_insert_id($link);
                                if($exe){   
                                    $call_info = $this->getDetails(LEAD_TBL,"*"," id='".$data['lead_id']."' ");
                                    $emp_info  = $this->getDetails(EMPLOYEE,"*"," id='".$data['employee_id']."' ");
                                    $emp_info_app = $this->getDetails(EMPLOYEE,"*"," id='".$info['created_to']."' ");
                                    /*if ($emp_info_app['android_device_token']!="") {
                                        $registrationIds = array();
                                        $registrationIds[] = $emp_info_app['android_device_token'];
                                        $msg_array = array(
                                            'title'     => "Your call has been reassigned ",
                                            'subtitle'  => $call_info['call_id']." has been removed from your list",
                                            'type'      => 'reassign',
                                            'user_id'   => $info['created_to'],
                                            'call_id'   => $data['call_id']
                                        );
                                        $this->sendPushNotification($registrationIds,$msg_array);
                                    }
                                    if ($emp_info['android_device_token']!="") {
                                        $registrationIds = array();
                                        $registrationIds[] = $emp_info['android_device_token'];
                                        $msg_array = array(
                                            'title'     => "New call assigned to you",
                                            'subtitle'  => $call_info['call_id']." assigned for you, click to view",
                                            'type'      => 'assign',
                                            'user_id'   => $data['employee_id'],
                                            'call_id'   => $data['call_id']
                                        );
                                        $this->sendPushNotification($registrationIds,$msg_array);
                                    }*/

                                    $options['ref_id']      = $data['lead_id'];
                                    $options['created_by']  = $user_id;
                                    $options['created_to']  = $data['employee_id'];
                                    $options['remarks']     = $this->cleanString($data['remarks']);
                                    $notification           = $this->leadLogs("reassigned",$options);   
                                    unset($_SESSION['add_reassign_lead_key']);
                                    return 1;                                               
                                }else{
                                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                                }       
                            }else{
                                return "Sorry!! Unexpected Error Occurred. Please try again.";
                            }
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again...";
                    }                           
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }


    /*--------------------------------------------- 
                Call Logs
    ----------------------------------------------*/

    // Add Call Logs

    function leadLogs($type,$options)
    {   
        $curr  = date("Y-m-d H:i:s");
        switch ($type) {
            case 'created':
                $created_by  = $options['created_by'];              
                $ref_id      = $options['ref_id'];
                $query = "INSERT INTO ".LEAD_LOGS." SET
                    type        = 'created',
                    lead_type   = 'manual',
                    created_by  = '$created_by',
                    created_to  = '0',
                    ref_id      = '$ref_id',
                    status      = '1',
                    remarks     = '',  
                    created_at  = '$curr',
                    updated_at  = '$curr' ";
            break;
            case 'assigned':
                $created_by  = $options['created_by'];  
                $created_to  = $options['created_to'];              
                $ref_id      = $options['ref_id'];
                $remarks     = $options['remarks'];
                $query = "INSERT INTO ".LEAD_LOGS." SET
                    type        = 'assigned',
                    lead_type   = 'manual',
                    created_by  = '$created_by',
                    created_to  = '$created_to',
                    ref_id      = '$ref_id',
                    remarks     = '$remarks',   
                    status      = '1',
                    created_at  = '$curr',
                    updated_at  = '$curr' ";
            break;
            case 'reassigned':
                $created_by  = $options['created_by'];  
                $created_to  = $options['created_to'];              
                $ref_id      = $options['ref_id'];
                $remarks     = $options['remarks'];
                $query = "INSERT INTO ".LEAD_LOGS." SET
                    type        = 'reassigned',
                    lead_type    = 'manual',
                    created_by  = '$created_by',
                    created_to  = '$created_to',
                    ref_id      = '$ref_id',
                    remarks     = '$remarks',
                    status      = '1',
                    created_at  = '$curr',
                    updated_at  = '$curr' ";
            break;
            default:
            break;
        }
        //return $query;
        $exe = $this->selectQuery($query);
        if ($exe) {
            return 1;
        }else{
            return "Sorry!! Unexpected Error Occurred. Please try again.";
        }
    }

    /*----------------------------
            Acitivity
    -----------------------------*/

    function addActivity($data)
    {
        $layout = "";
        if(isset($_SESSION['add_activity_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_activity_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $act_date = $this->changeDateFormat($data['act_date']);
                $query = "INSERT INTO ".LEAD_ACTIVITY." SET
                    lead_id         = '".$data['token']."',                    
                    date            = '".$act_date."',                   
                    activity_id     = '".$data['activity_id']."',
                    remarks         = '".$data['remarks']."',
                    employee_id     = '".$admin_id."',
                    status          = '1',
                    created_at      = '$curr',
                    updated_at      = '$curr' ";
                         //  return $query;
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){
                    unset($_SESSION['add_activity_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    function addleadActivity($data)
    {
        $layout = "";
        $token          = $this->generateRandomString("30");
        $admin_id       = $_SESSION["crm_admin_id"];
        $curr           = date("Y-m-d H:i:s");
        $act_date = $this->changeDateFormat($data['act_date']);
        $query = "INSERT INTO ".LEAD_ACTIVITY." SET
            lead_id         = '".$data['token']."',            
            date            = '".$act_date."',           
            activity_id     = '".$data['activity_id']."',
            remarks         = '".$data['remarks']."',
            employee_id     = '".$admin_id."',
            status          = '1',
            created_at      = '$curr',
            updated_at      = '$curr' ";
                 //  return $query;
        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
        $exe =mysqli_query($link,$query);
        $last_id = mysqli_insert_id($link);
        if($exe){
            return 1;
        }else{
            return "Sorry!! Unexpected Error Occurred. Please try again.";
        }   
    }

     // Edit 

    function editleadactivity($id='')
    {   
        $result =  array();
        $_SESSION['edit_leadactivity_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_ACTIVITY,"id,date,lead_id,employee_id,activity_id,remarks"," id='".$id."' ");
        $leadinfo = $this->getDetails(LEAD_TBL,"id,fname,lname,mobile,email"," id='".$info['lead_id']."' ");
        $name = $leadinfo['fname'].' '.$leadinfo['lname'] ;
        $result['id']           = $id;
        $result['layout']       = "
            <input type='hidden' name='session_token' id='session_token' value='".$this->encryptData($id)."'>
                <input type='hidden' name='fkey' id='fkey' value='".$_SESSION['edit_leadactivity_key']."'>
                <input type='hidden' name='token' id='token' value='".$id."'>
                <div class='modal-body modal-body-sm'>
                    <h5 class='title'>Add Activity</h5>
                    <h5 class='title name'></h5>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <p >
                                            <em class='icon ni ni-user'></em> <span style='margin-right: 10px;'>".$name."</span> 
                                            <em class='icon ni ni-mobile'></em> <span style='margin-right: 10px;'>".$leadinfo['mobile']."</span> 
                                            <em class='icon ni ni-mail'></em> <span >".$leadinfo['email']."</span>
                                        </p>
                                    </div>
                                </div>
                                
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label' for='company'>Activity Type <en>*</en></label>
                                        <select class='form-control form-select ' id='activity_id' name='activity_id' data-placeholder='Select a Flat/Villa' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true'>
                                            ".$this->getActivityType($info['activity_id'])."
                                        </select>
                                    </div>
                                </div>
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label'>Activity Date <en>*</en></label>
                                        <div class='form-control-wrap'>
                                            <input type='text' name='act_date' id='act_date' class='form-control date-picker' data-date-format='dd/mm/yyyy' placeholder='dd/mm/yyyy' value='".date("d/m/Y",strtotime($info['date']))."'>
                                        </div>
                                    </div>
                                </div> 
                                
                                <div class='col-lg-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='company'>Remarks <en>*</en></label>
                                        <textarea class='form-control form-control-sm' id='remarks' name='remarks' placeholder='Write your Remarks'>".$info['remarks']."</textarea>
                                    </div>
                                </div>
                                <div class='col-12'>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right'>
                                        <li>
                                            <a href='#' data-dismiss='modal' class='btn btn-lg btn-danger'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        ";
        return $result;
    }   

    // Update 

    function updateLeadActivity($data)
    {
        if(isset($_SESSION['edit_leadactivity_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_leadactivity_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $act_date       = $this->changeDateFormat($data['act_date']);
                $id             = $this->decryptData($data['session_token']);
                $query = "UPDATE ".LEAD_ACTIVITY." SET 
                        date            = '".$act_date."',
                        activity_id     = '".$data['activity_id']."',
                        remarks         = '".$data['remarks']."',
                        updated_at      = '$curr'
                        WHERE id        = '".$id."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                    unset($_SESSION['edit_leadactivity_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Lead Status Model 


    function leadStatusModel($id='')
    {   
        $result =  array();
        $_SESSION['update_leadstatus_key'] = $this->generateRandomString("40");
        $info               = $this->getDetails(LEAD_TBL,"id,fname,lname,mobile,email,lead_status_id","id='$id'");
        $result['id']       = $id;
        $token              = $this->encryptData($id);
        $result['layout']       = "
            <input type='hidden' name='session_token' id='session_token' value='".$this->encryptData($id)."'>
                <input type='hidden' name='fkey' id='fkey' value='".$_SESSION['update_leadstatus_key']."'>
                <input type='hidden' name='session_token' id='session_token' value='".$token."'>
                <input type='hidden' name='token' id='token' value='0'>
                <div class='modal-body modal-body-sm'>
                    <h5 class='title'>Update Lead Status</h5>
                    <h5 class='title name'></h5>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>                                
                                <div class='col-lg-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='leadstatus_id'>Lead Status <en>*</en></label>
                                        <select class='form-control form-select ' id='leadstatus_id' name='leadstatus_id' data-placeholder='Select a Lead Status' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true'>
                                            ".$this->getLeadStatus($info['lead_status_id'])."
                                        </select>
                                    </div>
                                </div>
                                <div class='col-12'>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right'>
                                        <li>
                                            <a href='#' data-dismiss='modal' class='btn btn-lg btn-danger'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        ";
        return $result;
    } 

    // Update 

    function updateLeadStatus($data)
    {
        if(isset($_SESSION['update_leadstatus_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['update_leadstatus_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $id             = $this->decryptData($data['session_token']);
                $query = "UPDATE ".LEAD_TBL." SET 
                           lead_status_id = '".$this->cleanString($data['leadstatus_id'])."',
                           updated_at     = '$curr'
                           WHERE id       = '".$id."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                    unset($_SESSION['update_leadstatus_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                } 
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    /*--------------------------------------------- 
                Import Alumni Data
    ----------------------------------------------*/ 

    function getEmployeesIds($id,$ids_for)
    {
        $employee_ids =array();

        if($ids_for=="general_manager") {
            $query1 = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role='3' AND assigned_employee='".$id."' AND E.super_admin='0' AND E.status='1' ";
            $branch_manager_ids =array();
            $exe1 = $this->selectQuery($query1);  
            if(mysqli_num_rows($exe1) > 0){
                while ($list = mysqli_fetch_array($exe1)) {
                     $branch_manager_ids[] = $list['id'];
                }
            }

            if(count($branch_manager_ids)) {
                $bm_condition = " AND assigned_employee IN (".implode(",", $branch_manager_ids).") "; 
            } else {
                $bm_condition = " AND assigned_employee=NULL ";
            }

            $query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role!='2' AND E.role!='3' $bm_condition  AND E.super_admin='0' AND E.status='1' ";


        } else {
            $query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role!='1' AND E.role!='2' AND E.role!='3'  AND assigned_employee='".$id."' AND E.super_admin='0' AND E.status='1' ";
        }
        
        $exe = $this->selectQuery($query);
        if(mysqli_num_rows($exe) > 0){
            while ($list = mysqli_fetch_array($exe)) {
                    $employee_ids[] = $list['id'];
            }
        }

        if(count($employee_ids)) {
            $condition = " AND L.employee_id IN (".implode(",", $employee_ids).") "; 
        } else {
            $condition = " AND L.employee_id=NULL ";
        }

        return $condition ;
    }

    // Save Excel Session

    function saveExcelSession($data)
    {
        $data = explode("`",$data);
        $sheet_name     = $this->cleanString($data[0]);
        $token          = $this->cleanString($data[1]);
        $curr = date("Y-m-d H:i:s");
        $query = "INSERT INTO ".DATASHEET_TBL." SET branch_id='".$_SESSION['selected_branch']."' ,import_token='$token', uploaded_sheet='$sheet_name', created_at='$curr', updated_at='$curr', status='0' ";
        $execute = $this->selectQuery($query);
        if($execute){
            return 1;
        }else{
            return $this->errorMsg ("Error Occured Please try Again");
        }
    }

    // Approve Import Data

    function approveImportData($token) {
        $q = "SELECT * FROM ".TEMPEXCEL_TBL." WHERE upload_token='$token' ";
        $temp_q = $this->selectQuery($q);
        if(mysqli_num_rows($temp_q) > 0){


            while ($data = mysqli_fetch_array($temp_q)) {
                //$user_token     = $this->generateRandomString("35").date("Ymdhis").$this->generateRandomString("30");
                //$mobile_token   = $this->mobileToken("8");
                //$email_token    = $this->generateRandomString("50");
                $curr           = date("Y-m-d H:i:s");
                $admin_id       = $_SESSION["crm_admin_id"];
                
                if(($data['primary_mobile']!=="")){
                    $check_user = $this -> check_query(LEAD_TBL,"id"," mobile='".$data['primary_mobile']."' ");
                }else{
                    $check_user = 1;
                }
                if($check_user == 0){
                    $leadcount = $this -> check_query(LEAD_TBL,"id"," 1 ");
                    $leadstatus_info = $this -> getDetails(LEAD_TYPE,"id"," default_settings='1' ");
                    $today          = date("Y-m-d");

                    $leadsource = $this->getCheckLeadSourceId($data['leadsource']);
                    //$flattype_id = $this->getCheckFlatTypeId($data['flat_type']);
                    $flattype_id = '0';
                    $customer_profile_id = $this->getCheckCustomerProfileId($data['profile']);
                    $leadstatus_id      = $this->getCheckLeadStatusId($data['lead_status']);

                    $chit_master_id     = $this->getCheckChitMasterId($data['chit']);
                    $deposite_master_id = $this->getCheckDepositeMasterId($data['deposite']);
                    $vas_master_id      = $this->getCheckVASMasterId($data['vas']);

                    $has_chit_master_id = (($data['chit']!='') ? 1 : 0 ) ;
                    $has_deposite_master_id = (($data['chit']!='') ? 1 : 0 ) ;
                    $has_vas_master_id = (($data['chit']!='') ? 1 : 0 ) ;


                    $q = "INSERT INTO ".LEAD_TBL." SET
                        uid             = '',
                        branch_id       = '".$this->cleanString($data['branch_id'])."',
                        import_token    = '".$token."',
                        auth_type       = 'excel',
                        token           = '".$this->hyphenize($data['fname'].$data['lname'])."',
                        property_id     = '0',
                        customer_id     = '0',
                        lead_date       = '".$today."',
                        enquiry_date    = '".$this->cleanString($data['enquiry_date'])."',
                        fname           = '".$this->cleanString($data['fname'])."',
                        lname           = '".$this->cleanString($data['lname'])."',
                        leadsource      = '".$leadsource."',
                        lead_status     = '".$this->cleanString($data['lead_status'])."',
                        chit_id         = '".$chit_master_id."',
                        deposite_id     = '".$deposite_master_id."',
                        vas_id          = '".$vas_master_id."',
                        has_chit        = '".$has_chit_master_id."',
                        has_deposite    = '".$has_deposite_master_id."',
                        has_vas         = '".$has_vas_master_id."',
                        flattype_id         = '0',
                        site_visit_status   = '',
                        site_visit_date     = '',
                        customer_profile_id = '".$customer_profile_id."',
                        gender          = '".$this->cleanString($data['gender'])."',
                        mobile          = '".$this->cleanString($data['primary_mobile'])."',
                        email           = '".$this->cleanString($data['primary_email'])."',
                        secondary_mobile= '',
                        secondary_email = '',
                        address         = '".$this->cleanString($data['address'])."',
                        city            = '".$this->cleanString($data['city'])."',
                        state           = '".$this->cleanString($data['state'])."',
                        pincode         = '".$this->cleanString($data['pincode'])."',
                        description     = '".$this->cleanString($data['description'])."',
                        lead_status_id  = '".$leadstatus_id."',
                        employee_id     = '0',
                        assign_status   = '0',
                        closed_status   = '0',
                        moved_status    = '0',
                        move_remarks    = '',
                        status          = '1',
                        deleted_status   = '0',
                        deleted_by       = '0',
                        deleted_on       = '',
                        restored_by      = '0',
                        restored_on      = '',
                        restored_remarks = '',
                        added_by        = '$admin_id',  
                        created_at      = '$curr',
                        updated_at      = '$curr' "; 

                    $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                    $exe =mysqli_query($link,$q);
                    $last_id = mysqli_insert_id($link);
                    if($exe){ 
                        $lead_uid = "LE".str_pad($last_id, 4,0, STR_PAD_LEFT);
                        $qu =" UPDATE ".LEAD_TBL." SET uid='".$lead_uid."' WHERE id='".$last_id."' ";
                        $result = $this->selectQuery($qu);
                    }
                }
            }
        }
        $curre  = date("Y-m-d H:i:s");
        $query = "UPDATE ".DATASHEET_TBL." SET status='1', updated_at='$curre' WHERE import_token='$token' ";
        $execute = $this->selectQuery($query);
        if($execute){
            return 1;
        }else{
            return "Error Occured Please try Again";
        }
    }

    // Deleted Imported Data

    function deleteImportData($data){
        $delete_temp = $this->deleteRow(TEMPEXCEL_TBL," upload_token='$data' ");
        if($delete_temp){
            $delete = $this->deleteRow(DATASHEET_TBL," import_token='$data' ");
            if ($delete) {
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }

    function deleteSingleRowImportData($data){
        $delete_temp = $this->deleteRow(TEMPEXCEL_TBL," id='$data' ");
        if($delete_temp){
                return 1;
        }else{
            return 0;
        }
    }

    function getCheckLeadSourceId($leadsource){
        $default = $this->masterhyphenize($leadsource);
        $check = $this -> check_query(LEAD_SOURCE,"token"," token ='".$default."' ");
        if($check==0){
            $admin_id       = $_SESSION["crm_admin_id"];
            $curr           = date("Y-m-d H:i:s");
            $query = "INSERT INTO ".LEAD_SOURCE." SET
                token           = '".$default."',
                lead_source     = '".$leadsource."',
                added_by        = '$admin_id',
                status          = '1',
                created_at      = '$curr',
                updated_at      = '$curr' ";
            //return $query;
            $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
            $exe =mysqli_query($link,$query);
            $get_id = mysqli_insert_id($link);
            return $get_id;
        }else{
            $get_id = $this->getDetails(LEAD_SOURCE,"id"," token ='".$default."' ");
            return $get_id['id'];
        }
    }

    function getCheckLeadStatusId($lead_type){
        $default = $this->masterhyphenize($lead_type);
        $check = $this -> check_query(LEAD_TYPE,"token"," token ='".$default."' ");
        if($check==0){
            $admin_id       = $_SESSION["crm_admin_id"];
            $curr           = date("Y-m-d H:i:s");
            $query = "INSERT INTO ".LEAD_TYPE." SET
                token           = '".$default."',
                lead_type       = '".$lead_type."',
                default_settings= '0',
                added_by        = '$admin_id',
                status          = '1',
                created_at      = '$curr',
                updated_at      = '$curr' ";
            //return $query;
            $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
            $exe =mysqli_query($link,$query);
            $get_id = mysqli_insert_id($link);
            return $get_id;
        }else{
            $get_id = $this->getDetails(LEAD_TYPE,"id"," token ='".$default."' ");
            return $get_id['id'];
        }
    }

    function getCheckCustomerProfileId($customerprofile){
        $default = $this->masterhyphenize($customerprofile);
        $check = $this -> check_query(CUSTOMER_PROFILE,"token"," token ='".$default."' ");
        if($check==0){
            $admin_id       = $_SESSION["crm_admin_id"];
            $curr           = date("Y-m-d H:i:s");
            $query = "INSERT INTO ".CUSTOMER_PROFILE." SET
                token           = '".$default."',
                customer_profile= '".$customerprofile."',
                added_by        = '$admin_id',
                status          = '1',
                created_at      = '$curr',
                updated_at      = '$curr' ";
            //return $query;
            $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
            $exe =mysqli_query($link,$query);
            $get_id = mysqli_insert_id($link);
            return $get_id;
        }else{
            $get_id = $this->getDetails(CUSTOMER_PROFILE,"id"," token ='".$default."' ");
            return $get_id['id'];
        }
    }

    function getCheckChitMasterId($chit)
    {
        $default = $this->masterhyphenize($chit);
        $check   = $this -> check_query(CHIT_MASTER,"token"," token ='".$default."' ");
        if($check==0){
            $admin_id   = $_SESSION["crm_admin_id"];
            $curr       = date("Y-m-d H:i:s");
            $query      = "INSERT INTO ".CHIT_MASTER." SET
                token           = '".$default."',
                item_type       = '".$chit."',
                added_by        = '$admin_id',
                status          = '1',
                created_at      = '$curr',
                updated_at      = '$curr' ";
            //return $query;
            $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
            $exe =mysqli_query($link,$query);
            $get_id = mysqli_insert_id($link);
            return $get_id;
        }else{
            $get_id = $this->getDetails(CHIT_MASTER,"id"," token ='".$default."' ");
            return $get_id['id'];
        }
    }

    function getCheckDepositeMasterId($deposite)
    {
        $default = $this->masterhyphenize($deposite);
        $check = $this -> check_query(DEPOSITE_MASTER,"token"," token ='".$default."' ");
        if($check==0){
            $admin_id       = $_SESSION["crm_admin_id"];
            $curr           = date("Y-m-d H:i:s");
            $query = "INSERT INTO ".DEPOSITE_MASTER." SET
                token           = '".$default."',
                item_type       = '".$deposite."',
                added_by        = '$admin_id',
                status          = '1',
                created_at      = '$curr',
                updated_at      = '$curr' ";
            //return $query;
            $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
            $exe =mysqli_query($link,$query);
            $get_id = mysqli_insert_id($link);
            return $get_id;
        }else{
            $get_id = $this->getDetails(DEPOSITE_MASTER,"id"," token ='".$default."' ");
            return $get_id['id'];
        }
    }

    function getCheckVASMasterId($vas)
    {
        $default = $this->masterhyphenize($vas);
        $check = $this -> check_query(VAS_MASTER,"token"," token ='".$default."' ");
        if($check==0){
            $admin_id       = $_SESSION["crm_admin_id"];
            $curr           = date("Y-m-d H:i:s");
            $query = "INSERT INTO ".VAS_MASTER." SET
                token           = '".$default."',
                item_type       = '".$vas."',
                added_by        = '$admin_id',
                status          = '1',
                created_at      = '$curr',
                updated_at      = '$curr' ";
            //return $query;
            $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
            $exe =mysqli_query($link,$query);
            $get_id = mysqli_insert_id($link);
            return $get_id;
        }else{
            $get_id = $this->getDetails(VAS_MASTER,"id"," token ='".$default."' ");
            return $get_id['id'];
        }
    }

     /*--------------------------------------------- 
                Customer Import Alumni Data
    ----------------------------------------------*/ 

    // Save Excel Session

    function saveCustomerExcelSession($data)
    {
        $data = explode("`",$data);
        $sheet_name     = $this->cleanString($data[0]);
        $token          = $this->cleanString($data[1]);
        $curr = date("Y-m-d H:i:s");
        $query = "INSERT INTO ".CUSTOMERDATASHEET_TBL." SET branch_id='".$_SESSION['selected_branch']."' ,import_token='$token', uploaded_sheet='$sheet_name', created_at='$curr', updated_at='$curr', status='0' ";
        $execute = $this->selectQuery($query);
        if($execute){
            return 1;
        }else{
            return $this->errorMsg ("Error Occured Please try Again");
        }
    }

    // Approve Import Data

    function approveCustomerImportData($token) {
        $q = "SELECT * FROM ".TEMPCUSTOMEREXCEL_TBL." WHERE upload_token='$token' ";
        $temp_q = $this->selectQuery($q);
        if(mysqli_num_rows($temp_q) > 0){
            while ($data = mysqli_fetch_array($temp_q)) {
                //$user_token     = $this->generateRandomString("35").date("Ymdhis").$this->generateRandomString("30");
                //$mobile_token   = $this->mobileToken("8");
                //$email_token    = $this->generateRandomString("50");
                $curr           = date("Y-m-d H:i:s");
                $admin_id       = $_SESSION["crm_admin_id"];
                
                /*if(($data['mobile']!="") && ($data['email']!="")){
                    $check_user = $this -> check_query(CUSTOMER_TBL,"id"," email='".$data['email']."' OR mobile   ='".$data['mobile']."' ");
                }elseif($data['mobile'] ==""){
                    $check_user = $this -> check_query(CUSTOMER_TBL,"id"," email='".$data['email']."' ");
                }elseif($data['email'] ==""){
                    $check_user = $this -> check_query(CUSTOMER_TBL,"id"," mobile='".$data['mobile']."' ");
                }else{
                    $check_user = 1;
                }*/

               
                if($data['email']!=''){
                    $check_user = $this -> check_query(CUSTOMER_TBL,"id"," email ='".$data['email']."' ");
                    if($check_user == 0){
                        $leadcount  = $this->check_query(CUSTOMER_TBL,"id"," 1 ");
                        $uid        = "LE".str_pad($leadcount, 4,0, STR_PAD_LEFT);
                        $today      = date("Y-m-d");

                        $token          = $this->generateRandomString("30");
                        $password       = $this->generateRandomStringGenerator("8");
                        $psw            = $this->encryptPassword($password);
                        $query = "INSERT INTO ".CUSTOMER_TBL." SET 
                                branch_id    = '".$this->cleanString($data['branch_id'])."',
                                import_token = '".$this->cleanString($data['upload_token'])."',
                                token       = '".$this->hyphenize($data['name'])."',
                                priority    = 'low',
                                name        = '".$this->cleanString($data['name'])."',
                                gender      = '',
                                mobile      = '".$this->cleanString($data['mobile'])."',
                                email       = '".$this->cleanString($data['email'])."',                            
                                password    = '".$psw."',
                                has_psw     = '".$password."',
                                address     = '".$this->cleanString($data['addresss'])."',
                                city        = '',
                                state       = '',
                                pincode     = '',
                                kyc_status  = '0',
                                assign_status = '0',
                                kyc_remarks = '',
                                status      = '1',
                                added_by    = '$admin_id',  
                                created_at  = '$curr',
                                updated_at  = '$curr' "; 
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            $customer_id = "CU".str_pad($last_id, 4,0, STR_PAD_LEFT);
                            $q =" UPDATE ".CUSTOMER_TBL." SET uid='".$customer_id."' WHERE id='".$last_id."' ";
                            $result = $this->selectQuery($q);


                            $sender_mail    = NO_REPLY;
                            $subject        = ucwords($data['name'])." Account login details";
                            $receiver       = $data['email'];
                            $user_token     = $this->hyphenize($data['name']);
                            $email_temp     = $this->memberLoginInfoTemp($data['name'],$data['email'],$password,$user_token,$last_id,$data['name']);
                            $sendemail      = $this->send_mail_import($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                        }
                    }
                }
            }
        }
        $curre  = date("Y-m-d H:i:s");
        $query = "UPDATE ".CUSTOMERDATASHEET_TBL." SET status='1', updated_at='$curre' WHERE import_token='$token' ";
        $execute = $this->selectQuery($query);
        if($execute){
            return 1;
        }else{
            return "Error Occured Please try Again";
        }
    }

    // Deleted Imported Data

    function deleteCustomerImportDatass($data){
        $delete_temp = $this->deleteRow(TEMPCUSTOMEREXCEL_TBL," upload_token='$data' ");
        if($delete_temp){
            $delete = $this->deleteRow(CUSTOMERDATASHEET_TBL," import_token='$data' ");
            if ($delete) {
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }

    function deleteCustomerSingleRowImportData($data){
        $delete_temp = $this->deleteRow(TEMPCUSTOMEREXCEL_TBL," id='$data' ");
        if($delete_temp){
                return 1;
        }else{
            return 0;
        }
    }

    // Delete Status

    function deletLead($data)
    {    
        $info           = $this->getDetails(LEAD_TBL,"deleted_status"," id ='$data' ");
        $admin_id       = $_SESSION["crm_admin_id"] ;
        $curr           = date("Y-m-d H:i:s");
        if($info['deleted_status'] == 0){
            $query = "UPDATE ".LEAD_TBL." SET
                 deleted_status ='1',
                 deleted_by     ='".$admin_id."',
                 deleted_on     ='".$curr."',
                 restored_by    ='0',
                 restored_on    ='',
                 restored_remarks = ''  WHERE id='$data' ";
        }else{
             $query = "UPDATE ".LEAD_TBL." SET
                 deleted_status  ='0',
                 deleted_by      ='0',
                 deleted_on      ='',
                 restored_by     ='".$admin_id."',
                 restored_on     ='".$curr."',
                 restored_remarks = '' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Revert Lead 

    function undoLead($id='')
    {   
        $result =  array();
        $_SESSION['undo_leads_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_TBL,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['undo_leads_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='restored_remarks'>Remarks  <en>*</en></label>
                                        <textarea class='form-control' name='restored_remarks' id='restored_remarks'></textarea>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateLead($data)
    {
        if(isset($_SESSION['undo_leads_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['undo_leads_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $query = " UPDATE ".LEAD_TBL." SET 
                    deleted_status  = '0',
                    deleted_by      = '0',
                    deleted_on      = '',
                    restored_by     = '".$admin_id."',
                    restored_on     = '".$curr."',
                    restored_remarks= '".$data['restored_remarks']."',
                    updated_at      = '".$curr."'
                    WHERE id        = '".$data['token']."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                    unset($_SESSION['undo_leads_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    // Delete Status

    function deleteGallery($data)
    {    
        $info           = $this->getDetails(GALLERY_TBL,"deleted_status"," id ='$data' ");
        $admin_id       = $_SESSION["crm_admin_id"] ;
        $curr           = date("Y-m-d H:i:s");
        if($info['deleted_status'] == 0){
            $query = "UPDATE ".GALLERY_TBL." SET
                 deleted_status ='1',
                 deleted_by     ='".$admin_id."',
                 deleted_on     ='".$curr."',
                 restored_by    ='0',
                 restored_on    ='',
                 restored_remarks = ''  WHERE id='$data' ";
        }else{
             $query = "UPDATE ".GALLERY_TBL." SET
                 deleted_status  ='0',
                 deleted_by      ='0',
                 deleted_on      ='',
                 restored_by     ='".$admin_id."',
                 restored_on     ='".$curr."',
                 restored_remarks = '' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Revert Invoice 

    function undoGallery($id='')
    {   
        $result =  array();
        $_SESSION['undo_gallery_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(GALLERY_TBL,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['undo_gallery_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='restored_remarks'>Remarks  <en>*</en></label>
                                        <textarea class='form-control' name='restored_remarks' id='restored_remarks'></textarea>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateGallery($data)
    {
        if(isset($_SESSION['undo_gallery_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['undo_gallery_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $query = " UPDATE ".GALLERY_TBL." SET 
                    deleted_status  = '0',
                    deleted_by      = '0',
                    deleted_on      = '',
                    restored_by     = '".$admin_id."',
                    restored_on     = '".$curr."',
                    restored_remarks= '".$data['restored_remarks']."',
                    updated_at      = '".$curr."'
                    WHERE id        = '".$data['token']."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                    unset($_SESSION['undo_gallery_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    function sendcustomer_mail($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(NEWS_TBL,"send_mail_status"," id ='$data' ");
        if($info['send_mail_status'] ==1){
            $query = "UPDATE ".NEWS_TBL." SET send_mail_status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".NEWS_TBL." SET send_mail_status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }
    
    /*----------------------------
            Settings
    ------------------------------*/

    // Add Chit Type

    function addChitMaster($data)
    {
        $layout = "";
        if(isset($_SESSION['chit_master_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['chit_master_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $check = $this->check_query(CHIT_MASTER,"id"," token ='".$this->masterhyphenize($data['item_type'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".CHIT_MASTER." SET
                        branch_id       = '".((isset($_SESSION['selected_branch']))?  $_SESSION['selected_branch'] : '')."',
                        token           = '".$this->masterhyphenize($data['item_type'])."',
                        item_type       = '".$this->cleanString($data['item_type'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                    $exe = $this->selectQuery($query);
                    if($exe){   
                        unset($_SESSION['chit_master_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Chit Master is already exist!.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editChitMaster($id='')
    {   
        $result =  array();
        $_SESSION['edit_master_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(CHIT_MASTER,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_master_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='editemail'>Chit Type  <en>*</en></label>
                                        <input type='text' class='form-control form-control-lg' id='item_type' name='item_type' value='".$info['item_type']."' placeholder='Enter Chit Type'>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateChitMaster($data)
    {
        if(isset($_SESSION['edit_master_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_master_key']){
                $check = $this->check_query(CHIT_MASTER,"id"," token ='".$this->masterhyphenize($data['item_type'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".CHIT_MASTER." SET 
                               token           = '".$this->masterhyphenize($data['item_type'])."',
                               item_type       = '".$this->cleanString($data['item_type'])."',
                               added_by        = '$admin_id',
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        unset($_SESSION['edit_master_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Chit Master is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    // Avtive & Inactive Status 

    function chitMasterStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this->getDetails(CHIT_MASTER,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".CHIT_MASTER." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".CHIT_MASTER." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Add Deposite Type

    function addDepositeMaster($data)
    {
        $layout = "";
        if(isset($_SESSION['deposite_master_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['deposite_master_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $check = $this->check_query(DEPOSITE_MASTER,"id"," token ='".$this->masterhyphenize($data['item_type'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".DEPOSITE_MASTER." SET
                        branch_id       = '".((isset($_SESSION['selected_branch']))?  $_SESSION['selected_branch'] : '')."',
                        token           = '".$this->masterhyphenize($data['item_type'])."',
                        item_type       = '".$this->cleanString($data['item_type'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                    $exe = $this->selectQuery($query);
                    if($exe){   
                        unset($_SESSION['deposite_master_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Deposite Master is already exist!.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editDepositeMaster($id='')
    {   
        $result =  array();
        $_SESSION['edit_deposite_master_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(DEPOSITE_MASTER,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_deposite_master_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='editemail'>Deposite Type  <en>*</en></label>
                                        <input type='text' class='form-control form-control-lg' id='item_type' name='item_type' value='".$info['item_type']."' placeholder='Enter Deposite Type'>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateDepositeMaster($data)
    {
        if(isset($_SESSION['edit_deposite_master_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_deposite_master_key']){
                $check = $this->check_query(DEPOSITE_MASTER,"id"," token ='".$this->masterhyphenize($data['item_type'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".DEPOSITE_MASTER." SET 
                               token          = '".$this->masterhyphenize($data['item_type'])."',
                               item_type      = '".$this->cleanString($data['item_type'])."',
                               added_by       = '$admin_id',
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        unset($_SESSION['edit_deposite_master_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Deposite Master is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    // Avtive & Inactive Status 

    function depositeMasterStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this->getDetails(DEPOSITE_MASTER,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".DEPOSITE_MASTER." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".DEPOSITE_MASTER." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Add Deposite Type

    function addVASMaster($data)
    {
        $layout = "";
        if(isset($_SESSION['vas_master_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['vas_master_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $check = $this->check_query(VAS_MASTER,"id"," token ='".$this->masterhyphenize($data['item_type'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".VAS_MASTER." SET
                        branch_id       = '".((isset($_SESSION['selected_branch']))?  $_SESSION['selected_branch'] : '')."',
                        token           = '".$this->masterhyphenize($data['item_type'])."',
                        item_type       = '".$this->cleanString($data['item_type'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                    $exe = $this->selectQuery($query);
                    if($exe){   
                        unset($_SESSION['vas_master_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered VAS Master is already exist!.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editVASMaster($id='')
    {   
        $result =  array();
        $_SESSION['edit_vas_master_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(VAS_MASTER,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_vas_master_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='editemail'>VAS Type  <en>*</en></label>
                                        <input type='text' class='form-control form-control-lg' id='item_type' name='item_type' value='".$info['item_type']."' placeholder='Enter VAS Type'>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateVASMaster($data)
    {
        if(isset($_SESSION['edit_vas_master_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_vas_master_key']){
                $check = $this->check_query(VAS_MASTER,"id"," token ='".$this->masterhyphenize($data['item_type'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".VAS_MASTER." SET 
                               token          = '".$this->masterhyphenize($data['item_type'])."',
                               item_type      = '".$this->cleanString($data['item_type'])."',
                               added_by       = '$admin_id',
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        unset($_SESSION['edit_vas_master_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered VAS Master is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    // Avtive & Inactive Status 

    function VASMasterStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this->getDetails(VAS_MASTER,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".VAS_MASTER." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".VAS_MASTER." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*-------------------------------------
            Branch Master Functions
    --------------------------------------*/

    // Add Branch Master 

    function addBranchMaster($data)
    {
        $layout = "";
        if(isset($_SESSION['branch_master_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['branch_master_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $check = $this->check_query(BRANCH_MASTER,"id"," token ='".$this->masterhyphenize($data['branch_name'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".BRANCH_MASTER." SET
                        token           = '".$this->masterhyphenize($data['branch_name'])."',
                        branch          = '".$this->cleanString($data['branch_name'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                    $exe = $this->selectQuery($query);
                    if($exe){   
                        unset($_SESSION['branch_master_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Branch Name is already exist!.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit Branch Master 

    function editBranchMaster($id='')
    {   
        $result =  array();
        $_SESSION['edit_branch_master_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(BRANCH_MASTER,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_branch_master_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='editemail'>Branch Name  <en>*</en></label>
                                        <input type='text' class='form-control form-control-lg' id='branch_name' name='branch_name' value='".$info['branch']."' placeholder='Enter Branch Name'>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update Branch Master 

    function updateBranchMaster($data)
    {
        if(isset($_SESSION['edit_branch_master_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_branch_master_key']){
                $check = $this->check_query(BRANCH_MASTER,"id"," token ='".$this->masterhyphenize($data['branch_name'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".BRANCH_MASTER." SET 
                               token          = '".$this->masterhyphenize($data['branch_name'])."',
                               branch      = '".$this->cleanString($data['branch_name'])."',
                               added_by       = '$admin_id',
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        unset($_SESSION['edit_branch_master_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Branch Name is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Avtive & Inactive Branch Status 

    function branchMasterStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this->getDetails(BRANCH_MASTER,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".BRANCH_MASTER." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".BRANCH_MASTER." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*-------------------------------------
            Employee Role Functions
    --------------------------------------*/

    // Add Employee Role 

    function addEmployeeRoleMaster($data)
    {
        $layout = "";
        if(isset($_SESSION['employee_role_master_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['employee_role_master_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $check = $this->check_query(EMPLOYEE_ROLE,"id"," token ='".$this->masterhyphenize($data['employee_role'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".EMPLOYEE_ROLE." SET
                        branch_id       = '".((isset($_SESSION['selected_branch']))?  $_SESSION['selected_branch'] : '')."',
                        token           = '".$this->masterhyphenize($data['employee_role'])."',
                        employee_role   = '".$this->cleanString($data['employee_role'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                    $exe = $this->selectQuery($query);
                    if($exe){   
                        unset($_SESSION['employee_role_master_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Employee Role Name is already exist!.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit Employee Role 

    function editEmployeeRoleMaster($id='')
    {   
        $result =  array();
        $_SESSION['edit_employee_role_master_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(EMPLOYEE_ROLE,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_employee_role_master_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='editemail'>Employee Role Name  <en>*</en></label>
                                        <input type='text' class='form-control form-control-lg' id='employee_role' name='employee_role' value='".$info['employee_role']."' placeholder='Enter Employee Role Name'>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;
    } 

    // Update Employee Role 

    function updateEmployeeRoleMaster($data)
    {
        if(isset($_SESSION['edit_employee_role_master_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_employee_role_master_key']){
                $check = $this->check_query(EMPLOYEE_ROLE,"id"," token ='".$this->masterhyphenize($data['employee_role'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".EMPLOYEE_ROLE." SET 
                               token          = '".$this->masterhyphenize($data['employee_role'])."',
                               employee_role  = '".$this->cleanString($data['employee_role'])."',
                               added_by       = '$admin_id',
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        unset($_SESSION['edit_employee_role_master_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Employee Role is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Avtive & Inactive Employee Role Status 

    function employeeRoleMasterStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this->getDetails(EMPLOYEE_ROLE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".EMPLOYEE_ROLE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".EMPLOYEE_ROLE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }
    
    /*------------------------------------
                Lead Upload Images
    -------------------------------------*/

    // Add Lead images to the Album

    function addLeadGalleryImages($images=[],$token)
    {
        $id         = $this->decryptData($token);
        $curr       = date("Y-m-d H:i:s");
        $admin_id       = $_SESSION["crm_admin_id"];
        $q="";
        if(count($images)>0){
            foreach ($images as $value) {
                $output = str_replace("uploads/srcimg/", "", $value);
                $q = "INSERT INTO ".LEAD_GALLERY. " SET 
                    lead_id         = '$id', 
                    image           = '$output',
                    status          = '1',
                    created_at      = '$curr',
                    updated_at      = '$curr' ";
                $exe = $this->selectQuery($q);
            }
            return 1;
        }else{
            return "Please select images to upload.";
        }
    }

    // Remove Lead Gallery Image

    function removeaddLeadImage($id)
    {
        return $this->deleteRow(LEAD_GALLERY," id='$id' ");
    }   


//-----------------Dont'delete----------------
	
}


?>