<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>employee">Manage Employee</a></li>
                                <li class="breadcrumb-item active">Add Employee</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <form id="addEmployee" name="addEmployee" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['add_employee_key'] ?>" name="fkey" id="fkey">
                            <div class="nk-block-head">
                                <div class="nk-block-head-content">
                                    <h4 class="title nk-block-title">Add Employee</h4>
                                </div>
                            </div>
                            <div class="row g-gs">
                                <div class="col-lg-12">
                                    <div class="form-error"></div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="card card-bordered h-100">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Basic Information</h5>
                                            </div>
                                            
                                                <div class="form-group">
                                                    <label class="form-label" for="name">Employee name <en>*</en></label>
                                                    <input type="text" class="form-control" id="name" name="name">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="employee">Employee Role <en>*</en></label>
                                                    <select class="form-control form-select select2-hidden-accessible" id="role" name="role" data-placeholder="Select a Employee Role" required=""  tabindex="-1" aria-hidden="true" >
                                                        <option value="">Select Employee Role</option>
                                                        <?php echo $data['emp_role_list'] ?>
                                                    </select>
                                                </div>
                                                <div class="form-group branchListMulti display_none ">
                                                    <label class="form-label" for="branch">Assign Branch <en>*</en></label>
                                                    <select class="form-control form-select select2-hidden-accessible" id="assigned_branch_multi" name="assigned_branch_multi[]" data-placeholder="Select a Branch" required=""  aria-hidden="true" multiple="">
                                                        <?php echo $data['branch_list'] ?>
                                                    </select>
                                                </div>
                                                <div class="form-group branchList display_none">
                                                    <label class="form-label" for="branch">Assign Branch <en>*</en></label>
                                                    <select class="form-control form-select select2-hidden-accessible" id="assigned_branch" name="assigned_branch" data-placeholder="Select a Branch" required=""  aria-hidden="true" >
                                                        <option value="">Select Branch</option>
                                                        <?php echo $data['branch_list'] ?>
                                                    </select>
                                                </div>
                                                <?php if($_SESSION['super_admin']==1 || $_SESSION['employee_role']==2 ) { ?>
                                                <div class="form-group branchManagerList display_none">
                                                    <label class="form-label" for="branch Manager">Assign Branch Manager <en>*</en></label>
                                                    <select class="form-control form-select select2-hidden-accessible" id="assigned_branch_manager" name="assigned_branch_manager" data-placeholder="Select a Branch Manager" required=""  aria-hidden="true" >
                                                        <?php echo $data['bm_list'] ?>
                                                    </select>
                                                </div>
                                                <?php } else { ?>
                                                    <input type="hidden" name="assigned_branch_manager" value="<?php echo $_SESSION['crm_admin_id']; ?>">
                                                <?php } ?>
                                                <?php if($_SESSION['super_admin']==1) { ?>
                                                <div class="form-group generalManagerList display_none">
                                                    <label class="form-label" for="general Manager">Assign General Manager <en>*</en></label>
                                                    <select class="form-control form-select select2-hidden-accessible" id="assigned_general_manager" name="assigned_general_manager" data-placeholder="Select a General Manager" required=""  aria-hidden="true" >
                                                    </select>
                                                </div>
                                                <?php } ?>
                                                <div class="form-group">
                                                    <div class="row g-3 align-center">
                                                        <div class="col-lg-12">
                                                            <label class="form-label">Gender</label>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <ul class="custom-control-group g-3 align-center flex-wrap">
                                                                <li>
                                                                    <div class="custom-control custom-radio">
                                                                        <input type="radio" class="custom-control-input" checked="" name="gender" id="reg-enable" value="male">
                                                                        <label class="custom-control-label" for="reg-enable">Male</label>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="custom-control custom-radio">
                                                                        <input type="radio" class="custom-control-input" name="gender" id="reg-disable" value="female">
                                                                        <label class="custom-control-label" for="reg-disable">Female</label>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="cf-full-name">Login Password <en>*</en></label>
                                                    <input type="password" class="form-control" id="password" name="password" >
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Contact Information</h5>
                                            </div>
                                            <div class="row g-4">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="email">Email <en>*</en></label>
                                                        <input type="text" class="form-control" id="email" name="email">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="mobile">Mobile <en>*</en></label>
                                                        <input type="text" class="form-control" id="mobile" name="mobile">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="address">Address</label>
                                                        <input type="text" class="form-control" id="address" name="address">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="city">City</label>
                                                        <input type="text" class="form-control" id="city" name="city">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="state">State</label>
                                                        <input type="text" class="form-control" id="state" name="state">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="pincode">Pincode</label>
                                                        <input type="text" class="form-control" id="pincode" name="pincode">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>employee' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg"></div>
                    <div class="nk-block nk-block-lg"></div>
                    <div class="nk-block nk-block-lg"></div>

                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>

<script type="text/javascript">
   
    $("#role").change(function() {
        var role = $(this).val();
         if(role==2 || role==3) {
            if(role==2) {
                $('.branchListMulti').removeClass('display_none');
                $('.branchList').addClass('display_none');
                $('.branchManagerList').addClass('display_none');
            } else {
                $('.branchList').removeClass('display_none');
                $('.branchListMulti').addClass('display_none');
                $('.branchManagerList').addClass('display_none');
                $(".generalManagerList").addClass('display_none');
            }
            $(".generalManagerList").addClass('display_none');
        } else if(role!=1 && role!=2 && role!=3) {
            $('.branchList').addClass('display_none');
            $('.branchListMulti').addClass('display_none');
            $('.branchManagerList').removeClass('display_none');
            $(".generalManagerList").addClass('display_none');
        } else {
            $('.branchList').addClass('display_none');
            $('.branchListMulti').addClass('display_none');
            $('.branchManagerList').addClass('display_none');
            $(".generalManagerList").addClass('display_none');
        }

    });

    $("#assigned_branch").change(function() {
        if($("#role").val()==3) {
            var id = $(this).val();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=getGmListForAssign",
                dataType: "html",
                data: { result: id },
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    var data = JSON.parse(data);
                    $("#assigned_general_manager").html(data);
                    $(".generalManagerList").removeClass('display_none');
                }
            });
        }
    });

</script>