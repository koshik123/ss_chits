<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Manage Lead</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg minheight100">
                        
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Manage Lead</h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="float_right">
                                        <div class="card card-bordered card-stretch" style="float: left;margin-right:10px ">
                                            <div class="card-inner-group">
                                                <div class="position-relative card-tools-toggle" data-select2-id="23">
                                                    <div class="card-title-group" data-select2-id="22">
                                                        <div class="card-tools mr-n1" data-select2-id="21">
                                                            <ul class="btn-toolbar gx-1" data-select2-id="20">
                                                                <li data-select2-id="19">
                                                                    <div class="toggle-wrap" data-select2-id="18">
                                                                        <a href="#" class="btn btn-icon btn-trigger toggle" data-target="cardTools"><em class="icon ni ni-menu-right"></em></a>
                                                                        <div class="toggle-content" >
                                                                            <form role="form" method="POST" action="#" id="leadFilter">   
                                                                                <ul class="btn-toolbar gg gx-1" data-select2-id="16">
                                                                                    <li data-select2-id="15">
                                                                                        <div class="dropdown" data-select2-id="14">
                                                                                            <a href="#" class="btn btn-trigger btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                                                
                                                                                                <em class="icon ni ni-filter-alt"></em>  Filter
                                                                                            </a> 
                                                                                            <div class="filter-wg dropdown-menu dropdown-menu-xl dropdown-menu-right" style="">
                                                                                                <div class="dropdown-head">
                                                                                                    <span class="sub-title dropdown-title">Lead Filter </span>
                                                                                                </div>
                                                                                                <div class="dropdown-body dropdown-body-rg">
                                                                                                    <div class="row gx-6 gy-3">
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="overline-title overline-title-alt">Lead Source</label>
                                                                                                                <select class="form-select " name="leadsource" id="leadsource" data-placeholder='Select Lead Source'>
                                                                                                                    <option value="">Select Lead Source</option>
                                                                                                                    <?php echo $data['source_list'] ?>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="overline-title overline-title-alt">Moved to Customer</label>
                                                                                                                <select class="form-select " name="movecustomer" id="movecustomer" data-placeholder='Select Moved to Customer'>
                                                                                                                    <option value="">Select Moved to Customer</option>
                                                                                                                    <option value="1">Yes</option>
                                                                                                                    <option value="0">No</option>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                       <?php if(($_SESSION['super_admin'])==1){     ?>
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="overline-title overline-title-alt">Assigned Status</label>
                                                                                                                <select class="form-select " name="assignedstatus" id="assignedstatus" data-placeholder='Select Assigned Status'>
                                                                                                                    <option value="">Select Assigned Status</option>
                                                                                                                    <option value="1" >Assigned </option>
                                                                                                                    <option value="0" >Un Assigned</option>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="overline-title overline-title-alt">Assigned To</label>
                                                                                                                <select class="form-select " name="assignedto" id="assignedto" data-placeholder='Select Assigned To'>
                                                                                                                    <option value="">Select Assigned To</option>
                                                                                                                    <?php echo $data['emp_list'] ?>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    <?php }else{ ?>
                                                                                                        <input type="hidden" name="assignedstatus" id="assignedstatus">
                                                                                                        <input type="hidden" name="assignedto" id="assignedto">
                                                                                                    <?php } ?>
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="overline-title overline-title-alt">Chit</label>
                                                                                                                <select class="form-select " name="chitstatus" id="chitstatus" data-placeholder='Select Chit Scheme'>
                                                                                                                    <option value="">Select Chit Scheme</option>
                                                                                                                    <?php echo $data['chit_type'] ?>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="overline-title overline-title-alt">Deposite</label>
                                                                                                                <select class="form-select " name="depositestatus" id="depositestatus" data-placeholder='Select Deposite Scheme'>
                                                                                                                    <option value="">Select Deposite Scheme</option>
                                                                                                                    <?php echo $data['deposite_type'] ?>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="overline-title overline-title-alt">VAM</label>
                                                                                                                <select class="form-select " name="vamstatus" id="vamstatus" data-placeholder='Select VAM Scheme'>
                                                                                                                    <option value="">Select VAM Scheme</option>
                                                                                                                    <?php echo $data['vas_type'] ?>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="overline-title overline-title-alt">Lead Status</label>
                                                                                                                <select class="form-select " name="leadstatus" id="leadstatus" data-placeholder='Select Lead Status'>
                                                                                                                    <option value="">Select Lead Status</option>
                                                                                                                    <?php echo $data['leadstatus'] ?>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-12">
                                                                                                            <div class="form-group float_right">
                                                                                                                <button type="submit" class="btn btn-secondary">Filter</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                
                                                                                            </div><!-- .filter-wg -->
                                                                                        </div><!-- .dropdown -->
                                                                                    </li>
                                                                                </ul>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-search search-wrap" data-search="search"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <a href="<?php echo COREPATH ?>lead/add" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add Lead</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                        <div class="filter_tag_wrap">
                            <span class='tag_element'>
                                <h5 class="type_name"><i class="la la-file-o"></i> Showing Results: 
                                    <?php if ($data['leadsource']!="") { ?>
                                    Lead Source - <strong> <?php echo ucwords($data['leadsource']) ?> </strong>
                                    <?php } ?>

                                    <?php if ($data['movecustomer']!="") { ?>
                                        <?php if ($data['leadsource']!="") { ?> ,<?php } ?>
                                     Customer Status - <strong> <?php echo $data['movecustomer']=='1' ? 'Moved' : 'Not moved' ?> </strong>
                                    <?php } ?>

                                    <?php if ($data['assignedstatus']!="") { ?>
                                        <?php if ($data['movecustomer']!="") { ?> ,<?php } ?>
                                    Assigned Status - <strong> <?php echo $data['assignedstatus']=='1' ? 'Assigned' : 'Un Assigned' ?> </strong>
                                    <?php } ?>

                                    <?php if ($data['assignedto']!="") { ?>
                                        <?php if ($data['assignedstatus']!="") { ?> ,<?php } ?>
                                    Assigned To - <strong> <?php echo $data['assignedto'] ?> </strong>
                                    <?php } ?>
                                    

                                    <?php if ($data['lead_status']!="") { ?>
                                        <?php if ($data['assignedto']!="") { ?> ,<?php } ?>
                                    Lead Status - <strong> <?php echo ucwords($data['lead_status']) ?> </strong>
                                    <?php } ?>

                                    <?php if ($data['chit_status']!="") { ?>
                                        <?php if ($data['lead_status']!="") { ?> ,<?php } ?>
                                    Lead Status - <strong> <?php echo ucwords($data['chit_status']) ?> </strong>
                                    <?php } ?>

                                    <?php if ($data['deposite_status']!="") { ?>
                                        <?php if ($data['chit_status']!="") { ?> ,<?php } ?>
                                    Lead Status - <strong> <?php echo ucwords($data['deposite_status']) ?> </strong>
                                    <?php } ?>

                                    <?php if ($data['vam_status']!="") { ?>
                                        <?php if ($data['deposite_status']!="") { ?> ,<?php } ?>
                                    Lead Status - <strong> <?php echo ucwords($data['vam_status']) ?> </strong>
                                    <?php } ?>
                                </h5>  
                                <span class="clearfix"></span>
                                <p class="result_count"> About <?php echo $data['list']['count'] ?> results </p>
                            </span>
                            <span class="float_right remove_filters"><a href="<?php echo COREPATH ?>lead" class="btn btn-danger btn-sm"><em class="icon ni ni-cross"></em> Remove Filter</a></span>
                        </div>


                        <div class="form-error"></div>
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-bordered ">
                                    <thead>
                                        <tr>
                                            <th >#</th>
                                            <th >UID</th>
                                            <th >Name</th>
                                            <th >Product</th>
                                            <th >Lead Status</th>
                                            <?php 
                                            if(($_SESSION['super_admin'])==1){
                                             ?>
                                             <th >Assigned To</th>
                                            <?php } ?>
                                            <th >Lead Source</th>
                                            <th >Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php echo $data['list']['layout'] ?>
                                    </tbody>
                                </table>
                            </div>

                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade editLeadModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Add Activity</h5>
                <form  id="addActivity" name="addActivity" method="POST" action="#" enctype="multipart/form-data">
                    <input type="hidden" name="fkey" id="fkey">
                    <input type="hidden" name="token" id="token">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label" for="activityName">Lead Name <en></en></label>
                                        <input type="text" class="form-control" id="activityName" name="lead_name" disabled="">
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label" for="activityMobile">Mobile <en></en></label>
                                        <input type="text" class="form-control" id="activityMobile" name="mobile" disabled="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label" for="activityEmail">Email <en></en></label>
                                        <input type="email" class="form-control" id="activityEmail" name="email" disabled="">
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="activity_id">Select Activity <en>*</en></label>
                                        <div class="form-control-wrap ">
                                            <select class="form-control form-select select2-hidden-accessible" id="activity_id" name="activity_id" data-placeholder="Select a Activity Type" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                <?php echo $data['activity_types'] ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Activity Date <en>*</en></label>
                                        <div class="form-control-wrap">
                                            <input type="text" name="act_date" id="act_date" class="form-control date-picker" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="remarks">Remarks
                                            <en>*</en></label>
                                         <textarea class="form-control form-control-sm" id="remarks" name="remarks" placeholder="Write your message"></textarea>
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 float_right">
                                        <li>
                                            <a href="javascript:void();" class="btn btn-lg btn-danger " data-dismiss="modal">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                       
                    </div><!-- .tab-content -->
                </form>
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade requestTypeModel" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
        <form id="updateRequestType" name="updateRequestType" method="POST" action="#" enctype="multipart/form-data">

        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>

            <div class="modal-body modal-body-sm">
                <div class="form-error"></div><br>
                <h5 class="title">Update Request type</h5>
                    <input type="hidden" value="" name="fkey" id="editfkey">
                    <input type="hidden" value="" name="token" id="token">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="requesttype_info">Request type <en>*</en></label>
                                        <input type="text" class="form-control" id="requesttype_info" name="request_type">
                                    </div>
                                </div>
                               
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div><!-- .tab-content -->
               
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
        </form>
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade addAssignLeadModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Assign Lead to Employee</h5>
                <form  id="assignLead" name="assignLead" method="POST" action="#" enctype="multipart/form-data">
                    <input type="hidden" name="fkey" id="fkey1">
                    <input type="hidden" name="lead_id" id="token1">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="lead_name">Lead Name <en>*</en></label>
                                        <input type="text" class="form-control" id="lead_name" name="lead_name">
                                    </div>
                                </div>
                                
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label" for="priority">Lead Priority <en>*</en></label>
                                        <select class="form-control " name="priority" id="priority">
                                           <option value="low">Low</option>
                                           <option value="medium">Medium</option>
                                           <option value="high">High</option>
                                           <option value="urgent">Urgent</option>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="mobile">Mobile <en>*</en></label>
                                        <input type="text" class="form-control" id="mobile" name="mobile">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="email">Email <en>*</en></label>
                                        <input type="email" class="form-control" id="email" name="email">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="employee_id">Select Employee <en>*</en></label>
                                        <div class="form-control-wrap ">
                                            <select class="form-control form-select select2-hidden-accessible" id="employee_id" name="employee_id" data-placeholder="Select a Employee" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                <?php echo $data['emp_list'] ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="remarks">Remarks
                                            <en>*</en></label>
                                         <textarea class="form-control form-control-sm" id="remarks" name="remarks" placeholder="Write your message"></textarea>
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 float_right">
                                        <li>
                                            <a href="javascript:void();" class="btn btn-lg btn-danger " data-dismiss="modal">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                       
                    </div><!-- .tab-content -->
                </form>
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade ReassignLeadModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form  id="reassignLead" name="reassignLead" method="POST" action="#" enctype="multipart/form-data">
                <div class="layout"></div>
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade moveLeadToCustomerModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Move Lead to Customer</h5>
                <form  id="moveLeadToCustomer" name="moveLeadToCustomer" method="POST" action="#" enctype="multipart/form-data">
                    <input type="hidden" name="fkey" id="movefkey">
                    <input type="hidden" name="lead_id" id="movetoken">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="moveremarks">Remarks
                                            <en>*</en></label>
                                         <textarea class="form-control form-control-sm" id="moveremarks" name="moveremarks" placeholder="Write your message"></textarea>
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 float_right">
                                        <li>
                                            <a href="javascript:void();" class="btn btn-lg btn-danger " data-dismiss="modal">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                       
                    </div><!-- .tab-content -->
                </form>
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>


<?php include 'includes/bottom.html'; ?>




<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Lead added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>


<?php if (isset($_GET['e'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Lead updated successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['as'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Successfully Assigned! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['rs'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Successfully Re-Assigned! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['aa'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Activity added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>


<?php if (isset($_GET['ca'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Lead moved successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

