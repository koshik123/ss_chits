<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>customer">Manage Customer</a></li>
                                <li class="breadcrumb-item active">Customer Info </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Manage Customer Info </h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="float_right">
                                    
                                        <button class="btn btn-danger" onclick="window.close();" ><em class="icon ni ni-cross"></em> <span>Close</span></button>
                                    </div> 
                                </div>                               
                            </div>
                        </div>
                        <div class="nk-block">
                        <div class="row gy-5">
                            <div class="col-lg-7">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Customer Information</h5>
                                        
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">ID</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['uid']) ?></div>
                                            </div>
                                        </li>


                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Name</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['name']) ?></div>
                                            </div>
                                        </li>
                                        
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Email Address</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['email']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Mobile</div>
                                                <div class="data-value text-soft"><em><?php echo ucwords($data['info']['mobile']) ?></em></div>
                                            </div>
                                        </li>

                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Gender</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['gender']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Date of Birth</div>
                                                <div class="data-value"><?php echo $data['info']['dob']=="" ? '' : date("d M, Y",strtotime($data['info']['dob'])) ?></div>
                                            </div>
                                        </li>

                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Full Address</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['address']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Country of Residence</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['nationality']) ?></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div><!-- .col -->
                            
                            
                        </div><!-- .row -->
                    </div><!-- .nk-block -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade kycModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form method="POST" action="#" name="updateapproverejectkyc"  id="updateapproverejectkyc">
                <div class="modal-body modal-body-sm">
                    <h5 class="title">KYC Status</h5>
                    <h5 class="title name"></h5>
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <!-- <div class="kycInfo"></div> -->
                                    <input type='hidden' value='' name='fkey' id='fkey'>
                                    <input type='hidden' value='' name='kyc_status' id='kyc_status'>         
                                    <input type='hidden' value='' name='token' id='token'>
                                    <div class='form-group'>
                                        <label class='form-label' for='cf-full-name'>Remarks <en>*</en></label>
                                        <textarea class='form-control form-control-sm' id='cf-default-textarea' placeholder='Write your message' name='remarks' ></textarea>
                                    </div>


                                </div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                    </div>
                </div><!-- .modal-body -->
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<?php include 'includes/bottom.html'; ?>




