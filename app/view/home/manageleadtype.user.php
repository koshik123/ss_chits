<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Lead Type Master </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Lead Type Master  </h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="float_right">
                                    <a href="#" class="btn btn-primary "  data-toggle="modal" data-target="#profile-edit"><em class="icon ni ni-plus"></em> <span>Add Lead Type</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-bordered card-preview">
                            <div class="card-inner">
                                <table class="datatable-init table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>UID</th>
                                            <th>Lead Type</th>
                                            <th>Added by</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php echo $data['list'] ?>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .card-preview -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="profile-edit">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Add Lead Type </h5>
                <div class="form-error"></div>
                 <form  id="addLeadType" name="addLeadType" method="POST" action="#" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $_SESSION['lead_type_key'] ?>" name="fkey" id="fkey">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="lead_type">Lead Type <en>*</en></label>
                                        <input type="text" class="form-control form-control-lg" id="lead_type" name="lead_type" value="" placeholder="Enter Lead Type">
                                    </div>
                                </div>
                                <?php if ($data['defaultcheck']!=1): ?>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row g-3 align-center">
                                                <div class="col-lg-12">
                                                    <ul class="custom-control-group g-3 align-center flex-wrap">
                                                        <li>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" name="default_settings" id="default_settings">
                                                                <label class="custom-control-label" for="default_settings">Select as default Lead type</label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif ?>
                                
                                <div class="col-12 ">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 float_right">
                                        <li>
                                            <a href="javascript:void();" class="btn btn-lg btn-danger " data-dismiss="modal">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                       
                    </div><!-- .tab-content -->
                </form>
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade editLeadModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <div class="form-error"></div> <br>
                <h5 class="title">Update Lead Type </h5>
                 <form  id="updateLeadType" name="updateLeadType" method="POST" action="#" enctype="multipart/form-data">
                    <div class="editLeadInfo"></div>
                </form>
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<?php include 'includes/bottom.html'; ?>


<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Lead Type added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['e'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Lead Type updated successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>
