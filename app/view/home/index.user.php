<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Overview</h3>
                            <div class="nk-block-des text-soft">
                                <p>Welcome to Dashboard.</p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a href="javascript:void();" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                <div class="toggle-expand-content" data-content="pageMenu">
                                    <!-- <ul class="nk-block-tools g-3">
                                        
                                        <li class="nk-block-tools-opt"><a href="<?php echo COREPATH ?>reports" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Reports</span></a></li>
                                    </ul> -->
                                </div>
                            </div>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div><!-- .nk-block-head -->
                <div class="nk-block">
                    <div class="row g-gs">
                        <div class="col-xxl-12">
                            <div class="row g-gs">
                                <div class="col-lg-12 col-xxl-12">
                                    <div class="row g-gs">
                                        <?php if($_SESSION['super_admin']==1) { ?>
                                        <div class="col-sm-3 col-lg-3 col-xxl-3">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start mb-2">
                                                        <div class="card-title">
                                                            <h6 class="title">Total Customers</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Total Customers"></em>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['customer_count'] ?></span>
                                                            
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div>
                                        <div class="col-sm-3 col-lg-3 col-xxl-3">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start mb-2">
                                                        <div class="card-title">
                                                            <h6 class="title">Total General Managers</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Total Employees"></em>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['gm_count'] ?></span>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div>
                                        <div class="col-sm-3 col-lg-3 col-xxl-3">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start mb-2">
                                                        <div class="card-title">
                                                            <h6 class="title">Total Branch Managers</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Total Employees"></em>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['bm_count'] ?></span>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div>
                                        <div class="col-sm-3 col-lg-3 col-xxl-3">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start mb-2">
                                                        <div class="card-title">
                                                            <h6 class="title">Total Employees</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Total Employees"></em>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['employee_count'] ?></span>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div>
                                        <?php } elseif( $_SESSION['employee_role'] == 2 || $_SESSION['employee_role'] == 3) { ?>
                                        <div class="col-sm-4 col-lg-4 col-xxl-4">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start mb-2">
                                                        <div class="card-title">
                                                            <h6 class="title">Total Customers</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Total Customers"></em>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['customer_count'] ?></span>
                                                            
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div>
                                        <div class="col-sm-4 col-lg-4 col-xxl-4">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start mb-2">
                                                        <div class="card-title">
                                                            <h6 class="title">Total Branch Managers</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Total Employees"></em>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['bm_count'] ?></span>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div>
                                        <div class="col-sm-4 col-lg-4 col-xxl-4">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start mb-2">
                                                        <div class="card-title">
                                                            <h6 class="title">Total Employees</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Total Employees"></em>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['employee_count'] ?></span>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div>
                                        <?php } else { ?>
                                        <div class="col-sm-4 col-lg-4 col-xxl-4">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start mb-2">
                                                        <div class="card-title">
                                                            <h6 class="title">Total Leads</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Total Leads"></em>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['employee_lead_count'] ?></span>
                                                            
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div>
                                        <div class="col-sm-4 col-lg-4 col-xxl-4">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start mb-2">
                                                        <div class="card-title">
                                                            <h6 class="title">Total Completed Leads</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Total Completed Leads"></em>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['compleated_lead_count'] ?></span>
                                                            
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div>
                                        <div class="col-sm-4 col-lg-4 col-xxl-4">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start mb-2">
                                                        <div class="card-title">
                                                            <h6 class="title">Total Hot Leads</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Total Hot Leads"></em>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['hot_lead_count'] ?></span>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div>
                                        <?php } ?>
                                    </div><!-- .row -->
                                </div>
                            </div><!-- .row -->
                        </div>

                        <div class="col-md-8 col-xxl-8">
                            
                            <?php if($_SESSION['super_admin']==1 || $_SESSION['employee_role'] == 2 || $_SESSION['employee_role'] == 3) { ?>
                            <div class="card card-bordered ">
                                <div class="card-inner-group">
                                    <div class="card-inner">
                                        <div class="card-title-group">
                                            <div class="card-title">
                                                <h6 class="title">New Customers</h6>
                                            </div>
                                            <div class="card-tools">
                                                <a href="<?php echo COREPATH ?>customer" class="link">View All</a>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <?php echo $data['customer'] ?>
                                </div>
                            </div><!-- .card -->
                            <?php } else { ?>
                                <div class="card card-bordered ">
                                <div class="card-inner-group">
                                    <div class="card-inner">
                                        <div class="card-title-group">
                                            <div class="card-title">
                                                <h6 class="title">Active Leads</h6>
                                            </div>
                                            <div class="card-tools">
                                                <a href="<?php echo COREPATH ?>lead" class="link">View All</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-inner p-0">
                                            <div class="nk-tb-list nk-tb-ulist">
                                                <div class="nk-tb-item nk-tb-head">
                                                    <div class="nk-tb-col tb-col-mb"><span class="sub-text">S.No</span></div>
                                                    <div class="nk-tb-col tb-col-mb"><span class="sub-text">UID</span></div>
                                                    <div class="nk-tb-col"><span class="sub-text">Name</span></div>
                                                    <div class="nk-tb-col tb-col-mb"><span class="sub-text">Product</span></div>
                                                    <div class="nk-tb-col tb-col-mb"><span class="sub-text">Lead Stauts</span></div>
                                                    <div class="nk-tb-col tb-col-md"><span class="sub-text">Lead Source</span></div>
                                                    <div class="nk-tb-col tb-col-lg"><span class="sub-text">Lead Date</span></div>
                                                </div>
                                                <?php echo $data['emp_lead_list']; ?>                                               
                                            </div><!-- .nk-tb-list -->
                                        </div>
                                </div>
                            </div>
                            <?php }  ?>
                        </div>
                    </div><!-- .row -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>