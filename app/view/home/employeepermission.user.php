<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>employee">Manage Employee</a></li>
                                <li class="breadcrumb-item active">Employee Permission</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        
                        <div class="row g-gs">
                            
                            <div class="col-lg-6">
                                <div class="">
                                    <div class="nk-block-head">
                                        <div class="nk-block-head-content">
                                            <h4 class="title nk-block-title"> Employee Contact information</h4>
                                        </div>
                                    </div>      
                                    <div class="card panel panel-default panel-table">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">Name </th>
                                                        <td> <?php echo ucwords($data['info']['name']) ?> </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <th scope="row">Role </th>
                                                        <td><?php echo ucwords($data['emp_role']['employee_role']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Gender </th>
                                                        <td><?php echo ucwords($data['info']['gender']) ?> </td>
                                                    </tr>
                                                    

                                                    <tr>
                                                        <th scope="row">Email </th>
                                                        <td><?php echo ($data['info']['email']) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Mobile</th>
                                                        <td><?php echo ($data['info']['mobile']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Address</th>
                                                        <td><?php echo ($data['info']['address']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">City </th>
                                                        <td><?php echo ucwords($data['info']['city']) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">State</th>
                                                        <td><?php echo ucwords($data['info']['state']) ?></td>
                                                    </tr>
                                                   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="col-lg-6">
                                    <div class="form-error"></div>
                                    <form id="employeePermissions" method="POST" accept-charset="utf-8" action="javascript:void();">
                                        <input type="hidden" value="<?php echo $_SESSION['create_permission_key'] ?>" name="fkey" id="fkey">
                                        <input type="hidden" value="<?php echo $data['token'] ?>" id="session_token" name="session_token">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <h5 class="  pull-left">
                                               <span class="ks-icon la la-key"></span> Permissions 
                                                </h5>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="select_box"><label class='checkbox-inline checkbox-success'>
                                                <input class='styled' type='checkbox' id="selectAllPost"> Select All</label></p>                             
                                            </div>
                                        </div>

                                        <div class="card panel panel-default panel-table">
                                            <div class="table-responsive">
                                                
                                                <table class="table table-bordered permission_checkbox ">
                                                    <tbody>
                                                        <tr>
                                                            <th width="20%" scope="row"># </th>
                                                            <th> Allowed Permissions</th>
                                                        </tr> 
                                                        <tr>
                                                            <th width="" scope="row" class="verticalalign">1 </th>
                                                            <td width="">
                                                                <div class="checkbox">
                                                                    <input name='permission[]' value='customers' class='post_permission' id="c1" type="checkbox" <?php echo (($data['emp_info']['customers']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="c1" class="marginleft5">
                                                                       <strong>Customer Management </strong> 
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='manage_customer' class='post_permission customer_permission' id="c2" type="checkbox" <?php echo (($data['emp_info']['manage_customer']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="c2" class="marginleft5">
                                                                        Manage Customer
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='import_customer' class='post_permission customer_permission' id="c3" type="checkbox" <?php echo (($data['emp_info']['import_customer']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="c3" class="marginleft5">
                                                                        Import Customer
                                                                    </label>
                                                                </div> 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th width="" scope="row" class="verticalalign">2 </th>
                                                            <td width="">
                                                                <div class="checkbox">
                                                                    <input name='permission[]' value='lead' class='post_permission' id="l1" type="checkbox" <?php echo (($data['emp_info']['lead']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l1" class="marginleft5">
                                                                       <strong>Lead Management </strong> 
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='manage_lead' class='post_permission lead_permission' id="l2" type="checkbox" <?php echo (($data['emp_info']['manage_lead']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l2" class="marginleft5">
                                                                        Manage Lead
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='lead_type' class='post_permission lead_permission' id="l3" type="checkbox" <?php echo (($data['emp_info']['lead_type']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l3" class="marginleft5">
                                                                        Lead Type Master
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='activity_type' class='post_permission lead_permission' id="l4" type="checkbox" <?php echo (($data['emp_info']['activity_type']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l4" class="marginleft5">
                                                                        Activity Type Master
                                                                    </label>
                                                                </div>  

                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='lead_source' class='post_permission lead_permission' id="l5" type="checkbox" <?php echo (($data['emp_info']['lead_source']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l5" class="marginleft5">
                                                                        Lead Source
                                                                    </label>
                                                                </div>  
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='customer_profile' class='post_permission lead_permission' id="l6" type="checkbox" <?php echo (($data['emp_info']['customer_profile']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l6" class="marginleft5">
                                                                        Customer Profile Master
                                                                    </label>
                                                                </div>  
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='import_lead' class='post_permission lead_permission' id="l7" type="checkbox" <?php echo (($data['emp_info']['import_lead']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l7" class="marginleft5">
                                                                        Import Lead
                                                                    </label>
                                                                </div>  

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th width="" scope="row" class="verticalalign">3 </th>
                                                            <td width="">
                                                                <div class="checkbox">
                                                                    <input name='permission[]' value='employee' class='post_permission' id="e1" type="checkbox" <?php echo (($data['emp_info']['employee']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="e1" class="marginleft5">
                                                                       <strong>Manage Employee  </strong> 
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='manageemployee' class='post_permission employee_permission' id="e2" type="checkbox" <?php echo (($data['emp_info']['manageemployee']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="e2" class="marginleft5">
                                                                        Manage Employee
                                                                    </label>
                                                                </div> 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th width="" scope="row" class="verticalalign">6 </th>
                                                            <td width="">
                                                                <div class="checkbox">
                                                                    <input name='permission[]' value='reports' class='post_permission' id="r1" type="checkbox" <?php echo (($data['emp_info']['reports']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="r1" class="marginleft5">
                                                                       <strong>Reports   </strong> 
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='lead_report' class='post_permission report_permission' id="r3" type="checkbox" <?php echo (($data['emp_info']['lead_report']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="r3" class="marginleft5">
                                                                        Lead Reports
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='customer_report' class='post_permission report_permission' id="r4" type="checkbox" <?php echo (($data['emp_info']['customer_report']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="r4" class="marginleft5">
                                                                        Customer Reports
                                                                    </label>
                                                                </div>   
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th width="" scope="row" class="verticalalign">5 </th>
                                                            <td width="">
                                                                <div class="checkbox">
                                                                    <input name='permission[]' value='settings' class='post_permission' id="m1" type="checkbox" <?php echo (($data['emp_info']['settings']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m1" class="marginleft5">
                                                                       <strong>Master settings  </strong> 
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='chit_master' class='post_permission master_permission' id="m2" type="checkbox" <?php echo (($data['emp_info']['chit_master']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m2" class="marginleft5">
                                                                        Chit Master
                                                                    </label>
                                                                </div>   
                                                                 <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='deposit_master' class='post_permission master_permission' id="m3" type="checkbox" <?php echo (($data['emp_info']['deposit_master']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m3" class="marginleft5">
                                                                         Deposit Master
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='vam_master' class='post_permission master_permission' id="m4" type="checkbox" <?php echo (($data['emp_info']['vam_master']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m4" class="marginleft5">
                                                                        Vam Master
                                                                    </label>
                                                                </div>  
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='branch_master' class='post_permission master_permission' id="m5" type="checkbox" <?php echo (($data['emp_info']['branch_master']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m4" class="marginleft5">
                                                                        Branch Master
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='employee_role_master' class='post_permission master_permission' id="m6" type="checkbox" <?php echo (($data['emp_info']['employee_role_master']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m4" class="marginleft5">
                                                                        Employee Role Master
                                                                    </label>
                                                                </div>   
                                                                
                                                            </td>
                                                        </tr>


                                                        

                                                        <tr>
                                                            <td colspan="2">
                                                                <div class="text-right m-b-0">
                                                                    <button type="submit" class="btn btn-success">Submit</button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>