<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Customer Reports </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Customer Reports</h4>
                                
                            </div>
                        </div>
                        <div class="row g-gs">
                            <div class="col-lg-10">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner">
                                        <form id="customerReport" method="POST" action="#">
                                            <div class="row gy-4">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="birth-day">From Date</label>
                                                        <input type="text" class="form-control  date-picker" id="from_date" name="from_date" placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="birth-day">To Date</label>
                                                        <input type="text" class="form-control date-picker" id="to_date" name="to_date" placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <!-- <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fv-topics">Select Branch</label>
                                                        <div class="form-control-wrap facilities3">
                                                            <select class="form-control  form-select-lg " id="select_customer_branch" name="branch_id[]" data-placeholder="Select a Branch" >
                                                                <option label="Select a Branch" value="">Select a Branch</option>
                                                                <?php echo $data['branch_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fv-topics">Select Customer</label>
                                                        <div class="form-control-wrap facilities4">
                                                            <select class="form-control  form-select-lg" id="branch_cus" name="customer_id[]" data-placeholder="Select a Customer" >
                                                                <option label="Select a Customer" value="">Select a Customer</option>
                                                                <?php echo $data['customer_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group  ">
                                                        <label class="form-label" for="fva-topics">Select Chit Scheme </label>
                                                        <div class="form-control-wrap facilities">
                                                            <select class="form-control select2-hidden-accessible" id="fva-top1" name="chit_id[]" data-placeholder="Select a Chit Scheme" >
                                                                <option label="Select a Chit Scheme" value="">Select a Chit Scheme</option>
                                                                <?php echo $data['chit_type'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Deposit Scheme </label>
                                                        <div class="form-control-wrap facilities1">
                                                            <select class="form-control select2-hidden-accessible" id="fva-top11" name="deposit_id[]" data-placeholder="Select a Deposit Scheme" >
                                                                <option label="Select a Deposit Scheme" value="">Select a Deposit Scheme</option>
                                                                <?php echo $data['deposite_type'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select VAM Scheme </label>
                                                        <div class="form-control-wrap facilities2">
                                                            <select class="form-control select2-hidden-accessible" id="fva-top111" name="vam_id[]" data-placeholder="Select a VAM Scheme" >
                                                                <option label="Select a VAM Scheme" value="">Select a VAM Scheme</option>
                                                                <?php echo $data['vas_type'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                     <div class="form-group float_right">
                                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>