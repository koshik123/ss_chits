<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>reports/customer">Customer Reports</a></li>
                                <li class="breadcrumb-item active"><strong><?php echo date("d M'y ", strtotime($data['from'])).' to '.date("d M'y ", strtotime($data['to'])) ?></strong>     </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Filter</h4>
                            </div>
                        </div>
                        <div class="row g-gs ">
                            
                            <div class="col-lg-12">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner">
                                        <form id="customerReport" method="POST" action="#">
                                            <div class="row gy-4">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fromdate">From Date <en>*</en></label>
                                                        <input type="text" class="form-control  date-picker" id="fromdate" name='from_date' placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="todate">To Date <en>*</en></label>
                                                        <input type="text" class="form-control date-picker" id="todate" name='to_date' placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fv-topics">Select Customer</label>
                                                        <div class="form-control-wrap facilities3">
                                                            <select class="form-control  form-select-lg " id="fva-top0
                                                            " name="customer_id[]" data-placeholder="Select a Customer" >
                                                                <option label="Select a Customer" value="">Select a Customer</option>
                                                                <?php echo $data['customer_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Chit Scheme </label>
                                                        <div class="form-control-wrap facilities">
                                                            <select class="form-control select2-hidden-accessible" id="fva-top1" name="chit_id[]" data-placeholder="Select a Chit Scheme" >
                                                                <option value="">Select a Chit Scheme</option>
                                                                <?php echo $data['chit_type'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Deposit Scheme </label>
                                                        <div class="form-control-wrap facilities1">
                                                            <select class="form-control select2-hidden-accessible" id="fva-top11" name="deposit_id[]" data-placeholder="Select a Deposit Scheme" >
                                                                <option value="">Select a Deposit Scheme</option>
                                                                <?php echo $data['deposite_type'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select VAM Scheme </label>
                                                        <div class="form-control-wrap facilities2">
                                                            <select class="form-control select2-hidden-accessible" id="fva-top111" name="vam_id[]" data-placeholder="Select a VAM Scheme" >
                                                                <option value="">Select a VAM Scheme</option>
                                                                <?php echo $data['vas_type'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                     <div class="form-group float_right">
                                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <?php if ($data['from']!="" && $data['to']!="" ){ ?>
                                <div class="daybook_report-form">
                                    <h3 style="font-size: 21px;"> <i class="fa fa-file-o"></i> Showing Results From - <strong><?php echo date("d M'y ", strtotime($data['from'])) ?></strong>  to <strong> <?php echo date("d M'y ", strtotime($data['to']))?></strong>
                                        <?php if ($data['customer_id']!=""){ ?>
                                            , <strong>Customer:</strong>  <?php echo ucwords($data['customer_status'])?>
                                        <?php } ?>
                                        
                                         <?php if ($data['chit_id']!="") { ?>
                                        
                                        , <strong>Chit:</strong> <?php echo ucwords($data['chit_status']) ?> 
                                        <?php } ?>

                                        <?php if ($data['deposit_id']!="") { ?>
                                           
                                       , <strong>Deposit:</strong> <?php echo ucwords($data['deposite_status']) ?> 
                                        <?php } ?>

                                        <?php if ($data['vam_id']!="") { ?>
                                            
                                        , <strong>VAM:</strong> <?php echo ucwords($data['vam_status']) ?> 
                                        <?php } ?>

                                      </h3>
                                </div>
                                <?php } ?>
                                <!-- <a class="btn btn-info btn-sm mt15" href="<?php echo COREPATH ?>resource/lead_excel_report.php?from=<?php echo $data['from'] ?>&to=<?php echo $data['to'] ?>&customer_id=<?php echo $data['customer_id'] ?>&chit_id=<?php echo $data['chit_id'] ?>&deposit_id=<?php echo $data['deposit_id'] ?>&vam_id=<?php echo $data['vam_id'] ?>" ><em class="icon ni ni-file-docs"></em>  Export Report</a> -->
                            </div>

                            <div class="col-lg-12">
                                <div class="card-box table-responsive">
                                    <table id="datatable" class="table table-bordered ">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Cutomer Name</th>
                                                <th>Product</th>
                                               <!--  <th>Lead Source</th>
                                                <th>Lead Status</th> -->
                                                <th>Assigned To</th>
                                                <!-- <th>Action</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php echo $data['list'] ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>