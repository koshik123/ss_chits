<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block">
                    <div class="card card-bordered">
                        <div class="card-aside-wrap">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head nk-block-head-lg">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h4 class="nk-block-title">Personal Information</h4>
                                            <div class="nk-block-des">
                                                <p>Basic info, like your name and address, that you use on Nio Platform.</p>
                                            </div>
                                        </div>
                                        <div class="nk-block-head-content align-self-start d-lg-none">
                                            <a href="#" class="toggle btn btn-icon btn-trigger mt-n1" data-target="userAside"><em class="icon ni ni-menu-alt-r"></em></a>
                                        </div>
                                    </div>
                                    
                                    
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
                                    <div class="nk-data data-list">
                                        <div class="data-head">
                                            <h6 class="overline-title">Basics</h6>
                                        </div>
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit">
                                            <div class="data-col">
                                                <span class="data-label">Full Name</span>
                                                <span class="data-value"><?php echo ucwords($data['info']['name']) ?></span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->
                                       
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit">
                                            <div class="data-col">
                                                <span class="data-label">Email</span>
                                                <span class="data-value"><?php echo ($data['info']['email']) ?></span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit">
                                            <div class="data-col">
                                                <span class="data-label">Phone Number</span>
                                                <span class="data-value "><?php echo ($data['info']['mobile']) ?></span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit">
                                            <div class="data-col">
                                                <span class="data-label">Gender</span>
                                                <span class="data-value "><?php echo ucwords($data['info']['gender']) ?></span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->


                                        
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit" data-tab-target="#address">
                                            <div class="data-col">
                                                <span class="data-label">Address</span>
                                                <span class="data-value"><?php echo ($data['info']['address'].'<br>'.$data['info']['city']) ?><br><?php echo ($data['info']['pincode']) ?></span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->
                                    </div><!-- data-list -->
                                    
                                </div><!-- .nk-block -->
                            </div>
                            <div class="card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg" data-content="userAside" data-toggle-screen="lg" data-toggle-overlay="true">
                                <div class="card-inner-group" data-simplebar>
                                    <div class="card-inner">
                                        <div class="user-card">
                                            <div class="user-avatar bg-primary">
                                                <span>JD</span>
                                            </div>
                                            <div class="user-info">
                                                <span class="lead-text"><?php echo ucwords($data['info']['name']) ?></span>
                                                <span class="sub-text"><?php echo ($data['info']['email']) ?></span>
                                            </div>
                                            
                                        </div><!-- .user-card -->
                                    </div><!-- .card-inner -->
                                   
                                    <div class="card-inner p-0">
                                        <ul class="link-list-menu">
                                            <li><a class="active" href="<?php echo COREPATH ?>profile"><em class="icon ni ni-user-fill-c"></em><span>Personal Infomation</span></a></li>

                                            <!-- <li><a href="<?php echo COREPATH ?>profile/activity"><em class="icon ni ni-activity-round-fill"></em><span>Account Activity</span></a></li> -->
                                            <li><a href="<?php echo COREPATH ?>profile/changepassword"><em class="icon ni ni-shield-star-fill"></em><span>Change Password</span></a></li>
                                            <?php if($_SESSION['super_admin']==1) { ?>
                                            <li><a href="<?php echo COREPATH ?>trash/leads"><em class="icon ni ni-trash"></em><span>Trash Management</span></a></li>
                                            <?php }?>
                                        </ul>
                                    </div><!-- .card-inner -->
                                </div><!-- .card-inner-group -->
                            </div><!-- card-aside -->
                        </div><!-- .card-aside-wrap -->
                    </div><!-- .card -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>


<div class="modal fade" tabindex="-1" role="dialog" id="profile-edit">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <form id="editProfile" method="POST" accept-charset="utf-8" action="#">
                    <input type="hidden" value="<?php echo $_SESSION['edit_profile_key'] ?>" name="fkey" id="fkey">
                    <input type="hidden" value="<?php echo $data['token'] ?>" name="session_token" id="session_token">
                    <div class="modal-body modal-body-lg">
                        <h5 class="title">Update Profile</h5>
                        <ul class="nk-nav nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#personal">Personal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#address">Address</a>
                            </li>
                        </ul><!-- .nav-tabs -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <div class="row gy-4">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="full-name">Full Name <en>*</en></label>
                                            <input type="text" class="form-control form-control-lg" id="name" name="name" value="<?php echo ucwords($data['info']['name']) ?>" placeholder="Enter Full name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="display-email">Email <en>*</en></label>
                                            <input type="text" class="form-control form-control-lg"  name="email" value="<?php echo ucwords($data['info']['email']) ?>" placeholder="Enter email" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="phone-no">Mobile Number <en>*</en></label>
                                            <input type="text" class="form-control form-control-lg" id="phone-no" name="mobile" value="<?php echo ($data['info']['mobile']) ?>" placeholder="Mobile Number">
                                        </div>
                                    </div>
                                    
                                     <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label" for="gender">Gender</label>
                                                <select class="form-select" id="gender" name="gender" data-ui="lg">
                                                    <option value="male" <?php echo (($data['info']['gender']=="male") ? "selected" : "" ); ?>>Male</option>
                                                    <option value="female" <?php echo (($data['info']['gender']=="female") ? "selected" : "" ); ?>>Female</option>
                                                </select>
                                            </div>
                                        </div>
                                   
                                    <div class="col-12">
                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                            <li>
                                                <button type="submit" class="btn btn-lg btn-primary">Update Profile</button>
                                            </li>
                                            <li>
                                                <a href="#" data-dismiss="modal" class="link link-light">Cancel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .tab-pane -->
                            <div class="tab-pane" id="address">
                                <div class="row gy-4">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="address-l1">Address  <en>*</en></label>
                                            <input type="text" class="form-control form-control-lg" id="address-l1" name="address" value="<?php echo $data['info']['address'] ?>">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="address-st">City <em>*</em></label>
                                            <input type="text" class="form-control form-control-lg" id="address-st" name="city" value="<?php echo $data['info']['city'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="address-l2">State</label>
                                            <input type="text" class="form-control form-control-lg" id="address-l2" name="state" value="<?php echo $data['info']['state'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="address-county">Pincode</label>
                                            <input type="text" class="form-control form-control-lg" id="address-county" name="pincode" value="<?php echo $data['info']['pincode'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                            <li>
                                                <button type="submit" class="btn btn-lg btn-primary">Update Address</button>
                                            </li>
                                            <li>
                                                <a href="#" data-dismiss="modal" class="link link-light">Cancel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .tab-pane -->
                        </div><!-- .tab-content -->
                    </div><!-- .modal-body -->
                </form>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>


<?php if (isset($_GET['p'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Personal Information updated successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>