<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Manage Employee </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                       
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Manage Employee</h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="float_right">
                                    <a href="<?php echo COREPATH ?>employee/add" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add Employee</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-bordered card-preview">
                            <div class="card-inner">
                                <table class="datatable-init table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Role</th>
                                            <?php if($_SESSION['super_admin']==1 || $_SESSION['employee_role']==2 ) {?>
                                                <th>Assigned Branch</th>
                                            <?php } ?>
                                            <th>Mobile </th>
                                            <th>Email </th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php echo $data['list'] ?>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .card-preview -->
                    </div> <!-- nk-block -->
                    
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>



<div class="modal fade employeeChangePasswordShow" tabindex="-1" role="dialog" id="">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-lg">
                    <h5 class="title">Update New Password</h5>
                    <form method="POST" accept-charset="utf-8" id="empChangePassword">
                        <input type="hidden" value="" name="fkey" id="employee_fkey">
                        <input type="hidden" value="" name="token" id="employee_token">
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <div class="row gy-4">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label" for="password">New Password <en>*</en></label>
                                            <input type="password" class="form-control form-control-lg" id="password" name="password"  placeholder="Enter New Password">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label" for="confirm_pass">Confirm Password <en>*</en></label>
                                            <input type="password" class="form-control form-control-lg" name="confirm_pass" id="confirm_pass" placeholder="Enter Confirm Password">
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="col-12">
                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                            <li>
                                                <button type="submit" class="btn btn-lg btn-primary">Update Password</button>
                                            </li>
                                            <li>
                                                <a href="#" data-dismiss="modal" class="link link-light">Cancel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .tab-pane -->
                           
                        </div><!-- .tab-content -->
                    </form>
                </div><!-- .modal-body -->
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>

    <!-- Assign Branch Modal Start -->
        <div class="modal fade editLeadModel" tabindex="-1" role="dialog" id="assignBranchModal">
            <div class="modal-dialog modal-dialog-centered " role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                    <div class="modal-body modal-body-sm">
                        <h5 class="title">Assign Branch</h5>
                        <form  id="assignBranch" name="assignBranch" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" name="fkey" id="fkey">
                            <input type="hidden" name="token" id="token">
                            <div class="tab-content">
                                <div class="tab-pane active" id="personal">
                                    <div class="row gy-4">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-label" for="branch_id">Select Branch <en>*</en></label>
                                                <div class="form-control-wrap ">
                                                    <select class="form-control form-select select2-hidden-accessible" id="branch_id" name="branch_id[]" data-placeholder="Select a Activity Type" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true" multiple="">
                                                        <?php echo $data['branch_list'] ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 ">
                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-2 float_right">
                                                <li>
                                                    <a href="javascript:void();" class="btn btn-lg btn-danger " data-dismiss="modal">Cancel</a>
                                                </li>
                                                <li>
                                                    <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .modal-body -->
                </div><!-- .modal-content -->
            </div><!-- .modal-dialog -->
        </div>
    <!-- Assign Branch Modal End -->



<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Employee added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['e'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Employee updated successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['u'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Employee  Permissions updated successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['ab'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Branch assigned successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>