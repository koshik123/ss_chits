<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>lead">Manage Lead</a></li>
                                <li class="breadcrumb-item active">Reassign Lead to Employee</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Reassign Lead to Employee</h4>
                            </div>
                        </div>
                        <div class="form-error"></div><br>

                        <form id="reassignLead" name="reassignLead" method="POST" action="#" enctype="multipart/form-data">
                            
                            
                            <input type="hidden" value="<?php echo $_SESSION['add_reassign_lead_key'] ?>" name="fkey" id="fkey">
                            <div class="customer_search_warp">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="input-group icon icon-lg icon-color-primary">
                                            <input id="dropdown_employee" type="text" name="reassign_employee" class="form-control search" placeholder="Search employee and assign lead" required="">

                                            <input class="form-control" id="assign_employee_id" value="<?php echo $data['assign_emp']['created_to'] ?>" type="hidden">
                                            <input class="form-control" name="employee_id" id='employee_id' type="hidden">
                                            <input class="form-control" name="lead_id" value="<?php echo $data['info']['id'] ?>" type="hidden">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span class="clearfix"></span>
                            <div class="reassign_title">
                                <div class="change_btn display_none" id="reassign_warp">
                                    <a href="javascript:void();" class="btn btn-danger change_customer_reassign m10">Change Employee</a>
                                </div>
                            </div>    
                            


                                           

                            <div class="row ">
                                <div class="col-lg-5" id="">
                                    <div class=" panel panel-default ks-information ks-light assigncall_warp mt30">
                                        <div class="nk-block-head">
                                            <div class="nk-block-head-content">
                                                <h5 class="nk-block-title title">Lead Information</h5>
                                            </div>
                                        </div>
                                        <div class="card card-bordered">
                                            <ul class="data-list is-compact">
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Lead UID</div>
                                                        <div class="data-value"><a target="_blank" href="<?php echo BASEPATH ?>lead/details/<?php echo $data['info']['id'] ?>"><?php echo ucwords($data['info']['uid']) ?></a></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Lead Date</div>
                                                        <div class="data-value"><?php echo date("d/m/Y",strtotime($data['info']['lead_date'])) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Name</div>
                                                        <div class="data-value"><?php echo ucwords($data['info']['fname'].' '.$data['info']['lname']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Lead Source</div>
                                                        <div class="data-value"><?php echo ucwords($data['info']['leadsource']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Primary Mobile</div>
                                                        <div class="data-value"><?php echo ucwords($data['info']['mobile']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Primary Email</div>
                                                        <div class="data-value"><?php echo ucwords($data['info']['email']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Secondary Mobile</div>
                                                        <div class="data-value"><?php echo ($data['info']['secondary_mobile']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label"> Secondary Email</div>
                                                        <div class="data-value"><?php echo ucwords($data['info']['secondary_email']) ?></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>           
                                </div>
                                <div class="col-lg-7 display_none mt30" id="employee_info">
                                    <div class="nk-block-head">
                                        <div class="nk-block-head-content">
                                            <h5 class="nk-block-title title">Add Lead Information</h5>
                                        </div>
                                    </div>
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="row g-4">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="name">Lead Priority <en>*</en></label>
                                                        <select class="form-control search" name="priority">
                                                           <option value="low">Low</option>
                                                           <option value="medium">Medium</option>
                                                           <option value="high">High</option>
                                                           <option value="urgent">Urgent</option>
                                                        </select> 
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="company">Remarks <en>*</en></label>
                                                        <textarea rows="3" class="form-control" name="remarks" placeholder="Enter Remarks" required="" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form_submit_footer">
                                <div class="form_footer_contents">
                                    <div class="text-right m-b-0">
                                        <a href="<?php echo BASEPATH ?>Lead" class="btn btn-danger">Skip</a>
                                        <button type="submit" class="btn btn-success">Reassign</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        


                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>