<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Adhoc Reports </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Lead Reports</h4>
                                
                            </div>
                        </div>
                        <div class="row g-gs">
                            <div class="col-lg-8">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner">
                                        <form id="leadReport" method="POST" action="#">
                                            
                                            <div class="row gy-4">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fromdate">From Date <en>*</en></label>
                                                        <input type="text" class="form-control  date-picker" id="fromdate" name='from_date' placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="todate">To Date <en>*</en></label>
                                                        <input type="text" class="form-control date-picker" id="todate" name='to_date' placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                              <!-- <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Branch </label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="fva-top-branch-id" name="branch_id" data-placeholder="Select  Branch">
                                                                <option value="">Select Branch</option>
                                                                <?php echo $data['branch_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div> -->
                                            <?php if($_SESSION['super_admin']==1) { ?>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select General Manger </label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="select_gm" name="gm_id" data-placeholder="Select  General Manger" >
                                                                <option value="">Select General Manger</option>
                                                                <?php echo $data['gm_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Branch Manger </label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="select_bm" name="bm_id" data-placeholder="Select Branch Manger">
                                                                <option value="">Select Branch Manger</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Employee </label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="employee_id" name="employee_id" data-placeholder="Select a Employee">
                                                                <option value="">Select a Employee</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } elseif($_SESSION['employee_role']==2) { ?>
                                                <input type="hidden" name="gm_id" value="<?php echo $_SESSION['crm_admin_id'] ?>">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Branch Manger </label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="select_bm" name="bm_id" data-placeholder="Select Branch Manger">
                                                                <option value="">Select Branch Manger</option>
                                                                    <?php echo $data['bm_list']; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Employee </label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="employee_id" name="employee_id" data-placeholder="Select a Employee">
                                                                <option value="">Select a Employee</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } elseif($_SESSION['employee_role']==3) { ?>
                                                <input type="hidden" name="gm_id" value="<?php echo $data['user']['assigned_employee'] ?>">
                                                <input type="hidden" name="bm_id" value="<?php echo $_SESSION['crm_admin_id'] ?>">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Employee </label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="employee_id" name="employee_id" data-placeholder="Select a Employee">
                                                                <option value="">Select a Employee</option>
                                                                    <?php echo $data['emp_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <input type="hidden" name="employee_id" value="<?php echo $_SESSION['crm_admin_id'] ?>" >
                                            <?php } ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Lead Status<en></en></label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="fva-topics" name="leadstatus_id" data-placeholder="Select a Lead Status">  <option value="">Select a Lead Status</option>
                                                                <?php echo $data['lead_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group  ">
                                                        <label class="form-label" for="fva-topics">Select Chit Scheme </label>
                                                        <div class="form-control-wrap facilities">
                                                            <select class="form-control select2-hidden-accessible" id="fva-top1" name="chit_id[]" data-placeholder="Select a Chit Scheme" >
                                                                <option value="">Select a Chit Scheme</option>
                                                                <?php echo $data['chit_type'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Deposit Scheme </label>
                                                        <div class="form-control-wrap facilities1">
                                                            <select class="form-control select2-hidden-accessible" id="fva-top11" name="deposit_id[]" data-placeholder="Select a Deposit Scheme" >
                                                                <option value="">Select a Deposit Scheme</option>
                                                                <?php echo $data['deposite_type'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select VAM Scheme </label>
                                                        <div class="form-control-wrap facilities2">
                                                            <select class="form-control select2-hidden-accessible" id="fva-top111" name="vam_id[]" data-placeholder="Select a VAM Scheme" >
                                                                <option value="">Select a VAM Scheme</option>
                                                                <?php echo $data['vas_type'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                     <div class="form-group float_right">
                                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>