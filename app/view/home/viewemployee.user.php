<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>employee">Manage Employee</a></li>
                                <li class="breadcrumb-item active"><?php echo ucwords($data['info']['name']) ?> </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block">
                            <div class="card card-bordered">
                                <div class="card-aside-wrap">
                                    <div class="card-content">
                                        <!-- .nav-tabs -->
                                        <div class="card-inner">
                                            <div class="nk-block">
                                                <div class="nk-block-head">
                                                    <h5 class="title">Personal Information</h5>
                                                    <!-- <p>Basic info, like your name and address, that you use on Nio Platform.</p> -->
                                                </div><!-- .nk-block-head -->
                                                <div class="profile-ud-list">
                                                    <div class="profile-ud-item">
                                                        <div class="profile-ud wider">
                                                            <span class="profile-ud-label">Name</span>
                                                            <span class="profile-ud-value"><?php echo ucwords($data['info']['name']) ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="profile-ud-item">
                                                        <div class="profile-ud wider">
                                                            <span class="profile-ud-label">Address</span>
                                                            <span class="profile-ud-value"><?php echo ucwords($data['info']['address']) ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="profile-ud-item">
                                                        <div class="profile-ud wider">
                                                            <span class="profile-ud-label">Role</span>
                                                            <span class="profile-ud-value"><?php echo ucwords($data['emp_role']['employee_role']) ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="profile-ud-item">
                                                        <div class="profile-ud wider">
                                                            <span class="profile-ud-label">City</span>
                                                            <span class="profile-ud-value"><?php echo ucwords($data['info']['city']) ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="profile-ud-item">
                                                        <div class="profile-ud wider">
                                                            <span class="profile-ud-label">Email</span>
                                                            <span class="profile-ud-value"><?php echo ($data['info']['email']) ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="profile-ud-item">
                                                        <div class="profile-ud wider">
                                                            <span class="profile-ud-label">State</span>
                                                            <span class="profile-ud-value"><?php echo ucwords($data['info']['state']) ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="profile-ud-item">
                                                        <div class="profile-ud wider">
                                                            <span class="profile-ud-label">Mobile</span>
                                                            <span class="profile-ud-value"><?php echo ucwords($data['info']['mobile']) ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="profile-ud-item">
                                                        <div class="profile-ud wider">
                                                            <span class="profile-ud-label">Pincode</span>
                                                            <span class="profile-ud-value"><?php echo ucwords($data['info']['pincode']) ?></span>
                                                        </div>
                                                    </div>
                                                </div><!-- .profile-ud-list -->
                                            </div>
                                            <?php if($_SESSION['super_admin']==1 || $_SESSION['employee_role']==2 || $_SESSION['employee_role']==3 ) {?><div class="nk-block">
                                                <?php if ($data['info']['role']==2 || $data['info']['role']==3) { ?>
                                                <ul class="nav nav-tabs">
                                                    <?php if($data['info']['role']==2)  { ?>
                                                    <li class="nav-item">
                                                        <a class="nav-link <?php echo ($data['info']['role']==2)?  'active' : ''  ;?>" data-toggle="tab" href="#tabItem5"><em class="icon ni ni-users-fill"></em><span>Branch Managers</span></a>
                                                    </li>
                                                    <?php } ?>
                                                    <?php if ($data['info']['role']==2 || $data['info']['role']==3) { ?>
                                                    <li class="nav-item">
                                                        <a class="nav-link <?php echo ($data['info']['role']==3)?  'active' : ''  ;?>" data-toggle="tab" href="#tabItem6"><em class="icon ni ni-users"></em><span>Employees</span></a>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                                <?php } ?>
                                                <div class="tab-content">
                                                    <?php if($data['info']['role']==2)  { ?>
                                                    <div class="tab-pane <?php echo ($data['info']['role']==2)?  'active' : ''  ;?>" id="tabItem5">
                                                        <div class="nk-block nk-block-lg">
                                                            <div class="card card-bordered card-preview">
                                                                <div class="card-inner">
                                                                    <table class="datatable-init table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>#</th>
                                                                                <th>Name</th>
                                                                                <th>Role</th>
                                                                                <?php if($_SESSION['super_admin']==1 || $_SESSION['employee_role']==2 ) {?>
                                                                                    <?php if($data['info']['role']==2) { ?>
                                                                                    <th>Assigned Branch</th>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                                <th>Mobile </th>
                                                                                <th>Email </th>
                                                                                <th>Status</th>
                                                                                <th>Action</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                           <?php echo $data['bm_list'] ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?> 
                                                    <?php if ($data['info']['role']==2 || $data['info']['role']==3) { ?>
                                                    <div class="tab-pane <?php echo ($data['info']['role']==3)?  'active' : ''  ;?>" id="tabItem6">
                                                        <div class="nk-block nk-block-lg">
                                                            <div class="card card-bordered card-preview">
                                                                <div class="card-inner">
                                                                    <table class="datatable-init table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>#</th>
                                                                                <th>Name</th>
                                                                                <th>Role</th>
                                                                                <th>Mobile </th>
                                                                                <th>Email </th>
                                                                                <th>Status</th>
                                                                                <th>Action</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                           <?php echo $data['emp_list'] ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?> 
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade employeeChangePasswordShow" tabindex="-1" role="dialog" id="">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-lg">
                    <h5 class="title">Update New Password</h5>
                    <form method="POST" accept-charset="utf-8" id="empChangePassword">
                        <input type="hidden" value="" name="fkey" id="employee_fkey">
                        <input type="hidden" value="" name="token" id="employee_token">
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <div class="row gy-4">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label" for="password">New Password <en>*</en></label>
                                            <input type="password" class="form-control form-control-lg" id="password" name="password"  placeholder="Enter New Password">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label" for="confirm_pass">Confirm Password <en>*</en></label>
                                            <input type="password" class="form-control form-control-lg" name="confirm_pass" id="confirm_pass" placeholder="Enter Confirm Password">
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="col-12">
                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                            <li>
                                                <button type="submit" class="btn btn-lg btn-primary">Update Password</button>
                                            </li>
                                            <li>
                                                <a href="#" data-dismiss="modal" class="link link-light">Cancel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .tab-pane -->
                           
                        </div><!-- .tab-content -->
                    </form>
                </div><!-- .modal-body -->
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>
</div>




<?php include 'includes/bottom.html'; ?>




