<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block">
                    <div class="card card-bordered">
                        <div class="card-aside-wrap">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head nk-block-head-lg">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h4 class="nk-block-title">General Documents</h4>
                                            
                                        </div>
                                        <div class="nk-block-head-content align-self-start d-lg-none">
                                            <a href="#" class="toggle btn btn-icon btn-trigger mt-n1" data-target="userAside"><em class="icon ni ni-menu-alt-r"></em></a>
                                        </div>
                                    </div>
                                    
                                    
                                </div><!-- .nk-block-head -->
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <table class="datatable-init table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Image</th>
                                                    <th>Title </th>
                                                    <th>Deleted by</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php echo $data['list'] ?> 
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- .card-preview -->
                            </div>
                            <div class="card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg" data-content="userAside" data-toggle-screen="lg" data-toggle-overlay="true">
                                <div class="card-inner-group" data-simplebar>
                                    <div class="card-inner">
                                        <div class="user-card">
                                            <div class="user-info">
                                                <span class="lead-text" style="font-size: 25px;">Trash Management </span>
                                            </div>
                                        </div><!-- .user-card -->
                                    </div><!-- .card-inner -->
                                   
                                    <div class="card-inner p-0">
                                        <ul class="link-list-menu">
                                             <li><a class="" href="<?php echo COREPATH ?>trash/property"><em class="icon ni ni-home"></em><span>Property </span></a></li>
                                            <li><a class="" href="<?php echo COREPATH ?>trash"><em class="icon ni ni-user-fill-c"></em><span>General Documents</span></a></li>
                                            <li><a  href="<?php echo COREPATH ?>trash/checklist"><em class="icon ni ni-trash-alt"></em><span>Document Checklists</span></a></li>
                                            <li><a href="<?php echo COREPATH ?>trash/news"><em class="icon ni ni-bell-fill"></em><span>News</span></a></li>
                                            <li><a href="<?php echo COREPATH ?>trash/leads"><em class="icon ni ni-activity-round-fill"></em><span>Leads</span></a></li>
                                            <li><a href="<?php echo COREPATH ?>trash/invoice"><em class="icon ni ni-shield-star-fill"></em><span>Invoices/Receipts</span></a></li>
                                            <li><a class="active" href="<?php echo COREPATH ?>trash/gallery"><em class="icon ni ni-shield-star-fill"></em><span>Gallery</span></a></li>
                                            <li><a href="<?php echo COREPATH ?>trash/brochure"><em class="icon ni ni-shield-star-fill"></em><span>Property Brochure</span></a></li>
                                        </ul>
                                    </div><!-- .card-inner -->
                                </div><!-- .card-inner-group -->
                            </div><!-- card-aside -->
                        </div><!-- .card-aside-wrap -->
                    </div><!-- .card -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade editRestoredModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <div class="form-error"></div>
                 <form  id="updateGallery" name="updateGallery" method="POST" action="#" enctype="multipart/form-data">
                    <div class="editRestoredInfo"></div>
                </form>
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<?php include 'includes/bottom.html'; ?>





<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Restored Information successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>