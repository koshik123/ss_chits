<?php

class Customer extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managecustomer");
	        if ($check==1) {		
				$this->view('home/managecustomer', 
					[	
						'active_menu' 		=> 'customer',
						'meta_title'  		=> COLNAME.' | Manage Customer',
						'page_title'  		=> COLNAME,
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description'	=> META_DESCRIPTION,
						'list' 				=> $user->manageCustomer(),
						'scripts'			=> 'managecustomer',	
						'branch_modal_list'	=> $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list'	=> $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managecustomer");
	        if ($check==1) {
				if(!isset($_SESSION['add_customer_key'])){
					$_SESSION['add_customer_key'] = $user->generateRandomString("40");
				}
				$this->view('home/addcustomer', 
					[	
						'active_menu' 		=> 'customer',
						'meta_title'  		=>  COLNAME.' | Add Customer',
						'page_title'  		=>  COLNAME,
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'scripts'			=> 'home',	
						'branch_modal_list'	=> $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"]),
					'branch_modal_list'	=> $user->getBranchList()
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function edit($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("managecustomer");
	        if ($check==1) {
				if(!isset($_SESSION['edit_customer_key'])){
					$_SESSION['edit_customer_key'] = $user->generateRandomString("40");
				}	
				$validate = $user->check_query(CUSTOMER_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(CUSTOMER_TBL,"*"," id='$token' ");
					$this->view('home/editcustomer', 
						[	
							'active_menu' 		=> 'customer',
							'meta_title'  		=>  COLNAME.' | Edit Customer',
							'page_title'  		=>  COLNAME,
							'meta_keywords' 	=> META_KEYWORDS,
							'meta_description' 	=> META_DESCRIPTION,
							'info'				=> $info,
							'token'				=> $user->encryptData($info['id']),
						//	'property_list' => $user->getPropertyList(),
							'scripts'			=> 'home',	
							'branch_modal_list'	=> $user->getBranchList(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  		=> '404 Error - Page Not Found',
							'page_title'  		=> '404 Error - Page Not Found',
							'branch_modal_list'	=> $user->getBranchList(),
							'user'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"]),
					'branch_modal_list'	=>  $user->getBranchList()
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function details($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$pcheck = $user->pagePermission("managecustomer");
	        if ($pcheck==1) {
				if(!isset($_SESSION['edit_customer_key'])){
					$_SESSION['edit_customer_key'] = $user->generateRandomString("40");
				}	
				$validate = $user->check_query(CUSTOMER_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(CUSTOMER_TBL,"*"," id='$token' ");
					$this->view('home/viewcustomer', 
						[	
							'active_menu' 		=> 'customer',
							'meta_title'  		=>  COLNAME.' | View Customer',
							'page_title'  		=>  COLNAME,
							'meta_keywords' 	=> META_KEYWORDS,
							'meta_description' 	=> META_DESCRIPTION,
							'info'				=> $info,
							'scripts'			=> 'home',	
							'branch_modal_list'	=> $user->getBranchList(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"]),
					'branch_modal_list'	=> $user->getBranchList(),
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	
	
	public function errors()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 		=> 'customer',
					'meta_title'  		=> '404 Error - Page Not Found',
					'scripts'			=> 'error',
					'page_title'  		=>  COLNAME,
					'meta_keywords' 	=> META_KEYWORDS,
					'meta_description' 	=> META_DESCRIPTION,
					'branch_modal_list'	=> $user->getBranchList(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
}


?>