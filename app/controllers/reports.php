<?php

class Reports extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("adhocreport");
	        if ($check==1) {
				$this->view('home/customerreport', 
					[	
						'active_menu' 		=> 'customerreport',
						'meta_title'  		=>  COLNAME.' | Customer Reports',
						'page_title'  		=>  COLNAME,
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description'	=> META_DESCRIPTION,
						'scripts'			=> 'home',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
	
	public function lead()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("leadreport");
	        if ($check==1) {
	        	
				$this->view('home/leadreport', 
					[	
						'active_menu' 		=> 'leadreport',
						'meta_title'  		=>  COLNAME.' | Lead Reports',
						'page_title'  		=>  COLNAME,
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'lead_list' 		=> $user->getLeadStatus(),
						'employee_list' 	=> $user->getEmployeeList(),
						'chit_type'  		=> $user->getChitTypeList(),
						'deposite_type'		=> $user->getDepositeTypeList(),
						'vas_type'  		=> $user->getVASTypeList(),
						'branch_list'  		=> $user->getBranchListMultiDropDown(),
						'gm_list'  			=> $user->getGMListDropDown(),
						'bm_list'  			=> $user->getBMListDropDown(),
						'emp_list'  		=> $user->getEMPListDropDown(),
						'scripts'			=> 'leadreports',		
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title' 	 	=> '404 Error - Page Not Found',
					'page_title' 	 	=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);		
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function leadreport($from="",$to="")
	{		
		if(isset($_SESSION["crm_admin_id"])){		
			$user = $this->model('User'); 
			$check = $user->pagePermission("leadreport");
	        if ($check==1) {
				$employee_id = $_GET['employee_id'];
				$leadstatus  = $_GET['leadstatus'];
				$chit_id 	 = $_GET['chit_id'];
				$deposit_id  = $_GET['deposit_id'];
				$vam_id 	 = $_GET['vam_id'];
				$gm_id 	 	 = $_GET['general_manager_id'];
				$bm_id 	 	 = $_GET['branch_manager_id'];

				$gminfo 	 	 = $user->getDetails(EMPLOYEE,"*"," id='$gm_id'  ");	
				$bminfo 	 	 = $user->getDetails(EMPLOYEE,"*"," id='$bm_id'  ");	
				$employeeinfo 	 = $user->getDetails(EMPLOYEE,"*"," id='$employee_id'  ");	
				$chit_name 		 = $user->getMultiChitName($chit_id);
				$deposite_name   = $user->getMultiDepositeName($deposit_id);
				$vam_name        = $user->getMultiVamName($vam_id);
	        	$this->view('home/leadreportinfo', 
	                [
	                    'active_menu' 		=> 'leadreport',
						'meta_title'		=> 'Lead Reports - '.COMPANY_NAME,
						'page_title'  		=> 'Lead Reports',
						'scripts'			=> 'leadreports',
						'empname' 			=>  (($employeeinfo) ? $employeeinfo['name'] : ""), 
						'gmname' 			=>  (($gminfo) ? $gminfo['name'] : ""), 
						'bmname' 			=>  (($bminfo) ? $bminfo['name'] : ""), 
						'chit_status'		=>  (($chit_id!="") ? $chit_name : ""),
						'deposite_status'	=>  (($deposit_id!="") ? $deposite_name : ""),
						'vam_status'		=>  (($vam_id!="") ? $vam_name : ""),
						'employee_id' 		=>  $employee_id,
						'gm_id' 		    =>  $gm_id,
						'bm_id' 		    =>  $bm_id,
						'chit_id' 			=>  $chit_id,
						'deposit_id' 		=>  $deposit_id,
						'vam_id' 			=>  $vam_id,
						'status' 			=>  $leadstatus,
					//	'list'          	=>  $user->manageCustomerReport($from,$to),
						'list'				=> $user->leadReport($from,$to,$gm_id,$bm_id,$employee_id,$leadstatus,$chit_id,$deposit_id,$vam_id),
						'lead_list' 		=> $user->getLeadStatus(),
						'employee_list' 	=> $user->getEmployeeList(),
						'chit_type'  		=> $user->getChitTypeList(),
						'deposite_type'		=> $user->getDepositeTypeList(),
						'vas_type'  		=> $user->getVASTypeList(),
						'gm_list'  			=> $user->getGMListDropDown(),
						'bm_list'  			=> $user->getBMListDropDown(),
						'emp_list'  		=> $user->getEMPListDropDown(),
						'from'				=> (($from!="") ? $from : ""),
						'to'				=> (($to!="") ? $to : ""),
						'branch_modal_list' =>  $user->getBranchList(),
						'user'   			=>  $user->userInfo($_SESSION["crm_admin_id"])   
	                ]);
	        }else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
        }else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}		
	    
	}

	public function customer()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("customerreport");
	        if ($check==1) {
				$this->view('home/customerreport', 
					[	
						'active_menu' 		=> 'customerreport',
						'meta_title'  		=>  COLNAME.' | Customer Reports',
						'page_title'  		=>  COLNAME,
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'customer_list' 	=> $user->getCustomerList(),
						'branch_list'    	=> $user->getBranchListMultiDropDown(),
						'chit_type'  		=> $user->getChitTypeList(),
						'deposite_type'		=> $user->getDepositeTypeList(),
						'vas_type'  		=> $user->getVASTypeList(),
						'scripts'			=> 'leadreports',		
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function customerreport($from="",$to="")
	{		
		if(isset($_SESSION["crm_admin_id"])){		
			$user = $this->model('User'); 
			$check = $user->pagePermission("customerreport");
	        if ($check==1) {
				$customer_id = $_GET['customer_id'];
				$chit_id 	 = $_GET['chit_id'];
				$deposit_id  = $_GET['deposit_id'];
				$vam_id 	 = $_GET['vam_id'];

				$customer_name 	 = $user->getMultiCustomerName($customer_id);
				$chit_name 		 = $user->getMultiChitName($chit_id);
				$deposite_name   = $user->getMultiDepositeName($deposit_id);
				$vam_name        = $user->getMultiVamName($vam_id);
	        	$this->view('home/customerreportinfo', 
	                [
	                    'active_menu' 		=> 'customerreportinfo',
						'meta_title'		=> 'Customer Reports - '.COMPANY_NAME,
						'page_title'  		=> 'Customer Reports',
						'scripts'			=> 'leadreports',

						'customer_status'	=>  (($customer_id!="") ? $customer_name : ""),
						'chit_status'	 	=>  (($chit_id!="") ? $chit_name : ""),
						'deposite_status'	=>  (($deposit_id!="") ? $deposite_name : ""),
						'vam_status'	 	=>  (($vam_id!="") ? $vam_name : ""),

						'customer_id' 		=>  $customer_id,
						'chit_id' 			=>  $chit_id,
						'deposit_id' 		=>  $deposit_id,
						'vam_id' 			=>  $vam_id,
					//	'list'          	=>  $user->manageCustomerReport($from,$to),
						'list'				=> $user->customerReport($from,$to,$customer_id,$chit_id,$deposit_id,$vam_id),
						'customer_list' 	=> $user->getCustomerList(),
						'chit_type'  		=> $user->getChitTypeList(),
						'deposite_type'		=> $user->getDepositeTypeList(),
						'vas_type'  		=> $user->getVASTypeList(),
						'from'				=> (($from!="") ? $from : ""),
						'to'				=> (($to!="") ? $to : ""),
						'branch_modal_list' =>  $user->getBranchList(),
						'user'   			=>  $user->userInfo($_SESSION["crm_admin_id"])   
	                ]);
	        }else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
        }else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}		
	    
	}
	
	public function errors()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
				$this->view('home/error', 
					[	
						'active_menu' 		=> 'report',
						'meta_title'  		=> '404 Error - Page Not Found',
						'scripts'			=> 'error',
						'page_title'  		=>  COLNAME,
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
}


?>