<?php

/**
* 
*/
class Errors extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error',
				[
					'meta_title'		=>'404 Error - Page Not Found',
					'scripts'			=> 'error',
					'page_title'  		=>  COLNAME,
					'meta_keywords' 	=> META_KEYWORDS,
					'meta_description' 	=> META_DESCRIPTION,
					'branch_modal_list' => $user->getBranchList(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}

	}

	public function errors()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error',
				[
					'meta_title'		=>'404 Error - Page Not Found',
					'scripts'			=> 'error',
					'page_title'  		=>  COLNAME,
					'meta_keywords' 	=> META_KEYWORDS,
					'meta_description' 	=> META_DESCRIPTION,
					'branch_modal_list' => $user->getBranchList(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
	


}


?>