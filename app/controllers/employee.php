<?php

class Employee extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manageemployee");
	        if ($check==1) {
				$this->view('home/manageemployee', 
					[	
						'active_menu' 		=> 'employee',
						'meta_title'  		=>  COLNAME.' | Manage Employee',
						'page_title'  		=>  COLNAME,
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'list' 				=> $user->manageEmployee(),
						'branch_list'		=> $user->getBranchListDropDown(),
						'scripts'			=> 'home',	
						'branch_modal_list'	=> $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list'	=> $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manageemployee");
	        if ($check==1) {
				if(!isset($_SESSION['add_employee_key'])){
					$_SESSION['add_employee_key'] = $user->generateRandomString("40");
				}	
				$this->view('home/addemployee', 
					[	
						'active_menu' 		=> 'addemployee',
						'meta_title'  		=> COLNAME.' | Add Employee',
						'page_title'  		=> COLNAME,
						'meta_keywords'	 	=> META_KEYWORDS,
						'meta_description'  => META_DESCRIPTION,
						'emp_role_list' 	=> $user->getEmployeeRole(),
						'branch_list'		=> $user->getBranchListDropDown(),
						'bm_list'			=> $user->getBranchManagerDropDown(),
						'scripts'			=> 'home',	
						'branch_modal_list'	=> $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list'	=> $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function edit($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manageemployee");
	        if ($check==1) {
				if(!isset($_SESSION['edit_employee_key'])){
						$_SESSION['edit_employee_key'] = $user->generateRandomString("40");
				}
				$validate = $user->check_query(EMPLOYEE,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(EMPLOYEE,"*"," id='$token' ");
					$this->view('home/editemployee', 
						[	
							'active_menu' 		=> 'addemployee',
							'meta_title'  		=> COLNAME.' | Edit Employee',
							'page_title'  		=> 'Edit Employee',
							'meta_keywords' 	=> META_KEYWORDS,
							'meta_description'  => META_DESCRIPTION,
							'emp_role_list' 	=> $user->getEmployeeRole($info['role']),
							'token'				=> $user->encryptData($info['id']),
							'branch_list'		=> $user->getBranchListDropDown($info['assigned_branch']),
							'bm_list'			=> $user->getBranchManagerDropDown($info['assigned_employee']),
							'gm_list'			=> $user->getGeneralManagerDropDown($info['assigned_branch'],$info['assigned_employee']),
							'info'				=> $info,
							'scripts'			=> 'employee',
							'branch_modal_list'	=> $user->getBranchList(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  		=> '404 Error - Page Not Found',
							'page_title'  		=> '404 Error - Page Not Found',
							'branch_modal_list'	=> $user->getBranchList(),
							'user'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list'	=> $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}						
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}

	public function details($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manageemployee");
	        if ($check==1) {
				$validate = $user->check_query(EMPLOYEE,"id"," id='$token' ");
				if($validate==1){
					$info  	   = $user->getDetails(EMPLOYEE,"*"," id='$token' ");
					$emp_role  = $user->getDetails(EMPLOYEE_ROLE,"*"," id='".$info['role']."' ");

					$this->view('home/viewemployee', 
						[	
							'active_menu' 		=> 'addemployee',
							'meta_title'  		=>  COLNAME.' | View Employee',
							'page_title'  		=>  'View Employee',
							'emp_role'			=>  $emp_role,
							'meta_keywords' 	=>  META_KEYWORDS,
							'meta_description'  =>  META_DESCRIPTION,
							'token'				=>	$user->encryptData($info['id']),
							'bm_list' 			=> 	$user->manageAssignedBranchManager($info['id']),
							'emp_list' 			=> 	$user->manageAssignedEmployee($info['id']),
							'info'				=>  $info,
							'scripts'			=>  'employee',
							'branch_modal_list'	=> 	$user->getBranchList(),
							'user' 	 			=>  $user->userInfo($_SESSION["crm_admin_id"]),
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  		=> '404 Error - Page Not Found',
							'page_title'  		=> '404 Error - Page Not Found',
							'branch_modal_list'	=> $user->getBranchList(),
							'user'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list'	=> $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
						
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}

	public function permission($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manageemployee");
	        if ($check==1) {
				if(!isset($_SESSION['create_permission_key'])){
						$_SESSION['create_permission_key'] = $user->generateRandomString("40");
				}
				$validate = $user->check_query(EMPLOYEE,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(EMPLOYEE,"*"," id='$token' ");
					//$role  = $user->getDetails(EMPLOYEE_TYPE,"employee_type"," id='".$info['em_type']."' ");
					$emp_role  = $user->getDetails(EMPLOYEE_ROLE,"*"," id='".$info['role']."' ");
					$employee_permission = $user->getDetails(PERMISSION,"*"," employee_id = '".$info['id']."'  ");
					$this->view('home/employeepermission', 
						[	
							'active_menu' 		=>  'employee',
							'meta_title'  		=>  'Manage Permission',
							'meta_keywords'  	=>  'Manage Permission',
							'meta_description'  =>  'Manage Permission',
							'page_title'  		=>  'Manage Permission',
							'emp_role'			=>  $emp_role,
							'token'				=>	$user->encryptData($info['id']),
							'info'				=>  $info,
							'emp_info'			=>  $employee_permission,
						//	'role' 				=>  $role['employee_type'],
							'scripts'			=>  'employee',
							'branch_modal_list'	=> $user->getBranchList(),
							'user' 	 			=>  $user->userInfo($_SESSION["crm_admin_id"]),
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  		=> '404 Error - Page Not Found',
							'page_title'  		=> '404 Error - Page Not Found',
							'branch_modal_list'	=> $user->getBranchList(),
							'user'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list'	=> $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
						
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}
	
	public function errors()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 		=> 'home',
					'meta_title'  		=> '404 Error - Page Not Found',
					'scripts'			=> 'error',
					'page_title'  		=>  COLNAME,
					'meta_keywords' 	=> META_KEYWORDS,
					'meta_description'	=> META_DESCRIPTION,
					'branch_modal_list' => $user->getBranchList(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
}


?>