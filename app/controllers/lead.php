<?php

class  Lead  extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managelead");
	        if ($check==1) {
				$this->view('home/managelead ', 
					[	
						'active_menu' 		=> 'lead',
						'meta_title'  		=> COLNAME.' | Manage Lead',
						'page_title'  		=> COLNAME,
						'meta_keywords'		=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'list' 				=> $user->manageLead(),
						'activity_types'	=> $user->getActivityType(),
						'emp_list' 			=> $user->getEmployeeList(),
						'source_list'  		=> $user->getLeadSource(),
						'leadstatus'		=> $user->getLeadStatus(),
						'chit_type'  		=> $user->getChitTypeList(),
						'deposite_type'		=> $user->getDepositeTypeList(),
						'vas_type'  		=> $user->getVASTypeList(),
						'scripts'			=> 'managelead',	
						'branch_modal_list' => $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function filter()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managelead");
	        if ($check==1) {
	        	$leadsource 	= @$_GET['leadsource'];
				$movecustomer 	= @$_GET['movecustomer'];
				$assignedstatus = @$_GET['assignedstatus'];
				$assignedto 	= @$_GET['assignedto'];
				$leadstatus 	= @$_GET['leadstatus'];
				$chitstatus 	= @$_GET['chitstatus'];
				$depositestatus = @$_GET['depositestatus'];
				$vamstatus 		= @$_GET['vamstatus'];

				$leadstatus_name = $user->getLeadStatusName($leadstatus);
				$assignedto_name = $user->getEmployeesName($assignedto);
				$chit_name 		 = $user->getChitName($chitstatus);
				$deposite_name   = $user->getDepositeName($depositestatus);
				$vam_name        = $user->getVamName($vamstatus);


				$this->view('home/manageleadfilter ', 
					[	
						'active_menu' 		=> 'lead',
						'meta_title'  		=>  COLNAME.' | Manage Lead',
						'page_title'  		=>  COLNAME,
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description'	=> META_DESCRIPTION,

						'leadsource'		=> (($leadsource!="") ? $leadsource : ""),
						'movecustomer'		=> (($movecustomer!="") ? $movecustomer : ""),
						'assignedstatus'	=> (($assignedstatus!="") ? $assignedstatus : ""),
						'assignedto'		=> (($assignedto!="") ? $assignedto_name : ""),
						'lead_status'		=> (($leadstatus!="") ? $leadstatus_name : ""),
						'chit_status'		=> (($chitstatus!="") ? $chit_name : ""),
						'deposite_status'	=> (($depositestatus!="") ? $deposite_name : ""),
						'vam_status'		=> (($vamstatus!="") ? $vam_name : ""),

						'list' 				=> $user->manageFilterLead($leadsource,$movecustomer,$assignedstatus,$assignedto,$leadstatus,$chitstatus,$depositestatus,$vamstatus),
						'activity_types' 	=> $user->getActivityType(),
						'emp_list' 			=> $user->getEmployeeList(),

						'source_list'  		=> $user->getLeadSource(),
						'leadstatus'		=> $user->getLeadStatus(),
						'chit_type'  		=> $user->getChitTypeList(),
						'deposite_type'		=> $user->getDepositeTypeList(),
						'vas_type'  		=> $user->getVASTypeList(),
						'scripts'			=> 'managelead',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managelead");
	        if ($check==1) {
				if(!isset($_SESSION['add_lead_key'])){
					$_SESSION['add_lead_key'] = $user->generateRandomString("40");
				}
				$this->view('home/addlead', 
					[	
						'active_menu' 		=> 'lead',
						'meta_title'  		=>  COLNAME.' | Add Lead',
						'page_title'  		=>  COLNAME,
						'meta_keywords'		=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'source_list'  		=> $user->getLeadSource(),

						'chit_type'  		=> $user->getChitTypeList(),
						'deposite_type'		=> $user->getDepositeTypeList(),
						'vas_type'  		=> $user->getVASTypeList(),

						'customerprofile_list' => $user->getCustomerProfileList(),
						'scripts'			=> 'home',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function edit($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("managelead");
	        if ($check==1) {
				if(!isset($_SESSION['edit_lead_key'])){
					$_SESSION['edit_lead_key'] = $user->generateRandomString("40");
				}	
				$validate = $user->check_query(LEAD_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(LEAD_TBL,"*"," id='$token' ");
					$this->view('home/editlead', 
						[	
							'active_menu' 		=> 'lead',
							'meta_title'  		=>  COLNAME.' | Edit Lead',
							'page_title'  		=>  COLNAME,
							'meta_keywords' 	=> META_KEYWORDS,
							'meta_description' 	=> META_DESCRIPTION,
							'info'				=> $info,
							'token'				=> $user->encryptData($info['id']),
							'source_list'  		=> $user->getLeadSource($info['leadsource']),

							'chit_type'  		=> $user->getChitTypeList($info['chit_id']),
							'deposite_type'		=> $user->getDepositeTypeList($info['deposite_id']),
							'vas_type'  		=> $user->getVASTypeList($info['vas_id']),

							'customerprofile_list' => $user->getCustomerProfileList($info['customer_profile_id']),
							'scripts'			=> 'home',	
							'branch_modal_list' =>  $user->getBranchList(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  		=> '404 Error - Page Not Found',
							'page_title'  		=> '404 Error - Page Not Found',
							'branch_modal_list' =>  $user->getBranchList(),
							'user'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function assignlead($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->check_query(LEAD_TBL,"id"," id='$token' ");
			if ($check==1) {
				$info  = $user->getDetails(LEAD_TBL,"*"," id='$token' ");
				//$assign_employee    = $user->getAssignedEmployeeInfo($token);
 				if(!isset($_SESSION['add_assign_lead_key'])){
					$_SESSION['add_assign_lead_key'] = $user->generateRandomString("40");
				}
				$this->view('home/assignlead',
					[	
						'active_menu' 		=> 'lead',
						'meta_title'  		=> 'Assigned Lead',
						'page_title'  		=> 'Assigned Lead',
						'meta_keywords'  	=> 'Assigned Lead',
						'meta_description'  => 'Assigned Lead',
						'scripts'			=> 'assigncalls',
					//	'employee_list'		=>  $user->getEmployeeList(),
					//	'employee_list'		=>  $user->getEmployeeList($assign_employee['created_to']),
						'info'				=>  $info,
					//	'call_info'			=>  $call_info,
					//	'stats'				=>  $user->dashboardStats(),
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
					]);
			}else{
				$this->view('home/error', 
				[	
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
				]);	
			}			
		}else{
		$this->view('home/login',
			array(
				'meta_title'=> 'User Login - '.COMPANY_NAME
			));
		}
	}

	public function reassign($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->check_query(LEAD_TBL,"id"," id='$token' ");
			if ($check==1) {
				$info  = $user->getDetails(LEAD_TBL,"*"," id='$token' ");
				//$call_info = $user->getCallInfo($token);
				$assign_employee    = $user->getAssignedEmployeeInfo($token);				
 				if(!isset($_SESSION['add_reassign_lead_key'])){
					$_SESSION['add_reassign_lead_key'] = $user->generateRandomString("40");
				}
				$this->view('home/reassignlead',
					[	
						'active_menu' 		=> 'lead',
						'meta_title'  		=> 'Reassign Lead',
						'meta_keywords'  	=> 'Reassign Lead',
						'meta_description'  => 'Reassign Lead',
						'page_title'  		=> 'Reassign Lead',
						'scripts'			=> 'assigncalls',
					//	'employee_list'		=>  $user->getEmployeeList($assign_employee['created_to']),
						'info'				=>  $info,
					//	'call_info'			=>  $call_info,
						'assign_emp'		=>  $assign_employee,
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
					]);
			}else{
				$this->view('home/error', 
				[	
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
				]);
			}			
		}else{
		$this->view('home/login',
			array(
				'meta_title'=> 'User Login - '.COMPANY_NAME
			));
		}
	}

	public function details($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$pcheck = $user->pagePermission("managelead");
	        if ($pcheck==1) {

	        	if($_SESSION['super_admin']==1 || $_SESSION['employee_role'] == 2 || $_SESSION['employee_role'] == 3) {
	        		$check_employee = "";
	        	} else {
	        		$check_employee = "AND employee_id='".$_SESSION['crm_admin_id']."'";
	        	}
				$validate = $user->check_query(LEAD_TBL,"id"," id='$token' $check_employee ");
				if($validate==1){
					if(!isset($_SESSION['update_leadstatus_key'])){
					$_SESSION['update_leadstatus_key'] = $user->generateRandomString("40");
				}	
					$info  = $user->getDetails(LEAD_TBL,"*"," id='$token' ");
					//$projectinfo  = $user->getDetails(PROJECT_TYPE,"*"," id='".$info['project_id']."' ");
					$leadstatusinfo  = $user->getDetails(LEAD_TYPE,"*"," id='".$info['lead_status_id']."' ");
					$leadSourceinfo  = $user->getDetails(LEAD_SOURCE,"*"," id='".$info['leadsource']."' ");

					$chitinfo  		= $user->getDetails(CHIT_MASTER,"*"," id='".$info['chit_id']."' ");
					$depositeinfo   = $user->getDetails(DEPOSITE_MASTER,"*"," id='".$info['deposite_id']."' ");
					$vaminfo  		= $user->getDetails(VAS_MASTER,"*"," id='".$info['vas_id']."' ");

					$assign_btn = (($info['assign_status']=="0")? "
                	
                		<a href='#' class='btn btn-info assignLeadModel' data-value='".$info['id']."' data-type='".$info['id']."' data-option='".$info['id']."'><em class='icon ni ni-check'></em> Assign</a>" : "<a href='#' class='btn btn-blue reassignLeadModel' data-value='".$info['id']."' data-type='".$info['id']."' data-option='".$info['id']."'><em class='icon ni ni-check'></em> Reassign</a>" );

                	$assign_info = $user->leadAssignTo($info['id']);

                	if($assign_info!="") {
                		if( $_SESSION["manageemployee"]==1 || $_SESSION['super_admin']==1 || $_SESSION['employee_role'] == 2 || $_SESSION['employee_role'] == 3) { 
                			$assigned_to = "<a href='".COREPATH."employee/details/".$assign_info['created_to']."' target='_blank' >".ucwords($assign_info['name'])."</a>";
                		} else {
                			$assigned_to = ucwords($assign_info['name']);
                		}
                	} else {
                		$assigned_to = "<span class='badge badge-warning'>Not Assigned</span>" ;
                	}

					$this->view('home/viewlead', 
						[	
							'active_menu' 		=> 'lead',
							'meta_title'  		=>  COLNAME.' | View Lead',
							'page_title'  		=>  'View Lead',
							'meta_keywords' 	=> META_KEYWORDS,
							'meta_description' 	=> META_DESCRIPTION,
							'leadstatus'		=> $user->getLeadStatus($info['lead_status_id']),	
							'activity_types'	=> $user->getActivityType(),
							'token'				=> $user->encryptData($info['id']),
							'info'				=> $info,
							'emp_list' 			=> $user->getEmployeeList(),
							'chitinfo'			=> $chitinfo,
							'depositeinfo'		=> $depositeinfo,
							'vaminfo'			=> $vaminfo,
						//	'projectinfo' 		=> $projectinfo,
							'assign_btn'   		=> $assign_btn,
							'assigned_to'  		=> $assigned_to,
							'leadstatusinfo'	=> $leadstatusinfo,
							'leadSourceinfo'	=> $leadSourceinfo,
						//	'activity_logs'		=> $user->manageActivityLogs($token),
							'newactivity_logs'	=> $user->manageNewActivityLogs($token),
							'scripts'			=> 'home',	
							'branch_modal_list' =>  $user->getBranchList(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  		=> '404 Error - Page Not Found',
							'page_title'  		=> '404 Error - Page Not Found',
							'branch_modal_list' =>  $user->getBranchList(),
							'user'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function addactivity($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managelead");
	        if ($check==1) {
    			$checkl = $user->check_query(LEAD_TBL,"id"," id='$token' ");
				if ($checkl==1) {
					$info  = $user->getDetails(LEAD_TBL,"*"," id='$token' ");
					if(!isset($_SESSION['add_activity_key'])){
						$_SESSION['add_activity_key'] = $user->generateRandomString("40");
					}
					$this->view('home/addactivitylead', 
						[	
							'active_menu' 		=> 'lead',
							'meta_title'  		=>  COLNAME.' | Add Lead',
							'page_title'  		=>  COLNAME,
							'meta_keywords' 	=> META_KEYWORDS,
							'meta_description'	=> META_DESCRIPTION,
							'info' 			 	=> $info,
							'activity_types'	=> $user->getActivityType(),
							'scripts'			=> 'home',	
							'branch_modal_list' =>  $user->getBranchList(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),			
						]);	
				}else{
					$this->view('home/error', 
					[	
						'active_menu' 		=> 'property',
						'meta_title'  		=> '404 Error - Page Not Found',
						'page_title'  		=> '404 Error - Page Not Found',
						'branch_modal_list' =>  $user->getBranchList(),
						'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function activetype()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("activitytype");
		    if ($check==1) {
				if(!isset($_SESSION['activity_type_key'])){
					$_SESSION['activity_type_key'] = $user->generateRandomString("40");
				}	
				$this->view('home/manageactivitytype', 
					[	
						'active_menu' 		=> 'activitytype',
						'meta_title'  		=>  COLNAME.' | Activity Type',
						'page_title'  		=>  'Activity Type',
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'list' 				=> $user->manageActivityType(),
						'scripts'			=> 'home',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 		=> 'property',
						'meta_title'  		=> '404 Error - Page Not Found',
						'page_title'  		=> '404 Error - Page Not Found',
						'branch_modal_list' =>  $user->getBranchList(),
						'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function leadtype()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("leadtypes");
		    if($check==1){
				if(!isset($_SESSION['lead_type_key'])){
					$_SESSION['lead_type_key'] = $user->generateRandomString("40");
				}	
				$defaultcheck = $user->check_query(LEAD_TYPE,"id"," default_settings='1' ");
				$this->view('home/manageleadtype', 
					[	
						'active_menu' 		=> 'leadtype',
						'meta_title'  		=>  COLNAME.' | Manage Lead Type',
						'page_title'  		=>  'Manage Lead Type',
						'meta_keywords'	 	=> META_KEYWORDS,
						'defaultcheck'  	=> $defaultcheck,
						'meta_description'	=> META_DESCRIPTION,
						'list' 				=> $user->manageLeadType(),
						'scripts'			=> 'home',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 		=> 'property',
						'meta_title'  		=> '404 Error - Page Not Found',
						'page_title'  		=> '404 Error - Page Not Found',
						'branch_modal_list' =>  $user->getBranchList(),
						'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);		
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function leadsource()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("leadsource");
		    if ($check==1) {
				if(!isset($_SESSION['lead_source_key'])){
					$_SESSION['lead_source_key'] = $user->generateRandomString("40");
				}	
				$this->view('home/manageleadsource', 
					[	
						'active_menu' 		=> 'leadsource',
						'meta_title'  		=>  COLNAME.' | Manage Lead Source',
						'page_title'  		=>  'Manage Lead Source',
						'meta_keywords'		=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'list' 				=> $user->manageLeadSource(),
						'scripts'			=> 'home',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 		=> 'property',
						'meta_title'  		=> '404 Error - Page Not Found',
						'page_title'  		=> '404 Error - Page Not Found',
						'branch_modal_list' =>  $user->getBranchList(),
						'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);		
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function customerprofile()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("customerprofile");
		    if ($check==1) {
				if(!isset($_SESSION['customer_profile_key'])){
					$_SESSION['customer_profile_key'] = $user->generateRandomString("40");
				}	
				$this->view('home/managecustomerprofile', 
					[	
						'active_menu' 		=> 'customerprofile',
						'meta_title'  		=>  COLNAME.' | Manage Customer Profile',
						'page_title'  		=>  'Manage Customer Profile',
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description'	=> META_DESCRIPTION,
						'list' 				=> $user->manageCustomerProfile(),
						'scripts'			=> 'home',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 		=> 'property',
						'meta_title'  		=> '404 Error - Page Not Found',
						'page_title'  		=> '404 Error - Page Not Found',
						'branch_modal_list' =>  $user->getBranchList(),
						'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function images($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$pcheck = $user->pagePermission("managelead");
	        if ($pcheck==1) {
				if(!isset($_SESSION['add_leadimg_key'])){
					$_SESSION['add_leadimg_key'] = $user->generateRandomString("40");
				}
				$check = $user->check_query(LEAD_TBL,"id"," id='$token' ");
		        if($check){
					 $info = $user->getDetails(LEAD_TBL,"*"," id='$token'  ");
					$this->view('home/leadgalleryimages', 
						[	
							'active_menu' 	  	=> 'lead',
							'meta_title'	  	=> 'Add Images - '.COMPANY_NAME,
							'page_title'  	  	=> 'Add Images',
							'redirect'		  	=> '',
							'images'			=>	$user->leadGalleryImages($info['id']),
							'token'			  	=>  $user->encryptData($info['id']),
							'info'			  	=>  $info,
							'scripts'		  	=> 'addimages',
							'branch_modal_list' =>  $user->getBranchList(),
							'user'   		  	=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title' 	 	=> '404 Error - Page Not Found',
							'page_title'  		=> '404 Error - Page Not Found',
							'active_menu'	 	=> 'gallery',
							'branch_modal_list' =>  $user->getBranchList(),
							'user'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);		
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list'	=>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}	
		}else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}	

	public function errors()
	{	
		if(isset($_SESSION["crm_admin_id"])){
		$user = $this->model('User');
		$this->view('home/error', 
			[	
				'active_menu' 		=> 'home',
				'meta_title'  		=> '404 Error - Page Not Found',
				'scripts'			=> 'error',
				'page_title'  		=>  COLNAME,
				'meta_keywords' 	=> META_KEYWORDS,
				'meta_description' 	=> META_DESCRIPTION,
				'branch_modal_list'	=>  $user->getBranchList(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
}


?>