<?php

class Home extends Controller
{
	
	public function index()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/index', 
				[	
					'active_menu' 	   => 'home',
					'meta_title'  	   =>  COLNAME.' | Home',
					'page_title'  	   =>  COLNAME,
					'meta_keywords'    =>  META_KEYWORDS,
					'meta_description' =>  META_DESCRIPTION,
					'stats'  		   =>  $user->dashboardStats(),
					'customer' 		   =>  $user->dashboardCustomer(),
					'emp_lead_list'	   =>  $user->getEmployeeLeadList(),
					'scripts'		   =>  'home',	
					'branch_modal_list'=>  $user->getBranchList(),
					'user'   		   =>  $user->userInfo($_SESSION["crm_admin_id"])
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function forgotpassword()
	{
		$user = $this->model('User');
		$this->view('home/forgotpassword', 
			[	
				'active_menu' 	=> 'home',
				'meta_title'  	=>  COLNAME.' | Home',
				'page_title'  	=>  COLNAME,
				'meta_keywords' => META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'scripts'		=> 'home',			
			]);
	}
	
	
	public function errors()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'branch_modal_list'=>  $user->getBranchList(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function logout()
	{
		if(isset($_SESSION['crm_admin_id'])){
			$user = $this->model('User');
			if($user->sessionOut($user_type="User",$_SESSION['crm_admin_id'])){
				unset($_SESSION['crm_admin_id']);
				session_destroy();
			}	
		}
		header("Location:".COREPATH);
	}	


}


?>