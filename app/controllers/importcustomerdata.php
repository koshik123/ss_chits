<?php

class Importcustomerdata extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("importcustomer");
	        if ($check==1) {
				$this->view('home/importcustomerdata', 
					[	
						'active_menu' 		=> 'importcustomer',
						'meta_title'  		=>  COLNAME.' | Import Data',
						'page_title'  		=>  'Import Data',
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description'	=> META_DESCRIPTION,
						'layout'			=>	$user->getCustomerDataSheetList(),
						'scripts'			=> 'home',	
						'branch_modal_list'	=>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list'	=>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function approve($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("importcustomer");
	        if ($check==1) {	
	        	$count = $user -> check_query(TEMPCUSTOMEREXCEL_TBL,"id"," upload_token	='$token' ");
				if($count >0){
					$this->view('home/approvecustomerimport', 
						[	
							'active_menu' 		=> 'alumni_manage',
							'meta_title'  		=> 'Approve Imported Data - '.COLNAME,
							'page_title'  		=> 'Approve Imported Data',
							'scripts'			=> 'importdata',
							'layout'			=>	$user->dataCustomerImported($token),
							'count'				=>	$count,
							'token'				=>  $token,
							'branch_modal_list'	=>  $user->getBranchList(),
							'user'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  		=> '404 Error - Page Not Found',
							'page_title'  		=> '404 Error - Page Not Found',
							'branch_modal_list'	=>  $user->getBranchList(),
							'user'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
				}

			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list'	=>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function errors()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 		=> 'home',
					'meta_title'  		=> '404 Error - Page Not Found',
					'scripts'			=> 'error',
					'page_title'  		=>  COLNAME,
					'meta_keywords'		=> META_KEYWORDS,
					'meta_description' 	=> META_DESCRIPTION,
					'branch_modal_list'	=>  $user->getBranchList(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
	
}


?>