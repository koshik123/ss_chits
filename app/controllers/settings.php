<?php

class  Settings  extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("requesttype");
	        if ($check==1) {
				if(!isset($_SESSION['add_request_type_key'])){
					$_SESSION['add_request_type_key'] = $user->generateRandomString("40");
				}	
				$this->view('home/managerequesttypemaster ', 
					[	
						'active_menu' 		=> 'requesttype',
						'meta_title'  		=>  COLNAME.' | Manage Request type Master',
						'page_title'  		=>  COLNAME,
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'list' 				=> $user->manageRequestType(),
						'scripts'			=> 'home',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 		=> 'property',
					'meta_title'  		=> '404 Error - Page Not Found',
					'page_title'  		=> '404 Error - Page Not Found',
					'branch_modal_list' =>  $user->getBranchList(),
					'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function chit()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("chitmaster");
		    if ($check==1) {
				if(!isset($_SESSION['chit_master_key'])){
					$_SESSION['chit_master_key'] = $user->generateRandomString("40");
				}	
				$this->view('home/managechitmaster', 
					[	
						'active_menu' 		=> 'chit',
						'meta_title'  		=>  COLNAME.' | Manage Chit Master',
						'page_title'  		=>  'Manage Chit Master',
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'list' 				=> $user->manageChitMaster(),
						'scripts'			=> 'home',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 		=> 'property',
						'meta_title'  		=> '404 Error - Page Not Found',
						'page_title'  		=> '404 Error - Page Not Found',
						'branch_modal_list' =>  $user->getBranchList(),
						'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function deposite()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("depositmaster");
		    if ($check==1) {
				if(!isset($_SESSION['deposite_master_key'])){
					$_SESSION['deposite_master_key'] = $user->generateRandomString("40");
				}	
				$this->view('home/managedepositemaster', 
					[	
						'active_menu' 		=> 'deposite',
						'meta_title'  		=>  COLNAME.' | Manage Deposite Master',
						'page_title'  		=>  'Manage Deposite Master',
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'list' 				=> $user->manageDepositeMaster(),
						'scripts'			=> 'home',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 		=> 'property',
						'meta_title'  		=> '404 Error - Page Not Found',
						'page_title'  		=> '404 Error - Page Not Found',
						'branch_modal_list' =>  $user->getBranchList(),
						'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function vas()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("vammaster");
		    if ($check==1) {
				if(!isset($_SESSION['vas_master_key'])){
					$_SESSION['vas_master_key'] = $user->generateRandomString("40");
				}	
				$this->view('home/managevasmaster', 
					[	
						'active_menu' 		=> 'settings',
						'meta_title'  		=>  COLNAME.' | Manage VAS Master',
						'page_title'  		=>  'Manage VAS Master',
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description' 	=> META_DESCRIPTION,
						'list' 				=> $user->manageVASMaster(),
						'scripts'			=> 'home',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 		=> 'property',
						'meta_title'  		=> '404 Error - Page Not Found',
						'page_title'  		=> '404 Error - Page Not Found',
						'branch_modal_list' =>  $user->getBranchList(),
						'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function branch()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("branchmaster");
		    if ($check==1) {
				if(!isset($_SESSION['branch_master_key'])){
					$_SESSION['branch_master_key'] = $user->generateRandomString("40");
				}	

				$this->view('home/managebranchmaster', 
					[	
						'active_menu' 		=> 'settings',
						'meta_title'  		=> COLNAME.' | Manage Branch Master',
						'page_title'  		=> 'Manage Branch Master',
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description'  => META_DESCRIPTION,
						'list' 				=> $user->manageBranchMaster(),
						'scripts'			=> 'home',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 		=> 'property',
						'meta_title'  		=> '404 Error - Page Not Found',
						'page_title'  		=> '404 Error - Page Not Found',
						'branch_modal_list' =>  $user->getBranchList(),
						'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);		
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function employeerole()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("employeerolemaster");
		    if ($check==1) {
				if(!isset($_SESSION['employee_role_master_key'])){
					$_SESSION['employee_role_master_key'] = $user->generateRandomString("40");
				}	
				$this->view('home/manageemployeerolehmaster', 
					[	
						'active_menu' 		=> 'settings',
						'meta_title'  		=> COLNAME.' | Manage Employee Role Master',
						'page_title'  		=> 'Manage Employee Role Master',
						'meta_keywords' 	=> META_KEYWORDS,
						'meta_description'  => META_DESCRIPTION,
						'list' 				=> $user->manageEmployeeRoleMaster(),
						'scripts'			=> 'home',	
						'branch_modal_list' =>  $user->getBranchList(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 		=> 'property',
						'meta_title'  		=> '404 Error - Page Not Found',
						'page_title'  		=> '404 Error - Page Not Found',
						'branch_modal_list' =>  $user->getBranchList(),
						'member'   			=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
		$user = $this->model('User');
		$this->view('home/error', 
			[	
				'active_menu' 		=> 'home',
				'meta_title'  		=> '404 Error - Page Not Found',
				'scripts'			=> 'error',
				'page_title'  		=>  COLNAME,
				'meta_keywords' 	=> META_KEYWORDS,
				'meta_description'	=> META_DESCRIPTION,
				'branch_modal_list' =>  $user->getBranchList(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
}


?>