<?php

class Trash extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$info_details = $user->getDetails(EMPLOYEE,"*"," id='".$_SESSION["crm_admin_id"]."' ");
			$info = $user->editPagePublish($info_details);
			$this->view('home/restoregeneraldocuments', 
			[	
				'active_menu' 		=> 'trash',
				'meta_title'  		=>  COLNAME.' | General Documents',
				'page_title'  		=>  COLNAME,
				'meta_keywords' 	=>  META_KEYWORDS,
				'meta_description' 	=> META_DESCRIPTION,
				'info'				=>  $info,
				'list' 				=> $user->trashGeneralDocuments(),
				'scripts'			=> 'home',	
				'branch_modal_list' =>  $user->getBranchList(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function leads()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$this->view('home/restoreleads', 
			[	
				'active_menu' 		=> 'trash',
				'meta_title'  		=>  COLNAME.' | Leads',
				'page_title'  		=>  COLNAME,
				'meta_keywords' 	=>  META_KEYWORDS,
				'meta_description' 	=> META_DESCRIPTION,
				'list' 				=> $user->trashLeads(),
				'scripts'			=> 'home',	
				'branch_modal_list' =>  $user->getBranchList(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function errors()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 		=> 'home',
					'meta_title'  		=> '404 Error - Page Not Found',
					'scripts'			=> 'error',
					'page_title'  		=>  COLNAME,
					'meta_keywords' 	=> META_KEYWORDS,
					'meta_description' 	=> META_DESCRIPTION,
					'branch_modal_list' =>  $user->getBranchList(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
	
}


?>