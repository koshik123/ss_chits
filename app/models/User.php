<?php
	require_once 'Model.php';
	require_once 'config/config.php';
	require_once 'app/core/classes/PHPMailerAutoload.php';

	class User extends Model
	{

	/*--------------------------------------------- 
					User Info
	----------------------------------------------*/

	// General User info for all pages

	function userInfo($id)
	{
		$today = date("Y-m-d");
		$query = "SELECT E.id,(E.name) as username, E.status,E.email,E.assigned_employee FROM ".EMPLOYEE." E  WHERE E.id ='".$id."' ";
		$exe = $this->selectQuery($query);
		$list = mysqli_fetch_assoc($exe);
		return $list;
	}

	/*--------------------------------------------- 
				Set Branch  
	----------------------------------------------*/

	function getBranchList()
	{
		$layout ="";
        $info = $this -> getDetails(EMPLOYEE,"role,assigned_branch"," id ='".$_SESSION['crm_admin_id']."' ");
        if($_SESSION['super_admin']==1)  {
        	$query = "SELECT id,token,branch,status,added_by,created_at FROM ".BRANCH_MASTER."  WHERE status='1'  " ;
			$exe = $this->selectQuery($query);	
			if(mysqli_num_rows($exe) > 0) {
				while ($list = mysqli_fetch_array($exe)) {
					$layout.= "<li><a href='javascript:void();' class='sellectBranch' data-option='".$list['id']."'>".ucwords($list['branch'])."</a></li>
					";
				}
			}
        } elseif($info['role']==2) {
        	if($info['assigned_branch']!=NULL) {
        		$query = "SELECT id,token,branch,status,added_by,created_at FROM ".BRANCH_MASTER."  WHERE id IN (".$info['assigned_branch'].") AND status='1'  " ;
				$exe = $this->selectQuery($query);	
				if(mysqli_num_rows($exe) > 0) {
					while ($list = mysqli_fetch_array($exe)) {
						$layout.= "<li><a href='javascript:void();' class='sellectBranch' data-option='".$list['id']."'>".ucwords($list['branch'])."</a></li>
						";
					}
				}
        	} else {
        		return 0;
        	}
        	
        } elseif($info['role']==3) {
        	if($info['assigned_branch']!=NULL) {
        		$b_info = $this->getDetails(BRANCH_MASTER,"id,branch","id='".$info['assigned_branch']."'");
            	$_SESSION['selected_branch']      = $b_info['id'];
            	$_SESSION['selected_branch_name'] = $b_info['branch'];
            } else {
        		return 0;
        	}
        } else {
        	$layout = "";
        }
		
		return $layout;
	}


	/*--------------------------------------------- 
					Login Activity
	----------------------------------------------*/

	function manageLoginActivity()
  	{
  		$layout = "";
  		$id=$_SESSION["crm_admin_id"];
		$q = "SELECT  id,logged_id,user_type,portal_type,auth_referer,auth_medium,auth_user_agent,auth_ip_address,session_in  FROM ".SESSION_TBL." M WHERE user_type='admin' AND logged_id='$id' ORDER BY id DESC LIMIT 20  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		= $this->editPagePublish($details);
	    		$layout .= "
	    			<tr>
                        <td>".($list['auth_user_agent'])."</td>
                        <td>".$list['auth_ip_address']."</td>
                        <td>".date("M D, y h:i a")."</td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
			 	General Settings
	----------------------------------------------*/

	function getChitTypeList($current="")
	{
		$layout ="";
		$query = "SELECT id,token,item_type,status  FROM ".CHIT_MASTER." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['item_type'])."</option>";
			}
		}
		return $layout;
	}

	function getBranchListMultiDropDown($current="")
	{
		$layout ="";
		$query = "SELECT id,branch,status  FROM ".BRANCH_MASTER." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['branch'])."</option>";
			}
		}
		return $layout;
	}

	function getDepositeTypeList($current="")
	{
		$layout ="";
		$query = "SELECT id,token,item_type,status  FROM ".DEPOSITE_MASTER." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['item_type'])."</option>";
			}
		}
		return $layout;
	}

	function getVASTypeList($current="")
	{
		$layout ="";
		$query = "SELECT id,token,item_type,status  FROM ".VAS_MASTER." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['item_type'])."</option>";
			}
		}
		return $layout;
	}


	function getCustomerList($current="")
	{
		$layout ="";
		$branch_check = ((isset($_SESSION['selected_branch']))? "AND branch_id='".$_SESSION['selected_branch']."'" : "" );
		$query = "SELECT id,token,name,status  FROM ".CUSTOMER_TBL." WHERE status='1' $branch_check ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['name'])."</option>";
			}
		}
		return $layout;
	}


	function getAssignCustomerList($current="")
	{
		$layout ="";
		$query = "SELECT id,token,name,status  FROM ".CUSTOMER_TBL." WHERE status='1' AND assign_status!='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['name'])."</option>";
			}
		}
		return $layout;
	}

	function getActivityType($current="")
	{
		$layout ="";
		$query = "SELECT id,token,activity_type,status  FROM ".ACTIVITY_TYPE." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['activity_type'])."</option>";
			}
		}
		return $layout;
	}

	function getLeadStatus($current="")
	{
		$layout ="";
		$query = "SELECT id,token,lead_type,status  FROM ".LEAD_TYPE." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['lead_type'])."</option>";
			}
		}
		return $layout;
	}

	function getLeadStatusName($current="")
	{
		$layout ="";
		$query = "SELECT id,token,lead_type,status  FROM ".LEAD_TYPE." WHERE id='".$current."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$layout.= ucwords($list['lead_type']);
			}
		}
		return $layout;
	}

	
	function getEmployeesName($current="")
	{
		$layout ="";
		$query = "SELECT id,name,status  FROM ".EMPLOYEE." WHERE id='".$current."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$layout.= ucwords($list['name']);
			}
		}
		return $layout;
	}

	function getRequestTypeName($current="")
	{
		$layout ="";
		$query = "SELECT id,token,request_type,status  FROM ".REQUEST_TYPE_TBL." WHERE id='".$current."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$layout.= ucwords($list['request_type']);
			}
		}
		return $layout;
	}

	function getEmployeeList($current="")
	{
		$layout ="";
		$branch_check = ((isset($_SESSION['selected_branch']))? "AND branch_id='".$_SESSION['selected_branch']."'" : "" );
		$query = "SELECT id,name,status  FROM ".EMPLOYEE." WHERE role!='1' AND role !='2' AND role!='3' AND status='1' AND super_admin='0' $branch_check ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['name'])."</option>";
			}
		}
		return $layout;
	}

	function getLeadSource($current="")
	{
		$layout ="";
		$query = "SELECT id,token,lead_source,status  FROM ".LEAD_SOURCE." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['lead_source'])."</option>";
			}
		}
		return $layout;
	}

	function getCustomerProfileList($current="")
	{
		$layout ="";
		$query = "SELECT id,token,customer_profile,status  FROM ".CUSTOMER_PROFILE." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['customer_profile'])."</option>";
			}
		}
		return $layout;
	}

	function getRequestTypeList($current="")
	{
		$layout ="";
		$query = "SELECT id,token,request_type,status  FROM ".REQUEST_TYPE_TBL." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['request_type'])."</option>";
			}
		}
		return $layout;
	}

	function getChitName($current="")
	{
		$layout ="";
		$query = "SELECT id,item_type,status  FROM ".CHIT_MASTER." WHERE id='".$current."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$layout.= ucwords($list['item_type']);
			}
		}
		return $layout;
	}

	function getDepositeName($current="")
	{
		$layout ="";
		$query = "SELECT id,item_type,status  FROM ".DEPOSITE_MASTER." WHERE id='".$current."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$layout.= ucwords($list['item_type']);
			}
		}
		return $layout;
	}

	function getVamName($current="")
	{
		$layout ="";
		$query = "SELECT id,item_type,status  FROM ".VAS_MASTER." WHERE id='".$current."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$layout.= ucwords($list['item_type']);
			}
		}
		return $layout;
	}

	function getMultiCustomerName($current="")
	{
		$layout ="";
		$dates_array = explode(',', $current);	
		$i = 0;
		$len = count($dates_array);
		foreach ($dates_array as $value) {
			$query = "SELECT id,token,name,status  FROM ".CUSTOMER_TBL." WHERE id='".$value."' ";
			$exe = $this->selectQuery($query);	
			if(mysqli_num_rows($exe) > 0){
				while ($list = mysqli_fetch_array($exe)) {
					if ($i == $len - 1) {
						$comas = ' ';
					}else{
						$comas = ', ';
					}
					$layout.= ucwords($list['name']).$comas;					
				}
			}
			$i++;
		}
		return $layout;
	}

	function getMultiChitName($current="")
	{
		$layout ="";
		$dates_array = explode(',', $current);	
		$i = 0;
		$len = count($dates_array);
		foreach ($dates_array as $value) {
			$query = "SELECT id,item_type,status  FROM ".CHIT_MASTER." WHERE id='".$value."' ";
			$exe = $this->selectQuery($query);	
			if(mysqli_num_rows($exe) > 0){
				while ($list = mysqli_fetch_array($exe)) {
					if ($i == $len - 1) {
						$comas = ' ';
					}else{
						$comas = ', ';
					}

					$layout.= ucwords($list['item_type']).$comas;					
				}
			}
			$i++;
		}
		return $layout;
	}

	function getMultiDepositeName($current="")
	{
		$layout ="";
		$dates_array = explode(',', $current);	
		$i = 0;
		$len = count($dates_array);
		foreach ($dates_array as $value) {
			$query = "SELECT id,item_type,status  FROM ".DEPOSITE_MASTER." WHERE id='".$value."' ";
			$exe = $this->selectQuery($query);	
			if(mysqli_num_rows($exe) > 0){
				while ($list = mysqli_fetch_array($exe)) {
					if ($i == $len - 1) {
						$comas = ' ';
					}else{
						$comas = ', ';
					}

					$layout.= ucwords($list['item_type']).$comas;					
				}
			}
			$i++;
		}
		return $layout;
	}

	function getMultiVamName($current="")
	{
		$layout ="";
		$dates_array = explode(',', $current);	
		$i = 0;
		$len = count($dates_array);
		foreach ($dates_array as $value) {
			$query = "SELECT id,item_type,status  FROM ".VAS_MASTER." WHERE id='".$value."' ";
			$exe = $this->selectQuery($query);	
			if(mysqli_num_rows($exe) > 0){
				while ($list = mysqli_fetch_array($exe)) {
					if ($i == $len - 1) {
						$comas = ' ';
					}else{
						$comas = ', ';
					}

					$layout.= ucwords($list['item_type']).$comas;					
				}
			}
			$i++;
		}
		return $layout;
	}

  	/*--------------------------------------------- 
				Customer List
	----------------------------------------------*/

	function manageCustomer()
  	{
  		$layout = "";
  		$branch_check = ((isset($_SESSION['selected_branch']))? "branch_id='".$_SESSION['selected_branch']."'" : '1' );
		$q = "SELECT id,name,uid,gender,mobile,email,address,city,state ,pincode,kyc_status,status,assign_status,priority  FROM ".CUSTOMER_TBL." WHERE $branch_check  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                $kyc_status1 = (($list['kyc_status']=='0') ? '<span class="badge badge-warning">Not submitted</span>' : $list['kyc_status']  );
                $kyc_status2  = (($list['kyc_status']=='1') ? '<span class="badge badge-success">Approved</span>' : $kyc_status1 );
                $kyc_status3  = (($list['kyc_status']=='2') ? '<span class="badge badge-info">Pending Approval</span>' : $kyc_status2 );
                $kyc_status  = (($list['kyc_status']=='3') ? '<span class="badge badge-danger">Rejected </span>' : $kyc_status3 );

                $assign_status = (($list['assign_status']!=1) ? "<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."customer/assign/".$list['id']."' class='btn btn-sm btn-info' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-account-setting'></em> Assign</a></div>" : '' );
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."customer/details/".$list['id']."' target='_blank'>".$list['uid']."</a></td>
                        <td><a href='".COREPATH."customer/details/".$list['id']."' target='_blank'>".ucwords($list['name'])."</a></td>
                        <td><i class='la la-phone'></i> ".$list['mobile']." </td>
                        <td><i class='la la-envelope'></i> ".$list['email']."</td>
                        <td>".ucwords($list['priority'])."</td>
                        <td>
		                    
		                    <div class='custom-control custom-control-sm custom-checkbox CustomerStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."customer/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."customer/details/".$list['id']."' target='_blank' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}



  	/*--------------------------------------------- 
				Employees
	----------------------------------------------*/

	function manageEmployee()
  	{
  		$layout 	  = "";
		
		if($_SESSION['super_admin']==1) {
			$emp_role = "role='2'";
		} elseif ($_SESSION['employee_role']==2) {
			$emp_role = "role='3' AND assigned_employee='".$_SESSION['crm_admin_id']."' ";
		} elseif ($_SESSION['employee_role']==3) {
			$emp_role = "role!='1' AND role!='2' AND role!='3' AND assigned_employee='".$_SESSION['crm_admin_id']."'";
		} else {
			$emp_role = "role!='1' AND role!='2' AND role!='3'";
		}


		$q 	  = "SELECT E.id,E.branch_id,E.token,E.name,E.role ,E.gender,E.assigned_branch ,E.email ,E.mobile ,E.city,E.state,E.pincode,E.status,ER.employee_role FROM ".EMPLOYEE." E LEFT JOIN ".EMPLOYEE_ROLE." ER ON (E.role=ER.id) WHERE $emp_role AND super_admin='0' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");

                $branch_check = in_array($_SESSION['selected_branch'], explode(",", $list['branch_id']));

                if($branch_check) {

	                if($_SESSION['super_admin']==1) {
	                		$assigned_branch_btn = " <div class='tb-odr-btns d-none d-md-inline'>
		                            <buttom  class='btn btn-sm btn-secondary assignBranchEmpInfo' data-option='".$list['id']."' data-toggle='modal' data-target='#assignBranchModal'><em class='icon ni ni-location'></em></em></buttom>
		                        </div>";
	                		if($list['assigned_branch']) {
	                			$assigned_branch_td = $this->getAssignedBranchlist($list['assigned_branch']); 
	                		} else {
	                			$assigned_branch_td = "<td><span class='tb-status status_list text-warning'>Not Assigned</span></td>"; 
	                		}
	                	
	                } elseif($_SESSION['employee_role']==2) {
	                		if($list['assigned_branch']) {
		            			$assigned_branch_td = $this->getAssignedBranchlist($list['assigned_branch']); 
		            		} else {
		            			$assigned_branch_td = "<td><span class='tb-status status_list text-warning'>Not Assigned</span></td>"; 
		            		}
	                 	$assigned_branch_btn = "";
	                } else {
	                	$assigned_branch_td	 = ""; 
	                	$assigned_branch_btn = ""; 
	                }	

		    		$layout .= "
		    			<tr>
	                        <td>".$i."</td>
	                        <td><a href='".COREPATH."employee/details/".$list['id']."'>".ucwords($list['name'])."</a></td>
	                        <td>".ucwords($list['employee_role'])."</td>
	                        ".$assigned_branch_td."
	                        <td>".$list['mobile']."</td>
	                        <td>".$list['email']."</td>
	                        
	                        <td>
			                    <div class='custom-control custom-control-sm custom-checkbox employeeStatus' data-option='".$this->encryptData($list['id'])."'>
	                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
	                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
	                            </div>
			    			</td>
	                        <td>
		                        <div class='tb-odr-btns d-none d-md-inline'>
		                            <a href='".COREPATH."employee/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
		                        </div>
		                        <div class='tb-odr-btns d-none d-md-inline'>
		                            <a href='#' class='btn btn-sm btn-primary changepasswordEmployee'  data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-lock-alt'></em></a>

		                        </div>
		                        <div class='tb-odr-btns d-none d-md-inline'>
		                            <a href='".COREPATH."employee/permission/".$list['id']."' class='btn btn-sm btn-info'><em class='icon ni ni-account-setting'></em></a>
		                        </div>
		                        <div class='tb-odr-btns d-none d-md-inline'>
		                            <a href='".COREPATH."employee/details/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
		                        </div>
		                        ".$assigned_branch_btn."
		                    </td>
	                    </tr>
		    		";
		    		
		    		$i++;
		    	}
	    	}
	    }
	    return $layout;
  	}

  	function getAssignedBranchlist($assigned_branch) 
  	{
  		$layout = "<td class='assigned_branch_td'>";
  		$query  = "SELECT id,branch,status FROM ".BRANCH_MASTER." WHERE id IN (".$assigned_branch.") ";
  		$exe 	= $this->selectQuery($query);
  		$count  = mysqli_num_rows($exe);
  			if(mysqli_num_rows($exe) > 0) {
  				$i = 1;
  				while ($list = mysqli_fetch_array($exe)) {
  					$layout  .= "<strong>".ucfirst($list['branch'])."</strong>";
  					if($i < $count) {
  						$layout .= ", ";
  					}
  				$i++;
  				}
  			}
  		$layout .= "</td>";
  		return $layout;
  	}

  	function getEmployeeRole($current="")
	{
		$layout ="";

		if($_SESSION['super_admin']==1) {
			$emp_role_filter = "";
		} elseif ($_SESSION['employee_role']==2) {
			$emp_role_filter = "id NOT IN (1,2) AND ";
		} elseif ($_SESSION['employee_role']==3) {
			$emp_role_filter = "id NOT IN (1,2,3) AND ";
		} else {
			$emp_role_filter = "id NOT IN (1,2,3) AND ";
		}

		$query = "SELECT id,token,employee_role,status  FROM ".EMPLOYEE_ROLE." WHERE ".$emp_role_filter." status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['employee_role'])."</option>";
			}
		}
		return $layout;
	}

	function getBranchListDropDown($current="")
	{
		$layout ="";
		$current_ids = explode(',', $current);

		if($_SESSION['super_admin']==1) {
			$branch_filter =  "";
		} elseif($_SESSION['employee_role']==2) {
			$info = $this->getDetails(EMPLOYEE,"role,assigned_branch","id='".$_SESSION['crm_admin_id']."'");
			if($info['assigned_branch']) {
				$branch_filter =  "AND id IN (".$info['assigned_branch'].") ";
			}
		} else {
			$branch_filter = "";
		}

		$query = "SELECT id,branch,status  FROM ".BRANCH_MASTER." WHERE status='1' $branch_filter  ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = ((in_array($list['id'], $current_ids)) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['branch'])."</option>";
			}
		}
		return $layout;
	}
	function getBranchManagerDropDown($current="")
	{
		$layout ="";
		$current_ids = explode(',', $current);
		$query = "SELECT E.id,E.name,E.status,E.role,B.branch FROM ".EMPLOYEE." E LEFT JOIN ".BRANCH_MASTER." B ON (B.id=E.assigned_branch) WHERE E.role='3' AND E.assigned_branch!='' AND E.super_admin='0' AND E.status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = ((in_array($list['id'], $current_ids)) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['name'])." ( ".ucfirst($list['branch'])." ) </option>";
			}
		}
		return $layout;
	}
	function getGeneralManagerDropDown($branch_id="",$assigned_employee="")
	{		
		$layout ="";
        $query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role='2' AND E.assigned_branch!='' AND E.super_admin='0' AND E.status='1' ";
        $exe = $this->selectQuery($query);  
        if(mysqli_num_rows($exe) > 0){
            $layout ="<option value=''>Select General Manager</option> ";
            while ($list = mysqli_fetch_array($exe)) {
                $current_ids = explode(',', $list['assigned_branch']);
                if(in_array($branch_id, $current_ids)) {
					$selected = ($list['id']==$assigned_employee)? "selected" : "";
                    $layout.= "<option value='".$list['id']."' $selected >".ucwords($list['name'])."</option>";
                }
            }
        }
        return $layout;
	}
	function manageAssignedBranchManager($employee_id)
  	{
  		$layout     	= "";
		$emp_info  		= $this->getDetails(EMPLOYEE,"*"," id='".$employee_id."' ");
		// $branch_check   = ((isset($_SESSION['selected_branch']))? "AND branch_id='".$_SESSION['selected_branch']."'" : '1' );
		if($emp_info['role']==2) {
			if($emp_info['assigned_branch']) {
				$branch_filter = "AND assigned_branch IN (".$emp_info['assigned_branch'].") AND assigned_employee='".$employee_id."'";
			} else {
				$branch_filter = "";			
			}
			$emp_filter = "role='3' $branch_filter ";

		} elseif ($emp_info['role']==3) {
			$emp_filter = "role!='2' AND role!='3' AND assigned_employee='".$employee_id."'";
		} else {
			$emp_filter = "role!='2' AND role!='3'";
		}
		$q 	  = "SELECT E.id,E.token,E.name,E.role ,E.gender,E.assigned_branch ,E.email ,E.mobile ,E.city,E.state,E.pincode,E.status,ER.employee_role FROM ".EMPLOYEE." E LEFT JOIN ".EMPLOYEE_ROLE." ER ON (E.role=ER.id) WHERE $emp_filter AND super_admin='0'  "  ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");

                if($list['role']==2) {
                	$assigned_branch_btn = " <div class='tb-odr-btns d-none d-md-inline'>
                            <buttom  class='btn btn-sm btn-secondary assignBranchEmpInfo' data-option='".$list['id']."' data-toggle='modal' data-target='#assignBranchModal'><em class='icon ni ni-location'></em></em></buttom>
                        </div>";
            		if($list['assigned_branch']) {
            			$assigned_branch_td = $this->getAssignedBranchlist($list['assigned_branch']); 
            		} else {
            			$assigned_branch_td = "<td><span class='tb-status status_list text-warning'>Not Assigned</span></td>"; 
            		}

                } elseif($list['role']==3) {
                	if($list['assigned_branch']) {
            			$assigned_branch_td = $this->getAssignedBranchlist($list['assigned_branch']); 
            		} else {
            			$assigned_branch_td = "<td><span class='tb-status status_list text-warning'>Not Assigned</span></td>"; 
            		}
                	$assigned_branch_btn = "";
                } else {
                	$assigned_branch_td	 = "";
                	$assigned_branch_btn = "";
                }


	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."employee/details/".$list['id']."'>".ucwords($list['name'])."</a></td>
                        <td>".ucwords($list['employee_role'])."</td>
                        ".$assigned_branch_td."
                        <td>".$list['mobile']."</td>
                        <td>".$list['email']."</td>
                        
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox employeeStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
	                        <div class='tb-odr-btns d-none d-md-inline'>
	                            <a href='".COREPATH."employee/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
	                        </div>
	                        <div class='tb-odr-btns d-none d-md-inline'>
	                            <a href='#' class='btn btn-sm btn-primary changepasswordEmployee'  data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-lock-alt'></em></a>

	                        </div>
	                        <div class='tb-odr-btns d-none d-md-inline'>
	                            <a href='".COREPATH."employee/permission/".$list['id']."' class='btn btn-sm btn-info'><em class='icon ni ni-account-setting'></em></a>
	                        </div>
	                        <div class='tb-odr-btns d-none d-md-inline'>
	                            <a href='".COREPATH."employee/details/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
	                        </div>
	                        ".$assigned_branch_btn."
	                    </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

    function getEmployeesIdsForGM($id,$ids_for)
  	{
  		$employee_ids =array();

  		if($ids_for=="general_manager") {
  			$query1 = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role='3' AND assigned_employee='".$id."' AND E.super_admin='0' AND E.status='1' ";
  			$branch_manager_ids =array();
  			$exe1 = $this->selectQuery($query1);  
	        if(mysqli_num_rows($exe1) > 0){
	            while ($list = mysqli_fetch_array($exe1)) {
	                 $branch_manager_ids[] = $list['id'];
	            }
	        }

	        if(count($branch_manager_ids)) {
	        	$bm_condition = " AND E.assigned_employee IN (".implode(",", $branch_manager_ids).") "; 
	        } else {
	        	$bm_condition = " AND E.assigned_employee=NULL ";
	        }

	        $query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role!='2' AND E.role!='3' $bm_condition  AND E.super_admin='0' AND E.status='1' ";


  		} else {
  			$query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role!='1' AND E.role!='2' AND E.role!='3'  AND assigned_employee='".$id."' AND E.super_admin='0' AND E.status='1' ";
  		}
        
        $exe = $this->selectQuery($query);
        if(mysqli_num_rows($exe) > 0){
            while ($list = mysqli_fetch_array($exe)) {
                    $employee_ids[] = $list['id'];
            }
        }

        if(count($employee_ids)) {
        	$condition = " AND E.id IN (".implode(",", $employee_ids).") "; 
        } else {
        	$condition = "AND E.id=NULL";
        }

        return $condition ;
  	}

  	function manageAssignedEmployee($employee_id)
  	{
  		$layout     	= "";
		$emp_info  		= $this->getDetails(EMPLOYEE,"*"," id='".$employee_id."' ");

		if($emp_info['role']==2) {
			$get_employees = $this->getEmployeesIdsForGM($emp_info['id'],"general_manager");
		} elseif ($emp_info['role']==3) {
			$get_employees = $this->getEmployeesIdsForGM($emp_info['id'],"branch_manager");
		} else {
			$get_employees = "AND E.id=NULL";
		}

		$q 	  = "SELECT E.id,E.token,E.name,E.role ,E.gender,E.assigned_branch ,E.email ,E.mobile ,E.city,E.state,E.pincode,E.status,ER.employee_role FROM ".EMPLOYEE." E LEFT JOIN ".EMPLOYEE_ROLE." ER ON (E.role=ER.id) WHERE super_admin='0'  $get_employees  "  ;

	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");

                if($list['role']==2) {
                	$assigned_branch_btn = " <div class='tb-odr-btns d-none d-md-inline'>
                            <buttom  class='btn btn-sm btn-secondary assignBranchEmpInfo' data-option='".$list['id']."' data-toggle='modal' data-target='#assignBranchModal'><em class='icon ni ni-location'></em></em></buttom>
                        </div>";
            		if($list['assigned_branch']) {
            			$assigned_branch_td = $this->getAssignedBranchlist($list['assigned_branch']); 
            		} else {
            			$assigned_branch_td = "<td><span class='tb-status status_list text-warning'>Not Assigned</span></td>"; 
            		}

                } elseif($list['role']==3) {
                	if($list['assigned_branch']) {
            			$assigned_branch_td = $this->getAssignedBranchlist($list['assigned_branch']); 
            		} else {
            			$assigned_branch_td = "<td><span class='tb-status status_list text-warning'>Not Assigned</span></td>"; 
            		}
                	$assigned_branch_btn = "";
                } else {
                	$assigned_branch_td	 = "";
                	$assigned_branch_btn = "";
                }


	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."employee/details/".$list['id']."'>".ucwords($list['name'])."</a></td>
                        <td>".ucwords($list['employee_role'])."</td>
                        <td>".$list['mobile']."</td>
                        <td>".$list['email']."</td>
                        
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox employeeStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
	                        <div class='tb-odr-btns d-none d-md-inline'>
	                            <a href='".COREPATH."employee/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
	                        </div>
	                        <div class='tb-odr-btns d-none d-md-inline'>
	                            <a href='#' class='btn btn-sm btn-primary changepasswordEmployee'  data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-lock-alt'></em></a>

	                        </div>
	                        <div class='tb-odr-btns d-none d-md-inline'>
	                            <a href='".COREPATH."employee/permission/".$list['id']."' class='btn btn-sm btn-info'><em class='icon ni ni-account-setting'></em></a>
	                        </div>
	                        <div class='tb-odr-btns d-none d-md-inline'>
	                            <a href='".COREPATH."employee/details/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
	                        </div>
	                        ".$assigned_branch_btn."
	                    </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}



  	/*--------------------------------------------- 
				Manage Request type
	----------------------------------------------*/

	function manageRequestType()
  	{
  		$layout = "";
		$q = "SELECT B.id,B.token,B.request_type,B.status,B.added_by,B.created_at,B.added_by,E.name FROM ".REQUEST_TYPE_TBL." B LEFT JOIN ".EMPLOYEE." E ON (E.id=B.added_by) WHERE 1  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['request_type'])."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox requestTypeStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editRequestType' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                           
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}


  	// Get Employee IDs

  	function getEmployeesIds($id,$ids_for)
  	{
  		$employee_ids =array();

  		if($ids_for=="general_manager") {
  			$query1 = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role='3' AND assigned_employee='".$id."' AND E.super_admin='0' AND E.status='1' ";
  			$branch_manager_ids =array();
  			$exe1 = $this->selectQuery($query1);  
	        if(mysqli_num_rows($exe1) > 0){
	            while ($list = mysqli_fetch_array($exe1)) {
	                 $branch_manager_ids[] = $list['id'];
	            }
	        }

	        if(count($branch_manager_ids)) {
	        	$bm_condition = " AND assigned_employee IN (".implode(",", $branch_manager_ids).") "; 
	        } else {
	        	$bm_condition = " AND assigned_employee=NULL ";
	        }

	        $query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role!='2' AND E.role!='3' $bm_condition  AND E.super_admin='0' AND E.status='1' ";


  		} else {
  			$query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role!='1' AND E.role!='2' AND E.role!='3'  AND assigned_employee='".$id."' AND E.super_admin='0' AND E.status='1' ";
  		}
        
        $exe = $this->selectQuery($query);
        if(mysqli_num_rows($exe) > 0){
            while ($list = mysqli_fetch_array($exe)) {
                    $employee_ids[] = $list['id'];
            }
        }

        if(count($employee_ids)) {
        	$condition = " AND L.employee_id IN (".implode(",", $employee_ids).") "; 
        } else {
        	$condition = " AND L.employee_id=NULL ";
        }

        return $condition ;
  	}

  	function leadReport($from="",$to="",$gm_id="",$bm_id="",$emp_id="",$lead_status="",$chit_id="",$deposit_id="",$vam_id="")
  	{
  		$layout = "";


  		if($_SESSION['super_admin']==1 || $_SESSION['employee_role']==2 || $_SESSION['employee_role']==3) {
  			if($gm_id!="") {
	  			if($gm_id!="" && $bm_id=="") {
	  				$employee_filter = $this->getEmployeesIds($gm_id,$ids_for="general_manager");
	  				$leads_filter = $employee_filter;

	  			} elseif($gm_id!="" && $bm_id!="" && $emp_id=="") {
	  				
	  				$employee_filter = $this->getEmployeesIds($bm_id,$ids_for="branch_manager");
	  				$leads_filter = $employee_filter;

	  			} elseif($gm_id!="" && $bm_id!="" && $emp_id!="") {
	  				
	  				$leads_filter = "AND L.employee_id='$emp_id'";

	  			}
	  		} else {
  	    		$leads_filter = "";
	  		}
  	    } else {
  	    	$leads_filter = "AND L.employee_id='$emp_id'";
  	    }

		$q = "SELECT L.id,L.token,L.import_token,L.designation,L.uid,L.auth_type,L.has_chit,L.has_deposite,L.has_vas,L.chit_id,L.deposite_id,L.vas_id,L.property_id,L.lead_date,L.fname,L.lname,L.leadsource,L.gender,L.mobile,L.email,L.secondary_mobile,L.secondary_email,L.address,L.city,L.state,L.pincode,L.description,L.lead_status_id,L.assign_status,L.closed_status,L.status,L.employee_id,L.added_by,L.created_at,E.name,E.assigned_employee,T.lead_type,CM.item_type as chit_name,DM.item_type as deposite_name,VM.item_type as vam_name,LS.lead_source,b.branch
		    		FROM ".LEAD_TBL." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) 
		    				  	LEFT JOIN ".LEAD_TYPE." T ON (T.id=L.lead_status_id) 
		    				  	LEFT JOIN ".CHIT_MASTER." CM ON (CM.id=L.chit_id) 
		    				  	LEFT JOIN ".DEPOSITE_MASTER." DM ON (DM.id=L.deposite_id) 
		    				  	LEFT JOIN ".VAS_MASTER." VM ON (VM.id=L.vas_id) 
		    				  	LEFT JOIN ".LEAD_SOURCE." LS ON (LS.id=L.leadsource) 
		    				  	LEFT JOIN ".BRANCH_MASTER." B ON (B.id=L.branch_id) 
		   			WHERE L.lead_date BETWEEN '$from' AND '$to'  $leads_filter  " ;

		if ($lead_status!="") {
			$q .=" AND L.lead_status_id='$lead_status' ";
		}

		$scheme_checked = array();

		if($chit_id!=""){
			$dates_array = explode(",", $chit_id);
			$i = 0;
			$len = count($dates_array);
			$scheme_checked['chit'] ="(";
			foreach ($dates_array as $value) {
				if ($i == $len - 1) {
					$or = ' ';
				}else{
					$or = ' OR ';
				}
				$scheme_checked['chit'] .=" L.chit_id='$value' $or ";
				$i++;
			}
			$scheme_checked['chit'] .=")";
        }
        if($deposit_id!=""){
        	$dates_array1 = explode(",", $deposit_id);
			$j = 0;
			$len1 = count($dates_array1);
			$scheme_checked['deposit'] ="(";
			foreach ($dates_array1 as $value1) {
				if ($j == $len1 - 1) {
					$or1 = ' ';
				}else{
					$or1 = ' OR ';
				}
				$scheme_checked['deposit'] .=" L.deposite_id='$value1' $or1 ";
				$j++;
			}
			$scheme_checked['deposit'] .=")";
        }
        if($vam_id!=""){
        	$dates_array2 = explode(",", $vam_id);
			$k = 0;
			$len2 = count($dates_array2);
			$scheme_checked['vam'] ="(";
			foreach ($dates_array2 as $value2) {
				if ($k == $len2 - 1) {
					$or2 = ' ';
				}else{
					$or2 = ' OR ';
				}
				$scheme_checked['vam'] .=" L.vas_id='$value2' $or2 ";
				$k++;
			}
			$scheme_checked['vam'] .=")";
        }

        if(count($scheme_checked) > 0) {
			$len = count($scheme_checked);
			$i=0;
        	$scheme_filter = "AND (";
        		foreach ($scheme_checked as $value) {
					if ($i == $len - 1) {
						$or = ' ';
					}else{
						$or = ' OR ';
					}
					$scheme_filter .=" $value $or ";
					$i++;
				}
        	$scheme_filter .= ")";
        } else {
        	$scheme_filter = "";
        }
        $q .= $scheme_filter;

		$q .=" ORDER BY L.lead_date DESC ";
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$assign_btn = (($list['assign_status']=="0")? "
                    <div class='tb-odr-btns d-none d-md-inline'>
                    <a href='".COREPATH."lead/assignlead/".$list['id']."' class='btn btn-info btn-sm'> Assign</a>
                    </div>" : "<div class='tb-odr-btns d-none d-md-inline'><a href='".COREPATH."lead/reassign/".$list['id']."' class='btn btn-info btn-sm'> Reassign</a></div>" );
                $assign_info = $this->leadAssignTo($list['id']);
                $assigned_to = (($assign_info!="") ? "<a href='".COREPATH."employee/details/".$assign_info['created_to']."' target='_blank' >".ucwords($assign_info['name'])."</a>" : "-" );
                $designation = $list['designation']!="" ? "<em class='icon ni ni-briefcase'></em> ".ucwords($list['designation'])." <br>" : "" ;
                $mobile = $list['mobile']!="" ? $list['mobile'] : "-" ;
                $email = $list['email']!="" ? $list['email'] : "-" ;

                $chit     = $list['has_chit']=='1' ? "<strong>Chit:</strong> ".ucwords($list['chit_name'])."<br>" : '' ;
                $deposite = $list['has_deposite']=='1' ? "<strong>Deposite:</strong> ".ucwords($list['deposite_name'])."<br>" : '' ;
                $vas      = $list['has_vas']=='1' ? "<strong>VAM:</strong> ".ucwords($list['vam_name'])."<br>" : '' ;

                if(($_SESSION['super_admin'])==1){
                	$assign_column = "<td>".ucwords($assigned_to).' '.$assign_btn."</td>";
                }else{
                	 $assign_column = '';
                }

                if($list['employee_id']) {
                	$employee = $this->getDetails(EMPLOYEE,"id,name,assigned_employee","id='".$list['employee_id']."'");
                	$bm_info  = $this->getDetails(EMPLOYEE,"id,name,assigned_employee","id='".$employee['assigned_employee']."'");
                	$gm_info  = $this->getDetails(EMPLOYEE,"id,name","id='".$bm_info['assigned_employee']."'");

                	$reporting = "<td>
		                        	<strong>Branch:</strong> ".ucwords($list['branch'])."<br>
		                        	<strong>G.M:</strong> ".ucwords($gm_info['name'])."<br>
		                        	<strong>B.M:</strong> ".ucwords($bm_info['name'])."<br>
		                        </td>";
                } else {
                	$reporting = "<td><span class='tb-status status_list text-warning'>Not Assigned</span></td>";
                }

	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".date("d/m/Y",strtotime($list['lead_date']))."</td>
                        <td><em class='icon ni ni-user'></em><a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class=''> ".ucwords($list['fname'].' '.$list['lname'])."</a> <br> ".$designation."  <em class='icon ni ni-mobile'></em> ".$mobile." <br><em class='icon ni ni-mail'></em>  ".$email."</td>
                        <td>".$chit." ".$deposite." ".$vas."</td>
                        <td><strong>Source:</strong> ".ucwords($list['lead_source'])." </br><strong>Status:</strong> ".ucwords($list['lead_type'])."
                        </td>
                        ".$reporting."
                        <td>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            $assign_btn 
                            <br>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/addactivity/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-plus'></em> Add Activity</a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class='btn btn-sm btn-primary'><em class='icon ni ni-eye'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function customerReport($from="",$to="",$customer_id="",$chit_id="",$deposit_id="",$vam_id="")
  	{
  		$layout = "";

		$q = "SELECT C.id,C.branch_id,C.lead_id,C.name as cus_name,C.gender,C.mobile,C.email,C.approved_on,C.assign_status,L.designation,L.has_chit,L.has_deposite,L.has_vas,L.chit_id,L.deposite_id,L.lname,L.vas_id,L.lead_date,L.assign_status,L.added_by,E.name,CM.item_type as chit_name,DM.item_type as deposite_name,VM.item_type as vam_name FROM ".CUSTOMER_TBL." C LEFT JOIN ".LEAD_TBL." L ON (C.lead_id=L.id) LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) LEFT JOIN ".CHIT_MASTER." CM ON (CM.id=L.chit_id) LEFT JOIN ".DEPOSITE_MASTER." DM ON (DM.id=L.deposite_id) LEFT JOIN ".VAS_MASTER." VM ON (VM.id=L.vas_id) WHERE L.lead_date BETWEEN '$from' AND '$to'";

		if ($customer_id!="") {
			$q .=" AND C.id ='$customer_id'  ";
			$dates_array3 = explode(",", $customer_id);
			$h = 0;
			$len3 = count($dates_array3);
			$q .=" AND ";
			foreach ($dates_array3 as $value3) {
				if ($h == $len3 - 1) {
					$or3 = ' ';
				}else{
					$or3 = ' OR ';
				}
				$q .=" C.id='$value3' $or3 ";
				$h++;
			}
		}else{
			$q .=" AND C.branch_id='".$_SESSION['selected_branch']."' ";
		}

		$scheme_checked = array();

		if($chit_id!=""){
			$dates_array = explode(",", $chit_id);
			$i = 0;
			$len = count($dates_array);
			$scheme_checked['chit'] ="(";
			foreach ($dates_array as $value) {
				if ($i == $len - 1) {
					$or = ' ';
				}else{
					$or = ' OR ';
				}
				$scheme_checked['chit'] .=" L.chit_id='$value' $or ";
				$i++;
			}
			$scheme_checked['chit'] .=")";
        }
        if($deposit_id!=""){
        	$dates_array1 = explode(",", $deposit_id);
			$j = 0;
			$len1 = count($dates_array1);
			$scheme_checked['deposit'] ="(";
			foreach ($dates_array1 as $value1) {
				if ($j == $len1 - 1) {
					$or1 = ' ';
				}else{
					$or1 = ' OR ';
				}
				$scheme_checked['deposit'] .=" L.deposite_id='$value1' $or1 ";
				$j++;
			}
			$scheme_checked['deposit'] .=")";
        }
        if($vam_id!=""){
        	$dates_array2 = explode(",", $vam_id);
			$k = 0;
			$len2 = count($dates_array2);
			$scheme_checked['vam'] ="(";
			foreach ($dates_array2 as $value2) {
				if ($k == $len2 - 1) {
					$or2 = ' ';
				}else{
					$or2 = ' OR ';
				}
				$scheme_checked['vam'] .=" L.vas_id='$value2' $or2 ";
				$k++;
			}
			$scheme_checked['vam'] .=")";
        }

        if(count($scheme_checked) > 0) {
			$len = count($scheme_checked);
			$i=0;
        	$scheme_filter = "AND (";
        		foreach ($scheme_checked as $value) {
					if ($i == $len - 1) {
						$or = ' ';
					}else{
						$or = ' OR ';
					}
					$scheme_filter .=" $value $or ";
					$i++;
				}
        	$scheme_filter .= ")";
        } else {
        	$scheme_filter = "";
        }
        $q .= $scheme_filter;

		$q .=" ORDER BY L.lead_date DESC ";

	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$assign_btn = (($list['assign_status']=="0")? "
                    <div class='tb-odr-btns d-none d-md-inline'>
                    <a href='".COREPATH."lead/assignlead/".$list['id']."' class='btn btn-info btn-sm'> Assign</a>
                    </div>" : "<div class='tb-odr-btns d-none d-md-inline'><a href='".COREPATH."lead/reassign/".$list['id']."' class='btn btn-info btn-sm'> Reassign</a></div>" );
                $assign_info = $this->leadAssignTo($list['lead_id']);
                $assigned_to = (($assign_info!="") ? "<a href='".COREPATH."employee/details/".$assign_info['created_to']."' target='_blank' >".ucwords($assign_info['name'])."</a>" : "-" );
                $designation = $list['designation']!="" ? "<em class='icon ni ni-briefcase'></em> ".ucwords($list['designation'])." <br>" : "" ;
                $mobile = $list['mobile']!="" ? $list['mobile'] : "-" ;
                $email = $list['email']!="" ? $list['email'] : "-" ;

                $chit     = $list['has_chit']=='1' ? "<strong>Chit:</strong> ".ucwords($list['chit_name'])."<br>" : '' ;
                $deposite = $list['has_deposite']=='1' ? "<strong>Deposite:</strong> ".ucwords($list['deposite_name'])."<br>" : '' ;
                $vas      = $list['has_vas']=='1' ? "<strong>VAM:</strong> ".ucwords($list['vam_name'])."<br>" : '' ;
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".date("d/m/Y",strtotime($list['lead_date']))."</td>
                        <td><em class='icon ni ni-user'></em><a href='".COREPATH."customer/details/".$list['id']."' target='_blank' class=''> ".ucwords($list['cus_name'].' '.$list['lname'])."</a> <br> ".$designation."  <em class='icon ni ni-mobile'></em> ".$mobile." <br><em class='icon ni ni-mail'></em>  ".$email."</td>
                        <td>".$chit." ".$deposite." ".$vas."</td>
                       
                        <td>".ucwords($assigned_to)."</td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}


  	/*-----------------------------------------
					 Dashboard
	------------------------------------------*/

  	function dashboardStats()
	{	
		$today		  = date("Y-m-d");
		$id 		  = $_SESSION["crm_admin_id"];
		$branch_check = ((isset($_SESSION['selected_branch']))? "branch_id='".$_SESSION['selected_branch']."'" : '1' );

		if($_SESSION['employee_role']==2 || $_SESSION['employee_role']==3) {
			$check_assigned_emp = " AND assigned_employee='".$_SESSION['crm_admin_id']."' ";
		} else {
			$check_assigned_emp = "";
		}

		$query = "SELECT id,					
					(SELECT COUNT(id) as tots FROM ".CUSTOMER_TBL." WHERE ".$branch_check.") as customer_count,
					(SELECT COUNT(id) as tots FROM ".EMPLOYEE." WHERE ".$branch_check." ".$check_assigned_emp." AND super_admin='0' AND role='3') as bm_count,				
					(SELECT COUNT(id) as tots FROM ".EMPLOYEE." WHERE ".$branch_check." ".$check_assigned_emp." AND super_admin='0' AND role!='1' AND role!='2' AND role!='3') as employee_count,				
					(SELECT COUNT(id) as tots FROM ".LEAD_TBL." WHERE employee_id='".$_SESSION['crm_admin_id']."') as employee_lead_count,		
					(SELECT COUNT(id) as tots FROM ".LEAD_TBL." WHERE employee_id='".$_SESSION['crm_admin_id']."' AND closed_status='1') as compleated_lead_count,	
					(SELECT COUNT(id) as tots FROM ".LEAD_TBL." WHERE employee_id='".$_SESSION['crm_admin_id']."' AND lead_status_id='2') as hot_lead_count
				FROM ".EMPLOYEE." WHERE id ='".$id."' ";

		$query_gm = "SELECT branch_id FROM ".EMPLOYEE." WHERE role='2' AND super_admin='0' ";
		$exe_gm   = $this->selectQuery($query_gm);
		$gm_count = 0;
		if($_SESSION['super_admin']==1) {
			if(mysqli_num_rows($exe_gm) > 0) {
				while ($list = mysqli_fetch_assoc($exe_gm)) {
					if(in_array( ((isset($_SESSION['selected_branch']))? $_SESSION['selected_branch'] : '0' )  , explode(",", $list['branch_id']))) {
						$gm_count ++;
					}
				}
			}
		}


		$exe = $this->selectQuery($query);	
		$list = mysqli_fetch_assoc($exe);
		$list['gm_count'] = $gm_count;
		return $list;
	}

	function dashboardCustomer()
  	{
  		$layout = "";
		$branch_check = ((isset($_SESSION['selected_branch']))? "branch_id='".$_SESSION['selected_branch']."'" : '1' );
		$q = "SELECT id,name,uid,gender,mobile,email FROM  ".CUSTOMER_TBL." WHERE $branch_check ORDER BY id DESC LIMIT 5 " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$words = explode(" ", $list['name']);
                $acronym = "";
                foreach ($words as $w) {
                $acronym .= $w[0];
            	}

	    		$layout .= "
	    			 <div class='card-inner card-inner-md'>
                        <div class='user-card'>
                            <div class='user-avatar bg-primary-dim'>
                                <span>".$acronym."</span>
                            </div>
                            <div class='user-info'>
                                <span class='lead-text'>".ucwords($list['name'])."</span>
                                <span class='sub-text'>".$list['email']."</span>
                            </div>
                            <div class='user-action'>
                                <div class='drodown'>
                                    <a href='javascript:void();' class='dropdown-toggle btn btn-icon btn-trigger mr-n1' data-toggle='dropdown' aria-expanded='false'><em class='icon ni ni-more-h'></em></a>
                                    <div class='dropdown-menu dropdown-menu-right'>
                                        <ul class='link-list-opt no-bdr'>
                                            <li><a href='javascript:void();'><em class='icon ni ni-eye'></em><span>View</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getEmployeeLeadList()
  	{
  		$layout = "";
		$q = "SELECT L.id,L.token,L.uid,L.property_id,L.has_chit,L.has_deposite,L.has_vas,L.chit_id,L.deposite_id,L.vas_id,L.lead_date,L.designation,L.fname,L.lname,L.leadsource,L.gender,L.mobile,L.email,L.secondary_mobile,L.secondary_email,L.address,L.city,L.state,L.pincode,L.description,L.lead_status_id,L.assign_status,L.status,L.added_by,L.created_at,L.updated_at ,E.name,T.lead_type,L.closed_status,L.moved_status,L.deleted_status,L.customer_id,C.name as cus_name,LS.lead_source,CM.item_type as chit_name,DM.item_type as deposite_name,VM.item_type as vam_name FROM ".LEAD_TBL." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) LEFT JOIN ".LEAD_TYPE." T ON (T.id=L.lead_status_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=L.customer_id) LEFT JOIN ".LEAD_SOURCE." LS ON (LS.id=L.leadsource) LEFT JOIN ".CHIT_MASTER." CM ON (CM.id=L.chit_id) LEFT JOIN ".DEPOSITE_MASTER." DM ON (DM.id=L.deposite_id) LEFT JOIN ".VAS_MASTER." VM ON (VM.id=L.vas_id) WHERE L.deleted_status='0' AND L.employee_id='".$_SESSION['crm_admin_id']."' ORDER By L.id DESC LIMIT 5 " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                
                $assign_btn = (($list['assign_status']=="0")? "
                	<div class='tb-odr-btns d-none d-md-inline'>
                		<a href='#' class='btn btn-sm btn-info assignLeadModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> Assign</a>
                	</div>" : "<div class='tb-odr-btns d-none d-md-inline'><a href='#' class='btn btn-sm btn-blue reassignLeadModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> Reassign</a></div>" );

                $assign_info = $this->leadAssignTo($list['id']);
                $assigned_to = (($assign_info!="") ? "<a href='".COREPATH."employee/details/".$assign_info['created_to']."' target='_blank' >".ucwords($assign_info['name'])."</a> <br> " : "" );
                $completedbtn = (($list['closed_status']==1) ? "<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='#' class='btn btn-sm btn-info moveLeadToCustomer' data-option='".$this->encryptData($list['id'])."'><em class='icon ni ni-check'></em> Move Lead</a>
                            </div>" : "<a href='#'  class='btn btn-sm btn-primary updateCompletedStatus' data-option='".$this->encryptData($list['id'])."'><em class='icon ni ni-check'></em> <span>Mark as completed</span></a>" );
                $movebtn = (($list['moved_status']==1) ? "<a href='".COREPATH."customer/details/".$list['customer_id']."' target='_blank' ><em class='icon ni ni-user'></em> ".ucwords($list['cus_name'])."</a> " : $completedbtn );

                $deletedbtn = (($list['deleted_status']==0) ? "<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-danger btn-sm deletLead' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
                            </div>" : "" );

                $designation = $list['designation']!="" ? "<em class='icon ni ni-briefcase'></em> ".ucwords($list['designation'])." <br>" : "" ;
                $mobile = $list['mobile']!="" ? $list['mobile'] : "-" ;
                $email = $list['email']!="" ? $list['email'] : "-" ;

                $chit 	  = $list['has_chit']=='1' ? "<span class='tb-amount'>Chit :<span class='currency'> ".ucwords($list['chit_name'])."</span></span><br>" : '' ;
                $deposite = $list['has_deposite']=='1' ? "<span class='tb-amount'>Deposite :<span class='currency'> ".ucwords($list['deposite_name'])."</span></span><br>" : '' ;
                $vas 	  = $list['has_vas']=='1' ? "<span class='tb-amount'>VAM :<span class='currency'> ".ucwords($list['vam_name'])."</span></span><br>" : '' ;

                if(($_SESSION['super_admin'])==1){
                	$assign_column = "<td>".ucwords($assigned_to).' '.$assign_btn."</td>";
                }else{
                	 $assign_column = '';
                }

	    		$layout .= "<div class='nk-tb-item'>
						     <div class='nk-tb-col tb-col-sm'>
						        <span>".$i."</span>
						    </div>
						     <div class='nk-tb-col tb-col-md'>
						        <span><a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class=''>".($list['uid'])."</a></span>
						    </div><div class='nk-tb-col'>
						        <div class='user-card'>
						            <div class='user-info'>
						                <span class='tb-lead'><em class='icon ni ni-user-fill'></em><a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class=''> ".ucwords($list['fname'].' '.$list['lname'])."</a></span></span>
						                <span><em class='icon ni ni-mobile'></em> ".$email."</span><br>
						                <span><em class='icon ni ni-mobile'></em> ".$mobile."</span>
						            </div>
						        </div>
						    </div>
						    <div class='nk-tb-col tb-col-mb'>
						         <div class='user-card'>
						            <div class='user-info'>
						               ".$chit." ".$deposite." ".$vas."
						            </div>
						        </div>
						    </div>
						    <div class='nk-tb-col tb-col-md'>
						        <span class='badge badge-outline-gray'>".ucwords($list['lead_type'])."</span>
						    </div>
						    <div class='nk-tb-col tb-col-md'>
						        <span class='tb-amount'>".ucwords($list['lead_source'])."</span>
						    </div>
						    <div class='nk-tb-col tb-col-lg'>
						        <span>".date("d M, Y",strtotime($list['lead_date']))."</span>
						    </div>
						</div>";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}



  	/*--------------------------------------------- 
				Activity Type
	----------------------------------------------*/

	function manageActivityType()
  	{
  		$layout = "";
		$q = "SELECT A.id,A.token,A.activity_type,A.status,A.added_by,A.created_at,A.added_by,E.name FROM ".ACTIVITY_TYPE." A LEFT JOIN ".EMPLOYEE." E ON (E.id=A.added_by) WHERE 1  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['activity_type'])."</td>
                        <td>".ucwords($list['name'])."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox activityTypeStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editActivityType' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                           
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Lead Type
	----------------------------------------------*/

	function manageLeadType()
  	{
  		$layout = "";
		$q = "SELECT L.id,L.uid,L.token,L.lead_type,L.default_settings,L.status,L.added_by,L.created_at,L.added_by,E.name FROM ".LEAD_TYPE." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) WHERE 1  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                $default_settings = (($list['default_settings']==1) ? '(default Lead)' : '' );
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".$list['id']."</td>
                        <td>".ucwords($list['lead_type'])." $default_settings</td>
                        <td>".ucwords($list['name'])." </td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox leadTypeStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editLeadType' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                           
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Lead Source
	----------------------------------------------*/

	function manageLeadSource()
  	{
  		$layout = "";
		$q = "SELECT L.id,L.token,L.lead_source,L.status,L.added_by,L.created_at,L.added_by,E.name FROM ".LEAD_SOURCE." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) WHERE 1  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".($list['id'])." </td>
                        <td>".ucwords($list['lead_source'])." </td>
                        <td>".ucwords($list['name'])." </td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox leadSourceStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editLeadSource' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Manage Lead
	----------------------------------------------*/

	function manageLead()
  	{
  		$layout = "";
		$branch_check = ((isset($_SESSION['selected_branch']))? "AND L.branch_id='".$_SESSION['selected_branch']."'" : "AND L.employee_id='".$_SESSION['crm_admin_id']."'" );
		$q = "SELECT L.id,L.token,L.uid,L.property_id,L.has_chit,L.has_deposite,L.has_vas,L.chit_id,L.deposite_id,L.vas_id,L.lead_date,L.designation,L.fname,L.lname,L.leadsource,L.gender,L.mobile,L.email,L.secondary_mobile,L.secondary_email,L.address,L.city,L.state,L.pincode,L.description,L.lead_status_id,L.assign_status,L.status,L.added_by,L.created_at,L.updated_at ,E.name,T.lead_type,L.closed_status,L.moved_status,L.deleted_status,L.customer_id,C.name as cus_name,LS.lead_source,CM.item_type as chit_name,DM.item_type as deposite_name,VM.item_type as vam_name FROM ".LEAD_TBL." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) LEFT JOIN ".LEAD_TYPE." T ON (T.id=L.lead_status_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=L.customer_id) LEFT JOIN ".LEAD_SOURCE." LS ON (LS.id=L.leadsource) LEFT JOIN ".CHIT_MASTER." CM ON (CM.id=L.chit_id) LEFT JOIN ".DEPOSITE_MASTER." DM ON (DM.id=L.deposite_id) LEFT JOIN ".VAS_MASTER." VM ON (VM.id=L.vas_id) WHERE L.deleted_status='0' $branch_check " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                
                $assign_btn = (($list['assign_status']=="0")? "
                	<div class='tb-odr-btns d-none d-md-inline'>
                		<a href='#' class='btn btn-sm btn-info assignLeadModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> Assign</a>
                	</div>" : "<div class='tb-odr-btns d-none d-md-inline'><a href='#' class='btn btn-sm btn-blue reassignLeadModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> Reassign</a></div>" );

                $assign_info = $this->leadAssignTo($list['id']);
                $assigned_to = (($assign_info!="") ? "<a href='".COREPATH."employee/details/".$assign_info['created_to']."' target='_blank' >".ucwords($assign_info['name'])."</a> <br> " : "" );
                $completedbtn = (($list['closed_status']==1) ? "<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='#' class='btn btn-sm btn-info moveLeadToCustomer' data-option='".$this->encryptData($list['id'])."'><em class='icon ni ni-check'></em> Move Lead</a>
                            </div>" : "<a href='#'  class='btn btn-sm btn-primary updateCompletedStatus' data-option='".$this->encryptData($list['id'])."'><em class='icon ni ni-check'></em> <span>Mark as completed</span></a>" );
                $movebtn = (($list['moved_status']==1) ? "<a href='".COREPATH."customer/details/".$list['customer_id']."' target='_blank' ><em class='icon ni ni-user'></em> ".ucwords($list['cus_name'])."</a> " : $completedbtn );

                $deletedbtn = (($list['deleted_status']==0) ? "<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-danger btn-sm deletLead' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
                            </div>" : "" );

                $designation = $list['designation']!="" ? "<em class='icon ni ni-briefcase'></em> ".ucwords($list['designation'])." <br>" : "" ;
                $mobile = $list['mobile']!="" ? $list['mobile'] : "-" ;
                $email = $list['email']!="" ? $list['email'] : "-" ;

                $chit 	  = $list['has_chit']=='1' ? "<strong>Chit:</strong> ".ucwords($list['chit_name'])."<br>" : '' ;
                $deposite = $list['has_deposite']=='1' ? "<strong>Deposite:</strong> ".ucwords($list['deposite_name'])."<br>" : '' ;
                $vas 	  = $list['has_vas']=='1' ? "<strong>VAM:</strong> ".ucwords($list['vam_name'])."<br>" : '' ;

                if(($_SESSION['super_admin'])==1 || $_SESSION['employee_role']==2 || $_SESSION['employee_role']==3){
                	$assign_column = "<td>".ucwords($assigned_to).' '.$assign_btn."</td>";
                }else{
                	 $assign_column = '';
                }

	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class=''>".($list['uid'])."</a></td>
                        <td><em class='icon ni ni-user'></em><a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class=''> ".ucwords($list['fname'].' '.$list['lname'])."</a> <br> ".$designation."  <em class='icon ni ni-mobile'></em> ".$mobile." <br><em class='icon ni ni-mail'></em>  ".$email."</td>
                        <td>".$chit." ".$deposite." ".$vas."</td>
                        <td>".ucwords($list['lead_type'])." <br>
                        	<a href='javascript:void();'  class='btn btn-success btn-sm leadStatusModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'><em class='icon ni ni-check'></em> <span>Update Status</span></a>
                        </td>
                        ".$assign_column."
                        <td>".ucwords($list['lead_source'])."</td>                        
                        <td>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                
                                <a href='#' class='btn btn-sm btn-warning addActivityModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'><em class='icon ni ni-plus'></em> Activity</a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/images/".$list['id']."' class='btn btn-sm btn-info'><em class='icon ni ni-plus'></em> Gallery</a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class='btn btn-sm btn-primary'><em class='icon ni ni-eye'></em></a>
                            </div>
                            <br>
                            
                            ".$deletedbtn."
                        </td>
                    </tr>
	    		";
	    		/*
				<a href='".COREPATH."lead/assignlead/".$list['id']."' class='btn btn-info btn-sm'> Assign</a>
				<a href='".COREPATH."lead/reassign/".$list['id']."' class='btn btn-blue btn-sm'> Reassign</a>
	    		<a href='".COREPATH."lead/addactivity/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-plus'></em> Add Activity</a> */
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageFilterLead($leadsource="",$movecustomer="",$assignedstatus="",$assignedto="",$leadstatus="",$chitstatus="",$depositestatus="",$vamstatus="")
  	{
  		$layout = "";
  		$result = array();
		$q = "SELECT L.id,L.token,L.uid,L.designation,L.property_id,L.has_chit,L.has_deposite,L.has_vas,L.chit_id,L.deposite_id,L.vas_id,L.customer_id,L.lead_date,L.fname,L.lname,L.leadsource,L.gender,L.mobile,L.email,L.secondary_mobile,L.secondary_email,L.address,L.city,L.state,L.pincode,L.description,L.lead_status_id,L.assign_status,L.status,L.employee_id,L.added_by,L.created_at,L.updated_at ,E.name,T.lead_type,L.closed_status,L.moved_status,L.deleted_status,L.customer_id,C.name as cus_name,CM.item_type as chit_name,DM.item_type as deposite_name,VM.item_type as vam_name,LS.lead_source FROM ".LEAD_TBL." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) LEFT JOIN ".LEAD_TYPE." T ON (T.id=L.lead_status_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=L.customer_id) LEFT JOIN ".CHIT_MASTER." CM ON (CM.id=L.chit_id) LEFT JOIN ".DEPOSITE_MASTER." DM ON (DM.id=L.deposite_id) LEFT JOIN ".VAS_MASTER." VM ON (VM.id=L.vas_id) LEFT JOIN ".LEAD_SOURCE." LS ON (LS.id=L.leadsource) WHERE 1  " ;
		if($leadsource!=""){
			$q .=" AND L.leadsource='$leadsource' ";
		}
		if($movecustomer!=""){
			$q .=" AND  L.moved_status='$movecustomer' ";
		}
		if($assignedstatus!=""){
			$q .=" AND  L.assign_status='$assignedstatus' ";
		}
		if($assignedto!=""){
			$q .=" AND  L.employee_id='$assignedto' ";
		}
		if($leadstatus!=""){
			$q .=" AND  L.lead_status_id='$leadstatus' ";
		}
		if($chitstatus!=""){
			$q .=" AND  L.chit_id='$chitstatus' ";
		}
		if($depositestatus!=""){
			$q .=" AND  L.deposite_id='$depositestatus' ";
		}
		if($vamstatus!=""){
			$q .=" AND  L.vas_id='$vamstatus' ";
		}

		$q .=" AND L.deleted_status='0' ";
	    $query = $this->selectQuery($q);	
	    $count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");


                $assign_btn = (($list['assign_status']=="0")? "
                	<div class='tb-odr-btns d-none d-md-inline'>
                		<a href='#' class='btn btn-sm btn-info assignLeadModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> Assign</a>
                	</div>" : "<div class='tb-odr-btns d-none d-md-inline'><a href='#' class='btn btn-sm btn-blue reassignLeadModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> Reassign</a></div>" );

                $assign_info = $this->leadAssignTo($list['id']);
                $assigned_to = (($assign_info!="") ? "<a href='".COREPATH."employee/details/".$assign_info['created_to']."' target='_blank' >".ucwords($assign_info['name'])."</a> <br> " : "" );
                $completedbtn = (($list['closed_status']==1) ? "<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='#' class='btn btn-sm btn-info moveLeadToCustomer' data-option='".$this->encryptData($list['id'])."'><em class='icon ni ni-check'></em> Move Lead</a>
                            </div>" : "<a href='#'  class='btn btn-sm btn-primary updateCompletedStatus' data-option='".$this->encryptData($list['id'])."'><em class='icon ni ni-check'></em> <span>Mark as completed</span></a>" );
                $movebtn = (($list['moved_status']==1) ? "<a href='".COREPATH."customer/details/".$list['customer_id']."' target='_blank' ><em class='icon ni ni-user'></em> ".ucwords($list['cus_name'])."</a> " : $completedbtn );

                $deletedbtn = (($list['deleted_status']==0) ? "<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-danger btn-sm deletLead' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
                            </div>" : "" );

                $designation = $list['designation']!="" ? "<em class='icon ni ni-briefcase'></em> ".ucwords($list['designation'])." <br>" : "" ;
                $mobile = $list['mobile']!="" ? $list['mobile'] : "-" ;
                $email = $list['email']!="" ? $list['email'] : "-" ;

                $chit     = $list['has_chit']=='1' ? "<strong>Chit:</strong> ".ucwords($list['chit_name'])."<br>" : '' ;
                $deposite = $list['has_deposite']=='1' ? "<strong>Deposite:</strong> ".ucwords($list['deposite_name'])."<br>" : '' ;
                $vas      = $list['has_vas']=='1' ? "<strong>VAM:</strong> ".ucwords($list['vam_name'])."<br>" : '' ;

                if(($_SESSION['super_admin'])==1){
                	$assign_column = "<td>".ucwords($assigned_to).' '.$assign_btn."</td>";
                }else{
                	 $assign_column = '';
                }

	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class=''>".($list['uid'])."</a></td>
                        <td><em class='icon ni ni-user'></em><a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class=''> ".ucwords($list['fname'].' '.$list['lname'])."</a> <br> ".$designation."  <em class='icon ni ni-mobile'></em> ".$mobile." <br><em class='icon ni ni-mail'></em>  ".$email."</td>
                        <td>".$chit." ".$deposite." ".$vas."</td>
                        <td>".ucwords($list['lead_type'])." <br>
                            <a href='javascript:void();'  class='btn btn-success btn-sm leadStatusModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'><em class='icon ni ni-check'></em> <span>Update Status</span></a>
                        </td>
                        ".$assign_column."
                        <td>".ucwords($list['lead_source'])."</td>
                        
                        <td>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                
                                <a href='#' class='btn btn-sm btn-warning addActivityModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'><em class='icon ni ni-plus'></em> Activity</a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/images/".$list['id']."' class='btn btn-sm btn-info'><em class='icon ni ni-plus'></em> Gallery</a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class='btn btn-sm btn-primary'><em class='icon ni ni-eye'></em></a>
                            </div>
                            <br>
                            
                            ".$deletedbtn."
                        </td>
                    </tr>
	    		";
	    		/*
				<a href='".COREPATH."lead/assignlead/".$list['id']."' class='btn btn-info btn-sm'> Assign</a>
				<a href='".COREPATH."lead/reassign/".$list['id']."' class='btn btn-blue btn-sm'> Reassign</a>
	    		<a href='".COREPATH."lead/addactivity/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-plus'></em> Add Activity</a> */
	    		$i++;
	    	}
	    }
	    $result['layout'] = $layout;
	    $result['count']  = $count;
	    return $result;
  	}

  	function leadAssignTo($lead_id)
  	{
  		$q=" SELECT A.id,A.type,A.created_by,A.created_to,A.ref_id,A.status,A.updated_at,E.name  FROM ".ASSIGN_LEAD." A LEFT JOIN ".EMPLOYEE." E ON (E.id=A.created_to) WHERE A.ref_id='$lead_id' AND A.status='1' ";
  		$exe = $this->selectQuery($q);
  		$list = mysqli_fetch_array($exe);
  		return $list;
  	}

  	function getAssignedEmployeeInfo($lead_id)
  	{
  		$q = "SELECT C.id,C.type,C.created_by,C.created_to,C.priority,C.ref_id,C.remarks,C.status,E.id as emp_id,E.name,
  			(SELECT COUNT(A.id) FROM ".ASSIGN_LEAD." A LEFT JOIN ".LEAD_TBL." CA ON (CA.id=A.ref_id) WHERE A.created_to=emp_id AND A.status='1' AND CA.closed_status='0') as assign_count
  			FROM ".ASSIGN_LEAD." C  LEFT JOIN ".EMPLOYEE." E ON (E.id=C.created_to) WHERE C.ref_id='$lead_id' AND C.status='1'  " ;
  		$exe = $this->selectQuery($q);
  		$list = mysqli_fetch_array($exe);
  		return $list;
  	}


  	// Manage Activity Logs
  	
	function manageActivityLogs($lead_id)
  	{
  		$layout = "";
		$q = "SELECT  L.id,L.date,L.lead_id,L.employee_id,L.activity_id,L.lead_name,L.mobile,L.email,L.remarks,L.status,L.created_at,L.updated_at,E.name,A.activity_type FROM ".LEAD_ACTIVITY." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.employee_id) LEFT JOIN ".ACTIVITY_TYPE." A ON (A.id=L.activity_id) WHERE L.lead_id='$lead_id' ORDER BY L.id ASC " ;
	    $query = $this->selectQuery($q);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		= $this->editPagePublish($details);               
                
	    		$layout .= "
	    					<div class='cd-timeline-block'>
		                        <div class='cd-timeline-img cd-primary'>
		                            <em class='icon ni ni-check'></em>
		                        </div>
		                        <div class='cd-timeline-content editactivity_warp'>
		                            <h3>".ucwords($list['activity_type'])."</h3>
		                          <p> <span>Created By : </span><a href='".COREPATH."employee/details/".$list['employee_id']."' class=''  >".ucwords($list['name'])."</a></p>

		                          <p> <span>Lead Name : </span>".ucwords($list['lead_name'])."</p>
		                          <p> <span>Mobile : </span>".ucwords($list['mobile'])."</p>
		                          <p> <span>Email : </span>".ucwords($list['email'])."</p>

		                          <p><em class='icon ni ni-calendar'></em> ".date("d/m/Y - g:i A",strtotime($list['updated_at']))."</p> 
		                          <p>".$list['remarks']."</p>
		                          <div class='editwarp'>
		                          		<a href='javascript:void();' class='editleadactivity' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
		                          </div>
		                        </div>
		                    </div>
	    				";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageNewActivityLogs($lead_id)
	{
		$layout = "";
	    $q = "SELECT  L.id,L.date,L.lead_id,L.employee_id,L.activity_id,L.remarks,L.status,L.created_at,L.updated_at,E.name,A.activity_type FROM ".LEAD_ACTIVITY." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.employee_id) LEFT JOIN ".ACTIVITY_TYPE." A ON (A.id=L.activity_id) WHERE L.lead_id='$lead_id' ORDER BY L.id DESC " ;
	    $query = $this->selectQuery($q);
    	if(mysqli_num_rows($query) > 0){
      	$i=1;
      	while($list = mysqli_fetch_array($query)){

      		if( $_SESSION["manageemployee"]==1 || $_SESSION['super_admin']==1 || $_SESSION['employee_role'] == 2 || $_SESSION['employee_role'] == 3) {
      			$employee_name_link = "<a href='".COREPATH."employee/details/".$list['employee_id']."' class=''  >".ucwords($list['name'])."</a>";
      			$edit_class			= "editleadactivity";
	      		$tool_tip  			= "";
      		} else {
      			$employee_name_link = ucwords($list['name']);
      			if($_SESSION['crm_admin_id']==$list['employee_id']) {
	      			$edit_class			= "editleadactivity";
	      			$tool_tip  			= "";
	      		} else {
	      			$edit_class			= "";
	      			$tool_tip  			= "data-toggle='tooltip' data-placement='right' title='You notable to edit others activity info'";
	      		} 
      		}
      		
	        $layout .= "<tr>
	                <td>".$i."</td>
	                <td><em class='icon ni ni-calendar'></em> ".date("d/m/Y", strtotime($list['updated_at']))." <br><em class='icon ni ni-clock'></em> ".date("g:i A", strtotime($list['updated_at']))."</td>
	                <td>".ucwords($list['activity_type'])."</td>
	                <td>".$list['remarks']."</td>
	                <td>".$employee_name_link."</td>
	                <td>
	                  <a href='javascript:void();'  class='$edit_class btn btn-info' data-value='".$i."' data-option='".$list['id']."' $tool_tip ><em class='icon ni ni-edit'></em></a>
	                </td>               
	              </tr>"; 
	              /*<a href='javascript:;' name='".$list['import_token']."' class='deleteImportDatass btn btn-danger' ><em class='icon ni ni-trash'></em> Delete Data</a>*/          
	        $i++;     
	      }
	    }
	    return $layout;
	}

	/*--------------------------------------------- 
				Lead Report Filter
	----------------------------------------------*/

	function getGMListDropDown() 
	{
		$layout ="";
        $query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role='2' AND E.assigned_branch!='' AND E.super_admin='0' AND E.status='1' ";
        $exe = $this->selectQuery($query);
        $branch_id = (isset($_SESSION['selected_branch']))? $_SESSION['selected_branch'] : "";  
        if(mysqli_num_rows($exe) > 0){
            $layout ="<option value=''>Select General Manager</option> ";
            while ($list = mysqli_fetch_array($exe)) {
                $current_ids = explode(',', $list['assigned_branch']);
                if(in_array($branch_id, $current_ids)) {
                    $layout.= "<option value='".$list['id']."' >".ucwords($list['name'])."</option>";
                }
            }
        }
        return $layout;
	}

	function getBMListDropDown() 
	{
		$layout ="";
        $query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role='3' AND E.assigned_branch!='' AND assigned_employee='".$_SESSION['crm_admin_id']."' AND E.super_admin='0' AND E.status='1' ";
        $exe = $this->selectQuery($query);
        $branch_id = (isset($_SESSION['selected_branch']))? $_SESSION['selected_branch'] : "";  
        if(mysqli_num_rows($exe) > 0){
            $layout ="<option value=''>Select Branch Manager</option> ";
            while ($list = mysqli_fetch_array($exe)) {
                $current_ids = explode(',', $list['assigned_branch']);
                if(in_array($branch_id, $current_ids)) {
                    $layout.= "<option value='".$list['id']."' >".ucwords($list['name'])."</option>";
                }
            }
        }
        return $layout;
	}

	function getEMPListDropDown() 
	{
		$layout ="";
        $query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role!='1' AND E.role!='2' AND E.role!='3' AND assigned_employee='".$_SESSION['crm_admin_id']."' AND E.super_admin='0' AND E.status='1' ";
        $exe = $this->selectQuery($query);
        $branch_id = (isset($_SESSION['selected_branch']))? $_SESSION['selected_branch'] : "";  
        if(mysqli_num_rows($exe) > 0){
            $layout ="<option value=''>Select Branch Manager</option> ";
            while ($list = mysqli_fetch_array($exe)) {
                    $layout.= "<option value='".$list['id']."' >".ucwords($list['name'])."</option>";
            }
        }
        return $layout;
	}

	/*--------------------------------------------- 
				Customer Profile
	----------------------------------------------*/

	function manageCustomerProfile()
  	{
  		$layout = "";
		$q = "SELECT L.id,L.token,L.customer_profile,L.status,L.added_by,L.created_at,L.added_by,E.name FROM ".CUSTOMER_PROFILE." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) WHERE 1  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".($list['id'])." </td>
                        <td>".ucwords($list['customer_profile'])." </td>
                        <td>".ucwords($list['name'])." </td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox customerProfileStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editCustomerProfile' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Import Alumni Data
	----------------------------------------------*/

	// Data sheets import history

	function getDataSheetList()
	{
		$layout = "";
	    $q = "SELECT * FROM ".DATASHEET_TBL." ORDER BY id ASC ";
	    $query = $this->selectQuery($q);
    	if(mysqli_num_rows($query) > 0){
      	$i=1;
      	while($list = mysqli_fetch_array($query)){
      		$status = (($list['status']==1) ? "<label class='label label-success'>Migrated</label>" : "<label class='label label-danger'>Pending Approval</label>");
      		$display  = (($list['status']==1) ? "no_display" : " ");
      		$updated_at = (($list['status']==1) ? date("d M Y - H:i:s", strtotime($list['updated_at'])) : "NA "); 
      		$count_import = $this -> check_query(TEMPEXCEL_TBL,"id"," upload_token	='".$list['import_token']."' ");
      		$count_approve = $this -> check_query(LEAD_TBL,"id"," import_token	='".$list['import_token']."' ");
      		$duplicate = (($list['status']==1) ? ($count_import - $count_approve) : "NA ");
	        $layout .= "<tr>
	                <td>".$i."</td>
	                <td>".date("d M Y - H:i:s", strtotime($list['created_at']))."</td>
	                <td>".$count_import."</td>
	                <td>".$count_approve."</td>
	                <td>".$duplicate."</td>
	                <td><a  class='btn btn-primary' href='".UPLOADS."datasheet/".$list['uploaded_sheet']."'><span class='icon-file-excel'></span>  Download</a></td> 
	                <td>$status </td>
	                <td>".$updated_at."</td>
	                <td>
	                  <a href='".COREPATH."importdata/approve/".$list['import_token']."' class='btn btn-info $display'  ><span class='icon-checkmark'></span> Approve</a>
	                   <a href='javascript:;' name='".$list['import_token']."' class='deleteImportDatass btn btn-danger' ><em class='icon ni ni-trash'></em> Delete Data</a>
	                </td>               
	              </tr>";           
	        $i++;     
	      }
	    }
	    return $layout;
	}

	// List of Data imported

	function dataImported($token)
	{
		$layout = "";
	    $q = "SELECT * FROM ".TEMPEXCEL_TBL." WHERE upload_token='$token' ";
	    $query = $this->selectQuery($q);
    	if(mysqli_num_rows($query) > 0){
      	$i=1;
      	while($list = mysqli_fetch_array($query)){
	        $layout .= "<tr>
	                <td>".$i."</td>
	                <td>".$list['fname']." ".$list['lname']." </td>
	                <td><strong>Lead Source:</strong> ".ucwords($list['leadsource'])."<br>
	                	<strong>Customer Profile:</strong> ".ucwords($list['profile'])."
	                </td>
	                <td>".ucwords($list['gender'])."</td>
	                <td>".$list['primary_mobile']." </td>
	                <td>".$list['primary_email']."</td>
	                <td>".$list['address'].", ".$list['city'].", ".$list['state']." - ".$list['pincode']."</td>
	                <td>
	                  <a href='javascript:;' name='".$list['id']."' class='deleteSingleRowImportData btn btn-danger' ><em class='icon ni ni-trash'></em></a>
	                </td>               
	              </tr>";           
	        $i++;     
	      }
	    }
	    return $layout;
	}


	/*--------------------------------------------- 
				Customer Import Alumni Data
	----------------------------------------------*/

	// Data sheets import history

	function getCustomerDataSheetList()
	{
		$layout = "";
	    $q = "SELECT * FROM ".CUSTOMERDATASHEET_TBL." ORDER BY id ASC ";
	    $query = $this->selectQuery($q);
    	if(mysqli_num_rows($query) > 0){
      	$i=1;
      	while($list = mysqli_fetch_array($query)){
      		$status = (($list['status']==1) ? "<label class='label label-success'>Migrated</label>" : "<label class='label label-danger'>Pending Approval</label>");
      		$display  = (($list['status']==1) ? "no_display" : " ");
      		$updated_at = (($list['status']==1) ? date("d M Y - H:i:s", strtotime($list['updated_at'])) : "NA "); 
      		$count_import = $this -> check_query(TEMPCUSTOMEREXCEL_TBL,"id"," upload_token	='".$list['import_token']."' ");
      		$count_approve = $this -> check_query(CUSTOMER_TBL,"id"," import_token	='".$list['import_token']."' ");
      		$duplicate = (($list['status']==1) ? ($count_import - $count_approve) : "NA ");
	        $layout .= "<tr>
	                <td>".$i."</td>
	                <td>".date("d M Y - H:i:s", strtotime($list['created_at']))."</td>
	                <td>".$count_import."</td>
	                <td>".$count_approve."</td>
	                <td>".$duplicate."</td>
	                <td><a  class='btn btn-primary' href='".UPLOADS."datasheet/".$list['uploaded_sheet']."'><span class='icon-file-excel'></span>  Download</a></td> 
	                <td>$status </td>
	                <td>".$updated_at."</td>
	                <td>
	                  <a href='".COREPATH."importcustomerdata/approve/".$list['import_token']."' class='btn btn-info $display'  ><span class='icon-checkmark'></span> Approve</a>
	                   <a href='javascript:;' name='".$list['import_token']."' class='deleteCustomerImportDatass btn btn-danger' ><em class='icon ni ni-trash'></em> Delete Data</a>
	                </td>               
	              </tr>";           
	        $i++;     
	      }
	    }
	    return $layout;
	}

	// List of Data imported

	function dataCustomerImported($token)
	{
		$layout = "";
	    $q = "SELECT * FROM ".TEMPCUSTOMEREXCEL_TBL." WHERE upload_token='$token' ";
	    $query = $this->selectQuery($q);
    	if(mysqli_num_rows($query) > 0){
      	$i=1;
      	while($list = mysqli_fetch_array($query)){
	        $layout .= "<tr>
	                <td width='1%'>".$i."</td>
	                <td width='10%'>".$list['name']."  </td>
	                <td width='20%'>".$list['addresss']."</td>
	                <td width='10%'>".$list['mobile']."</td>
	                <td width='10%'>".$list['email']." </td>
	                <td width='40%'>
	                  <a href='javascript:;' name='".$list['id']."' class='deleteCustomerSingleRowImportData btn btn-danger' ><em class='icon ni ni-trash'></em></a>
	                </td>               
	              </tr>";           
	        $i++;     
	      }
	    }
	    return $layout;
	}

	/*--------------------------------------------- 
				Trash Management
	----------------------------------------------*/

	

  	function trashLeads()
  	{
  		$layout = "";
		$q = "SELECT L.id,L.token,L.uid,L.property_id,L.lead_date,L.fname,L.lname,L.leadsource,L.gender,L.mobile,L.email,L.secondary_mobile,L.secondary_email,L.address,L.city,L.state,L.pincode,L.description,L.lead_status_id,L.assign_status,L.status,L.added_by,L.created_at,L.updated_at ,E.name,L.deleted_by FROM ".LEAD_TBL." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.deleted_by) WHERE L.deleted_status='1'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td >".$i."</td>
                        <td><em class='icon ni ni-user'></em> ".ucwords($list['fname'].' '.$list['lname'])." <br><em class='icon ni ni-mobile'></em> ".($list['mobile'])." <br><em class='icon ni ni-mail'></em>  ".($list['email'])."</td>
                        <td>".ucwords($list['leadsource'])."</td>
                        <td>
		                   <a href='".COREPATH."employee/details/".$list['deleted_by']."' target='_blank' >".ucwords($list['name'])."</a>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-info btn-sm undoLead' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-repeat'></span> Undo</a>	
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  

  	/*-------------------------------------
  				Settings
  	---------------------------------------*/

	function manageChitMaster()
  	{
  		$layout = "";
		$q = "SELECT M.id,M.token,M.item_type,M.status,M.added_by,M.created_at,E.name FROM ".CHIT_MASTER." M LEFT JOIN ".EMPLOYEE." E ON (E.id=M.added_by) WHERE 1  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['item_type'])." </td>
                        <td>".ucwords($list['name'])." </td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox chitMasterStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editChitMaster' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageDepositeMaster()
  	{
  		$layout = "";
		$q = "SELECT M.id,M.token,M.item_type,M.status,M.added_by,M.created_at,E.name FROM ".DEPOSITE_MASTER." M LEFT JOIN ".EMPLOYEE." E ON (E.id=M.added_by) WHERE 1  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['item_type'])." </td>
                        <td>".ucwords($list['name'])." </td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox depositeMasterStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editDepositeMaster' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageVASMaster()
  	{
  		$layout = "";
		$q = "SELECT M.id,M.token,M.item_type,M.status,M.added_by,M.created_at,E.name FROM ".VAS_MASTER." M LEFT JOIN ".EMPLOYEE." E ON (E.id=M.added_by) WHERE 1  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['item_type'])." </td>
                        <td>".ucwords($list['name'])." </td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox VASMasterStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editVASMaster' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageBranchMaster()
  	{
  		$layout = "";
		$q = "SELECT B.id,B.token,B.branch,B.status,B.added_by,B.created_at,E.name FROM ".BRANCH_MASTER." B LEFT JOIN ".EMPLOYEE." E ON (E.id=B.added_by) WHERE 1  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['branch'])." </td>
                        <td>".ucwords($list['name'])." </td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox branchMasterStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editBranchMaster' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageEmployeeRoleMaster()
  	{
  		$layout = "";
		$q = "SELECT ER.id,ER.token,ER.employee_role,ER.status,ER.added_by,ER.created_at,E.name FROM ".EMPLOYEE_ROLE." ER LEFT JOIN ".EMPLOYEE." E ON (E.id=ER.added_by) WHERE 1  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['employee_role'])." </td>
                        <td>".ucwords($list['name'])." </td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox employeeRoleMasterStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	// Lead Gallery Images

  	function leadGalleryImages($lead_id)
  	{
  		$layout = "";
  		$result = array();
  		$q = "SELECT id,image FROM ".LEAD_GALLERY." WHERE lead_id='$lead_id' ORDER BY id ASC" ;
  		$query = $this->selectQuery($q);
  		$count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query)>0){
	    	$i=1;
	    	while($list = mysqli_fetch_array($query)){
	    		$layout .= "
    			<li id='pro_$i' class='col-masonry'>
    				<div class='product_image_item'>
    					<img src='".SRCIMG.$list['image']."' class='img-responsive' />
    					<p><a href='javascript:void();' class='removeaddLeadImage' data-option='$i' data-token='".$this->encryptData($list['id'])."' ><i class='icon ni ni-trash' style='color: red;'></i></a> </p>
    					<span class='clearfix'></span>
    				</div>
    			</li>";
    			$i++;
	    	}
	    }
	    $result['layout'] = $layout;
	    $result['count'] = $count ;
	    return $result;

  	}


	/*-----------Dont'delete---------*/

	}


	?>




