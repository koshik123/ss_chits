<?php


class Model
{
	
	/*--------------------------------------------- 
					Base Methods
	----------------------------------------------*/


	function __construct()
	{
		date_default_timezone_set("Asia/Calcutta");
	}
	
	function encryptPassword($data){
		return $encrypted = sha1($data);		
	}
	function decryptPassword($data){
		return $decrypted = sha1($data);
	}
	function encryptData($data){
		return $encrypted = base64_encode(base64_encode($data));		
	}
	function decryptData($data){
		return $decrypted1 = base64_decode(base64_decode($data));
	}
	
	function check_query($table,$column,$where){
		$connect = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
		$query = "SELECT $column FROM $table WHERE $where";
		$exe = mysqli_query($connect,$query);
		$no_rows = @mysqli_num_rows($exe);
		return $no_rows;
	}

	function getDetails($table,$column,$where){
		$connect = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
		$query = "SELECT $column FROM $table WHERE $where";
		$exe = mysqli_query($connect,$query);
		$rows = mysqli_fetch_array($exe);
		return $rows;
	}
	function selectQuery($query){
		$connect = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
		$rows = mysqli_query($connect,$query);
		return $rows;
	}

	function deleteRow($table,$where)
	{
		$q = "DELETE FROM $table WHERE $where ";
		$exe = $this->selectQuery($q);
		if($exe){
			return 1;
		}else{
			return 0;
		}
	}

	 function escapeString($data)
  	{
  		//$escaped = mysqli_real_escape_string($data);
		//$escaped = array_map('htmlentities', $escaped);
		$escaped = ($data);
		return $escaped;
  	}


	function cleanString($data)
  	{
      //$string = str_replace("'", "\'", $data);
      //$string = str_replace('"', '\"', $string);
      $string = trim($data);
      $string = $this->escapeString($string);
      return $string;
  	}

  	function publishContent($data)
  	{
  		 $string = str_replace("\'", "'", $data);
  		 $string = str_replace('\"', '"', $string);
  		 return $string;
  	}
  	 
  	function hyphenize($string) {
   		return preg_replace(
            	array('#[\\s-]+#', '#[^A-Za-z0-9\. -]+#'),
           		array('-', ''),
              urldecode(strtolower($string))
        );
	}

	function masterhyphenize($string) {
   		return preg_replace(
            	array('#[\\s-]+#', '#[^A-Za-z0-9\. -]+#'),
           		/*array('-', ''),*/
           		array('', ''),
              urldecode(strtolower($string))
        );
	}

	function unHyphenize($string) {
   		return ucfirst(str_replace('-', " ", $string));
	}

  	// Random String

  	function generateRandomString($length) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function generateRandomStringGenerator($length) {
	    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	// Editpage Publish Content

	function editPagePublish($data)
 	{
	  $response = array();
	  foreach ($data as $key => $value) {
	   $response[$key] = $this->publishContent($value);
	  }
	  return $response;
 	}
	
	// Function Error Msg

	function errorMsg($value){
		$err = "
				<div class='alert alert-fill alert-danger alert-dismissible alert-icon'>
                    <em class='icon ni ni-cross-circle'></em> <strong>".$value."</strong>! <button class='close' data-dismiss='alert'></button>
                </div>

				";
		return $err;
	}

	function successMsg($value){
		$err = "<div class='alert alert-fill alert-info alert-dismissible alert-icon'>
                    <em class='icon ni ni-check'></em> <strong>".$value."</strong>! <button class='close' data-dismiss='alert'></button>
                </div>";
		return $err;
	}
	
	// Get User Agent

	function get_user_agent() {
		if ( !isset( $_SERVER['HTTP_USER_AGENT'] ) )
		return '-';		
		$ua = strip_tags( html_entity_decode( $_SERVER['HTTP_USER_AGENT'] ));
		$ua = preg_replace('![^0-9a-zA-Z\':., /{}\(\)\[\]\+@&\!\?;_\-=~\*\#]!', '', $ua );			
		return substr( $ua, 0, 254 );
	}

	// Get User IP Address

	function get_IP() {
		$ip = '';
		// Precedence: if set, X-Forwarded-For > HTTP_X_FORWARDED_FOR > HTTP_CLIENT_IP > HTTP_VIA > REMOTE_ADDR
		$headers = array( 'X-Forwarded-For', 'HTTP_X_FORWARDED_FOR', 'HTTP_CLIENT_IP', 'HTTP_VIA', 'REMOTE_ADDR' );
		foreach( $headers as $header ) {
			if ( !empty( $_SERVER[ $header ] ) ) {
				$ip = $_SERVER[ $header ];
				break;
			}
		}		
		// headers can contain multiple IPs (X-Forwarded-For = client, proxy1, proxy2). Take first one.
		if ( strpos( $ip, ',' ) !== false )
			$ip = substr( $ip, 0, strpos( $ip, ',' ) );		
		return $ip;
	}	

	// Change the Date format

	function changeDateFormat($date)
	{
		$array = explode("/", $date);
		$new_date = $array[2]."/".$array[1]."/".$array[0];
		$date 			= date_create($new_date);
		$final_date		= date_format($date,"Y-m-d");
		return $final_date;
	}

	function getActivityType($current="")
	{
		$layout ="";
		$query = "SELECT id,token,activity_type,status  FROM ".ACTIVITY_TYPE." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['activity_type'])."</option>";
			}
		}
		return $layout;
	}

	/*---------	------------------------------------
				Session In and Out
	----------------------------------------------*/ 

	
	// Session In

	function sessionIn($user_type,$finance_type,$id,$referer="",$medium="")
	{		
		$auth_user_agent =	$this->get_user_agent();
		$auth_ip_address =	$this->get_IP();
		$curr 	= date("Y-m-d H:i:s");
		$q 		= "INSERT INTO ".SESSION_TBL." SET logged_id ='".$id."', user_type='$user_type', portal_type='$finance_type', auth_referer='$referer', auth_medium='$medium', auth_user_agent='$auth_user_agent', auth_ip_address='$auth_ip_address', session_in='$curr'  ";
		$exe = $this->selectQuery($q);
		if ($exe) {
			return 1;
		}else{
			return 0;
		}
	}

	// Session Out

	function sessionOut($id)
	{
		$today 	= date("Y-m-d");
		$curr 	= date("Y-m-d H:i:s");
		$info 	= $this->getDetails(SESSION_TBL,"id"," logged_id='".$id."' ORDER BY id DESC LIMIT 1");
		$q 		= "UPDATE ".SESSION_TBL." SET session_out='$curr' WHERE logged_id='".$id."' AND id='".$info['id']."'  ";
		$exe  	= $this->selectQuery($q);
		if($exe) {
			return 1;
		}else{
			return 0;
		}
	}

	
	/*--------------------------------------------- 
					Mail Functions
	----------------------------------------------*/

  	function send_mail($sender,$sender_mail,$receiver_mail,$subject,$message,$bcc=""){

			$mail = new PHPMailer;
            $mail->isSMTP();
            $mail->Port = SMTP_PORT; 
            $mail->Host = MAIL_HOST;
            $mail->SMTPAuth = true;
            $mail->Username = MAIL_USERNAME;
            $mail->Password = MAIL_PASSWORD;
            //$mail->SMTPSecure = 'tls';
            $mail->From = $sender_mail;
            $mail->FromName = COMPANY_NAME;
            $mail->addAddress($receiver_mail, '');
            $mail->AddBCC($bcc, '');
            //$mail->addReplyTo(REPLY_TO, COMPANY_NAME);
            $mail->WordWrap = 50;
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body    = $message;
            if(!$mail->send()) {
			   echo 'Message could not be sent.';
			   echo 'Mailer Error: ' . $mail->ErrorInfo;
			   exit;
			}else{
				return 1;
			}
	}


	
	// Member Create Password

 	function memberLoginInfoTemp($name="",$email_username="",$password="",$user_token="",$id="",$company=""){
 		$token = $this->encryptData($id); 
	    $layout = "
			<div bgcolor='#d9e8f3' style='margin:0;padding:0'>
				<table cellpadding='0' cellspacing='0' border='0' width='100%' bgcolor='#e1e8ed' style='background-color:#d9e8f3;padding:0;margin:0;line-height:1px;font-size:1px'>
					<tbody>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'>							
							</td>
						</tr>
						<tr>
							<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
								<table align='center' width='750' style='width:750px;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td height='10' style='line-height:1px;display:block;height:10px;padding:0;margin:0;font-size:1px'></td>
										</tr>
									</tbody>
								</table>  
								<table align='center' width='750' style='width:750px;background-color:#fff;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table cellpadding='0' cellspacing='0' border='0' width='100%' style='width:100%;padding:0;margin:0;line-height:1px;font-size:1px' align='left'>
													<tbody>
														<tr>
															<td align='left' width='15' style='width:15px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='left' width='160' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																<a href='".COREPATH."' style='text-decoration:none;border-style:none;border:0;padding:0;margin:0;' target='_blank'>
																<img align='left' width='160' src='".COREPATH."resource/uploads/logo.png' style='width:160px;padding-bottom:2px;margin:0;padding:0;display:block;border:none;outline:none' class='CToWUd'></a> 
															</td>
															<td align='left' width='10' style='width:10px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='right' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:5px;margin:0px;font-weight:300;line-height:100%;text-align:right'></td>
														</tr>	
													</tbody>
												</table> 
											</td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:10px 0 0 0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td colspan='2' height='1' style='line-height:1px;display:block;height:1px;background-color:#e1e8ed;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
											<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
													<tbody>
														<tr>
															<td height='30' style='height:30px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px'> <span style='font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:20px;padding:0px;margin:0px;font-weight:600;line-height:100%;text-align:left'>Dear ".$name.",</span> </td>
														</tr>
														<tr>
															<td height='12' style='height:12px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p> Welcome to ".ucwords($company).". </p></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p>Kindly find your account details of your account below.</p></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:600;line-height:23px;text-align:left'> <p>Account Details</p></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p> <br>Email: ".$email_username." <br>Password: ".$password."</p></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'> <br><a href='".BASEPATH."' target='_blank'>Click here to login to you account to view more details </a></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:13px;padding:0px;margin:0px;font-weight:300;line-height:16px;text-align:left'> <br><br><br><p>Regards,<br>
																".COMPANY_NAME."<br>
																Mobile: ".MOBILE." <br>
																Email: ".EMAIL."<br>
																Web: ".WEBSITE."<br>
																Address: ".ADDRESS."</p></td>
														</tr>	
														<tr>
															<td height='40' style='height:40px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														
														
														
													</tbody>
												</table> 
											</td>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>
						 	</td>
						</tr>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
						</tr>
					</tbody>
				</table>
			</div>";
		return $layout;
		/*<tr>
		<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'> <br><a href='".COREPATH."verification/email/".$user_token."/".$token."' target='_blank'>Click here to login </a></td>
	</tr>*/
  	}

	// Forgot Password Tempalate

 	function forgotPasswordTemp($name,$token){
 		
	    $layout = "
			<div bgcolor='#d9e8f3' style='margin:0;padding:0'>
				<table cellpadding='0' cellspacing='0' border='0' width='100%' bgcolor='#e1e8ed' style='background-color:#d9e8f3;padding:0;margin:0;line-height:1px;font-size:1px'>
					<tbody>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'>							
							</td>
						</tr>
						<tr>
							<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
								<table align='center' width='750' style='width:750px;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td height='10' style='line-height:1px;display:block;height:10px;padding:0;margin:0;font-size:1px'></td>
										</tr>
									</tbody>
								</table>  
								<table align='center' width='750' style='width:750px;background-color:#fff;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table cellpadding='0' cellspacing='0' border='0' width='100%' style='width:100%;padding:0;margin:0;line-height:1px;font-size:1px' align='left'>
													<tbody>
														<tr>
															<td align='left' width='15' style='width:15px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='left' width='160' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																<a href='".BASEPATH."' style='text-decoration:none;border-style:none;border:0;padding:0;margin:0;' target='_blank'>
																<img align='left' width='160' src='".BASEPATH."resource/uploads/logo.png' style='width:160px;padding-bottom:2px;margin:0;padding:0;display:block;border:none;outline:none' class='CToWUd'></a> 
															</td>
															<td align='left' width='10' style='width:10px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='right' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:5px;margin:0px;font-weight:300;line-height:100%;text-align:right'></td>
														</tr>	
													</tbody>
												</table> 
											</td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:10px 0 0 0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td colspan='2' height='1' style='line-height:1px;display:block;height:1px;background-color:#e1e8ed;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
											<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
													<tbody>
														<tr>
															<td height='30' style='height:30px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px'> <span style='font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:20px;padding:0px;margin:0px;font-weight:600;line-height:100%;text-align:left'>Dear ".$name.",</span> </td>
														</tr>
														<tr>
															<td height='12' style='height:12px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>


														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p>You have requested for Password Reset for your account. To Reset your password just click the link below and create a new password for your account. </p></td>
														</tr>
														<tr>
														<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																<table border='0' cellspacing='0' cellpadding='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																	<tbody>
																		<tr>
																			<td style='padding:0;margin:0;line-height:1px;font-size:1px'>
																				<table width='100%' border='0' cellspacing='0' cellpadding='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																					<tbody>
																						<tr>
																							<td style='padding:0;margin:0;line-height:1px;font-size:1px'>
																								<table border='0' cellspacing='0' cellpadding='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																									<tbody>
																										<tr>
																											<td align='center' bgcolor='#e16f30' style='padding:0;margin:0;line-height:1px;font-size:1px;border-radius:4px;line-height:18px'>
																												<a href='".BASEPATH."resetpassword/email/".$token."?utm_ref=email&utm_type=reset&utf=$token' style='text-decoration:none;border-style:none;border:0;padding:0;margin:0;font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:22px;font-weight:500;color:#ffffff;text-align:center;text-decoration:none;border-radius:4px;padding:11px 30px;border:1px solid #e16f30;display:inline-block' target='_blank'>
																										 		<strong>Reset Password</strong></a>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																	</tbody>
																</table> 
															</td>
														</tr>
														<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:13px;padding:0px;margin:0px;font-weight:300;line-height:16px;text-align:left'> <br><br><br><p>Regards,<br>
																".COMPANY_NAME."<br>
																Web: www.ushaelectronics.in<br>
																Phone: +91 97884 77000, 76048 70700 <br>
																Email: ADMIN_TBL@ushaelectronics.in<br>
																Address: 71-A, MGR Nagar, 7th Cross,<br>
                    		 											Krishna Colony Extn.,<br>
                    													 Singanallur, Coimbatore - 641 005.</p></td>
														</tr>

														



														<tr>
															<td height='40' style='height:40px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														
														
														
													</tbody>
												</table> 
											</td>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>
						 	</td>
						</tr>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
						</tr>
					</tbody>
				</table>
			</div>";
		return $layout;
  	}

	// Send Push notification GCM

	function sendPushNotification($registration_id,$message)
	{
		$fields = array(
			'registration_ids' 	=> $registration_id,
			'data'			=> $message
		);
		 
		$headers = array
		(
			'Authorization: key='.API_ACCESS_KEY,
			'Content-Type: application/json',
		);
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		return $result;
	}

	// Send Push notification FCM

	function sendPushNotificationFcm($registration_id,$message)
	{
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			'registration_ids'	=> $registration_id,
			'data'			=> $message
		);
		 
		$headers = array
		(
			'Authorization: key='.API_SERVER_KEY,
			'Content-Type: application/json',
		);
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, $url );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		return $result;
	}

	/*-----------------------------------------------
					Excel Export
	------------------------------------------------*/

	// Year List

	function getYearsList($yr="")
	{
		$layout="";
		$c_year = date("Y");
		for ($i=$c_year; $i >2012  ; $i--) { 
			$selected = (($yr==$i) ? 'selected' : '');
			$layout.= "<option value='$i' $selected>$i</option>";
		}
		return $layout;
	}

	/*----------------------------------
			india rupee formate
	-----------------------------------*/

	function indianMoneyFormate($amount) {
     $Arraycheck = array("4"=>"K","5"=>"K","6"=>"L","7"=>"L","8"=>"Cr","9"=>"Cr");
     // define decimal values
     $numberLength = strlen($amount); //count the length of numbers
     if ($numberLength > 3) {
        foreach ($Arraycheck as $Lengthnum=>$unitval) {
            if ($numberLength == $Lengthnum) {
               if ($Lengthnum % 2 == 0) {
                  $RanNumber = substr($amount,1,2);
                  $NmckGtZer = ($RanNumber[0]+$RanNumber[1]);
                  if ($NmckGtZer < 1) { 
                      $RanNumber = "0";
                  } else {
                     if ($RanNumber[1] == 0) {
                        $RanNumber[1] = "0";
                  } 
             }
      $amount = substr($amount,0,$numberLength-$Lengthnum+1) . "." . $RanNumber . " $unitval ";
    } else {
         $RanNumber = substr($amount,2,2);
         $NmckGtZer = ($RanNumber[0]+$RanNumber[1]);
         if ($NmckGtZer < 1) { 
            $RanNumber  = 0;
        } else {
            if ($RanNumber[1] == 0)  {
                $RanNumber[1] = "0";
            }   
        }
         $amount = substr($amount,0,$numberLength-$Lengthnum+2) . ".". $RanNumber . " $unitval";   
       }
     }
	 }
	 } else {
	     '₹ '.$amount;    
	 }
	 return '₹ '.$amount;
	 }


	 //---------------------Permission---------------------//
	
	// Koushik - 05.008.2020

	function pagePermission($page)  {	        
        if ($_SESSION['super_admin']==1) {
            return 1;
        }else{
            switch ($page) {
            	case 'customers':
                    return $_SESSION['customers']  ;
                break;
                case 'managecustomer':
                    return $_SESSION['manage_customer'];
                break;
                case 'importcustomer':
                    return $_SESSION['import_customer'];
                break;

            	case 'lead':
                    return $_SESSION['lead'];
                break;
                case 'managelead':
                    return $_SESSION['manage_lead'];
                break;
                case 'leadtypes':
                    return $_SESSION['lead_type'];
                break;
                case 'activitytype':
                    return $_SESSION['activity_type'];
                break;
                case 'leadsource':
                    return $_SESSION['lead_source'];
                break;
                case 'customerprofile':
                    return $_SESSION['customer_profile'];
                break;
                case 'importlead':
                    return $_SESSION['import_lead'];
                break;

                case 'employee':
                    return $_SESSION['employee'];
                break; 
                case 'manageemployee':
                    return $_SESSION['manageemployee'];
                break; 
                                
                case 'reports':
                    return $_SESSION['reports'];
                break; 
                case 'leadreport':
                    return $_SESSION['lead_report'];
                break; 
                case 'customerreport':
                    return $_SESSION['customer_report'];
                break; 

                case 'settings':
                    return $_SESSION['settings'];
                break; 
                case 'chitmaster':
                    return $_SESSION['chit_master'];
                break; 
                case 'depositmaster':
                    return $_SESSION['deposit_master'];
                break; 
                case 'vammaster':
                    return $_SESSION['vam_master'];
                break; 
                case 'branchmaster':
                    return $_SESSION['branch_master'];
                break;
                case 'employeerolemaster':
                    return $_SESSION['employee_role_master'];
                break;
                
                default:
                    return 0;
                break;
            }
        }
    }


    /*=====================================
				Autocomplete
	======================================*/

	// Employee 

	function employeeAutoComplete($string)
	{
		$a_json = array();
		$string_decode = urldecode($string);
		$q = "SELECT E.id,E.employee_uid,E.name,E.role,E.gender,E.email,E.mobile,E.address,E.city,E.state,E.pincode,E.status FROM ".EMPLOYEE." E WHERE E.Status='1' AND E.super_admin!='1' AND ((E.name LIKE '$string_decode%') OR (E.mobile LIKE '$string_decode%') OR (E.email LIKE '$string_decode%') OR (E.role LIKE '$string_decode%') ) ORDER BY E.id ASC LIMIT 10";
		$exe = $this->selectQuery($q);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$a_json[] = array(
					'name'    			=> strip_tags(html_entity_decode($this->publishContent(ucwords($list['name']).' ('.$list['role'].') '.' - '.$list['mobile']), ENT_QUOTES, 'UTF-8')),
					'id' 				=> $list['id'],
					'emp_name' 			=> ucwords($list['name']),
					'gender' 			=> ucwords($list['gender']),					
					'mobile' 			=> ($list['mobile']),
					'email' 			=> ($list['email']),
					'address' 			=> ucwords($list['address']).', '.ucwords($list['city']).', '.ucwords($list['state']).' - '.$list['pincode'],
					
				); 
			}
		}else{
			$a_json[] = array(
					'id'   		=> 0,
					'name'     	=> "No Employee Found."
				); 
		}
		return $a_json;
	}

	// Reassign Employee 

	function reassignEmployeeAutoComplete($string,$emp_id)
	{
		$a_json = array();
		$string_decode = urldecode($string);
		$em_id = urldecode($emp_id);
		$q = "SELECT E.id,E.employee_uid,E.name,E.role,E.gender,E.email,E.mobile,E.address,E.city,E.state,E.pincode,E.status  FROM ".EMPLOYEE." E WHERE E.status='1' AND E.super_admin!='1' AND E.id!='$em_id' AND ((E.name LIKE '$string_decode%') OR (E.mobile LIKE '$string_decode%') OR (E.email LIKE '$string_decode%') OR (E.role LIKE '$string_decode%') ) ORDER BY E.id ASC LIMIT 10";
		$exe = $this->selectQuery($q);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$a_json[] = array(
					'name'    			=> strip_tags(html_entity_decode($this->publishContent(ucwords($list['name']).' ('.$list['role'].') '.' - '.$list['mobile']), ENT_QUOTES, 'UTF-8')),
					'id' 				=> $list['id'],
					'emp_name' 			=> ucwords($list['name']),
					'gender' 			=> ucwords($list['gender']),					
					'mobile' 			=> ($list['mobile']),
					'email' 			=> ($list['email']),
					'address' 			=> ucwords($list['address']).', '.ucwords($list['city']).', '.ucwords($list['state']).' - '.$list['pincode'],
					
				); 
			}
		}else{
			$a_json[] = array(
					'id'   		=> 0,
					'name'     	=> "No Employee Found."
				); 
		}
		return $a_json;
	}


	function leadAssignToReport($lead_id)
  	{
  		$q=" SELECT A.id,A.type,A.created_by,A.created_to,A.ref_id,A.status,A.updated_at,E.name  FROM ".ASSIGN_LEAD." A LEFT JOIN ".EMPLOYEE." E ON (E.id=A.created_to) WHERE A.ref_id='$lead_id' AND A.status='1' ";
  		$exe = $this->selectQuery($q);	
  		$list = mysqli_fetch_array($exe);
  		return $list;
  	}

  	// Auto Complete Customer Json

	function searchCustomerJson($string)
	{
		$a_json = array();
		$string_decode = urldecode($string);
		$q = "SELECT  id,uid,priority,property_id,name,mobile,email,ad_city FROM ".CUSTOMER_TBL." WHERE status='1' AND ((name LIKE '$string_decode%') OR (mobile LIKE '$string_decode%') OR (email LIKE '$string_decode%') ) ORDER BY id ASC LIMIT 10";
		$exe = $this->selectQuery($q);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$a_json[] = array(
					'id'    		=> strip_tags(html_entity_decode($this->publishContent($list['id']), ENT_QUOTES, 'UTF-8')),
					'name'    			=> strip_tags(html_entity_decode($this->publishContent(ucwords($list['name']).' ('.$list['mobile'].') '.' - '.$list['email']), ENT_QUOTES, 'UTF-8')),
					'category_name' 		=> $this->publishContent("Select Customer")
				); 
			}
		}else{
			$a_json[] = array(
					'id'    		=> 0,
					'name'      			=> "No Customers Found",
					'category_name' 		=> $this->publishContent("Select Customers")
				); 
		}
		return $a_json;
	}

	function searchEditCustomerJson($string,$cust_ids)
	{
		$a_json = array();
		$string_decode = urldecode($string);
		$customer_ids = urldecode($cust_ids);
		$q = "SELECT  id,uid,priority,property_id,name,mobile,email,ad_city FROM ".CUSTOMER_TBL." WHERE status='1' AND ((name LIKE '$string_decode%') OR (mobile LIKE '$string_decode%') OR (email LIKE '$string_decode%') ) AND id NOT IN ($customer_ids) ORDER BY id ASC LIMIT 10";
		$exe = $this->selectQuery($q);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$a_json[] = array(
					'id'    		=> strip_tags(html_entity_decode($this->publishContent($list['id']), ENT_QUOTES, 'UTF-8')),
					'name'    			=> strip_tags(html_entity_decode($this->publishContent(ucwords($list['name']).' ('.$list['mobile'].') '.' - '.$list['email']), ENT_QUOTES, 'UTF-8')),
					'category_name' 		=> $this->publishContent("Select Customer")
				); 
			}
		}else{
			$a_json[] = array(
					'id'    		=> 0,
					'name'      			=> "No Customers Found",
					'category_name' 		=> $this->publishContent("Select Customers")
				); 
		}
		return $a_json;
	}


	function getEmployeeList($current="")
	{
		$layout ="";
		$branch_check = ((isset($_SESSION['selected_branch']))? "AND branch_id='".$_SESSION['selected_branch']."'" : "" );
		$query = "SELECT id,name,status  FROM ".EMPLOYEE." WHERE role!='1' AND role !='2' AND role!='3' AND status='1' $branch_check ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['name'])."</option>";
			}
		}
		return $layout;
	}

	function getLeadStatus($current="")
	{
		$layout ="";
		$query = "SELECT id,token,lead_type,status  FROM ".LEAD_TYPE." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['lead_type'])."</option>";
			}
		}
		return $layout;
	}

}


?>