<?php
	
	define("SESSION_TBL","session_tbl");
	define("ADMIN_TBL","admin_tbl"); 
	define("CUSTOMER_TBL","customer_tbl"); 	

	define("EMPLOYEE","employee_tbl"); 
	define("PERMISSION","employee_permission_tbl"); 

	define("LEAD_TBL","lead_tbl");
	define("ASSIGN_LEAD","assignlead_tbl"); 
	define("LEAD_LOGS","lead_logs_tbl");  
	define("LEAD_ACTIVITY","lead_activity_tbl");
	
	define("ACTIVITY_TYPE","activity_type_tbl");
	define("LEAD_TYPE","lead_type_tbl");
	define("LEAD_SOURCE","lead_source_tbl");
	define("CUSTOMER_PROFILE","customer_profile_tbl");
	define("CHIT_MASTER","chit_master_tbl");
	define("DEPOSITE_MASTER","deposite_master_tbl");
	define("VAS_MASTER","vas_master_tbl");
	define("BRANCH_MASTER","branch_master_tbl");
	define("EMPLOYEE_ROLE","employee_role_master_tbl");
	define("LEAD_GALLERY","lead_gallery_tbl"); 

	define("TEMPEXCEL_TBL","temp_excel_tbl");
	define("DATASHEET_TBL","datasheet_tbl");

	define("TEMPCUSTOMEREXCEL_TBL","temp_customer_excel_tbl");
	define("CUSTOMERDATASHEET_TBL","customerdatasheet_tbl");

	

?>