<?php

	//---------------------  WEBSITE SETTINGS --------------------//

	// Mobile
	define('MOBILE', '+91 12345 12345');
	// Email
	define('EMAIL', 'postsalecrm@mail.com'); 
	// Website
	define('WEBSITE', 'www.postsalecrm.com'); 
	// Address
	define('ADDRESS', '95A, Vyshnav Building,<br>Race Course,<br>Coimbatore - 641018.');
	

	//--------------------- FRONT WEBSITE SETTINGS --------------------//

	// Footer Content
	define('FOOTER_CONTENT', ''); 
	// Footer Address
	define('FOOTER_ADDRESS', '');
	// Footer Phone
	define('FOOTER_PHONE', ''); 

	//--------------------- SOCIAL LINKS --------------------//

	// Facebook Link
	define('FACEBOOK_LINK', '');
	// Twitter Link
	define('TWITTER_LINK', '');
	// Google Plus Link
	define('GOOGLEPLUS_LINK', '');
	// Linkedin Link
	define('LINKEDIN_LINK', '');
	// Youtube Link
	define('YOUTUBE_LINK', '');
	// Instagram Link
	define('INSTAGRAM_LINK', '');
	
	



?>