<?php 
session_start();
//require_once '../config/config.php';
require_once '../app/core/user_ajaxcontroller.php';
require_once '../app/excel/PHPExcel.php';
$route = new Ajaxcontroller();
//$client_id 	=  @$_REQUEST["client_id"];

$from	 	 = @$_GET['from'];
$to	 		 = @$_GET['to'];
$emp_id 	 = @$_GET['employee_id'];
$lead_status = @$_GET['status'];
$chit_id 	 = @$_GET['chit_id'];
$deposit_id  = @$_GET['deposit_id'];
$vam_id 	 = @$_GET['vam_id'];
$gm_id 	     = @$_GET['general_manager_id'];
$bm_id  	 = @$_GET['branch_manager_id'];

$today = date("d-m-y");


$title = "lead_report_".$today;


	//------------- Create Header ------------//

	// PHPExcel Instance
	$sheet = new PHPExcel();
	// Set document properties
	$sheet->getProperties()->setCreator("Post CRM")
	               ->setLastModifiedBy(COMPANY_NAME)
	               ->setTitle("Lead Report")
	               ->setSubject("Lead Report")
	               ->setDescription("Lead Report")
	               ->setKeywords("Post CRM")
	               ->setCategory("Excel Report");
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$sheet->setActiveSheetIndex(0);
	$activeSheet = $sheet->getActiveSheet();
	
	// Create Header
	$sheet->setActiveSheetIndex(0)
				->setCellValue('A1', 'S.No')
	           	->setCellValue('B1', 'Date')
	            ->setCellValue('C1', 'Name')
	            ->setCellValue('D1', 'Designation')
	            ->setCellValue('E1', 'Chit')
	            ->setCellValue('F1', 'Deposit')
	            ->setCellValue('G1', 'VAM')
	            ->setCellValue('H1', 'Mobile')
	            ->setCellValue('I1', 'Email')
	            ->setCellValue('J1', 'Assigned To')
	            ->setCellValue('K1', 'Lead Source')
	            ->setCellValue('L1', 'Lead Status')
	            ->setCellValue('M1', 'City')
	            ->setCellValue('N1', 'State')
	            ->setCellValue('O1', 'Description');

	$activeSheet->getColumnDimension('A')->setAutoSize(true);
	$activeSheet->getColumnDimension('B')->setAutoSize(true);
	$activeSheet->getColumnDimension('C')->setAutoSize(true);
	$activeSheet->getColumnDimension('D')->setAutoSize(true);
	$activeSheet->getColumnDimension('E')->setAutoSize(true);
	$activeSheet->getColumnDimension('F')->setAutoSize(true);
	$activeSheet->getColumnDimension('G')->setAutoSize(true);
	$activeSheet->getColumnDimension('H')->setAutoSize(true);
	$activeSheet->getColumnDimension('I')->setAutoSize(true);
	$activeSheet->getColumnDimension('J')->setAutoSize(true);
	$activeSheet->getColumnDimension('K')->setAutoSize(true);
	$activeSheet->getColumnDimension('L')->setAutoSize(true);
	$activeSheet->getColumnDimension('M')->setAutoSize(true);
	$activeSheet->getColumnDimension('N')->setAutoSize(true);
	$activeSheet->getColumnDimension('O')->setAutoSize(true);

	//------------- Create Header Ends ------------//

	//------------- Create Report Body ------------//

  	$today = date("Y-m-d");
	$users = array();

	function getEmployeesIds($id,$ids_for)
  	{
  		$employee_ids =array();

  		if($ids_for=="general_manager") {
  			$query1 = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role='3' AND assigned_employee='".$id."' AND E.super_admin='0' AND E.status='1' ";
  			$branch_manager_ids =array();
  			$exe1 = $route->selectQuery($query1);  
	        if(mysqli_num_rows($exe1) > 0){
	            while ($list = mysqli_fetch_array($exe1)) {
	                 $branch_manager_ids[] = $list['id'];
	            }
	        }

	        if(count($branch_manager_ids)) {
	        	$bm_condition = " AND assigned_employee IN (".implode(",", $branch_manager_ids).") "; 
	        } else {
	        	$bm_condition = " AND assigned_employee=NULL ";
	        }

	        $query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role!='2' AND E.role!='3' $bm_condition  AND E.super_admin='0' AND E.status='1' ";


  		} else {
  			$query = "SELECT E.id,E.name,E.status,E.role,E.assigned_branch FROM ".EMPLOYEE." E WHERE E.role!='1' AND E.role!='2' AND E.role!='3'  AND assigned_employee='".$id."' AND E.super_admin='0' AND E.status='1' ";
  		}
        
        $exe = $route->selectQuery($query);
        if(mysqli_num_rows($exe) > 0){
            while ($list = mysqli_fetch_array($exe)) {
                    $employee_ids[] = $list['id'];
            }
        }

        if(count($employee_ids)) {
        	$condition = " AND L.employee_id IN (".implode(",", $employee_ids).") "; 
        } else {
        	$condition = " AND L.employee_id=NULL ";
        }

        return $condition ;
  	}

	if($_SESSION['super_admin']==1 || $_SESSION['employee_role']==2 || $_SESSION['employee_role']==3) {
		if($gm_id!="") {
			if($gm_id!="" && $bm_id=="") {
				$employee_filter = $route->getEmployeesIds($gm_id,$ids_for="general_manager");
				$leads_filter = $employee_filter;

			} elseif($gm_id!="" && $bm_id!="" && $emp_id=="") {
				
				$employee_filter = $route->getEmployeesIds($bm_id,$ids_for="branch_manager");
				$leads_filter = $employee_filter;

			} elseif($gm_id!="" && $bm_id!="" && $emp_id!="") {
				
				$leads_filter = "AND L.employee_id='$emp_id'";

			}
		} else {
    		$leads_filter = "";
		}
    } else {
    	$leads_filter = "AND L.employee_id='$emp_id'";
    }

	
	$q = "SELECT L.id,L.token,L.import_token,L.uid,L.designation,L.auth_type,L.has_chit,L.has_deposite,L.has_vas,L.chit_id,L.deposite_id,L.vas_id,L.property_id,L.lead_date,L.fname,L.lname,L.leadsource,
		L.gender,L.mobile,L.email,L.secondary_mobile,L.secondary_email,L.address,L.city,L.state,L.pincode,L.description,L.lead_status_id,L.assign_status,L.closed_status,L.status,L.added_by,L.created_at,E.name,T.lead_type,LS.lead_source,CM.item_type as chit_name,DM.item_type as deposite_name,VM.item_type as vam_name FROM ".LEAD_TBL." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) LEFT JOIN ".LEAD_TYPE." T ON (T.id=L.lead_status_id) LEFT JOIN ".LEAD_SOURCE." LS ON (LS.id=L.leadsource) LEFT JOIN ".CHIT_MASTER." CM ON (CM.id=L.chit_id) LEFT JOIN ".DEPOSITE_MASTER." DM ON (DM.id=L.deposite_id) LEFT JOIN ".VAS_MASTER." VM ON (VM.id=L.vas_id) WHERE L.lead_date BETWEEN '$from' AND '$to' $leads_filter   " ;
		if ($lead_status!="") {
			$q .=" AND L.lead_status_id='$lead_status' ";
		}
		
		$scheme_checked = array();

		if($chit_id!=""){
			$dates_array = explode(",", $chit_id);
			$i = 0;
			$len = count($dates_array);
			$scheme_checked['chit'] ="(";
			foreach ($dates_array as $value) {
				if ($i == $len - 1) {
					$or = ' ';
				}else{
					$or = ' OR ';
				}
				$scheme_checked['chit'] .=" L.chit_id='$value' $or ";
				$i++;
			}
			$scheme_checked['chit'] .=")";
        }
        if($deposit_id!=""){
        	$dates_array1 = explode(",", $deposit_id);
			$j = 0;
			$len1 = count($dates_array1);
			$scheme_checked['deposit'] ="(";
			foreach ($dates_array1 as $value1) {
				if ($j == $len1 - 1) {
					$or1 = ' ';
				}else{
					$or1 = ' OR ';
				}
				$scheme_checked['deposit'] .=" L.deposite_id='$value1' $or1 ";
				$j++;
			}
			$scheme_checked['deposit'] .=")";
        }
        if($vam_id!=""){
        	$dates_array2 = explode(",", $vam_id);
			$k = 0;
			$len2 = count($dates_array2);
			$scheme_checked['vam'] ="(";
			foreach ($dates_array2 as $value2) {
				if ($k == $len2 - 1) {
					$or2 = ' ';
				}else{
					$or2 = ' OR ';
				}
				$scheme_checked['vam'] .=" L.vas_id='$value2' $or2 ";
				$k++;
			}
			$scheme_checked['vam'] .=")";
        }

        if(count($scheme_checked) > 0) {
			$len = count($scheme_checked);
			$i=0;
        	$scheme_filter = "AND (";
        		foreach ($scheme_checked as $value) {
					if ($i == $len - 1) {
						$or = ' ';
					}else{
						$or = ' OR ';
					}
					$scheme_filter .=" $value $or ";
					$i++;
				}
        	$scheme_filter .= ")";
        } else {
        	$scheme_filter = "";
        }
        $q .= $scheme_filter;
		
	$q .=" ORDER BY L.lead_date DESC ";
	$exe = $route->selectQuery($q);	
    $row_count = mysqli_num_rows($exe);
    if(mysqli_num_rows($exe) > 0){
    	$i=1;
    	while($list = mysqli_fetch_array($exe)){
    			$assign_info = $route->leadAssignToReport($list['id']);
                $assigned_to = (($assign_info!="") ? ucwords($assign_info['name']) : "-" );	
                $chit     = $list['has_chit']=='1' ? "Chit:".ucwords($list['chit_name']) : '-' ;
                $deposite = $list['has_deposite']=='1' ? "Deposite:".ucwords($list['deposite_name']) : '-' ;
                $vas      = $list['has_vas']=='1' ? "VAM:".ucwords($list['vam_name']) : '-' ;

    			$element 	=  array();
				$element[] 	=  $i;
				$element[] 	=  date("d/m/Y",strtotime($list['lead_date']));
				$element[] 	=  ucwords($list['fname']);
				$element[] 	=  ucwords($list['designation']);
				$element[] 	=  $chit;
				$element[] 	=  $deposite;
				$element[] 	=  $vas;
				$element[] 	=  ($list['mobile']);
				$element[] 	=  ($list['email']);
				$element[] 	=  ucwords($assigned_to);
				$element[] 	=  ucwords($list['lead_source']);
				$element[] 	=  ucwords($list['lead_type']);
				$element[] 	=  ucwords($list['city']);
				$element[] 	=  ucwords($list['state']);
				$element[] 	=  ($list['description']);
				$users[] 	=  $element;
			  	$i++;
    	}
    }

    $row = 2;
	foreach ($users as $key => $value) {
		$col = 0;
	    foreach ($value as $key=> $value) {
	        //echo $row." $col -- ".$key."=".$value."<br/>";
	        $sheet->getActiveSheet()->setCellValueByColumnAndRow($key, $row, $value);
	        $col++;
	   }
	   $row++;
	}

	//------------- Create Report Body Ends------------//

	// Rename worksheet
	
	$sheet_name = "Lead Report";
	
	$activeSheet ->setTitle($sheet_name);
	$filename = $title.".csv";
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename='.$filename);
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	//ob_end_clean();
	$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'CSV');
	$objWriter->save('php://output');
?>