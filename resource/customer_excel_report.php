<?php 
session_start();
//require_once '../config/config.php';
require_once '../app/core/user_ajaxcontroller.php';
require_once '../app/excel/PHPExcel.php';
$route = new Ajaxcontroller();

//$from	 	= @$_GET['from'];

$today = date("d-m-y");


$title = "customer_report_".$today;



	//------------- Create Header ------------//

	// PHPExcel Instance
	$sheet = new PHPExcel();
	// Set document properties
	$sheet->getProperties()->setCreator("Post CRM")
	               ->setLastModifiedBy(COMPANY_NAME)
	               ->setTitle("Customer Report")
	               ->setSubject("Customer Report")
	               ->setDescription("Customer Report")
	               ->setKeywords("Post CRM")
	               ->setCategory("Customer Report");
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$sheet->setActiveSheetIndex(0);
	$activeSheet = $sheet->getActiveSheet();
	
	// Create Header
	$sheet->setActiveSheetIndex(0)
				->setCellValue('A1', 'S.No')
	           	->setCellValue('B1', 'Date')
	           	->setCellValue('C1', 'Branch')
	           	->setCellValue('D1', 'UID')
	            ->setCellValue('E1', 'Name')
	            ->setCellValue('F1', 'Gender')
	            ->setCellValue('G1', 'Email')
	            ->setCellValue('H1', 'Mobile')
	            ->setCellValue('I1', 'Address')
	            ->setCellValue('J1', 'City')
	            ->setCellValue('K1', 'Priority');

	$activeSheet->getColumnDimension('A')->setAutoSize(true);
	$activeSheet->getColumnDimension('B')->setAutoSize(true);
	$activeSheet->getColumnDimension('C')->setAutoSize(true);
	$activeSheet->getColumnDimension('D')->setAutoSize(true);
	$activeSheet->getColumnDimension('E')->setAutoSize(true);
	$activeSheet->getColumnDimension('F')->setAutoSize(true);
	$activeSheet->getColumnDimension('G')->setAutoSize(true);
	$activeSheet->getColumnDimension('H')->setAutoSize(true);
	$activeSheet->getColumnDimension('I')->setAutoSize(true);
	$activeSheet->getColumnDimension('J')->setAutoSize(true);
	$activeSheet->getColumnDimension('K')->setAutoSize(true);

	//------------- Create Header Ends ------------//

	//------------- Create Report Body ------------//

  	$today = date("Y-m-d");
	$users = array();
	$branch_check = ((isset($_SESSION['selected_branch']))? "C.branch_id='".$_SESSION['selected_branch']."'" : '1' );
	$q = "SELECT C.id,C.branch_id,C.name,C.uid,C.gender,C.property_id,C.allotment_date,C.mobile,C.email,C.address,C.city,C.state ,C.pincode,C.kyc_status,C.status,C.assign_status,C.priority,C.created_at,P.title,b.branch   FROM ".CUSTOMER_TBL." C LEFT JOIN ".PROPERTY_TBL." P ON (P.id=C.property_id) LEFT JOIN ".BRANCH_MASTER." B ON (B.id=C.branch_id) WHERE $branch_check  " ;
	$q .=" ORDER BY C.id DESC ";
	$exe = $route->selectQuery($q);	
    $row_count = mysqli_num_rows($exe);
    if(mysqli_num_rows($exe) > 0){
    	$i=1;
    	while($list = mysqli_fetch_array($exe)){
    			//$assign_info = $route->leadAssignToReport($list['id']);
                $property_name = (($list['property_id']!="") ? ucwords($list['title']) : "-" );	
                $kyc_status1   = (($list['kyc_status']=='0') ? 'Not submitted' : $list['kyc_status']  );
                $kyc_status2   = (($list['kyc_status']=='1') ? 'Approved' : $kyc_status1 );
                $kyc_status3   = (($list['kyc_status']=='2') ? 'Pending Approval' : $kyc_status2 );
                $kyc_status    = (($list['kyc_status']=='3') ? 'Rejected' : $kyc_status3 );
                $assign_status = (($list['assign_status']!=1) ? 'Assign' : 'Unassign' );


    			$element 	=  array();
				$element[] 	=  $i;
				$element[] 	=  date("d-m-Y",strtotime($list['created_at']));
				$element[] 	=  ($list['branch']);
				$element[] 	=  ($list['uid']);
				$element[] 	=  ucwords($list['name']);
				$element[] 	=  (($list['gender']=="male")? "Male" : "Female");
				$element[] 	=  ($list['email']);
				$element[] 	=  ($list['mobile']);
				$element[] 	=  ucwords($list['address']);
				$element[] 	=  ucwords($list['city']);
				$element[] 	=  ucwords($list['priority']);
				$users[] 	=  $element;
			  	$i++;
    	}
    }

    $row = 2;
	foreach ($users as $key => $value) {
		$col = 0;
	    foreach ($value as $key=> $value) {
	        //echo $row." $col -- ".$key."=".$value."<br/>";
	        $sheet->getActiveSheet()->setCellValueByColumnAndRow($key, $row, $value);
	        $col++;
	   }
	   $row++;
	}

	//------------- Create Report Body Ends------------//

	// Rename worksheet
	
	$sheet_name = "Customer Report";
	
	$activeSheet ->setTitle($sheet_name);
	$filename = $title.".csv";
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename='.$filename);
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	//ob_end_clean();
	$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'CSV');
	$objWriter->save('php://output');
?>