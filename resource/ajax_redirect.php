<?php 

session_start();
/*require_once '../config/config.php';*/
require_once '../app/core/user_ajaxcontroller.php';
require_once '../app/excel/PHPExcel.php';
$route = new Ajaxcontroller();


$page = @$_REQUEST["page"];
$data = @$_REQUEST["element"];
$term = @$_REQUEST["term"];
$post = @$_POST["result"];

// Get Client IP Address

switch($page){

	
	/*-------------------------------------------------------------------------------------
								User Management
	---------------------------------------------------------------------------------------*/

	case 'userLogin':		
		echo $route -> userLogin($_POST);
	break;	
	case 'changePassword':		
		echo $route -> changePassword($_POST);
	break;
	case 'editProfile':		
		echo $route -> editProfile($_POST);
	break;

	/*-------------------------------------------------------------------------------------
								Customer Management
	---------------------------------------------------------------------------------------*/

	case 'addCustomer':		
		echo $route -> addCustomer($_POST);
	break;	
	case 'editCustomer':		
		echo $route -> editCustomer($_POST);
	break;	
	case 'CustomerStatus':	
		echo $route->CustomerStatus($post);
	break;	


	/*============================
			  Employee Info
	==============================*/	

	case 'addEmployee':
		echo $route -> addEmployee($_POST);
	break;
	case 'editEmployee':
		echo $route -> editEmployee($_POST);
	break;
	case 'employeeStatus':	
		echo $route->employeeStatus($post);
	break;	
	case 'employeePermissions': 
		echo $route->employeePermissions($_POST);		
	break;
	case 'changepasswordEmployee':
		$array 	=  $route -> changepasswordEmployee($post);
		echo json_encode($array);
	break;
	case 'empChangePassword':
		$array 	=  $route -> empChangePassword($_POST);
		echo json_encode($array);
	break;


	/*============================
			Activity type
	==============================*/	

	case 'addActivityType':
		echo $route -> addActivityType($_POST);
	break;
	case 'activityTypeStatus':	
		echo $route->activityTypeStatus($post);
	break;	
	case 'editActivityType':
		$array 	=  $route -> editActivityType($post);
		echo json_encode($array);
	break;
	case 'updateActivityType':
		$array 	=  $route -> updateActivityType($_POST);
		echo json_encode($array);
	break;	

	/*============================
			Lead Type
	==============================*/	

	case 'addLeadType':
		echo $route -> addLeadType($_POST);
	break;
	case 'leadTypeStatus':	
		echo $route->leadTypeStatus($post);
	break;	
	case 'editLeadType':
		$array 	=  $route->editLeadType($post);
		echo json_encode($array);
	break;
	case 'updateLeadType':
		$array 	=  $route->updateLeadType($_POST);
		echo json_encode($array);
	break;	

	/*============================
			Lead Source
	==============================*/	

	case 'addLeadSource':
		echo $route -> addLeadSource($_POST);
	break;
	case 'leadSourceStatus':	
		echo $route->leadSourceStatus($post);
	break;	
	case 'editLeadSource':
		$array 	=  $route->editLeadSource($post);
		echo json_encode($array);
	break;
	case 'updateLeadSource':
		$array 	=  $route->updateLeadSource($_POST);
		echo json_encode($array);
	break;	

	/*============================
			Customer Profile
	==============================*/	

	case 'addCustomerProfile':
		echo $route -> addCustomerProfile($_POST);
	break;
	case 'customerProfileStatus':	
		echo $route->customerProfileStatus($post);
	break;	
	case 'editCustomerProfile':
		$array 	=  $route->editCustomerProfile($post);
		echo json_encode($array);
	break;
	case 'updateCustomerProfile':
		$array 	=  $route->updateCustomerProfile($_POST);
		echo json_encode($array);
	break;	


	/*---------------------
			Lead Gallery
	-----------------------*/

	case 'removeaddLeadImage':
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(LEAD_GALLERY,"image", " id= '$id' ");
		if ($info['image']!="") {
			unlink("uploads/srcimg/".$info['image']);
		}
		echo $route -> removeaddLeadImage($id);
	break;

	

	/*-------------------------------------------------------------------------------------
							Delet Option
	---------------------------------------------------------------------------------------*/



	case 'deletLead':
		echo $route -> deletLead($post);
	break;
	case 'undoLead':
		$array 	=  $route->undoLead($post);
		echo json_encode($array);
	break;
	case 'updateLead':
		$array 	=  $route->updateLead($_POST);
		echo json_encode($array);
	break;	

	

	case 'deleteGallery':
		echo $route -> deleteGallery($post);
	break;
	case 'undoGallery':
		$array 	=  $route->undoGallery($post);
		echo json_encode($array);
	break;
	case 'updateGallery':
		$array 	=  $route->updateGallery($_POST);
		echo json_encode($array);
	break;


	

	case 'leadReport':
    $format_date1    = $route->changeDateFormat($_POST['from_date']);
    $date1           = date_create($format_date1);
    $new_from        = date_format($date1,"Y-m-d");
    $format_date2    = $route->changeDateFormat($_POST['to_date']);
    $date2           = date_create($format_date2);
    $new_to          = date_format($date2,"Y-m-d");


    $general_manager_id = isset($_POST['gm_id'])? $_POST['gm_id'] : "";
    $branch_manager_id  = isset($_POST['bm_id'])? $_POST['bm_id'] : "";
    $employee_id  	    = isset($_POST['employee_id'])? $_POST['employee_id'] : "";
    $leadstatus_id      = $_POST['leadstatus_id'];

    if(isset($_POST['chit_id'])){
     	$chit_array     = $_POST['chit_id'];
     	$chit_id 		= implode(',', $chit_array);
    }else{
     	$chit_id     	 = '';
    }
    if(isset($_POST['deposit_id'])){
     	$deposit_array   = $_POST['deposit_id'];
     	$deposit_id 	 = implode(',', $deposit_array);
    }else{
     	$deposit_id     	 = '';
    }
    if(isset($_POST['vam_id'])){
     	$vam_array     = $_POST['vam_id'];
     	$vam_id 		= implode(',', $vam_array);
    }else{
     	$vam_id     	 = '';
    }

    echo $new_from."`".$new_to."`".$employee_id."`".$leadstatus_id."`".$chit_id."`".$deposit_id."`".$vam_id."`".$general_manager_id."`".$branch_manager_id;
	break;

	case 'customerReport':
    $format_date1    = $route->changeDateFormat($_POST['from_date']);
    $date1           = date_create($format_date1);
    $new_from        = date_format($date1,"Y-m-d");
    $format_date2    = $route->changeDateFormat($_POST['to_date']);
    $date2           = date_create($format_date2);
    $new_to          = date_format($date2,"Y-m-d");

    if(isset($_POST['customer_id'])){
     	$customer_array     = $_POST['customer_id'];
     	$customer_id 		= implode(',', $customer_array);
    }else{
     	$customer_id     	= '';
    }

    if(isset($_POST['chit_id'])){
     	$chit_array     = $_POST['chit_id'];
     	$chit_id 		= implode(',', $chit_array);
    }else{
     	$chit_id     	= '';
    }
    if(isset($_POST['deposit_id'])){
     	$deposit_array   = $_POST['deposit_id'];
     	$deposit_id 	 = implode(',', $deposit_array);
    }else{
     	$deposit_id     	 = '';
    }
    if(isset($_POST['vam_id'])){
     	$vam_array     = $_POST['vam_id'];
     	$vam_id 		= implode(',', $vam_array);
    }else{
     	$vam_id     	 = '';
    }

    echo $new_from."`".$new_to."`".$customer_id."`".$chit_id."`".$deposit_id."`".$vam_id;
	break;

	/*-------------------------------------------------------------------------------------
								Lead Management
	---------------------------------------------------------------------------------------*/

	case 'addLead':		
		echo $route -> addLead($_POST);
	break;	
	case 'editLead':		
		echo $route -> editLead($_POST);
	break;	
	case 'leadStatus':	
		echo $route->leadStatus($post);
	break;	
	case 'leadStatusModel':
		$array 	=  $route->leadStatusModel($post);
		echo json_encode($array);
	break;
	case 'assignLead':
		echo $route -> assignLead($_POST);
	break;
	case 'assignLeadModel':
		$array 	=  $route->assignLeadModel($post);
		echo json_encode($array);
	break;
	case 'reassignLeadModel':
		$array 	=  $route->reassignLeadModel($post);
		echo json_encode($array);
	break;
	case 'reassignLead':		
		echo $route -> reassignLead($_POST);
	break;

	case 'updateLeadToCustomerModel':
		$array 	=  $route->updateLeadToCustomerModel($post);
		echo json_encode($array);
	break;
	case 'updateCompletedStatus':	
		echo $route->updateCompletedStatus($post);
	break;	
	/*case 'moveLeadToCustomer':	
		echo $route->moveLeadToCustomer($post);
	break;	*/
	case 'moveLeadToCustomer':
		echo $route -> moveLeadToCustomer($_POST);
	break;


	//Lead Activity
	case 'addActivityModel':
		$array 	=  $route->addActivityModel($post);
		echo json_encode($array);
	break;
	case 'addActivity':		
		echo $route -> addActivity($_POST);
	break;
	case 'addleadActivity':		
		echo $route -> addleadActivity($_POST);
	break;	
	case 'editActivity':		
		echo $route -> editActivity($_POST);
	break;	

	/*============================
		Lead Report Filter
	==============================*/

	case'getBMListForGM':
		echo $route->getBMListForGM($post);
	break;

	case'getEmployeeListForBM':
		echo $route->getEmployeeListForBM($post);
	break;

	/*============================
		Customer Report Filter
	==============================*/

	case'getCustomerListForBranch':
		echo $route->getCustomerListForBranch($post);
	break;



		
	case 'updateLeadStatus':
		$array 	=  $route -> updateLeadStatus($_POST);
		echo json_encode($array);
	break;	

	case 'editleadactivity':
		$array 	=  $route -> editleadactivity($post);
		echo json_encode($array);
	break;
	case 'updateLeadActivity':
		$array 	=  $route -> updateLeadActivity($_POST);
		echo json_encode($array);
	break;	
		
	case 'sendcustomer_mail':	
		echo $route->sendcustomer_mail($post);
	break;	

	/*-----------------------*/

	case 'employeeAutoComplete':
		$data = $route -> employeeAutoComplete($_REQUEST['filter_name']);
		echo json_encode($data);
	break;

	case 'reassignEmployeeAutoComplete':
		$data = $route -> reassignEmployeeAutoComplete($_REQUEST['filter_name'],$_REQUEST['emp_id']);
		echo json_encode($data);
	break;

	case 'uploadExcel':
		
		if(!empty($_FILES["excelsheet"]["type"])) 
		{
			$errors = array();
			$msg = array();
			$validextensions = array("xls", "xlsx");
		    $temporary = explode(".", $_FILES["excelsheet"]["name"]); 
		    $file_extension = end($temporary);

		   /* if ((($_FILES["excelsheet"]["type"] == "application/vnd.ms-excel" || $_FILES["excelsheet"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) && ($_FILES["excelsheet"]["size"] < 9000000) && in_array($file_extension, $validextensions))
			{*/
				if ($_FILES["excelsheet"]["error"] > 0)
				{
		           $errors[] = "Return Code: " . $_FILES["excelsheet"]["error"] . "<br/><br/>";
		        } 
				else 
				{ 
					if (file_exists("uploads/datasheet/". $_FILES["excelsheet"]["name"])) {
		            	$err =  $_FILES["excelsheet"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
		            	$errors[] = $err;
					} 
					else 
					{	
						$rands = $route -> generateRandomString("8");
						$token = $route -> generateRandomString("15");
						$curr 	= date("Y-m-d H:i:s");
						$date = date("d-m-Y");
						$sourcePath = $_FILES["excelsheet"]['tmp_name'];
						$excel_name = $date."_".$rands."_".$_FILES["excelsheet"]['name'];
						$targetPath = "uploads/datasheet/".$date."_".$rands."_".$_FILES["excelsheet"]['name'];
						move_uploaded_file($sourcePath,$targetPath) ;						
						$objReader = PHPExcel_IOFactory::createReader('Excel2007');
						$objReader->setReadDataOnly(true);
						$objPHPExcel 	= $objReader->load("uploads/datasheet/".$date."_".$rands."_".$_FILES["excelsheet"]['name']);
						$worksheet 		= $objPHPExcel->getActiveSheet();
						$worksheetTitle     = $worksheet->getTitle();
					    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
					    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
					    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
					    $nrColumns = ord($highestColumn) - 64;
					    for ($row = 2; $row <= $highestRow; $row++ ) {
						    $val=array();
							for ($col = 0; $col < $highestColumnIndex; ++ $col) {
							   $cell = $worksheet->getCellByColumnAndRow($col, $row);
							   $val[] = $route->cleanString($cell->getValue());
							}
							//$address = strtolower($val[7]).", ".strtolower($val[8]);
							//$date_formate = explode("-", $val[4]);

							$phpexcepDate = $val[1]-25569;
							$enquiry_date      	= date("Y-m-d", strtotime("+$phpexcepDate days", mktime(0,0,0,1,1,1970)) );

							//$phpexcepDate1 = $val[11]-25569;
							//$site_visit_date      	= date("Y-m-d", strtotime("+$phpexcepDate1 days", mktime(0,0,0,1,1,1970)) );
							/*$mobile  	= explode(",", $val[13]);
							$mobile_1	= ((isset($mobile[1])) ? $mobile[1] : "" );*/

							//$profile_pic = (($val[1]!="") ? strtolower($val[1]).".png" : "");

							//$enquiry_array 	= explode("/", $val[1]);
							//$enquiry_new_date 	= $enquiry_array[2]."-".$enquiry_array[1]."-".$enquiry_array[0];
							//$final_enquiry_date = date('Y-m-d', strtotime($enquiry_new_date));
							

							/*$site_visit_array 		= explode("/", $val[11]);
							$new_date 				= $site_visit_array[2]."-".$site_visit_array[1]."-".$site_visit_array[0];
							$final_site_visit_date 	= date('Y-m-d', strtotime($new_date));*/



							//$date = 'July 25 2010';
							//date('d/m/Y', strtotime($date));

							$q = "INSERT INTO ".TEMPEXCEL_TBL."  SET 
									upload_token 	= '$token',
									branch_id  		= '".$_SESSION['selected_branch']."',
									enquiry_date 	= '".$enquiry_date."',
									fname 			= '".$route->cleanString($val[2])."',
									lname 			= '".$route->cleanString($val[3])."',
									gender 			= '".$route->cleanString($val[4])."',
									primary_mobile 	= '".$route->cleanString($val[5])."',
									primary_email 	= '".$route->cleanString($val[6])."',
									profile 		= '".$route->cleanString($val[7])."',
									leadsource 		= '".$route->cleanString($val[8])."',
									lead_status 	= '".$route->cleanString($val[9])."',
									chit 			= '".$route->cleanString($val[10])."',
									deposite  		= '".$route->cleanString($val[11])."',
									vas 			= '".$route->cleanString($val[12])."',
									address 		= '".$route->cleanString($val[13])."',
									city 			= '".$route->cleanString($val[14])."',
									state 			= '".$route->cleanString($val[15])."',
									pincode  		= '".$route->cleanString($val[16])."',
									description 	= '".$route->cleanString($val[17])."',
									created_at 		= '$curr',	 
									updated_at 		= '$curr',
									status 			= '0' ";
							//echo $q;
							$exe = $route->selectQuery($q);
						}
						$output =  "1`".$excel_name."`".$token;
					}				       
		        }        
		   /* }else{
		        $errmsg =  "Invaild File. Please Enter Only Excel Sheet with the Fields provided in the sample Excel.";
		        $errors[] = $errmsg;
		    }*/

			// Output

		    if(count($errors)==0){
				echo $output;
			}else{
				$op = "";
				foreach ($errors as $value) {
					$op .= $value;
				}
				echo "0`".$op;
			}
		}	
	break;
	case 'saveExcelSession':
		echo $route -> saveExcelSession($data);
	break;
	case 'deleteImportData':
		$info = $route->getDetails(DATASHEET_TBL,"uploaded_sheet", " import_token= '$data' ");
		unlink("uploads/datasheet/".$info['uploaded_sheet']);
		echo $route -> deleteImportData($data);
	break;
	case 'approveImportData':
		echo $route -> approveImportData($data);
	break;
	case 'deleteSingleRowImportData':
		echo $route -> deleteSingleRowImportData($data);
	break;


	case 'uploadCustomerExcel':
		
		if(!empty($_FILES["excelsheet"]["type"])) 
		{
			$errors = array();
			$msg = array();
			$validextensions = array("xls", "xlsx");
		    $temporary = explode(".", $_FILES["excelsheet"]["name"]); 
		    $file_extension = end($temporary);

		   /* if ((($_FILES["excelsheet"]["type"] == "application/vnd.ms-excel" || $_FILES["excelsheet"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) && ($_FILES["excelsheet"]["size"] < 9000000) && in_array($file_extension, $validextensions))
			{*/
				if ($_FILES["excelsheet"]["error"] > 0)
				{
		           $errors[] = "Return Code: " . $_FILES["excelsheet"]["error"] . "<br/><br/>";
		        } 
				else 
				{ 
					if (file_exists("uploads/datasheet/". $_FILES["excelsheet"]["name"])) {
		            	$err =  $_FILES["excelsheet"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
		            	$errors[] = $err;
					} 
					else 
					{	
						$rands = $route -> generateRandomString("8");
						$token = $route -> generateRandomString("15");
						$curr 	= date("Y-m-d H:i:s");
						$date = date("d-m-Y");
						$sourcePath = $_FILES["excelsheet"]['tmp_name'];
						$excel_name = $date."_".$rands."_".$_FILES["excelsheet"]['name'];
						$targetPath = "uploads/datasheet/".$date."_".$rands."_".$_FILES["excelsheet"]['name'];
						move_uploaded_file($sourcePath,$targetPath) ;						
						$objReader = PHPExcel_IOFactory::createReader('Excel2007');
						$objReader->setReadDataOnly(true);
						$objPHPExcel 	= $objReader->load("uploads/datasheet/".$date."_".$rands."_".$_FILES["excelsheet"]['name']);
						$worksheet 		= $objPHPExcel->getActiveSheet();
						$worksheetTitle     = $worksheet->getTitle();
					    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
					    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
					    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
					    $nrColumns = ord($highestColumn) - 64;
					    for ($row = 2; $row <= $highestRow; $row++ ) {
						    $val=array();
							for ($col = 0; $col < $highestColumnIndex; ++ $col) {
							   $cell = $worksheet->getCellByColumnAndRow($col, $row);
							   $val[] = $route->cleanString($cell->getValue());
							}
							//$address = strtolower($val[7]).", ".strtolower($val[8]);
							//$date_formate = explode("-", $val[4]);

							//$phpexcepDate = $val[2]-25569;
							//$dob      = date("Y-m-d", strtotime("+$phpexcepDate days", mktime(0,0,0,1,1,1970)) );
							//$mobile  = explode(",", $val[13]);
							//$mobile_1 = ((isset($mobile[1])) ? $mobile[1] : "" );

							//$profile_pic = (($val[1]!="") ? strtolower($val[1]).".png" : "");

							$q = "INSERT INTO ".TEMPCUSTOMEREXCEL_TBL."  SET 
									upload_token 	= '$token',
									branch_id  		= '".$_SESSION['selected_branch']."',
									name 			= '".$route->cleanString($val[1])."',
									addresss 		= '".$route->cleanString($val[2])."',
									mobile 			= '".$route->cleanString($val[3])."',
									email 			= '".$route->cleanString($val[4])."',
									created_at 		= '$curr',	 
									updated_at 		= '$curr',
									status 			= '0' ";
							$exe = $route->selectQuery($q);
						}
						$output =  "1`".$excel_name."`".$token;
					}				       
		        }        
		   /* }else{
		        $errmsg =  "Invaild File. Please Enter Only Excel Sheet with the Fields provided in the sample Excel.";
		        $errors[] = $errmsg;
		    }*/

			// Output

		    if(count($errors)==0){
				echo $output;
			}else{
				$op = "";
				foreach ($errors as $value) {
					$op .= $value;
				}
				echo "0`".$op;
			}
		}	
	break;
	case 'saveCustomerExcelSession':
		echo $route -> saveCustomerExcelSession($data);
	break;
	case 'deleteCustomerImportDatass':
		$info = $route->getDetails(CUSTOMERDATASHEET_TBL,"uploaded_sheet", " import_token= '$data' ");
		unlink("uploads/datasheet/".$info['uploaded_sheet']);
		echo $route -> deleteCustomerImportDatass($data);
	break;
	case 'approveCustomerImportData':
		echo $route -> approveCustomerImportData($data);
	break;
	case 'deleteCustomerSingleRowImportData':
		echo $route -> deleteCustomerSingleRowImportData($data);
	break;


	case 'searchCustomerJson':
		$data = $route -> searchCustomerJson($_REQUEST['filter_name']); 
		echo json_encode($data);
	break;

	case 'searchEditCustomerJson':
		$data = $route -> searchEditCustomerJson($_REQUEST['filter_name'],$_REQUEST['cust_ids']); 
		echo json_encode($data);
	break;

	/*============================
			Settings
	==============================*/	

	case 'addChitMaster':
		echo $route ->addChitMaster($_POST);
	break;
	case 'editChitMaster':
		$array 	=  $route->editChitMaster($post);
		echo json_encode($array);
	break;
	case 'updateChitMaster':
		$array 	=  $route->updateChitMaster($_POST);
		echo json_encode($array);
	break;	
	case 'chitMasterStatus':	
		echo $route->chitMasterStatus($post);
	break;	


	case 'addDepositeMaster':
		echo $route ->addDepositeMaster($_POST);
	break;
	case 'editDepositeMaster':
		$array 	=  $route->editDepositeMaster($post);
		echo json_encode($array);
	break;
	case 'updateDepositeMaster':
		$array 	=  $route->updateDepositeMaster($_POST);
		echo json_encode($array);
	break;	
	case 'depositeMasterStatus':	
		echo $route->depositeMasterStatus($post);
	break;	

	case 'addVASMaster':
		echo $route ->addVASMaster($_POST);
	break;
	case 'editVASMaster':
		$array 	=  $route->editVASMaster($post);
		echo json_encode($array);
	break;
	case 'updateVASMaster':
		$array 	=  $route->updateVASMaster($_POST);
		echo json_encode($array);
	break;	
	case 'VASMasterStatus':	
		echo $route->VASMasterStatus($post);
	break;	

	// Branch Master

	case 'addBranchMaster':
		echo $route ->addBranchMaster($_POST);
	break;
	case 'editBranchMaster':
		$array 	=  $route->editBranchMaster($post);
		echo json_encode($array);
	break;
	case 'updateBranchMaster':
		$array 	=  $route->updateBranchMaster($_POST);
		echo json_encode($array);
	break;	
	case 'branchMasterStatus':	
		echo $route->branchMasterStatus($post);
	break;	

	// Employee Role Master

	case 'addEmployeeRoleMaster':
		echo $route ->addEmployeeRoleMaster($_POST);
	break;
	case 'editEmployeeRoleMaster':
		$array 	=  $route->editEmployeeRoleMaster($post);
		echo json_encode($array);
	break;
	case 'updateEmployeeRoleMaster':
		$array 	=  $route->updateEmployeeRoleMaster($_POST);
		echo json_encode($array);
	break;	
	case 'employeeRoleMasterStatus':	
		echo $route->employeeRoleMasterStatus($post);
	break;	

	// GM List For add branch manager

	case 'getGmListForAssign':
	 	echo $route->getGmListForAssign($post);
	break; 

	// Branch sellect function

	case 'setSellectedBranch':
		echo $route->setSellectedBranch($post);
	break;

	case 'branchNotAssigned' :
		echo $route->branchNotAssigned();
	break;

	/*-----------------------------------------------------
						Manage Employee
	------------------------------------------------------*/

	// Get Employee Info to assign branch

	case 'assignBranchEmpInfo' :
		$array = $route->assignBranchEmpInfo($post);
		echo json_encode($array);
	break;

	// Assign branch to Gm

	case 'assignBranch':
		$array 	=  $route->assignBranch($_POST);
		echo json_encode($array);
	break;


	// Lead Filter

	case 'leadFilter':
    $escapedPost 	= $_POST;
    $leadsource 	= $escapedPost['leadsource'];
    $movecustomer 	= $escapedPost['movecustomer'];
    $assignedstatus = $escapedPost['assignedstatus'];
    $assignedto     = $escapedPost['assignedto'];
    $leadstatus     = $escapedPost['leadstatus'];

    $chitstatus     = $escapedPost['chitstatus'];
    $depositestatus = $escapedPost['depositestatus'];
    $vamstatus      = $escapedPost['vamstatus'];
    
    echo $leadsource."`".$movecustomer."`".$assignedstatus."`".$assignedto."`".$leadstatus."`".$chitstatus."`".$depositestatus."`".$vamstatus;
	break;

	// Adhoc Filter

	case 'adhocFilter':
    $escapedPost 	= $_POST;
    $requeststatus 	= $escapedPost['requeststatus'];
    $request_id 	= $escapedPost['request_id'];
    $priority 	= $escapedPost['priority'];
    
    echo $requeststatus."`".$request_id."`".$priority;
	break;

	// Property Filter

	case 'propertyFilter':
    $escapedPost 	= $_POST;
    $facing = $escapedPost['facing'];
    $block 	= $escapedPost['block'];
    $floor 	= $escapedPost['floor'];
    $room 	= $escapedPost['room'];
    
    echo $facing."`".$block."`".$floor."`".$room;
	break;


	/*-------------------------------------------------------------------------------------
								Test Mail
	---------------------------------------------------------------------------------------*/

	case 'sendMail':
		$sender = COMPANY_NAME;
		$sender_mail = NO_REPLY;
		$receiver = "webykart.com@gmail.com";
		$subject = "Reset Password Mail";
		$email_temp = "test1234567879";
		echo $route->send_mail($sender,$sender_mail,$receiver,$subject,$email_temp,$bcc="");
	break;

}	
	

?>