<?php
    session_start();
    require_once '../config/config.php';
    require_once '../app/core/user_ajaxcontroller.php';
    require_once '../app/excel/PHPExcel.php';
    $route = new Ajaxcontroller();
    $content = $_POST;
    
    if(!empty($_FILES["excelsheet"]["type"])) 
        {
            $errors = array();
            $msg = array();
            $validextensions = array("xls", "xlsx");
            $temporary = explode(".", $_FILES["excelsheet"]["name"]); 
            $file_extension = end($temporary);

            if ((($_FILES["excelsheet"]["type"] == "application/vnd.ms-excel" || $_FILES["excelsheet"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) && ($_FILES["excelsheet"]["size"] < 9000000) && in_array($file_extension, $validextensions))
            {
                if ($_FILES["excelsheet"]["error"] > 0)
                {
                   $errors[] = "Return Code: " . $_FILES["excelsheet"]["error"] . "<br/><br/>";
                } 
                else 
                { 
                    if (file_exists("uploads/datasheet/". $_FILES["excelsheet"]["name"])) {
                        $err =  $_FILES["excelsheet"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
                        $errors[] = $err;
                    } 
                    else 
                    {   
                        $rands = $route -> generateRandomString("8");
                        $token = $route -> generateRandomString("15");
                        $curr   = date("Y-m-d H:i:s");
                        $date = date("d-m-Y");
                        $sourcePath = $_FILES["excelsheet"]['tmp_name'];
                        $excel_name = $date."_".$rands."_".$_FILES["excelsheet"]['name'];
                        $targetPath = "uploads/datasheet/".$date."_".$rands."_".$_FILES["excelsheet"]['name'];
                        move_uploaded_file($sourcePath,$targetPath) ;                       
                        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                        $objReader->setReadDataOnly(true);
                        $objPHPExcel    = $objReader->load("uploads/datasheet/".$date."_".$rands."_".$_FILES["excelsheet"]['name']);
                        $worksheet      = $objPHPExcel->getActiveSheet();
                        $worksheetTitle     = $worksheet->getTitle();
                        $highestRow         = $worksheet->getHighestRow(); // e.g. 10
                        $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
                        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                        $nrColumns = ord($highestColumn) - 64;
                        for ($row = 2; $row <= $highestRow; $row++ ) {
                            $val=array();
                            for ($col = 0; $col < $highestColumnIndex; ++ $col) {
                               $cell = $worksheet->getCellByColumnAndRow($col, $row);
                               $val[] = $route->cleanString($cell->getValue());
                            }
                            /*$address = strtolower($val[7]).", ".strtolower($val[8]);
                            $date_formate = explode("-", $val[4]);
                            $phpexcepDate = $val[4]-25569;
                            $dob      = date("Y-m-d", strtotime("+$phpexcepDate days", mktime(0,0,0,1,1,1970)) );
                            $mobile  = explode(",", $val[13]);
                            $mobile_1 = ((isset($mobile[1])) ? $mobile[1] : "" );
                            $q = "INSERT INTO ".TEMPEXCEL_TBL."  SET 
                                    upload_token    = '$token',
                                    t_rollno        = '".str_replace("-", "", $route->hyphenize($val[0]))."',       
                                    t_degree        = '".$route->hyphenize($val[1])."',  
                                    t_course        = '".$route->hyphenize($val[2])."',
                                    t_name          = '".ucfirst(strtolower($val[3]))."',
                                    dob             = '".$dob."',
                                    t_year          = '2016',
                                    t_address       = '".ucfirst($address)."',
                                    t_city          = '".$route->hyphenize($val[9])."',
                                    t_state         = '".$route->hyphenize($val[10])."',
                                    t_country       = '".$route->hyphenize($val[11])."',
                                    pincode         = '".$val[12]."',
                                    t_mobile        = '".$mobile[0]."',
                                    otherphone      = '".$mobile_1."',
                                    t_email         = '".$val[14]."',                                   
                                    created_at      = '$curr',   
                                    updated_at      = '$curr',
                                    t_status        = '0' ";        
                            $exe = mysql_query($q);*/
                        }
                        $output =  "1`".$excel_name."`".$token;
                    }                      
                }        
            }else{
                $errmsg =  "Invaild File. Please Enter Only Excel Sheet with the Fields provided in the sample Excel.";
                $errors[] = $errmsg;
            }

            // Output

            if(count($errors)==0){
                echo $output;
            }else{
                $op = "";
                foreach ($errors as $value) {
                    $op .= $value;
                }
                echo "0`".$op;
            }
        }



?>