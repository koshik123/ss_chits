-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2021 at 02:42 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ss_chit_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_type_tbl`
--

CREATE TABLE `activity_type_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `activity_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_type_tbl`
--

INSERT INTO `activity_type_tbl` (`id`, `branch_id`, `token`, `activity_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'coldcall', 'Cold Call', 1, 1, '2020-08-12 22:00:16', '2020-10-08 12:32:18'),
(2, 1, 'follow-up-1', 'Follow Up 1', 1, 1, '2020-08-13 09:50:59', '2020-08-17 19:40:01'),
(3, 1, 'follow-up-2', 'Follow Up 2', 1, 1, '2020-08-17 18:41:51', '2020-08-17 19:40:06'),
(4, 1, 'inprocess', 'In Process', 1, 1, '2020-08-17 18:41:57', '2020-10-08 12:32:33'),
(5, 1, 'completed', 'completed', 1, 1, '2020-08-17 18:42:02', '2020-08-17 19:40:54'),
(6, 1, 'notattnthecall', 'Not attn the call', 1, 8, '2020-10-09 15:01:23', '2020-10-09 15:01:23'),
(7, 1, 'lowbudget', 'Low budget', 1, 8, '2020-10-09 16:46:14', '2020-10-09 16:46:14');

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_image_tbl`
--

CREATE TABLE `adhoc_image_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `ref_id` int(11) NOT NULL DEFAULT 0,
  `adoc_id` int(11) DEFAULT 0,
  `property_id` int(11) DEFAULT 0,
  `customer_id` int(11) DEFAULT 0,
  `image_name` varchar(200) DEFAULT NULL,
  `file_type` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_log_tbl`
--

CREATE TABLE `adhoc_log_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `type` varchar(200) DEFAULT NULL,
  `request_type` varchar(200) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_to` int(11) NOT NULL DEFAULT 0,
  `ref_id` int(11) NOT NULL DEFAULT 0,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_request_tbl`
--

CREATE TABLE `adhoc_request_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `ticket_uid` varchar(200) DEFAULT NULL,
  `property_id` int(11) DEFAULT 0,
  `customer_id` int(11) DEFAULT 0,
  `employee_id` int(11) DEFAULT 0,
  `ref_id` int(11) DEFAULT 0,
  `type` varchar(200) DEFAULT NULL,
  `request_type` varchar(200) DEFAULT NULL,
  `requesttype_id` int(11) NOT NULL DEFAULT 0,
  `subject` varchar(200) DEFAULT NULL,
  `remarks` longtext DEFAULT NULL,
  `request_status` int(11) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_tbl`
--

CREATE TABLE `admin_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `ad_token` varchar(200) DEFAULT NULL,
  `ad_name` varchar(200) DEFAULT NULL,
  `ad_mobile` varchar(50) DEFAULT NULL,
  `ad_email` varchar(200) DEFAULT NULL,
  `ad_dob` varchar(200) DEFAULT NULL,
  `address1` varchar(200) DEFAULT NULL,
  `address2` varchar(200) DEFAULT NULL,
  `ad_city` varchar(200) DEFAULT NULL,
  `ad_pincode` varchar(200) DEFAULT NULL,
  `ad_password` varchar(200) DEFAULT NULL,
  `ad_type` int(1) NOT NULL DEFAULT 0,
  `ad_super_admin` int(1) NOT NULL DEFAULT 0,
  `ad_status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_tbl`
--

INSERT INTO `admin_tbl` (`id`, `branch_id`, `ad_token`, `ad_name`, `ad_mobile`, `ad_email`, `ad_dob`, `address1`, `address2`, `ad_city`, `ad_pincode`, `ad_password`, `ad_type`, `ad_super_admin`, `ad_status`, `created_at`, `updated_at`) VALUES
(1, 0, 'admin', 'Admin', '987654321', 'admin@leadcrm.com', '2020-05-30', 'xxxxxx', 'yyyyy', 'cbe', 'tamil nadu', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 1, 1, 1, '2016-06-01 10:00:00', '2020-05-28 09:33:18');

-- --------------------------------------------------------

--
-- Table structure for table `assignlead_tbl`
--

CREATE TABLE `assignlead_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `type` varchar(200) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_to` int(11) NOT NULL DEFAULT 0,
  `ref_id` int(11) DEFAULT 0,
  `priority` varchar(200) DEFAULT NULL,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `remove_status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `block_tbl`
--

CREATE TABLE `block_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `block` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `block_tbl`
--

INSERT INTO `block_tbl` (`id`, `branch_id`, `token`, `block`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'block-1', 'Block 1', 1, 1, '2020-05-28 16:09:22', '2020-09-09 13:40:13'),
(2, 0, 'block-b', 'Block B', 1, 1, '2020-06-03 09:06:06', '2020-09-08 12:20:32'),
(3, 0, 'block-c', 'Block C', 1, 1, '2020-06-19 12:32:52', '2020-06-19 12:32:52'),
(4, 0, 'block-d', 'Block D', 1, 1, '2020-07-03 21:06:49', '2020-07-03 21:06:49'),
(5, 0, 'block-e', 'Block E', 0, 1, '2020-08-21 12:22:39', '2020-08-21 12:22:39'),
(6, 0, 'block-f', 'Block F', 0, 1, '2020-09-08 12:26:16', '2020-09-08 12:26:16'),
(7, 0, 'block-g', 'Block G', 0, 1, '2020-09-08 12:26:28', '2020-09-08 12:26:28'),
(8, 0, 'block-h', 'Block H', 0, 1, '2020-09-08 12:26:36', '2020-09-08 12:26:36'),
(9, 0, 'block-i', 'Block I', 0, 1, '2020-09-08 12:26:46', '2020-09-08 12:26:57'),
(10, 0, 'block-j', 'Block J', 0, 1, '2020-09-08 12:27:04', '2020-09-08 12:27:04'),
(11, 0, 'block-k', 'Block K', 1, 1, '2020-09-08 12:27:14', '2020-09-08 12:27:14'),
(12, 0, 'block-l', 'Block L', 1, 1, '2020-09-08 12:27:31', '2020-09-08 12:27:31');

-- --------------------------------------------------------

--
-- Table structure for table `branch_master_tbl`
--

CREATE TABLE `branch_master_tbl` (
  `id` int(11) NOT NULL,
  `token` varchar(200) DEFAULT NULL,
  `branch` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch_master_tbl`
--

INSERT INTO `branch_master_tbl` (`id`, `token`, `branch`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'coimbatore', 'Coimbatore', 1, 1, '2021-08-24 10:05:49', '2021-08-24 10:05:49'),
(2, 'salem', 'Salem', 1, 1, '2021-08-24 13:31:10', '2021-08-24 13:31:10'),
(3, 'erode', 'Erode', 1, 1, '2021-08-27 20:00:31', '2021-08-27 20:00:31'),
(4, 'madurai', 'Madurai', 1, 1, '2021-08-27 20:00:40', '2021-08-27 20:00:40');

-- --------------------------------------------------------

--
-- Table structure for table `chit_master_tbl`
--

CREATE TABLE `chit_master_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `item_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chit_master_tbl`
--

INSERT INTO `chit_master_tbl` (`id`, `branch_id`, `token`, `item_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '1l', '1L', 1, 1, '2021-07-26 12:02:45', '2021-07-26 12:58:37'),
(2, 0, '2l', '2L', 1, 1, '2021-07-26 12:04:06', '2021-07-26 12:04:06'),
(3, 0, '5l', '5L', 1, 1, '2021-07-26 12:04:12', '2021-07-26 12:04:12'),
(4, 0, '10l', '10L', 1, 1, '2021-07-26 12:04:24', '2021-07-26 12:04:24');

-- --------------------------------------------------------

--
-- Table structure for table `company_settings_tbl`
--

CREATE TABLE `company_settings_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `phone` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `website_address` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_settings_tbl`
--

INSERT INTO `company_settings_tbl` (`id`, `branch_id`, `phone`, `email`, `website_address`, `address`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '9942387100', 'admin@rtohome.com', 'admin@rtohome.com', '99A vyasuva building', 1, 1, '2020-07-22 21:52:59', '2020-07-22 21:52:59');

-- --------------------------------------------------------

--
-- Table structure for table `contactinfo_list_tbl`
--

CREATE TABLE `contactinfo_list_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `contact_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(200) DEFAULT NULL,
  `designation` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contactinfo_tbl`
--

CREATE TABLE `contactinfo_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `role` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contactinfo_tbl`
--

INSERT INTO `contactinfo_tbl` (`id`, `branch_id`, `token`, `name`, `role`, `email`, `mobile`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(2, 0, 'admin', 'admin', 'Admin', 'admin@rtohomes.com', '1234567890', 1, 1, '2020-06-06 10:35:33', '2020-10-06 15:11:16');

-- --------------------------------------------------------

--
-- Table structure for table `customerdatasheet_tbl`
--

CREATE TABLE `customerdatasheet_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `import_token` varchar(50) DEFAULT NULL,
  `uploaded_sheet` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customerdatasheet_tbl`
--

INSERT INTO `customerdatasheet_tbl` (`id`, `branch_id`, `import_token`, `uploaded_sheet`, `created_at`, `updated_at`, `status`) VALUES
(1, 0, 'zF0ldnRXr5K2iP4', '30-08-2021_BFHEKiGC_sample_customerdata (2).xlsx', '2021-08-30 20:49:33', '2021-08-30 20:49:33', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_profile_tbl`
--

CREATE TABLE `customer_profile_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `customer_profile` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_profile_tbl`
--

INSERT INTO `customer_profile_tbl` (`id`, `branch_id`, `token`, `customer_profile`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'doctor', 'Doctor', 1, 1, '2020-09-22 15:38:24', '2020-09-28 18:13:32'),
(2, 0, 'engineer', 'Engineer', 1, 1, '2020-09-28 18:13:38', '2020-09-28 18:13:38'),
(3, 0, 'business', 'Business', 1, 1, '2020-09-28 18:13:46', '2020-09-28 18:13:46'),
(4, 0, 'employee', 'Employee', 1, 1, '2020-09-28 18:14:05', '2020-09-28 18:14:05'),
(5, 0, 'lawyer', 'Lawyer', 1, 1, '2020-09-28 18:14:17', '2020-09-28 18:14:17'),
(6, 0, 'gg', 'Gg', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(7, 0, 'govtservices', 'govt services', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(8, 0, 'proprietor', 'proprietor', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(9, 0, 'manager', 'Manager', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(10, 0, 'ownuse', 'Own use', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(11, 0, 'guard', 'Guard', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(12, 0, 'chairmanmanagingdirectorandceo', 'Chairman Managing Director and CEO', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(13, 0, 'student', 'Student', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(14, 0, 'healthconsultant', 'Health Consultant', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(15, 0, 'assistantprofessor', 'Assistant Professor', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(16, 0, 'housewife', 'House wife', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(17, 0, 'ownerphotographer', 'Owner/Photographer', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(18, 0, 'h', 'h', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(19, 0, 'seniormanager', 'Senior Manager', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(20, 0, 'owner', 'Owner', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(21, 0, 'lecturer', 'Lecturer', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(22, 0, 'villa', 'Villa', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(23, 0, 'directorofphysicaleducation', 'Director of Physical Education', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(24, 0, 'freelancewriter', 'Freelance writer', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(25, 0, 'buyingvillaatcbe', 'buying villa at cbe', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(26, 0, 'soleproprietorship', 'Sole proprietorship', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(27, 0, '', '', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(28, 0, 'nb', 'nb', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(29, 0, 'md', 'Md', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(30, 0, 'flatdetails', 'flat details', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(31, 0, 'villaneeded', 'villa,needed', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(32, 0, 'selfemployed', 'Self-employed', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(33, 0, 'villas', 'villas', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(34, 0, 'commerciallineagency', 'Commercial Line - Agency', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(35, 0, 'regionalhead', 'Regional head', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(36, 0, 'ownbuisness', 'own buisness', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(37, 0, 'relationshipmanager', 'Relationship Manager', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(38, 0, 'workinginpsgcollege', 'Working in PSG college', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(39, 0, 'wealreadyvisitedandsatisfied.', 'We already visited and satisfied.', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(40, 0, 'pastorandevanglist', 'pastor and evanglist', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(41, 0, 'tamilnadugenerationanddistributioncorporationlimited', 'Tamil nadu Generation and Distribution corporation Limited', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(42, 0, 'buildingcontractor', 'building contractor', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(43, 0, 'private', 'Private', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(44, 0, '4bhkandamenities', '4bhk and amenities', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(45, 0, 'branchhead', 'Branch Head', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(46, 0, 'house', 'House', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(47, 0, 'headofthedepartment', 'Head of the department', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(48, 0, 'software', 'software', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(49, 0, 'mobileappdeveloper', 'Mobile App Developer', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(50, 0, 'section-officer', 'Section officer', 1, 8, '2020-10-20 15:31:12', '2020-10-20 15:31:12'),
(51, 0, 'associate', 'Associate', 1, 8, '2020-10-20 16:12:42', '2020-10-20 16:12:42'),
(52, 0, 'project-manager', 'Project manager', 1, 8, '2020-10-20 16:22:41', '2020-10-20 16:22:41'),
(53, 0, 'consultant', 'Consultant', 1, 8, '2020-10-20 16:29:05', '2020-10-20 16:29:05'),
(54, 0, 'currentlyworking', 'Currently Working', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(55, 0, 'boss', 'Boss', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(56, 0, 'was', 'was', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(57, 0, 'hi', 'hi', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(58, 0, 'designengineer', 'Design Engineer', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(59, 0, 'gmfinance', 'GM Finance', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(60, 0, 'salesmanager', 'sales manager', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(61, 0, 'foundermanagingdirector', 'Founder & Managing Director', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(62, 0, 'wherethialocationinpodanur3bedroomappramnetorvilla', 'Where thia location in podanur 3 bed room appramnet or villA', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(63, 0, 'founderandmanagingdirector', 'Founder and Managing Director', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(64, 0, 'ownbusiness', 'Own-Business', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(65, 0, 'technicalsalesengineer', 'Technical Sales Engineer', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(66, 0, 'charteredaccountant', 'Chartered accountant', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(67, 0, 'bpo', 'BPO', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(68, 0, 'humanresourcescoordinatorhrcoordinator', 'Human Resources Coordinator (HR Coordinator)', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(69, 0, 'managingdirector', 'Managing Director', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(70, 0, 'momscateringparisutham', 'Moms Catering Parisutham', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(71, 0, 'home', 'Home', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(72, 0, 'area', 'area', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(73, 0, 'grouplead', 'GroupLead', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(74, 0, 'distributionhead', 'distribution head', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(75, 0, 'madrashighcourt', 'Madras high court', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(76, 0, 'lead', 'lead', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(77, 0, 'ownerandfounder', 'Owner and Founder', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(78, 0, 'directcall', 'Direct call', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(79, 0, 'softwareengineer', 'Software engineer', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(80, 0, '.location', '.Location ???', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(81, 0, '2bhk', '2bhk', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(82, 0, 'teacher', 'Teacher', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(83, 0, 'teaching', 'teaching', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(84, 0, 'system-admin', 'System Admin', 1, 8, '2020-11-06 10:58:10', '2020-11-06 10:58:10'),
(85, 0, 'bde', 'Bde', 1, 8, '2020-11-06 10:58:41', '2020-11-06 10:58:41'),
(86, 0, 'it', 'IT', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(87, 0, 'seniorsystemsengineer', 'senior systems engineer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(88, 0, 'homemaker', 'homemaker', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(89, 0, 'plsendmethedetailspricesforreadytooccupy', 'Pl send me the details, prices for ready to occupy', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(90, 0, 'seniorsystemexecutive', 'Senior System Executive', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(91, 0, 'agiiitech', 'AG-III(Tech)', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(92, 0, 'subregister', 'sub register', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(93, 0, 'officer', 'officer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(94, 0, 'earthmovingspats', 'Earth Moving Spats', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(95, 0, 'cloudmigrationarchitect', 'Cloud Migration Architect', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(96, 0, 'salesofficer', 'SALES OFFICER', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(97, 0, 'phdcandidateresearcher', 'PhD Candidate/Researcher', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(98, 0, 'salaried', 'salaried', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(99, 0, 'assistant', 'Assistant', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(100, 0, 'newhome', 'New Home', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(101, 0, 'govtemployee', 'Govt employee', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(102, 0, 'purchase', 'purchase', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(103, 0, 'busness', 'Busness', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(104, 0, 'interested', 'interested', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(105, 0, 'driving', 'Driving', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(106, 0, 'proprietorofagcinterior', 'proprietor of Agcinterior', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(107, 0, 'need3bhkindividualhomesvillas', 'Need 3 BHK Individual homes villas', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(108, 0, 'owneroperator', 'Owner Operator', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(109, 0, 'marketingmanager', 'marketing manager', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(110, 0, 'territorymanager', 'Territory Manager', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(111, 0, 's', 's', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(112, 0, 'governmentorganization', 'Government organization', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(113, 0, 'qualityassuranceexecutive', 'Quality assurance executive', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(114, 0, 'specialist', 'Specialist', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(115, 0, 'managerqualityservices', 'Manager - Quality Services', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(116, 0, 'generalmanageroperationsaccounts', 'General Manager - Operations & Accounts', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(117, 0, 'professor', 'Professor', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(118, 0, 'securityconsultant', 'security consultant', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(119, 0, 'marketing', 'Marketing', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(120, 0, 'seniorqaengineer', 'Senior QA Engineer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(121, 0, 'trader', 'Trader', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(122, 0, 'enguir', 'enguir', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(123, 0, 'enigineer', 'Enigineer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(124, 0, 'independenthouse3bedroomsatcoimbatore', 'Independent house - 3 bedrooms at Coimbatore', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(125, 0, 'needkeralatypefrontsidetiltedroofhousers40lto50l.housingloanelegible.', 'Need Kerala type (front side tilted roof) house, Rs 40L to 50L. Housing loan elegible.', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(126, 0, 'lookingfor3bhkvillas', 'Looking for 3bhk villas', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(127, 0, 'pleasecallby3clk', 'Please call by 3clk', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(128, 0, 'bussiness', 'Bussiness', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(129, 0, 'camengineer', 'CAM Engineer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(130, 0, 'no', 'no', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(131, 0, 'insurance', 'insurance', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(132, 0, 'mycustomerisinbombaywantsvillawith3bhkincoimbatorepleaseshareit', 'My customer is in Bombay wants villa with 3 bhk in coimbatore please share it', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(133, 0, 'registeredmassagetherapist', 'Registered Massage Therapist', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(134, 0, 'accounts', 'accounts', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(135, 0, 'army', 'army', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(136, 0, 'buy', 'Buy', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(137, 0, 'music', 'music', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(138, 0, 'siteengineer', 'Site Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(139, 0, 'tobuy', 'To buy', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(140, 0, 'analyst', 'Analyst', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(141, 0, 'upvcwindowsfabricator', 'upvc windows fabricator', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(142, 0, 'executive', 'executive', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(143, 0, 'itprofessional', 'IT professional', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(144, 0, 'executiveofficer', 'Executive Officer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(145, 0, 'hr', 'HR', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(146, 0, 'plcengineer', 'PLC engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(147, 0, 'storemanager', 'Store Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(148, 0, 'singanallur', 'singanallur', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(149, 0, 'underwritingmanager', 'Underwriting Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(150, 0, 'seniorgraphicdesigner', 'Senior Graphic Designer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(151, 0, 'regionalsalesmanager', 'Regional Sales Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(152, 0, 'managingpartner', 'Managing Partner', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(153, 0, 'computeroperator', 'Computer Operator', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(154, 0, 'seniorofficer', 'Senior Officer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(155, 0, 'materialsale', 'material sale', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(156, 0, 'managerfinance', 'Manager - Finance', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(157, 0, 'miss', 'miss', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(158, 0, 'trainer', 'trainer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(159, 0, 'na', 'Na', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(160, 0, 'working', 'working', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(161, 0, 'projectengineer', 'Project Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(162, 0, 'govt', 'govt', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(163, 0, 'readytooccupyvilla', 'Ready to occupy villa', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(164, 0, '3bk', '3 bk', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(165, 0, 'lookingforvillabelow50lakhs', 'Looking for villa below 50 lakhs', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(166, 0, 'pricesfor3bhkvilla', 'Prices for 3bhk villa', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(167, 0, 'bussenes', 'Bussenes', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(168, 0, 'insidesalesatcameronmanufacturingltd', 'Inside sales at Cameron Manufacturing LTD', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(169, 0, 'projectleader', 'Project Leader', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(170, 0, 'consignment', 'Consignment', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(171, 0, 'jr.managermarketing', 'Jr. Manager Marketing', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(172, 0, 'lookingforproperty', 'looking for property', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(173, 0, 'wanttobuy', 'Want to buy', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(174, 0, 'taxconsultant', 'Tax consultant', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(175, 0, 'productdesigner', 'Product Designer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(176, 0, 'physicaleducationteacherpeteacher', 'Physical Education Teacher (PE Teacher)', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(177, 0, 'softwareassociate', 'Software associate', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(178, 0, 'itemployee', 'IT employee', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(179, 0, 'b', 'B', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(180, 0, 'banker', 'Banker', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(181, 0, 'needquotationfor3bhkand2bhk', 'Need quotation for 3bhk and 2bhk', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(182, 0, 'a', 'A', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(183, 0, 'purchaseofhouse', 'purchase of house', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(184, 0, 'seniorassociate', 'Senior Associate', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(185, 0, 'employed', 'Employed', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(186, 0, '36lakhs', '36 lakhs', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(187, 0, 'iaminterestedingatedcommunityindependentvillainandaroundramanathapuramcoimbatore', 'I am interested in gated community independent villa in and around Ramanathapuram, Coimbatore', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(188, 0, 'plscall', 'Pls call', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(189, 0, 'cncmachineoperatorcomputernumericallycontrolledmachineoperator', 'CNC Machine Operator (Computer Numerically Controlled Machine Operator)', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(190, 0, 'productionengineer', 'Production engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(191, 0, 'chuma', 'Chuma', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(192, 0, 'individual', 'individual', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(193, 0, 'branchmanager', 'Branch manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(194, 0, 'programmeranalyst', 'Programmer Analyst', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(195, 0, 'govt.', 'Govt.', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(196, 0, 'creditseniormanager', 'Credit Senior Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(197, 0, 'interiordesigner', 'interior designer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(198, 0, 'bcom', 'bcom', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(199, 0, 'softwareprofessional', 'Software professional', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(200, 0, '.', '.', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(201, 0, 'obgyndoctorobstetricsgynecologydoctor', 'OB/GYN Doctor (Obstetrics/Gynecology Doctor)', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(202, 0, 'seniormanagerbusinessdevelopment', 'Senior Manager - Business Development', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(203, 0, 'yes', 'yes', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(204, 0, 'assistantmanagerhr', 'Assistant Manager HR', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(205, 0, 'enterprisesystemanalyst', 'Enterprise System Analyst', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(206, 0, 'qualitycontrolanalyst', 'Quality Control Analyst', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(207, 0, 'google', 'Google', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(208, 0, 'government', 'Government', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(209, 0, 'lookingfor3bhkvillanearbyavinashird', 'Looking for 3BHK VILLA NEAR BY AVINASHI RD', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(210, 0, 'customerrelationshipofficer', 'Customer relationship officer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(211, 0, 'lookingfor3or4bhkvillasorindependenthousesincoimbatore.', 'Looking for 3 or 4 BHK villas or independent houses in Coimbatore.', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(212, 0, 'needsumdetails', 'Need sum DETAILS', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(213, 0, 'seniorprocessexecutive', 'Senior Process Executive', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(214, 0, 'seniorengineerplanning', 'Senior Engineer - Planning', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(215, 0, 'staff', 'staff', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(216, 0, 'bmp', 'BMP', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(217, 0, 'headfinance', 'Head Finance', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(218, 0, 'sakthivel', 'Sakthivel', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(219, 0, 'buss', 'buss', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(220, 0, 'servicedeliverycoordinator', 'service delivery coordinator', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(221, 0, 'banking', 'Banking', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(222, 0, 'staffnurse', 'Staff Nurse', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(223, 0, 'engineersustainabledesign', 'Engineer-sustainable design', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(224, 0, 'projectmanager', 'Project Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(225, 0, 'ownersoleproprietor', 'Owner/Sole Proprietor', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(226, 0, 'djproducer', 'DJ/Producer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(227, 0, 'kotesan', 'kotesan', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(228, 0, 'rm4683159gmall.com', 'rm4683159@gmall.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(229, 0, 'personalbanker', 'Personal Banker', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(230, 0, 'dainikbhaskurgmail.com', 'dainikbhaskur@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(231, 0, 'civil', 'Civil', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(232, 0, 'individualhome', 'Individual home', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(233, 0, 'technican', 'technican', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(234, 0, 'assisstantmanager', 'Assisstant Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(235, 0, 'itsupporter', 'IT-Supporter', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(236, 0, 'sme', 'SME', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(237, 0, 'buisness', 'buisness', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(238, 0, 'sakrediffmail.com', 'sa_k@rediffmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(239, 0, 'director', 'Director', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(240, 0, 'ms.jenifer96gmail.com', 'ms.jenifer96@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(241, 0, 'engineeringdepartment', 'Engineering department', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(242, 0, 'super', 'Super', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(243, 0, 'ashu', 'Ashu', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(244, 0, 'merchandiser', 'Merchandiser', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(245, 0, 'hrtalentmanager', 'HR Talent Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(246, 0, 'b.comcomplied', 'B.com complied', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(247, 0, 'freelancer', 'Freelancer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(248, 0, 'businessheadtechnology', 'Business Head - Technology', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(249, 0, 'businesses', 'businesses', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(250, 0, 'v.r.vishnuecsgmail.com', 'v.r.vishnuecs@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(251, 0, 'nazi2nazi2002gmail.com', 'nazi2nazi2002@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(252, 0, 'santhosh.balaramanyahoo.com', 'santhosh.balaraman@yahoo.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(253, 0, 'jothipriyakarthikeyangmail.com', 'jothipriyakarthikeyan@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(254, 0, 'pleasegetquoteforindependentvillaincoimbatore', 'Please get quote for independent villa in Coimbatore', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(255, 0, 'networking', 'Networking', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(256, 0, 'desginengineer', 'Desgin Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(257, 0, 'seniorterritorysalesexecutive', 'Senior Territory Sales Executive', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(258, 0, 'electricalengineer', 'electrical engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(259, 0, 'divisionalmanagerproduction', 'Divisional Manager Production', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(260, 0, 'individualhomesunder30lacs', 'Individual homes under 30 lacs', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(261, 0, 'sales', 'sales', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(262, 0, 'processassociate', 'Process Associate', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(263, 0, 'sendcatalogue', 'Send catalogue', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(264, 0, 'associatemanager', 'Associate Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(265, 0, 'estudante', 'Estudante', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(266, 0, 'centralgovt', 'central govt', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(267, 0, 'airlines', 'Airlines', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(268, 0, 'principalarchitectpartner', 'Principal Architect/Partner', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(269, 0, 'physiotherapist', 'Physiotherapist', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(270, 0, 'teamleader', 'Team Leader', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(271, 0, 'softwareengineeruiuxdesigner', 'Software Engineer UI UX Designer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(272, 0, 'applestore', 'Apple Store', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(273, 0, 'testprofessional', 'Test professional', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(274, 0, 'hotel', 'hotel', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(275, 0, 'bb', 'Bb', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(276, 0, 'start', 'start', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(277, 0, 'own', 'Own', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(278, 0, 'vjns', 'v j n s', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(279, 0, 'teamlead', 'Team lead', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(280, 0, 'whichareainpodanur', 'which area in podanur', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(281, 0, 'nagarajan', 'Nagarajan', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(282, 0, 'hemavasu415gmail.com', 'hemavasu415@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(283, 0, 'sakthishivaligmail.com', 'sakthishivali@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(284, 0, 'indianairforce', 'indian air Force', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(285, 0, 'telungupalayam', 'Telungupalayam', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(286, 0, 'mobileshop', 'Mobile Shop', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(287, 0, 'seniornetworksecurityengineer', 'Senior Network Security Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(288, 0, 'functionalconsultant', 'Functional Consultant', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(289, 0, 'need', 'need', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(290, 0, 'assistantmanagerproduction', 'Assistant Manager - Production', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(291, 0, 'expectation', 'expectation', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(292, 0, 'statestreethclservices', 'Statestreet Hcl services', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(293, 0, 'cloudengineer', 'Cloud Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(294, 0, 'finance', 'Finance', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(295, 0, '3bhkhone', '3 bhk hone', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(296, 0, 'southernrailway', 'Southern Railway', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(297, 0, 'required3bhkluxuryvilla', 'Required 3 bhk luxury villa', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(298, 0, 'gc', 'GC', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(299, 0, 'agent', 'Agent', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(300, 0, '3bhk', '3bhk', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(301, 0, 'mr', 'Mr', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(302, 0, 'professional', 'Professional', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(303, 0, 'coimbatoreinstituteoftechnology', 'coimbatore institute of technology', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(304, 0, 'techniciantrainee', 'Technician Trainee', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(305, 0, 'agri', 'agri', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(306, 0, 'bankofficer', 'bank officer', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(307, 0, 'modulelead', 'Module Lead', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(308, 0, 'susintechnologypltd', 'susin technology p ltd', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(309, 0, 'agaramagencies', 'Agaram Agencies', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(310, 0, 'callme', 'call me', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(311, 0, 'locationpricelayoutetcrequired', 'Location price layout etc required', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(312, 0, 'seniorsystemsexecutive', 'Senior Systems Executive', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(313, 0, 'officerdigitalmarketing', 'Officer - Digital Marketing', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(314, 0, 'areasalesmanager', 'Area Sales Manager', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(315, 0, 'ssdgfhmail.com', 'ssdgf@hmail.com', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(316, 0, 'sreena.gopishgmail.com', 'sreena.gopish@gmail.com', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(317, 0, 'detailsof3bhkvilla', 'Details of 3BHK VILLA', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(318, 0, 'generalsurgeon', 'General surgeon', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(319, 0, 'photoeditioranddesigner', 'Photo Editior and designer', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(320, 0, 'financemanager', 'Finance manager', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(321, 0, 'byjus', 'Byjus', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(322, 0, 'articleship', 'Articleship', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(323, 0, 'agencies', 'agencies', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(324, 0, 'corporate', 'Corporate', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(325, 0, 'assistantmanager', 'Assistant Manager', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(326, 0, 'acco', 'acco', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(327, 0, 'temple', 'Temple', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(328, 0, 'proprietorship', 'Proprietorship', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(329, 0, 'developer', 'Developer', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(330, 0, 'automotivetestengineer', 'Automotive Test Engineer', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(331, 0, 'stroes', 'Stroes', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(332, 0, 'iamintrested', 'I am intrested', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(333, 0, 'akmedicalcenter', 'Ak medical center', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(334, 0, 'architect', 'Architect', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(335, 0, 'photographer', 'photographer', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(336, 0, 'interested3bhkvillain36l', 'Interested 3bhk villa in 36L', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(337, 0, 'foreman', 'Foreman', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(338, 0, 'dringschool', 'Dring school', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(339, 0, 'logisticsincharge', 'Logistics Incharge', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(340, 0, 'homewife', 'home wife', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(341, 0, 'salesmanagerforindiabangladeshitandsrilankaatbuckman', 'Sales Manager for India, Bangladesh it and Sri Lanka at Buckman', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(342, 0, 'agri.officer', 'Agri.Officer', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(343, 0, 'snacks', 'Snacks', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(344, 0, 'businessdevelopmentexecutive', 'business development executive', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(345, 0, 'processlead', 'Process Lead', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(346, 0, 'testingexecutive', 'Testing Executive', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(347, 0, 'chatbot', 'Chatbot', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(348, 0, 'seniorengineer', 'Senior Engineer', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(349, 0, 'ceo', 'ceo', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(350, 0, 'merchandisingmanager', 'Merchandising Manager', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(351, 0, 'rsm', 'RSM', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(352, 0, 'hrbp', 'HRBP', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(353, 0, 'businessconsultant', 'Business consultant', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(354, 0, 'pastrychef', 'Pastry Chef', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(355, 0, 'academician', 'Academician', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(356, 0, 'se', 'SE', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(357, 0, 'assistantgeneralmanager', 'Assistant General Manager', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(358, 0, 'engineertechnician', 'Engineer Technician', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(359, 0, 'juniorengineer', 'Junior Engineer', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(360, 0, 'airconditioningservicecenter', 'Air-conditioning service center', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(361, 0, 'webdeveloper', 'Web Developer', 1, 1, '2021-08-30 21:21:31', '2021-08-30 21:21:31');

-- --------------------------------------------------------

--
-- Table structure for table `customer_tbl`
--

CREATE TABLE `customer_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `import_token` varchar(200) DEFAULT NULL,
  `token` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `lead_id` int(11) DEFAULT 0,
  `uid` varchar(200) DEFAULT NULL,
  `priority` varchar(200) DEFAULT NULL,
  `property_id` int(11) DEFAULT 0,
  `allotment_date` varchar(200) DEFAULT NULL,
  `assign_remarks` longtext DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `gender` varchar(200) DEFAULT NULL,
  `dob` varchar(200) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `has_psw` varchar(200) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `nationality` varchar(200) DEFAULT NULL,
  `pincode` varchar(200) DEFAULT NULL,
  `ad_contact_person` varchar(200) DEFAULT NULL,
  `ad_relationship` varchar(200) DEFAULT NULL,
  `ad_mobile` varchar(200) DEFAULT NULL,
  `ad_email` varchar(200) DEFAULT NULL,
  `ad_address` varchar(200) DEFAULT NULL,
  `ad_city` varchar(200) DEFAULT NULL,
  `ad_state` varchar(200) DEFAULT NULL,
  `ad_pincode` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `kyc_status` int(11) DEFAULT 0,
  `approved_on` varchar(200) DEFAULT NULL,
  `kyc_submittedon` varchar(200) DEFAULT NULL,
  `approved_by` int(11) DEFAULT 0,
  `kyc_remarks` longtext DEFAULT NULL,
  `assign_status` int(11) DEFAULT 0,
  `resetpassword` int(11) DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `datasheet_tbl`
--

CREATE TABLE `datasheet_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `import_token` varchar(50) DEFAULT NULL,
  `uploaded_sheet` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `deposite_master_tbl`
--

CREATE TABLE `deposite_master_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `item_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposite_master_tbl`
--

INSERT INTO `deposite_master_tbl` (`id`, `branch_id`, `token`, `item_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '1l', '1L', 1, 1, '2021-07-26 13:33:49', '2021-07-26 13:35:20'),
(2, 0, '2l', '2L', 1, 1, '2021-07-26 13:35:24', '2021-07-26 13:35:24'),
(3, 0, '5l', '5L', 1, 1, '2021-07-26 13:35:32', '2021-07-26 13:35:32'),
(4, 0, '10l', '10L', 1, 1, '2021-07-26 13:35:41', '2021-07-26 13:35:41'),
(5, 0, '15l', '15L', 1, 1, '2021-08-30 21:21:31', '2021-08-30 21:21:31');

-- --------------------------------------------------------

--
-- Table structure for table `employee_permission_tbl`
--

CREATE TABLE `employee_permission_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `employee_id` varchar(200) DEFAULT NULL,
  `customers` int(11) NOT NULL DEFAULT 0,
  `manage_customer` int(11) NOT NULL DEFAULT 0,
  `import_customer` int(11) NOT NULL DEFAULT 0,
  `lead` int(11) NOT NULL DEFAULT 0,
  `manage_lead` int(11) NOT NULL DEFAULT 0,
  `lead_type` int(11) NOT NULL DEFAULT 0,
  `activity_type` int(11) NOT NULL DEFAULT 0,
  `lead_source` int(11) NOT NULL DEFAULT 0,
  `customer_profile` int(11) NOT NULL DEFAULT 0,
  `import_lead` int(11) NOT NULL DEFAULT 0,
  `employee` int(11) NOT NULL DEFAULT 0,
  `manageemployee` int(11) NOT NULL DEFAULT 0,
  `reports` int(11) NOT NULL DEFAULT 0,
  `lead_report` int(11) NOT NULL DEFAULT 0,
  `customer_report` int(11) NOT NULL DEFAULT 0,
  `settings` int(11) NOT NULL DEFAULT 0,
  `chit_master` int(11) NOT NULL DEFAULT 0,
  `deposit_master` int(11) NOT NULL DEFAULT 0,
  `vam_master` int(11) NOT NULL DEFAULT 0,
  `branch_master` int(1) DEFAULT 0,
  `employee_role_master` int(1) DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_permission_tbl`
--

INSERT INTO `employee_permission_tbl` (`id`, `branch_id`, `employee_id`, `customers`, `manage_customer`, `import_customer`, `lead`, `manage_lead`, `lead_type`, `activity_type`, `lead_source`, `customer_profile`, `import_lead`, `employee`, `manageemployee`, `reports`, `lead_report`, `customer_report`, `settings`, `chit_master`, `deposit_master`, `vam_master`, `branch_master`, `employee_role_master`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '1', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, '2020-06-06 15:06:09', '2020-06-06 15:12:37');

-- --------------------------------------------------------

--
-- Table structure for table `employee_role_master_tbl`
--

CREATE TABLE `employee_role_master_tbl` (
  `id` int(11) NOT NULL,
  `token` varchar(200) DEFAULT NULL,
  `employee_role` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_role_master_tbl`
--

INSERT INTO `employee_role_master_tbl` (`id`, `token`, `employee_role`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'partner', 'Partner', 1, 1, '2021-08-24 11:23:41', '2021-08-24 11:23:41'),
(2, 'generalmanager', 'General Manager', 1, 1, '2021-08-24 15:28:24', '2021-08-24 16:14:15'),
(3, 'branchmanager', 'Branch Manager', 1, 1, '2021-08-24 16:14:30', '2021-08-24 16:14:30'),
(4, 'sr.executivesales', 'Sr. Executive Sales', 1, 1, '2021-08-24 11:20:33', '2021-08-24 11:20:33'),
(5, 'executiveoffice', 'Executive Office', 1, 1, '2021-08-24 11:21:09', '2021-08-24 11:21:09'),
(6, 'telecaller', 'Telecaller', 1, 1, '2021-08-24 11:23:01', '2021-08-24 11:23:01');

-- --------------------------------------------------------

--
-- Table structure for table `employee_tbl`
--

CREATE TABLE `employee_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `employee_uid` varchar(200) DEFAULT NULL,
  `token` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `role` int(1) DEFAULT 0,
  `gender` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `pincode` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `assigned_branch` varchar(100) DEFAULT NULL,
  `assigned_employee` int(11) DEFAULT 0,
  `super_admin` int(11) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_tbl`
--

INSERT INTO `employee_tbl` (`id`, `branch_id`, `employee_uid`, `token`, `name`, `role`, `gender`, `password`, `email`, `mobile`, `address`, `city`, `state`, `pincode`, `status`, `assigned_branch`, `assigned_employee`, `super_admin`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 2, 'EMP0001', 'admin', 'Admin', 2, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Admin@mail.com', '9942387100', '', 'coimbatore', '', '6441103', 1, NULL, 0, 1, 1, '2020-06-06 15:06:09', '2021-07-27 11:34:35');

-- --------------------------------------------------------

--
-- Table structure for table `flatvilla_documenttype_list_tbl`
--

CREATE TABLE `flatvilla_documenttype_list_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `document_id` int(11) NOT NULL DEFAULT 0,
  `doc_name` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` int(1) DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flatvilla_documenttype_list_tbl`
--

INSERT INTO `flatvilla_documenttype_list_tbl` (`id`, `branch_id`, `document_id`, `doc_name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'EB Form', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 1, '2020-06-16 09:54:06', '2020-06-16 09:54:06'),
(2, 0, 1, 'EB Bill', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 1, '2020-06-16 09:54:06', '2020-06-16 09:54:06'),
(3, 0, 2, 'Building Ouside View', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ', 1, '2020-06-16 09:55:52', '2020-06-16 09:55:52'),
(4, 0, 2, 'Building inner View', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 1, '2020-06-16 09:55:52', '2020-06-16 09:55:52'),
(5, 0, 4, 'Land Sub Document ', 'Add Document', 1, '2020-06-19 12:32:01', '2020-06-19 12:32:01'),
(6, 0, 5, 'building approval document', 'sample content', 1, '2020-08-21 11:58:53', '2020-08-21 11:58:53'),
(7, 0, 6, 'Booking Form', 'Booking Form', 1, '2020-09-04 18:34:11', '2020-09-04 18:34:11'),
(8, 0, 7, 'Allotment Letter', 'Allotment Letter', 1, '2020-09-04 18:34:42', '2020-09-04 18:34:42'),
(9, 0, 8, 'Sale Agreement', 'Sale Agreement', 1, '2020-09-04 18:35:07', '2020-09-04 18:35:07'),
(10, 0, 9, 'Construction Agreement', 'Construction Agreement', 1, '2020-09-04 18:35:33', '2020-09-04 18:35:33'),
(11, 0, 10, 'Tripartite Agreement', 'Tripartite Agreement', 1, '2020-09-04 18:36:05', '2020-09-04 18:36:05'),
(12, 0, 11, 'Sale Deed', 'Sale Deed', 1, '2020-09-04 18:36:29', '2020-09-04 18:36:29'),
(13, 0, 12, 'Builder Demand', 'Builder Demand', 1, '2020-09-04 18:36:55', '2020-09-04 18:36:55'),
(14, 0, 13, 'Property Tax Book', 'Property Tax Book', 1, '2020-09-04 18:37:21', '2020-09-04 18:37:21');

-- --------------------------------------------------------

--
-- Table structure for table `flatvilla_documenttype_tbl`
--

CREATE TABLE `flatvilla_documenttype_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `doc_title` varchar(200) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `publish_status` int(11) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flatvilla_documenttype_tbl`
--

INSERT INTO `flatvilla_documenttype_tbl` (`id`, `branch_id`, `token`, `doc_title`, `sort_order`, `status`, `publish_status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'eb-documents', 'EB Documents', 1, 0, 0, 0, 0, '', 0, '', '', 1, '2020-06-16 09:54:06', '2020-06-16 09:54:06'),
(2, 0, 'building-blue-print', 'Building Blue Print', 2, 0, 0, 0, 0, '', 0, '', '', 1, '2020-06-16 09:55:52', '2020-07-08 01:37:33'),
(3, 0, 'land-property-document', 'Land Property Document', 3, 0, 0, 0, 0, '', 0, '', '', 1, '2020-06-19 11:35:15', '2020-06-19 11:35:15'),
(4, 0, 'land-approval-document-', 'Land Approval Document', 2, 0, 0, 0, 0, '', 0, '', '', 1, '2020-06-19 12:32:01', '2020-06-19 12:32:01'),
(5, 0, 'building-approval-document', 'Building approval document', 4, 0, 0, 0, 0, '', 0, '', '', 1, '2020-08-21 11:58:53', '2020-08-21 11:58:53'),
(6, 0, 'booking-form', 'Booking Form', 6, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:34:11', '2020-09-04 18:34:11'),
(7, 0, 'allotment-letter', 'Allotment Letter', 7, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:34:42', '2020-09-04 18:34:42'),
(8, 0, 'sale-agreement', 'Sale Agreement', 8, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:35:07', '2020-09-04 18:35:07'),
(9, 0, 'construction-agreement', 'Construction Agreement', 9, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:35:33', '2020-09-04 18:35:33'),
(10, 0, 'tripartite-agreement', 'Tripartite Agreement', 10, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:36:05', '2020-09-04 18:36:05'),
(11, 0, 'sale-deed', 'Sale Deed', 11, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:36:29', '2020-09-04 18:36:29'),
(12, 0, 'builder-demand', 'Builder Demand', 12, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:36:55', '2020-09-04 18:36:55'),
(13, 0, 'property-tax-book', 'Property Tax Book', 13, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:37:21', '2020-09-04 18:37:21');

-- --------------------------------------------------------

--
-- Table structure for table `flat_type_master`
--

CREATE TABLE `flat_type_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `flat_variant` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flat_type_master`
--

INSERT INTO `flat_type_master` (`id`, `branch_id`, `token`, `flat_variant`, `description`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '1bhk', '1BHK', 'Sample', 1, 1, '2020-06-13 14:03:37', '2020-06-13 14:32:24'),
(2, 0, '2bhk', '2BHK', 'sample', 1, 1, '2020-06-13 14:04:06', '2020-06-13 14:04:06'),
(3, 0, '3bhk', '3BHK', 'sample', 1, 1, '2020-06-13 14:04:21', '2020-06-13 14:04:21'),
(4, 0, '4bhk', '4BHK', '', 1, 1, '2020-06-19 13:33:35', '2020-06-19 13:33:35'),
(6, 0, '5bhk', '5BHK', '', 0, 1, '2020-06-22 18:36:31', '2020-06-22 18:36:31'),
(8, 0, '', '', '', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `flat_type_masterlist`
--

CREATE TABLE `flat_type_masterlist` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `flattype_id` int(11) DEFAULT 0,
  `room_type` varchar(200) DEFAULT NULL,
  `sqft` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` int(1) DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flat_type_masterlist`
--

INSERT INTO `flat_type_masterlist` (`id`, `branch_id`, `flattype_id`, `room_type`, `sqft`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Bed Room', '256', 'sample', 1, '2020-06-13 14:03:37', '2020-06-13 14:03:37'),
(2, 0, 1, 'Kitchen', '100', 'sample', 1, '2020-06-13 14:03:37', '2020-06-13 14:03:37'),
(3, 0, 2, 'Bed Room', '256', 'sample', 1, '2020-06-13 14:04:06', '2020-06-13 14:04:06'),
(4, 0, 2, 'Kitchen', '100', 'sampl 1', 1, '2020-06-13 14:04:06', '2020-06-13 14:04:06'),
(5, 0, 3, 'Bed Room', '123', 'sample', 1, '2020-06-13 14:04:21', '2020-06-13 14:04:21'),
(7, 0, 4, '4 BHK', '220 Sqft', 'Appartment', 1, '2020-06-19 13:33:35', '2020-06-19 13:33:35'),
(8, 0, 5, '1 BHK', '290', 'Add Flat Type Master', 1, '2020-06-19 19:00:40', '2020-06-19 19:00:40');

-- --------------------------------------------------------

--
-- Table structure for table `floor_tbl`
--

CREATE TABLE `floor_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `floor` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `floor_tbl`
--

INSERT INTO `floor_tbl` (`id`, `branch_id`, `token`, `floor`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(2, 0, '1-floor', '1 Floor', 1, 1, '2020-05-28 16:42:59', '2020-07-08 01:39:59'),
(3, 0, '2-floor', '2 Floor', 1, 1, '2020-06-03 09:07:41', '2020-06-03 09:07:41'),
(4, 0, '3-floor', '3 Floor', 1, 1, '2020-06-13 17:29:44', '2020-06-13 17:29:44'),
(5, 0, '4-floor', '4 Floor', 1, 1, '2020-06-19 13:28:32', '2020-06-19 13:28:32'),
(6, 0, '5-floor', '5 Floor', 1, 1, '2020-07-03 21:18:43', '2020-07-03 21:18:43'),
(7, 0, '6-floor', '6 Floor', 1, 1, '2020-08-31 11:18:37', '2020-08-31 11:18:37'),
(8, 0, '7-floor', '7 Floor', 1, 1, '2020-08-31 11:18:45', '2020-08-31 11:18:45'),
(9, 0, '8-floor', '8 Floor', 1, 1, '2020-08-31 11:18:56', '2020-08-31 11:18:56');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_image_tbl`
--

CREATE TABLE `gallery_image_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `gallery_id` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_image_tbl`
--

INSERT INTO `gallery_image_tbl` (`id`, `branch_id`, `gallery_id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 0, '1', 'Yg7h12qOima_lH4p6W8QoTZUkf5NCc.jpg', 1, '2021-07-26 22:27:38', '2021-07-26 22:27:38'),
(4, 0, '1', 'pxN3SiQdwch916TyGzCXnPDeBIkHlm.jpg', 1, '2021-07-26 22:46:25', '2021-07-26 22:46:25'),
(5, 0, '1', 'gN0S1nxs8eXT_CWVfDmkboZMrhyGpw.jpg', 1, '2021-07-26 22:46:25', '2021-07-26 22:46:25');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_tbl`
--

CREATE TABLE `gallery_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `album_title` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `sort_order` int(11) DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gendral_documents_tbl`
--

CREATE TABLE `gendral_documents_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `documents` varchar(200) DEFAULT NULL,
  `document_type` varchar(200) DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_tbl`
--

CREATE TABLE `invoice_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `inv_uid` varchar(200) DEFAULT NULL,
  `customer_id` int(11) DEFAULT 0,
  `inv_number` varchar(200) DEFAULT NULL,
  `inv_date` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `raised_to` varchar(200) DEFAULT NULL,
  `documents` varchar(200) DEFAULT NULL,
  `document_type` varchar(200) DEFAULT NULL,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kyc_list_tbl`
--

CREATE TABLE `kyc_list_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `kyctype_id` int(11) NOT NULL DEFAULT 0,
  `image_name` varchar(200) DEFAULT NULL,
  `file_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `approval_status` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kyc_tbl`
--

CREATE TABLE `kyc_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `kyc_uid` varchar(200) DEFAULT NULL,
  `property_id` int(11) DEFAULT 0,
  `customer_id` int(11) DEFAULT 0,
  `id_type` varchar(200) DEFAULT NULL,
  `firstname` varchar(200) DEFAULT NULL,
  `lastname` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `dob` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `address1` varchar(200) DEFAULT NULL,
  `address2` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `nationiality` varchar(200) DEFAULT NULL,
  `pincode` varchar(200) DEFAULT NULL,
  `approval_status` int(11) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kyc_type_tbl`
--

CREATE TABLE `kyc_type_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `kyc_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kyc_type_tbl`
--

INSERT INTO `kyc_type_tbl` (`id`, `branch_id`, `token`, `kyc_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'aadhaar-card', 'Aadhaar Card', 1, 1, '2020-07-02 22:58:39', '2020-07-02 22:58:39'),
(2, 0, 'pan-card', 'PAN Card', 1, 1, '2020-07-02 22:59:04', '2020-07-02 22:59:04'),
(3, 0, 'ration-card', 'Ration Card', 1, 1, '2020-07-02 22:59:12', '2020-07-02 22:59:12'),
(4, 0, 'communication-address', 'Communication address', 1, 1, '2020-07-02 22:59:20', '2020-09-09 14:46:37'),
(5, 0, 'aadhar-card-witness-details-for-documentation', 'Aadhar card -Witness details for documentation', 1, 1, '2020-07-02 22:59:27', '2020-09-09 14:47:15'),
(6, 0, 'photo', 'Photo', 1, 1, '2020-07-03 22:13:12', '2020-09-09 14:46:13'),
(7, 0, 'bank-loan-sanction-letter', 'Bank Loan Sanction Letter', 1, 1, '2020-09-09 14:45:55', '2020-09-09 14:45:55');

-- --------------------------------------------------------

--
-- Table structure for table `lead_activity_tbl`
--

CREATE TABLE `lead_activity_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `date` varchar(200) DEFAULT NULL,
  `lead_id` varchar(200) DEFAULT NULL,
  `employee_id` int(11) NOT NULL DEFAULT 0,
  `activity_id` int(11) NOT NULL DEFAULT 0,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_gallery_tbl`
--

CREATE TABLE `lead_gallery_tbl` (
  `id` int(11) NOT NULL,
  `lead_id` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_logs_tbl`
--

CREATE TABLE `lead_logs_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `type` varchar(200) DEFAULT NULL,
  `lead_type` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `created_to` int(11) NOT NULL DEFAULT 0,
  `ref_id` int(11) NOT NULL DEFAULT 0,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_source_tbl`
--

CREATE TABLE `lead_source_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `lead_source` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_source_tbl`
--

INSERT INTO `lead_source_tbl` (`id`, `branch_id`, `token`, `lead_source`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'fb', 'Fb', 1, 1, '2020-09-22 11:39:38', '2020-09-22 11:39:38'),
(2, 0, 'instagram', 'Instagram', 1, 1, '2020-09-22 11:40:01', '2020-09-22 11:40:01'),
(3, 0, 'goggle-ads', 'goggle Ads', 1, 1, '2020-09-22 11:40:24', '2020-09-22 11:40:24'),
(4, 0, 'facebook', 'Facebook', 1, 1, '2020-10-01 16:29:40', '2020-10-01 16:29:40'),
(5, 0, 'google', 'Google', 1, 1, '2020-10-01 16:29:40', '2020-10-01 16:29:40'),
(6, 0, 'directcall', 'Direct Call', 1, 1, '2020-10-08 12:33:07', '2020-10-08 12:33:07'),
(7, 0, 'ig', 'ig', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(9, 0, 'fairyland', 'Fairyland', 1, 12, '2021-02-04 16:44:54', '2021-02-04 16:44:54'),
(10, 0, 'chatbot', 'Chatbot', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(11, 0, 'land', 'Land', 1, 1, '2021-08-30 21:21:31', '2021-08-30 21:21:31');

-- --------------------------------------------------------

--
-- Table structure for table `lead_tbl`
--

CREATE TABLE `lead_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `import_token` varchar(200) DEFAULT NULL,
  `uid` varchar(200) DEFAULT NULL,
  `auth_type` varchar(200) DEFAULT NULL,
  `property_id` int(11) DEFAULT 0,
  `customer_id` int(11) DEFAULT 0,
  `lead_date` varchar(200) DEFAULT NULL,
  `enquiry_date` varchar(100) DEFAULT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `lname` varchar(200) DEFAULT NULL,
  `leadsource` varchar(200) DEFAULT NULL,
  `designation` varchar(200) DEFAULT NULL,
  `has_chit` int(11) NOT NULL DEFAULT 0,
  `has_deposite` int(11) NOT NULL DEFAULT 0,
  `has_vas` int(11) NOT NULL DEFAULT 0,
  `chit_id` int(11) NOT NULL DEFAULT 0,
  `deposite_id` int(11) NOT NULL DEFAULT 0,
  `vas_id` int(11) NOT NULL DEFAULT 0,
  `lead_status` varchar(150) DEFAULT NULL,
  `flattype_id` int(11) DEFAULT 0,
  `site_visit_status` varchar(200) DEFAULT NULL,
  `site_visit_date` varchar(100) DEFAULT NULL,
  `customer_profile_id` int(11) DEFAULT 0,
  `gender` varchar(200) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `secondary_mobile` varchar(200) DEFAULT NULL,
  `secondary_email` varchar(200) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pincode` varchar(250) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `lead_status_id` int(11) NOT NULL DEFAULT 0,
  `employee_id` int(11) NOT NULL DEFAULT 0,
  `assign_status` int(11) DEFAULT 0,
  `closed_status` int(11) DEFAULT 0,
  `moved_status` int(1) NOT NULL DEFAULT 0,
  `move_remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_on` varchar(250) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(250) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_type_tbl`
--

CREATE TABLE `lead_type_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `uid` varchar(200) DEFAULT NULL,
  `token` varchar(200) DEFAULT NULL,
  `lead_type` varchar(200) DEFAULT NULL,
  `default_settings` int(1) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_type_tbl`
--

INSERT INTO `lead_type_tbl` (`id`, `branch_id`, `uid`, `token`, `lead_type`, `default_settings`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'LT0001', 'will-think-and-get-back', 'Will think and get back', 0, 1, 1, '2020-08-16 22:22:58', '2020-08-17 19:35:34'),
(2, 0, 'LT0002', 'hot-lead', 'Hot Lead', 1, 1, 1, '2020-08-16 22:23:09', '2020-08-17 19:35:22'),
(3, 0, 'LT0003', 'will-discuss-with-family', 'Will discuss with family', 0, 1, 1, '2020-08-16 22:26:03', '2020-08-17 19:35:41'),
(4, 0, 'LT0004', 'just-for-collecting-info', 'Just for collecting info', 0, 1, 1, '2020-08-17 19:35:53', '2020-08-17 19:35:53'),
(5, 0, 'LT0005', 'do-not-follow-up', 'Do not follow up', 0, 1, 1, '2020-08-17 19:35:59', '2020-08-17 19:35:59'),
(6, 0, 'LT0006', 'interested', 'Interested', 0, 1, 1, '2020-10-08 12:34:27', '2020-10-08 12:34:27'),
(7, 0, 'LT0007', 'notinterested', 'Not Interested', 0, 1, 1, '2020-10-08 12:34:39', '2020-10-08 12:34:39'),
(8, 0, 'LT0008', 'interestedinsomeotherlocation', 'Interested in Some other Location', 0, 1, 1, '2020-10-08 12:35:06', '2020-10-08 12:35:31'),
(9, 0, 'LT0009', 'notattnthecall', 'Not attn the call', 0, 1, 8, '2020-10-09 15:12:55', '2020-10-09 15:12:55'),
(10, 0, 'LT0010', 'lowbudget', 'Low budget', 0, 1, 8, '2020-10-09 16:54:31', '2020-10-09 16:54:31'),
(11, 0, 'LT0011', 'detailsshared', 'Details shared', 0, 1, 8, '2020-10-09 17:14:09', '2020-10-09 17:14:09'),
(12, 0, 'LT0012', 'callback', 'Call back', 0, 1, 8, '2020-10-12 10:49:23', '2020-10-12 10:49:23'),
(13, 0, 'LT0013', 'linebusy', 'Line busy', 0, 1, 8, '2020-10-12 11:05:01', '2020-10-12 11:05:01'),
(14, 0, 'LT0014', 'sitevisitdone', 'Site visit done', 0, 1, 8, '2020-10-15 11:31:19', '2020-10-15 11:31:19'),
(15, 0, 'LT0015', 'notreachable', 'Not reachable', 0, 1, 8, '2020-10-20 16:07:20', '2020-10-20 16:07:20'),
(16, 0, 'LT0016', 'followup', 'Follow up', 0, 1, 1, '2021-02-04 19:36:08', '2021-02-04 19:36:08'),
(17, 0, 'LT0017', 'other', 'Other', 0, 1, 1, '2021-02-04 19:36:08', '2021-02-04 19:36:08'),
(20, 0, NULL, '', '', 0, 1, 1, '2021-02-24 14:47:08', '2021-02-24 14:47:08'),
(21, 0, 'LT0021', '1l', '1L', 0, 1, 1, '2021-07-26 11:58:39', '2021-07-26 11:58:39'),
(22, 0, NULL, 'hotdeal', 'Hot Deal', 0, 1, 1, '2021-08-30 21:21:31', '2021-08-30 21:21:31');

-- --------------------------------------------------------

--
-- Table structure for table `news_tbl`
--

CREATE TABLE `news_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `date` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `email_notification` int(11) NOT NULL DEFAULT 0,
  `post_to` varchar(200) DEFAULT NULL,
  `customer_ids` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `show_popup` int(11) NOT NULL DEFAULT 0,
  `popup_till` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `send_mail_status` int(11) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification_email_tbl`
--

CREATE TABLE `notification_email_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `adhoc_permission` int(1) NOT NULL DEFAULT 0,
  `kyc_permission` int(1) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paymentinfo_tbl`
--

CREATE TABLE `paymentinfo_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `name` varchar(200) DEFAULT NULL,
  `account_number` varchar(200) DEFAULT NULL,
  `bank` varchar(200) DEFAULT NULL,
  `ifcs_code` varchar(200) DEFAULT NULL,
  `branch` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `pincode` int(11) DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project_type_tbl`
--

CREATE TABLE `project_type_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `project_name` varchar(200) DEFAULT NULL,
  `status` int(1) DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_broucher_tbl`
--

CREATE TABLE `property_broucher_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `description` longtext DEFAULT NULL,
  `brochures` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_document_list_items`
--

CREATE TABLE `property_document_list_items` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `document_id` int(11) NOT NULL DEFAULT 0,
  `document_list_id` int(11) NOT NULL DEFAULT 0,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `doc_name` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `document_file` varchar(200) DEFAULT NULL,
  `doc_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `doc_status` int(11) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_document_list_tbl`
--

CREATE TABLE `property_document_list_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `documents_id` int(11) NOT NULL DEFAULT 0,
  `type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_gallery_tbl`
--

CREATE TABLE `property_gallery_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `images` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_gendral_documents_tbl`
--

CREATE TABLE `property_gendral_documents_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `title` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `documents` varchar(200) DEFAULT NULL,
  `document_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_rooms_list_tbl`
--

CREATE TABLE `property_rooms_list_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `roomlist_id` int(11) NOT NULL DEFAULT 0,
  `image` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_tbl`
--

CREATE TABLE `property_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `customer_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `house_facing` varchar(200) DEFAULT NULL,
  `projectsize` varchar(200) DEFAULT NULL,
  `projectarea` varchar(200) DEFAULT NULL,
  `cost` varchar(200) DEFAULT NULL,
  `room_id` int(11) NOT NULL DEFAULT 0,
  `block_id` int(11) NOT NULL DEFAULT 0,
  `floor_id` int(11) NOT NULL DEFAULT 0,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `pincode` varchar(200) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `assign_status` int(11) DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `request_type_tbl`
--

CREATE TABLE `request_type_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `request_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_type_tbl`
--

INSERT INTO `request_type_tbl` (`id`, `branch_id`, `token`, `request_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'general', 'General', 1, 1, '2020-06-10 11:34:39', '2020-07-08 01:35:33'),
(2, 0, 'report-a-issue', 'Report a issue', 1, 1, '2020-06-19 14:18:14', '2020-06-19 14:18:14'),
(3, 0, 'other', 'Other', 1, 1, '2020-06-19 15:10:34', '2020-06-19 15:10:34'),
(4, 0, 'work-status-update', 'Work Status Update', 1, 1, '2020-06-19 15:17:48', '2020-06-19 15:17:48');

-- --------------------------------------------------------

--
-- Table structure for table `session_tbl`
--

CREATE TABLE `session_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `logged_id` int(11) NOT NULL DEFAULT 0,
  `user_type` varchar(50) DEFAULT NULL,
  `portal_type` varchar(100) DEFAULT NULL,
  `auth_referer` varchar(50) DEFAULT NULL,
  `auth_medium` varchar(50) DEFAULT NULL,
  `auth_user_agent` text DEFAULT NULL,
  `auth_ip_address` varchar(250) DEFAULT NULL,
  `session_in` datetime NOT NULL,
  `session_out` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session_tbl`
--

INSERT INTO `session_tbl` (`id`, `branch_id`, `logged_id`, `user_type`, `portal_type`, `auth_referer`, `auth_medium`, `auth_user_agent`, `auth_ip_address`, `session_in`, `session_out`) VALUES
(1, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '106.222.116.83', '2020-10-06 15:09:01', NULL),
(2, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '49.205.56.60', '2020-10-06 15:39:26', NULL),
(3, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '157.46.108.197', '2020-10-08 13:14:47', NULL),
(4, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '157.46.108.197', '2020-10-08 13:16:14', NULL),
(5, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '157.46.108.197', '2020-10-08 13:18:20', NULL),
(6, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '106.222.96.54', '2020-10-08 13:21:30', NULL),
(7, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '106.222.96.54', '2020-10-08 13:22:46', NULL),
(8, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '106.222.96.54', '2020-10-08 13:24:56', NULL),
(9, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '106.222.96.54', '2020-10-08 13:30:34', NULL),
(10, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '106.222.96.54', '2020-10-08 13:34:32', NULL),
(11, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '49.37.185.119', '2020-10-08 15:35:09', NULL),
(12, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '49.37.199.218', '2020-10-09 14:40:07', NULL),
(13, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', '49.37.199.218', '2020-10-09 14:41:43', NULL),
(14, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '49.37.185.214', '2020-10-12 10:42:48', NULL),
(15, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '49.37.188.237', '2020-10-15 11:17:27', NULL),
(16, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '49.37.188.126', '2020-10-20 15:23:00', NULL),
(17, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.190.126', '2020-10-27 11:43:35', NULL),
(18, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.187.25', '2020-11-03 14:02:53', NULL),
(19, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', '103.114.209.31', '2020-11-03 14:48:39', NULL),
(20, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.187.25', '2020-11-03 14:53:04', NULL),
(21, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.187.25', '2020-11-03 14:54:20', NULL),
(22, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.187.25', '2020-11-03 14:56:43', NULL),
(23, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.187.25', '2020-11-03 17:08:16', NULL),
(24, 0, 10, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '122.178.175.229', '2020-11-04 10:18:28', NULL),
(25, 0, 10, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0 Mobile/15E148 Safari/604.1', '122.178.175.229', '2020-11-04 10:20:05', NULL),
(26, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.184.154', '2020-11-06 10:29:21', NULL),
(27, 0, 10, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.1 Mobile/15E148 Safari/604.1', '122.178.116.241', '2020-11-07 20:21:01', NULL),
(28, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '49.37.217.56', '2020-11-17 10:37:53', NULL),
(29, 0, 9, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '49.37.217.56', '2020-11-17 10:39:28', NULL),
(30, 0, 9, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.3 Safari/605.1.15', '49.37.217.56', '2020-11-17 10:49:13', NULL),
(31, 0, 9, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.3 Safari/605.1.15', '49.37.217.56', '2020-11-17 10:50:27', NULL),
(32, 0, 9, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.3 Safari/605.1.15', '49.37.217.56', '2020-11-17 14:59:27', NULL),
(33, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', '49.206.171.194', '2020-11-17 17:14:26', NULL),
(34, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '106.222.99.119', '2020-11-18 10:56:41', NULL),
(35, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '106.222.99.119', '2020-11-18 10:59:01', NULL),
(36, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '106.222.99.119', '2020-11-18 11:07:06', NULL),
(37, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', '49.206.170.155', '2020-11-18 11:16:52', NULL),
(38, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '49.37.217.80', '2020-11-18 14:25:46', NULL),
(39, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', '49.37.217.80', '2020-11-18 17:00:40', NULL),
(40, 0, 32, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '49.206.163.187', '2020-11-18 17:29:52', NULL),
(41, 0, 40, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '157.46.65.81', '2020-11-18 18:50:05', NULL),
(42, 0, 47, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0 Mobile/15E148 Safari/604.1', '49.205.101.60', '2020-11-18 22:16:49', NULL),
(43, 0, 24, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0', '157.46.122.170', '2020-11-21 18:29:49', NULL),
(44, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '49.206.160.52', '2020-12-01 10:13:57', NULL),
(45, 0, 10, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.1 Mobile/15E148 Safari/604.1', '223.182.201.47', '2020-12-12 16:30:07', NULL),
(46, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '49.37.217.164', '2020-12-14 12:33:00', NULL),
(47, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '106.222.121.184', '2020-12-14 14:36:00', NULL),
(48, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '106.222.121.184', '2020-12-14 14:38:02', NULL),
(49, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '103.114.209.31', '2020-12-14 14:45:05', NULL),
(50, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '49.37.217.164', '2020-12-14 14:47:49', NULL),
(51, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '103.114.209.31', '2020-12-14 14:54:20', NULL),
(52, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '106.222.99.113', '2020-12-15 08:30:10', NULL),
(53, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '49.37.219.226', '2020-12-19 14:28:59', NULL),
(54, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '49.37.217.56', '2020-12-29 16:56:26', NULL),
(55, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '42.106.84.38', '2020-12-29 20:04:42', NULL),
(56, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '49.37.221.112', '2020-12-30 11:33:12', NULL),
(57, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '106.222.121.186', '2020-12-30 11:36:39', NULL),
(58, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '106.222.113.189', '2021-01-08 14:58:13', NULL),
(59, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '49.37.221.220', '2021-01-09 10:59:41', NULL),
(60, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', '49.37.217.80', '2021-01-12 11:10:16', NULL),
(61, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', '49.37.223.186', '2021-01-18 14:46:58', NULL),
(62, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', '106.222.113.181', '2021-01-19 09:21:05', NULL),
(63, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', '49.37.221.220', '2021-01-20 10:01:09', NULL),
(64, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', '49.37.221.232', '2021-01-21 11:08:08', NULL),
(65, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '106.222.113.190', '2021-02-02 14:46:31', NULL),
(66, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '43.225.165.49', '2021-02-02 14:47:30', NULL),
(67, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '106.222.121.187', '2021-02-02 22:12:30', NULL),
(68, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '43.225.167.38', '2021-02-03 10:13:39', NULL),
(69, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '43.225.167.38', '2021-02-03 15:30:56', NULL),
(70, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '106.222.121.187', '2021-02-03 17:06:40', NULL),
(71, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '103.21.79.24', '2021-02-04 10:37:21', NULL),
(72, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '106.222.121.176', '2021-02-04 10:52:22', NULL),
(73, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '103.21.79.24', '2021-02-04 11:03:59', NULL),
(74, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '103.21.79.24', '2021-02-04 11:04:52', NULL),
(75, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '103.21.79.24', '2021-02-04 11:07:01', NULL),
(76, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '103.21.79.24', '2021-02-04 11:15:01', NULL),
(77, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '106.222.121.176', '2021-02-04 11:17:03', NULL),
(78, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '103.21.79.24', '2021-02-04 12:08:56', NULL),
(79, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.146 Safari/537.36', '103.224.32.255', '2021-02-05 10:03:24', NULL),
(80, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '49.206.172.20', '2021-02-11 10:10:38', NULL),
(81, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '49.206.168.4', '2021-02-12 10:05:54', NULL),
(82, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '49.206.168.4', '2021-02-12 15:15:39', NULL),
(83, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Linux; Android 8.0.0; AUM-AL20) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.96 Mobile Safari/537.36', '42.106.87.163', '2021-02-12 19:58:09', NULL),
(84, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '49.206.185.12', '2021-02-15 13:55:39', NULL),
(85, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '49.206.184.118', '2021-02-16 10:30:49', NULL),
(86, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '117.98.176.106', '2021-02-17 10:10:47', NULL),
(87, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '117.98.176.106', '2021-02-18 11:27:23', NULL),
(88, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', '122.178.82.69', '2021-02-22 10:11:48', NULL),
(89, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', '122.178.82.69', '2021-02-23 10:00:22', NULL),
(90, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '122.178.82.69', '2021-02-24 09:54:45', NULL),
(91, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', '106.222.99.96', '2021-02-24 12:46:11', NULL),
(92, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '122.178.82.69', '2021-02-25 10:28:26', NULL),
(93, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '49.206.160.74', '2021-02-26 10:16:48', NULL),
(94, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.34.227', '2021-02-27 09:57:27', NULL),
(95, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.21.79.20', '2021-03-01 09:46:44', NULL),
(96, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.21.79.20', '2021-03-01 13:06:19', NULL),
(97, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '106.222.121.185', '2021-03-01 13:07:16', NULL),
(98, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.33.87', '2021-03-04 10:17:52', NULL),
(99, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.33.87', '2021-03-04 10:17:52', NULL),
(100, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.33.88', '2021-03-05 09:55:29', NULL),
(101, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.34.234', '2021-03-08 10:21:37', NULL),
(102, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '122.178.195.58', '2021-03-09 10:07:24', NULL),
(103, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '43.225.167.197', '2021-03-10 10:00:14', NULL),
(104, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '43.225.165.51', '2021-03-11 10:13:50', NULL),
(105, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.33.68', '2021-03-12 10:11:51', NULL),
(106, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.32.254', '2021-03-13 10:16:00', NULL),
(107, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.32.254', '2021-03-13 12:30:26', NULL),
(108, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', '103.224.33.73', '2021-03-15 10:36:32', NULL),
(109, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', '103.224.33.73', '2021-03-16 10:25:44', NULL),
(110, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.32.245', '2021-03-17 10:35:57', NULL),
(111, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '43.225.165.50', '2021-03-18 10:09:54', NULL),
(112, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.34.248', '2021-03-19 10:07:44', NULL),
(113, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.21.79.6', '2021-03-22 10:15:38', NULL),
(114, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.33.95', '2021-03-23 10:22:36', NULL),
(115, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '43.225.165.52', '2021-03-24 10:15:15', NULL),
(116, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.21.79.11', '2021-03-25 10:16:39', NULL),
(117, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.21.79.11', '2021-03-25 18:15:56', NULL),
(118, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.33.92', '2021-03-26 10:03:33', NULL),
(119, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.33.76', '2021-03-27 10:01:16', NULL),
(120, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.32.234', '2021-03-29 10:23:47', NULL),
(121, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '106.222.113.186', '2021-03-29 13:09:37', NULL),
(122, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.32.250', '2021-03-30 10:00:57', NULL),
(123, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.21.79.21', '2021-03-31 09:58:12', NULL),
(124, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.21.79.21', '2021-03-31 10:07:41', NULL),
(125, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '157.49.81.8', '2021-04-01 08:52:31', NULL),
(126, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', '106.211.210.45', '2021-04-05 12:13:33', NULL),
(127, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', '106.211.210.48', '2021-04-07 14:27:03', NULL),
(128, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', '157.51.44.201', '2021-04-08 13:14:37', NULL),
(129, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', '157.51.165.95', '2021-04-09 15:35:02', NULL),
(130, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', '157.51.188.246', '2021-04-10 10:52:13', NULL),
(131, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0', '127.0.0.1', '2021-04-12 14:40:21', NULL),
(132, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', '127.0.0.1', '2021-07-08 23:12:13', NULL),
(133, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-07-27 00:34:13', NULL),
(134, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-07-27 01:23:34', NULL),
(135, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-04 10:11:39', NULL),
(136, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-04 10:13:28', NULL),
(137, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-04 10:21:32', NULL),
(138, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-04 10:22:21', NULL),
(139, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-04 11:10:35', NULL),
(140, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-09 10:16:51', NULL),
(141, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '::1', '2021-08-09 10:22:34', NULL),
(142, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '::1', '2021-08-09 10:22:58', NULL),
(143, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '::1', '2021-08-09 10:25:39', NULL),
(144, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-09 10:58:50', NULL),
(145, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-09 11:00:19', NULL),
(146, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '::1', '2021-08-09 11:01:06', NULL),
(147, 0, 7, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '::1', '2021-08-09 11:04:36', NULL),
(148, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-09 11:13:24', NULL),
(149, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-09 11:14:58', NULL),
(150, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '::1', '2021-08-09 11:15:32', NULL),
(151, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-09 11:58:50', NULL),
(152, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', '127.0.0.1', '2021-08-14 13:32:12', NULL),
(153, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', '127.0.0.1', '2021-08-14 19:40:26', NULL),
(154, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', '127.0.0.1', '2021-08-23 15:32:30', NULL),
(155, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', '127.0.0.1', '2021-08-23 15:36:09', NULL),
(156, 0, 13, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', '127.0.0.1', '2021-08-23 15:54:03', NULL),
(157, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 08:33:25', NULL),
(158, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 08:47:55', NULL),
(159, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 08:48:57', NULL),
(160, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 08:50:51', NULL),
(161, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 09:12:43', NULL),
(162, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 09:59:32', NULL),
(163, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 10:45:49', NULL),
(164, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:32:19', NULL),
(165, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:32:45', NULL),
(166, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:33:20', NULL),
(167, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:33:57', NULL),
(168, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:34:23', NULL),
(169, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:34:48', NULL),
(170, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:35:32', NULL),
(171, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:35:49', NULL),
(172, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:36:30', NULL),
(173, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:40:28', NULL),
(174, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:57:52', NULL),
(175, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:00:53', NULL),
(176, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:01:47', NULL),
(177, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:09:36', NULL),
(178, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:09:57', NULL),
(179, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:12:59', NULL),
(180, 0, 7, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:13:53', NULL),
(181, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:24:48', NULL),
(182, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:44:40', NULL),
(183, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:50:41', NULL),
(184, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:51:03', NULL),
(185, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:54:33', NULL),
(186, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:55:57', NULL),
(187, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:56:30', NULL),
(188, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:56:42', NULL),
(189, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 16:25:42', NULL),
(190, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 16:25:55', NULL),
(191, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 16:29:08', NULL),
(192, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 16:29:30', NULL),
(193, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 16:30:21', NULL),
(194, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:06:17', NULL),
(195, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:08:55', NULL),
(196, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:16:35', NULL),
(197, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:42:30', NULL),
(198, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:42:50', NULL),
(199, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:44:24', NULL),
(200, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:50:22', NULL),
(201, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:55:17', NULL),
(202, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:59:24', NULL),
(203, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:00:05', NULL),
(204, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:05:35', NULL),
(205, 0, 14, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:06:56', NULL),
(206, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:10:58', NULL),
(207, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:11:50', NULL),
(208, 0, 15, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:13:31', NULL),
(209, 0, 14, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:13:47', NULL),
(210, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:14:46', NULL),
(211, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:21:13', NULL),
(212, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:28:58', NULL),
(213, 0, 14, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:29:16', NULL),
(214, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:29:23', NULL),
(215, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-25 09:24:30', NULL),
(216, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-25 09:25:25', NULL),
(217, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-25 09:57:35', NULL),
(218, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:05:22', NULL),
(219, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:07:40', NULL),
(220, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:08:11', NULL),
(221, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:25:32', NULL),
(222, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:26:10', NULL),
(223, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:36:41', NULL),
(224, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:37:53', NULL),
(225, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 11:42:03', NULL),
(226, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 13:17:04', NULL),
(227, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 16:19:49', NULL),
(228, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-27 09:50:44', NULL),
(229, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-27 10:44:39', NULL),
(230, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-27 11:00:15', NULL),
(231, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-27 11:14:19', NULL),
(232, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-27 18:21:20', NULL),
(233, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-27 19:24:19', NULL),
(234, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:20:57', NULL),
(235, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:21:00', NULL),
(236, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:21:01', NULL),
(237, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:21:01', NULL),
(238, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:21:03', NULL),
(239, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:21:05', NULL),
(240, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:21:05', NULL);
INSERT INTO `session_tbl` (`id`, `branch_id`, `logged_id`, `user_type`, `portal_type`, `auth_referer`, `auth_medium`, `auth_user_agent`, `auth_ip_address`, `session_in`, `session_out`) VALUES
(241, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:21:21', NULL),
(242, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:21:23', NULL),
(243, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:41:19', NULL),
(244, 0, 17, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:52:38', NULL),
(245, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:54:22', NULL),
(246, 0, 17, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:54:30', NULL),
(247, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 09:57:56', NULL),
(248, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:06:18', NULL),
(249, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:10:11', NULL),
(250, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:11:27', NULL),
(251, 0, 17, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:12:00', NULL),
(252, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:12:38', NULL),
(253, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:12:50', NULL),
(254, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:32:30', NULL),
(255, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:34:35', NULL),
(256, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:35:06', NULL),
(257, 0, 17, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:35:23', NULL),
(258, 0, 17, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:35:38', NULL),
(259, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:41:34', NULL),
(260, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:44:19', NULL),
(261, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:44:42', NULL),
(262, 0, 17, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:45:10', NULL),
(263, 0, 17, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:45:34', NULL),
(264, 0, 17, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:46:08', NULL),
(265, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 10:58:10', NULL),
(266, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 11:25:40', NULL),
(267, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 11:40:59', NULL),
(268, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 12:11:48', NULL),
(269, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 12:12:26', NULL),
(270, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 13:27:14', NULL),
(271, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 14:22:55', NULL),
(272, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 16:56:15', NULL),
(273, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 16:57:10', NULL),
(274, 0, 16, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 17:02:43', NULL),
(275, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 17:12:15', NULL),
(276, 0, 20, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 17:29:43', NULL),
(277, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 17:30:02', NULL),
(278, 0, 22, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 17:40:07', NULL),
(279, 0, 22, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 17:55:14', NULL),
(280, 0, 22, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 17:58:23', NULL),
(281, 0, 22, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 17:58:49', NULL),
(282, 0, 22, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 17:59:03', NULL),
(283, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 18:00:43', NULL),
(284, 0, 22, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 18:05:37', NULL),
(285, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 18:20:20', NULL),
(286, 0, 25, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 19:37:31', NULL),
(287, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 19:37:49', NULL),
(288, 0, 26, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-28 19:40:39', NULL),
(289, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 09:22:49', NULL),
(290, 0, 22, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 09:28:29', NULL),
(291, 0, 22, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 09:29:06', NULL),
(292, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 09:29:57', NULL),
(293, 0, 33, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 12:21:36', NULL),
(294, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 12:23:40', NULL),
(295, 0, 11, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 12:23:53', NULL),
(296, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 12:24:15', NULL),
(297, 0, 11, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 12:25:07', NULL),
(298, 0, 33, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 12:27:17', NULL),
(299, 0, 35, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 12:32:22', NULL),
(300, 0, 33, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 12:32:45', NULL),
(301, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 12:35:15', NULL),
(302, 0, 11, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 12:35:27', NULL),
(303, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 12:44:32', NULL),
(304, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 12:47:53', NULL),
(305, 0, 11, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 13:07:50', NULL),
(306, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 13:08:00', NULL),
(307, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 13:52:02', NULL),
(308, 0, 48, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 14:18:30', NULL),
(309, 0, 11, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 14:20:34', NULL),
(310, 0, 55, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 14:34:10', NULL),
(311, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 14:34:42', NULL),
(312, 0, 60, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 14:44:53', NULL),
(313, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 15:28:41', NULL),
(314, 0, 60, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 15:33:27', NULL),
(315, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 15:34:38', NULL),
(316, 0, 61, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 15:37:32', NULL),
(317, 0, 61, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 15:41:11', NULL),
(318, 0, 62, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 15:41:26', NULL),
(319, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 15:44:04', NULL),
(320, 0, 61, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 15:49:38', NULL),
(321, 0, 60, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 15:51:30', NULL),
(322, 0, 61, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 15:55:09', NULL),
(323, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 16:14:35', NULL),
(324, 0, 60, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 16:16:59', NULL),
(325, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 16:21:14', NULL),
(326, 0, 73, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 16:26:10', NULL),
(327, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 16:46:27', NULL),
(328, 0, 72, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 19:00:05', NULL),
(329, 0, 74, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 19:03:01', NULL),
(330, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 19:03:30', NULL),
(331, 0, 74, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 19:04:08', NULL),
(332, 0, 72, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 19:05:13', NULL),
(333, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 19:06:12', NULL),
(334, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 20:52:20', NULL),
(335, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-30 22:56:53', NULL),
(336, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 05:48:45', NULL),
(337, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 05:48:46', NULL),
(338, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 05:48:47', NULL),
(339, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 05:53:48', NULL),
(340, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:05:30', NULL),
(341, 0, 73, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:06:02', NULL),
(342, 0, 76, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:06:30', NULL),
(343, 0, 72, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:14:23', NULL),
(344, 0, 74, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:16:57', NULL),
(345, 0, 73, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:23:29', NULL),
(346, 0, 72, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:23:50', NULL),
(347, 0, 73, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:25:45', NULL),
(348, 0, 73, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:27:14', NULL),
(349, 0, 72, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:38:07', NULL),
(350, 0, 73, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:39:31', NULL),
(351, 0, 74, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:40:41', NULL),
(352, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 08:43:04', NULL),
(353, 0, 73, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 09:04:47', NULL),
(354, 0, 73, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 09:42:22', NULL),
(355, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 09:44:06', NULL),
(356, 0, 72, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 10:04:42', NULL),
(357, 0, 73, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 10:05:32', NULL),
(358, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 10:19:14', NULL),
(359, 0, 72, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 10:20:46', NULL),
(360, 0, 73, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 10:21:27', NULL),
(361, 0, 72, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 10:24:14', NULL),
(362, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 10:33:15', NULL),
(363, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-31 17:53:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `temp_customer_excel_tbl`
--

CREATE TABLE `temp_customer_excel_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `upload_token` varchar(50) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `addresss` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_excel_tbl`
--

CREATE TABLE `temp_excel_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `upload_token` varchar(50) DEFAULT NULL,
  `uid` varchar(80) DEFAULT NULL,
  `enquiry_date` varchar(50) DEFAULT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `lname` varchar(200) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `primary_mobile` varchar(200) DEFAULT NULL,
  `primary_email` varchar(200) DEFAULT NULL,
  `profile` varchar(200) DEFAULT NULL,
  `leadsource` varchar(200) DEFAULT NULL,
  `lead_status` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `pincode` varchar(150) DEFAULT NULL,
  `chit` varchar(100) DEFAULT NULL,
  `deposite` varchar(100) DEFAULT NULL,
  `vas` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_excel_tbl`
--

INSERT INTO `temp_excel_tbl` (`id`, `branch_id`, `upload_token`, `uid`, `enquiry_date`, `fname`, `lname`, `gender`, `primary_mobile`, `primary_email`, `profile`, `leadsource`, `lead_status`, `address`, `city`, `state`, `description`, `pincode`, `chit`, `deposite`, `vas`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 'WX2QeJwrLRAEg0N', NULL, '2021-08-30', 'Excel Export', 'A', 'Male', '9847563738', 'excelanandbechris@gmail.com', 'Web Developer', 'Land', 'Hot Deal', 'VVCR nager', 'Erode', 'Tamilnadu', 'No Comments', '638001', '3L', '15L', '', '2021-08-30 23:17:48', '2021-08-30 23:17:48', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vas_master_tbl`
--

CREATE TABLE `vas_master_tbl` (
  `id` int(11) NOT NULL,
  `token` varchar(200) DEFAULT NULL,
  `item_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vas_master_tbl`
--

INSERT INTO `vas_master_tbl` (`id`, `token`, `item_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'insurance', 'Insurance', 1, 1, '2021-07-26 13:53:15', '2021-07-26 13:54:51'),
(2, 'finance', 'Finance', 1, 1, '2021-07-26 13:55:00', '2021-07-26 13:55:00'),
(3, 'housingloan', 'Housing Loan', 1, 1, '2021-08-30 21:21:31', '2021-08-30 21:21:31');

-- --------------------------------------------------------

--
-- Table structure for table `virtualtour_menusettings_tbl`
--

CREATE TABLE `virtualtour_menusettings_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `menu` varchar(200) NOT NULL DEFAULT '0',
  `tour_code` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `virtualtour_menusettings_tbl`
--

INSERT INTO `virtualtour_menusettings_tbl` (`id`, `branch_id`, `menu`, `tour_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'hide', '', 1, '2020-10-06 15:13:33', '2020-10-06 15:13:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_type_tbl`
--
ALTER TABLE `activity_type_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adhoc_image_tbl`
--
ALTER TABLE `adhoc_image_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adhoc_log_tbl`
--
ALTER TABLE `adhoc_log_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adhoc_request_tbl`
--
ALTER TABLE `adhoc_request_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignlead_tbl`
--
ALTER TABLE `assignlead_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_tbl`
--
ALTER TABLE `block_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch_master_tbl`
--
ALTER TABLE `branch_master_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chit_master_tbl`
--
ALTER TABLE `chit_master_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_settings_tbl`
--
ALTER TABLE `company_settings_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactinfo_list_tbl`
--
ALTER TABLE `contactinfo_list_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactinfo_tbl`
--
ALTER TABLE `contactinfo_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customerdatasheet_tbl`
--
ALTER TABLE `customerdatasheet_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_profile_tbl`
--
ALTER TABLE `customer_profile_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_tbl`
--
ALTER TABLE `customer_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datasheet_tbl`
--
ALTER TABLE `datasheet_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposite_master_tbl`
--
ALTER TABLE `deposite_master_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_permission_tbl`
--
ALTER TABLE `employee_permission_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_role_master_tbl`
--
ALTER TABLE `employee_role_master_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_tbl`
--
ALTER TABLE `employee_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flatvilla_documenttype_list_tbl`
--
ALTER TABLE `flatvilla_documenttype_list_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flatvilla_documenttype_tbl`
--
ALTER TABLE `flatvilla_documenttype_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_type_master`
--
ALTER TABLE `flat_type_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_type_masterlist`
--
ALTER TABLE `flat_type_masterlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `floor_tbl`
--
ALTER TABLE `floor_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_image_tbl`
--
ALTER TABLE `gallery_image_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_tbl`
--
ALTER TABLE `gallery_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gendral_documents_tbl`
--
ALTER TABLE `gendral_documents_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_tbl`
--
ALTER TABLE `invoice_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kyc_list_tbl`
--
ALTER TABLE `kyc_list_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kyc_tbl`
--
ALTER TABLE `kyc_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kyc_type_tbl`
--
ALTER TABLE `kyc_type_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_activity_tbl`
--
ALTER TABLE `lead_activity_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_gallery_tbl`
--
ALTER TABLE `lead_gallery_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_logs_tbl`
--
ALTER TABLE `lead_logs_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_source_tbl`
--
ALTER TABLE `lead_source_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_tbl`
--
ALTER TABLE `lead_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_type_tbl`
--
ALTER TABLE `lead_type_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_tbl`
--
ALTER TABLE `news_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_email_tbl`
--
ALTER TABLE `notification_email_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paymentinfo_tbl`
--
ALTER TABLE `paymentinfo_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_type_tbl`
--
ALTER TABLE `project_type_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_broucher_tbl`
--
ALTER TABLE `property_broucher_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_document_list_items`
--
ALTER TABLE `property_document_list_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_document_list_tbl`
--
ALTER TABLE `property_document_list_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_gallery_tbl`
--
ALTER TABLE `property_gallery_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_gendral_documents_tbl`
--
ALTER TABLE `property_gendral_documents_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_rooms_list_tbl`
--
ALTER TABLE `property_rooms_list_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_tbl`
--
ALTER TABLE `property_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_type_tbl`
--
ALTER TABLE `request_type_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session_tbl`
--
ALTER TABLE `session_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_customer_excel_tbl`
--
ALTER TABLE `temp_customer_excel_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_excel_tbl`
--
ALTER TABLE `temp_excel_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vas_master_tbl`
--
ALTER TABLE `vas_master_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `virtualtour_menusettings_tbl`
--
ALTER TABLE `virtualtour_menusettings_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_type_tbl`
--
ALTER TABLE `activity_type_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `adhoc_image_tbl`
--
ALTER TABLE `adhoc_image_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adhoc_log_tbl`
--
ALTER TABLE `adhoc_log_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adhoc_request_tbl`
--
ALTER TABLE `adhoc_request_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `assignlead_tbl`
--
ALTER TABLE `assignlead_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `block_tbl`
--
ALTER TABLE `block_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `branch_master_tbl`
--
ALTER TABLE `branch_master_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `chit_master_tbl`
--
ALTER TABLE `chit_master_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `company_settings_tbl`
--
ALTER TABLE `company_settings_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contactinfo_list_tbl`
--
ALTER TABLE `contactinfo_list_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contactinfo_tbl`
--
ALTER TABLE `contactinfo_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customerdatasheet_tbl`
--
ALTER TABLE `customerdatasheet_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer_profile_tbl`
--
ALTER TABLE `customer_profile_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=362;

--
-- AUTO_INCREMENT for table `customer_tbl`
--
ALTER TABLE `customer_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `datasheet_tbl`
--
ALTER TABLE `datasheet_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deposite_master_tbl`
--
ALTER TABLE `deposite_master_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `employee_permission_tbl`
--
ALTER TABLE `employee_permission_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `employee_role_master_tbl`
--
ALTER TABLE `employee_role_master_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `employee_tbl`
--
ALTER TABLE `employee_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `flatvilla_documenttype_list_tbl`
--
ALTER TABLE `flatvilla_documenttype_list_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `flatvilla_documenttype_tbl`
--
ALTER TABLE `flatvilla_documenttype_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `flat_type_master`
--
ALTER TABLE `flat_type_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `flat_type_masterlist`
--
ALTER TABLE `flat_type_masterlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `floor_tbl`
--
ALTER TABLE `floor_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `gallery_image_tbl`
--
ALTER TABLE `gallery_image_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gallery_tbl`
--
ALTER TABLE `gallery_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gendral_documents_tbl`
--
ALTER TABLE `gendral_documents_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_tbl`
--
ALTER TABLE `invoice_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kyc_list_tbl`
--
ALTER TABLE `kyc_list_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kyc_tbl`
--
ALTER TABLE `kyc_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kyc_type_tbl`
--
ALTER TABLE `kyc_type_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `lead_activity_tbl`
--
ALTER TABLE `lead_activity_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lead_gallery_tbl`
--
ALTER TABLE `lead_gallery_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lead_logs_tbl`
--
ALTER TABLE `lead_logs_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lead_source_tbl`
--
ALTER TABLE `lead_source_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `lead_tbl`
--
ALTER TABLE `lead_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lead_type_tbl`
--
ALTER TABLE `lead_type_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `news_tbl`
--
ALTER TABLE `news_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_email_tbl`
--
ALTER TABLE `notification_email_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paymentinfo_tbl`
--
ALTER TABLE `paymentinfo_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_type_tbl`
--
ALTER TABLE `project_type_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_broucher_tbl`
--
ALTER TABLE `property_broucher_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_document_list_items`
--
ALTER TABLE `property_document_list_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_document_list_tbl`
--
ALTER TABLE `property_document_list_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_gallery_tbl`
--
ALTER TABLE `property_gallery_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_gendral_documents_tbl`
--
ALTER TABLE `property_gendral_documents_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_rooms_list_tbl`
--
ALTER TABLE `property_rooms_list_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_tbl`
--
ALTER TABLE `property_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_type_tbl`
--
ALTER TABLE `request_type_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `session_tbl`
--
ALTER TABLE `session_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=364;

--
-- AUTO_INCREMENT for table `temp_customer_excel_tbl`
--
ALTER TABLE `temp_customer_excel_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_excel_tbl`
--
ALTER TABLE `temp_excel_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vas_master_tbl`
--
ALTER TABLE `vas_master_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `virtualtour_menusettings_tbl`
--
ALTER TABLE `virtualtour_menusettings_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
