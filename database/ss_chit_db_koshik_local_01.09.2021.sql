-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 01, 2021 at 06:57 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ss_chit_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_type_tbl`
--

DROP TABLE IF EXISTS `activity_type_tbl`;
CREATE TABLE IF NOT EXISTS `activity_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `activity_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_type_tbl`
--

INSERT INTO `activity_type_tbl` (`id`, `branch_id`, `token`, `activity_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'holdcall', 'hold Call', 1, 1, '2020-08-12 22:00:16', '2021-09-01 10:26:30'),
(2, 1, 'follow-up-1', 'Follow Up 1', 1, 1, '2020-08-13 09:50:59', '2020-08-17 19:40:01'),
(3, 1, 'follow-up-2', 'Follow Up 2', 1, 1, '2020-08-17 18:41:51', '2020-08-17 19:40:06'),
(4, 1, 'inprocess', 'In Process', 1, 1, '2020-08-17 18:41:57', '2020-10-08 12:32:33'),
(5, 1, 'completed', 'completed', 1, 1, '2020-08-17 18:42:02', '2020-08-17 19:40:54');

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_image_tbl`
--

DROP TABLE IF EXISTS `adhoc_image_tbl`;
CREATE TABLE IF NOT EXISTS `adhoc_image_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `ref_id` int(11) NOT NULL DEFAULT 0,
  `adoc_id` int(11) DEFAULT 0,
  `property_id` int(11) DEFAULT 0,
  `customer_id` int(11) DEFAULT 0,
  `image_name` varchar(200) DEFAULT NULL,
  `file_type` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_log_tbl`
--

DROP TABLE IF EXISTS `adhoc_log_tbl`;
CREATE TABLE IF NOT EXISTS `adhoc_log_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `type` varchar(200) DEFAULT NULL,
  `request_type` varchar(200) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_to` int(11) NOT NULL DEFAULT 0,
  `ref_id` int(11) NOT NULL DEFAULT 0,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_request_tbl`
--

DROP TABLE IF EXISTS `adhoc_request_tbl`;
CREATE TABLE IF NOT EXISTS `adhoc_request_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `ticket_uid` varchar(200) DEFAULT NULL,
  `property_id` int(11) DEFAULT 0,
  `customer_id` int(11) DEFAULT 0,
  `employee_id` int(11) DEFAULT 0,
  `ref_id` int(11) DEFAULT 0,
  `type` varchar(200) DEFAULT NULL,
  `request_type` varchar(200) DEFAULT NULL,
  `requesttype_id` int(11) NOT NULL DEFAULT 0,
  `subject` varchar(200) DEFAULT NULL,
  `remarks` longtext DEFAULT NULL,
  `request_status` int(11) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_tbl`
--

DROP TABLE IF EXISTS `admin_tbl`;
CREATE TABLE IF NOT EXISTS `admin_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `ad_token` varchar(200) DEFAULT NULL,
  `ad_name` varchar(200) DEFAULT NULL,
  `ad_mobile` varchar(50) DEFAULT NULL,
  `ad_email` varchar(200) DEFAULT NULL,
  `ad_dob` varchar(200) DEFAULT NULL,
  `address1` varchar(200) DEFAULT NULL,
  `address2` varchar(200) DEFAULT NULL,
  `ad_city` varchar(200) DEFAULT NULL,
  `ad_pincode` varchar(200) DEFAULT NULL,
  `ad_password` varchar(200) DEFAULT NULL,
  `ad_type` int(1) NOT NULL DEFAULT 0,
  `ad_super_admin` int(1) NOT NULL DEFAULT 0,
  `ad_status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_tbl`
--

INSERT INTO `admin_tbl` (`id`, `branch_id`, `ad_token`, `ad_name`, `ad_mobile`, `ad_email`, `ad_dob`, `address1`, `address2`, `ad_city`, `ad_pincode`, `ad_password`, `ad_type`, `ad_super_admin`, `ad_status`, `created_at`, `updated_at`) VALUES
(1, 0, 'admin', 'Admin', '987654321', 'admin@leadcrm.com', '2020-05-30', 'xxxxxx', 'yyyyy', 'cbe', 'tamil nadu', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 1, 1, 1, '2016-06-01 10:00:00', '2020-05-28 09:33:18');

-- --------------------------------------------------------

--
-- Table structure for table `assignlead_tbl`
--

DROP TABLE IF EXISTS `assignlead_tbl`;
CREATE TABLE IF NOT EXISTS `assignlead_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `type` varchar(200) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_to` int(11) NOT NULL DEFAULT 0,
  `ref_id` int(11) DEFAULT 0,
  `priority` varchar(200) DEFAULT NULL,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `remove_status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `block_tbl`
--

DROP TABLE IF EXISTS `block_tbl`;
CREATE TABLE IF NOT EXISTS `block_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `block` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `branch_master_tbl`
--

DROP TABLE IF EXISTS `branch_master_tbl`;
CREATE TABLE IF NOT EXISTS `branch_master_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(200) DEFAULT NULL,
  `branch` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch_master_tbl`
--

INSERT INTO `branch_master_tbl` (`id`, `token`, `branch`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'coimbatore', 'Coimbatore', 1, 1, '2021-08-24 10:05:49', '2021-08-24 10:05:49'),
(2, 'salem', 'Salem', 1, 1, '2021-08-24 13:31:10', '2021-08-24 13:31:10'),
(3, 'erode', 'Erode', 1, 1, '2021-08-27 20:00:31', '2021-08-27 20:00:31'),
(4, 'madurai', 'Madurai', 1, 1, '2021-08-27 20:00:40', '2021-08-27 20:00:40');

-- --------------------------------------------------------

--
-- Table structure for table `chit_master_tbl`
--

DROP TABLE IF EXISTS `chit_master_tbl`;
CREATE TABLE IF NOT EXISTS `chit_master_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `item_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chit_master_tbl`
--

INSERT INTO `chit_master_tbl` (`id`, `branch_id`, `token`, `item_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '1l', '1L', 1, 1, '2021-07-26 12:02:45', '2021-07-26 12:58:37'),
(2, 0, '2l', '2L', 1, 1, '2021-07-26 12:04:06', '2021-07-26 12:04:06'),
(3, 0, '5l', '5L', 1, 1, '2021-07-26 12:04:12', '2021-07-26 12:04:12'),
(4, 0, '10l', '10L', 1, 1, '2021-07-26 12:04:24', '2021-07-26 12:04:24');

-- --------------------------------------------------------

--
-- Table structure for table `company_settings_tbl`
--

DROP TABLE IF EXISTS `company_settings_tbl`;
CREATE TABLE IF NOT EXISTS `company_settings_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `phone` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `website_address` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_settings_tbl`
--

INSERT INTO `company_settings_tbl` (`id`, `branch_id`, `phone`, `email`, `website_address`, `address`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '9942387100', 'admin@rtohome.com', 'admin@rtohome.com', '99A vyasuva building', 1, 1, '2020-07-22 21:52:59', '2020-07-22 21:52:59');

-- --------------------------------------------------------

--
-- Table structure for table `contactinfo_list_tbl`
--

DROP TABLE IF EXISTS `contactinfo_list_tbl`;
CREATE TABLE IF NOT EXISTS `contactinfo_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `contact_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(200) DEFAULT NULL,
  `designation` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contactinfo_tbl`
--

DROP TABLE IF EXISTS `contactinfo_tbl`;
CREATE TABLE IF NOT EXISTS `contactinfo_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `role` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contactinfo_tbl`
--

INSERT INTO `contactinfo_tbl` (`id`, `branch_id`, `token`, `name`, `role`, `email`, `mobile`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(2, 0, 'admin', 'admin', 'Admin', 'admin@rtohomes.com', '1234567890', 1, 1, '2020-06-06 10:35:33', '2020-10-06 15:11:16');

-- --------------------------------------------------------

--
-- Table structure for table `customerdatasheet_tbl`
--

DROP TABLE IF EXISTS `customerdatasheet_tbl`;
CREATE TABLE IF NOT EXISTS `customerdatasheet_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `import_token` varchar(50) DEFAULT NULL,
  `uploaded_sheet` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customerdatasheet_tbl`
--

INSERT INTO `customerdatasheet_tbl` (`id`, `branch_id`, `import_token`, `uploaded_sheet`, `created_at`, `updated_at`, `status`) VALUES
(7, 1, 'jvc8BMcRSOVQb2Z', '01-09-2021_jTnXHK26_cust test.xlsx', '2021-09-01 10:21:26', '2021-09-01 10:21:26', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_profile_tbl`
--

DROP TABLE IF EXISTS `customer_profile_tbl`;
CREATE TABLE IF NOT EXISTS `customer_profile_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `customer_profile` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_profile_tbl`
--

INSERT INTO `customer_profile_tbl` (`id`, `branch_id`, `token`, `customer_profile`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'doctor', 'Doctor', 1, 1, '2020-09-22 15:38:24', '2020-09-28 18:13:32'),
(2, 0, 'engineer', 'Engineer', 1, 1, '2020-09-28 18:13:38', '2020-09-28 18:13:38'),
(3, 0, 'business', 'Business', 1, 1, '2020-09-28 18:13:46', '2020-09-28 18:13:46'),
(4, 0, 'employee', 'Employee', 1, 1, '2020-09-28 18:14:05', '2020-09-28 18:14:05'),
(5, 0, 'lawyer', 'Lawyer', 1, 1, '2020-09-28 18:14:17', '2020-09-28 18:14:17'),
(6, 0, 'gg', 'Gg', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(7, 0, 'govtservices', 'govt services', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(8, 0, 'proprietor', 'proprietor', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(9, 0, 'manager', 'Manager', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(10, 0, 'ownuse', 'Own use', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(11, 0, 'guard', 'Guard', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(12, 0, 'chairmanmanagingdirectorandceo', 'Chairman Managing Director and CEO', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(13, 0, 'student', 'Student', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(14, 0, 'healthconsultant', 'Health Consultant', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(15, 0, 'assistantprofessor', 'Assistant Professor', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(16, 0, 'housewife', 'House wife', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(17, 0, 'ownerphotographer', 'Owner/Photographer', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(18, 0, 'h', 'h', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(19, 0, 'seniormanager', 'Senior Manager', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(20, 0, 'owner', 'Owner', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(21, 0, 'lecturer', 'Lecturer', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(22, 0, 'villa', 'Villa', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(23, 0, 'directorofphysicaleducation', 'Director of Physical Education', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(24, 0, 'freelancewriter', 'Freelance writer', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(25, 0, 'buyingvillaatcbe', 'buying villa at cbe', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(26, 0, 'soleproprietorship', 'Sole proprietorship', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(27, 0, '', '', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(28, 0, 'nb', 'nb', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(29, 0, 'md', 'Md', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(30, 0, 'flatdetails', 'flat details', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(31, 0, 'villaneeded', 'villa,needed', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(32, 0, 'selfemployed', 'Self-employed', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(33, 0, 'villas', 'villas', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(34, 0, 'commerciallineagency', 'Commercial Line - Agency', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(35, 0, 'regionalhead', 'Regional head', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(36, 0, 'ownbuisness', 'own buisness', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(37, 0, 'relationshipmanager', 'Relationship Manager', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(38, 0, 'workinginpsgcollege', 'Working in PSG college', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(39, 0, 'wealreadyvisitedandsatisfied.', 'We already visited and satisfied.', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(40, 0, 'pastorandevanglist', 'pastor and evanglist', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(41, 0, 'tamilnadugenerationanddistributioncorporationlimited', 'Tamil nadu Generation and Distribution corporation Limited', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(42, 0, 'buildingcontractor', 'building contractor', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(43, 0, 'private', 'Private', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(44, 0, '4bhkandamenities', '4bhk and amenities', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(45, 0, 'branchhead', 'Branch Head', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(46, 0, 'house', 'House', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(47, 0, 'headofthedepartment', 'Head of the department', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(48, 0, 'software', 'software', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(49, 0, 'mobileappdeveloper', 'Mobile App Developer', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(50, 0, 'section-officer', 'Section officer', 1, 1, '2020-10-20 15:31:12', '2020-10-20 15:31:12'),
(51, 0, 'associate', 'Associate', 1, 1, '2020-10-20 16:12:42', '2020-10-20 16:12:42'),
(52, 0, 'project-manager', 'Project manager', 1, 1, '2020-10-20 16:22:41', '2020-10-20 16:22:41'),
(53, 0, 'consultant', 'Consultant', 1, 1, '2020-10-20 16:29:05', '2020-10-20 16:29:05'),
(54, 0, 'currentlyworking', 'Currently Working', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(55, 0, 'boss', 'Boss', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(56, 0, 'was', 'was', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(57, 0, 'hi', 'hi', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(58, 0, 'designengineer', 'Design Engineer', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(59, 0, 'gmfinance', 'GM Finance', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(60, 0, 'salesmanager', 'sales manager', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(61, 0, 'foundermanagingdirector', 'Founder & Managing Director', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(62, 0, 'wherethialocationinpodanur3bedroomappramnetorvilla', 'Where thia location in podanur 3 bed room appramnet or villA', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(63, 0, 'founderandmanagingdirector', 'Founder and Managing Director', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(64, 0, 'ownbusiness', 'Own-Business', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(65, 0, 'technicalsalesengineer', 'Technical Sales Engineer', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(66, 0, 'charteredaccountant', 'Chartered accountant', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(67, 0, 'bpo', 'BPO', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(68, 0, 'humanresourcescoordinatorhrcoordinator', 'Human Resources Coordinator (HR Coordinator)', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(69, 0, 'managingdirector', 'Managing Director', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(70, 0, 'momscateringparisutham', 'Moms Catering Parisutham', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(71, 0, 'home', 'Home', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(72, 0, 'area', 'area', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(73, 0, 'grouplead', 'GroupLead', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(74, 0, 'distributionhead', 'distribution head', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(75, 0, 'madrashighcourt', 'Madras high court', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(76, 0, 'lead', 'lead', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(77, 0, 'ownerandfounder', 'Owner and Founder', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(78, 0, 'directcall', 'Direct call', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(79, 0, 'softwareengineer', 'Software engineer', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(80, 0, '.location', '.Location ???', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(81, 0, '2bhk', '2bhk', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(82, 0, 'teacher', 'Teacher', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(83, 0, 'teaching', 'teaching', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(84, 0, 'system-admin', 'System Admin', 1, 1, '2020-11-06 10:58:10', '2020-11-06 10:58:10'),
(85, 0, 'bde', 'Bde', 1, 1, '2020-11-06 10:58:41', '2020-11-06 10:58:41'),
(86, 0, 'it', 'IT', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(87, 0, 'seniorsystemsengineer', 'senior systems engineer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(88, 0, 'homemaker', 'homemaker', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(89, 0, 'plsendmethedetailspricesforreadytooccupy', 'Pl send me the details, prices for ready to occupy', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(90, 0, 'seniorsystemexecutive', 'Senior System Executive', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(91, 0, 'agiiitech', 'AG-III(Tech)', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(92, 0, 'subregister', 'sub register', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(93, 0, 'officer', 'officer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(94, 0, 'earthmovingspats', 'Earth Moving Spats', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(95, 0, 'cloudmigrationarchitect', 'Cloud Migration Architect', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(96, 0, 'salesofficer', 'SALES OFFICER', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(97, 0, 'phdcandidateresearcher', 'PhD Candidate/Researcher', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(98, 0, 'salaried', 'salaried', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(99, 0, 'assistant', 'Assistant', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(100, 0, 'newhome', 'New Home', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(101, 0, 'govtemployee', 'Govt employee', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(102, 0, 'purchase', 'purchase', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(103, 0, 'busness', 'Busness', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(104, 0, 'interested', 'interested', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(105, 0, 'driving', 'Driving', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(106, 0, 'proprietorofagcinterior', 'proprietor of Agcinterior', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(107, 0, 'need3bhkindividualhomesvillas', 'Need 3 BHK Individual homes villas', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(108, 0, 'owneroperator', 'Owner Operator', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(109, 0, 'marketingmanager', 'marketing manager', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(110, 0, 'territorymanager', 'Territory Manager', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(111, 0, 's', 's', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(112, 0, 'governmentorganization', 'Government organization', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(113, 0, 'qualityassuranceexecutive', 'Quality assurance executive', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(114, 0, 'specialist', 'Specialist', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(115, 0, 'managerqualityservices', 'Manager - Quality Services', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(116, 0, 'generalmanageroperationsaccounts', 'General Manager - Operations & Accounts', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(117, 0, 'professor', 'Professor', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(118, 0, 'securityconsultant', 'security consultant', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(119, 0, 'marketing', 'Marketing', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(120, 0, 'seniorqaengineer', 'Senior QA Engineer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(121, 0, 'trader', 'Trader', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(122, 0, 'enguir', 'enguir', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(123, 0, 'enigineer', 'Enigineer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(124, 0, 'independenthouse3bedroomsatcoimbatore', 'Independent house - 3 bedrooms at Coimbatore', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(125, 0, 'needkeralatypefrontsidetiltedroofhousers40lto50l.housingloanelegible.', 'Need Kerala type (front side tilted roof) house, Rs 40L to 50L. Housing loan elegible.', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(126, 0, 'lookingfor3bhkvillas', 'Looking for 3bhk villas', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(127, 0, 'pleasecallby3clk', 'Please call by 3clk', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(128, 0, 'bussiness', 'Bussiness', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(129, 0, 'camengineer', 'CAM Engineer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(130, 0, 'no', 'no', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(131, 0, 'insurance', 'insurance', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(132, 0, 'mycustomerisinbombaywantsvillawith3bhkincoimbatorepleaseshareit', 'My customer is in Bombay wants villa with 3 bhk in coimbatore please share it', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(133, 0, 'registeredmassagetherapist', 'Registered Massage Therapist', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(134, 0, 'accounts', 'accounts', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(135, 0, 'army', 'army', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(136, 0, 'buy', 'Buy', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(137, 0, 'music', 'music', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(138, 0, 'siteengineer', 'Site Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(139, 0, 'tobuy', 'To buy', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(140, 0, 'analyst', 'Analyst', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(141, 0, 'upvcwindowsfabricator', 'upvc windows fabricator', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(142, 0, 'executive', 'executive', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(143, 0, 'itprofessional', 'IT professional', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(144, 0, 'executiveofficer', 'Executive Officer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(145, 0, 'hr', 'HR', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(146, 0, 'plcengineer', 'PLC engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(147, 0, 'storemanager', 'Store Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(148, 0, 'singanallur', 'singanallur', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(149, 0, 'underwritingmanager', 'Underwriting Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(150, 0, 'seniorgraphicdesigner', 'Senior Graphic Designer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(151, 0, 'regionalsalesmanager', 'Regional Sales Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(152, 0, 'managingpartner', 'Managing Partner', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(153, 0, 'computeroperator', 'Computer Operator', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(154, 0, 'seniorofficer', 'Senior Officer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(155, 0, 'materialsale', 'material sale', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(156, 0, 'managerfinance', 'Manager - Finance', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(157, 0, 'miss', 'miss', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(158, 0, 'trainer', 'trainer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(159, 0, 'na', 'Na', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(160, 0, 'working', 'working', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(161, 0, 'projectengineer', 'Project Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(162, 0, 'govt', 'govt', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(163, 0, 'readytooccupyvilla', 'Ready to occupy villa', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(164, 0, '3bk', '3 bk', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(165, 0, 'lookingforvillabelow50lakhs', 'Looking for villa below 50 lakhs', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(166, 0, 'pricesfor3bhkvilla', 'Prices for 3bhk villa', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(167, 0, 'bussenes', 'Bussenes', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(168, 0, 'insidesalesatcameronmanufacturingltd', 'Inside sales at Cameron Manufacturing LTD', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(169, 0, 'projectleader', 'Project Leader', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(170, 0, 'consignment', 'Consignment', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(171, 0, 'jr.managermarketing', 'Jr. Manager Marketing', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(172, 0, 'lookingforproperty', 'looking for property', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(173, 0, 'wanttobuy', 'Want to buy', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(174, 0, 'taxconsultant', 'Tax consultant', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(175, 0, 'productdesigner', 'Product Designer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(176, 0, 'physicaleducationteacherpeteacher', 'Physical Education Teacher (PE Teacher)', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(177, 0, 'softwareassociate', 'Software associate', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(178, 0, 'itemployee', 'IT employee', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(179, 0, 'b', 'B', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(180, 0, 'banker', 'Banker', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(181, 0, 'needquotationfor3bhkand2bhk', 'Need quotation for 3bhk and 2bhk', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(182, 0, 'a', 'A', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(183, 0, 'purchaseofhouse', 'purchase of house', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(184, 0, 'seniorassociate', 'Senior Associate', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(185, 0, 'employed', 'Employed', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(186, 0, '36lakhs', '36 lakhs', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(187, 0, 'iaminterestedingatedcommunityindependentvillainandaroundramanathapuramcoimbatore', 'I am interested in gated community independent villa in and around Ramanathapuram, Coimbatore', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(188, 0, 'plscall', 'Pls call', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(189, 0, 'cncmachineoperatorcomputernumericallycontrolledmachineoperator', 'CNC Machine Operator (Computer Numerically Controlled Machine Operator)', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(190, 0, 'productionengineer', 'Production engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(191, 0, 'chuma', 'Chuma', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(192, 0, 'individual', 'individual', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(193, 0, 'branchmanager', 'Branch manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(194, 0, 'programmeranalyst', 'Programmer Analyst', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(195, 0, 'govt.', 'Govt.', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(196, 0, 'creditseniormanager', 'Credit Senior Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(197, 0, 'interiordesigner', 'interior designer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(198, 0, 'bcom', 'bcom', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(199, 0, 'softwareprofessional', 'Software professional', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(200, 0, '.', '.', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(201, 0, 'obgyndoctorobstetricsgynecologydoctor', 'OB/GYN Doctor (Obstetrics/Gynecology Doctor)', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(202, 0, 'seniormanagerbusinessdevelopment', 'Senior Manager - Business Development', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(203, 0, 'yes', 'yes', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(204, 0, 'assistantmanagerhr', 'Assistant Manager HR', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(205, 0, 'enterprisesystemanalyst', 'Enterprise System Analyst', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(206, 0, 'qualitycontrolanalyst', 'Quality Control Analyst', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(207, 0, 'google', 'Google', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(208, 0, 'government', 'Government', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(209, 0, 'lookingfor3bhkvillanearbyavinashird', 'Looking for 3BHK VILLA NEAR BY AVINASHI RD', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(210, 0, 'customerrelationshipofficer', 'Customer relationship officer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(211, 0, 'lookingfor3or4bhkvillasorindependenthousesincoimbatore.', 'Looking for 3 or 4 BHK villas or independent houses in Coimbatore.', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(212, 0, 'needsumdetails', 'Need sum DETAILS', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(213, 0, 'seniorprocessexecutive', 'Senior Process Executive', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(214, 0, 'seniorengineerplanning', 'Senior Engineer - Planning', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(215, 0, 'staff', 'staff', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(216, 0, 'bmp', 'BMP', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(217, 0, 'headfinance', 'Head Finance', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(218, 0, 'sakthivel', 'Sakthivel', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(219, 0, 'buss', 'buss', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(220, 0, 'servicedeliverycoordinator', 'service delivery coordinator', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(221, 0, 'banking', 'Banking', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(222, 0, 'staffnurse', 'Staff Nurse', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(223, 0, 'engineersustainabledesign', 'Engineer-sustainable design', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(224, 0, 'projectmanager', 'Project Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(225, 0, 'ownersoleproprietor', 'Owner/Sole Proprietor', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(226, 0, 'djproducer', 'DJ/Producer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(227, 0, 'kotesan', 'kotesan', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(228, 0, 'rm4683159gmall.com', 'rm4683159@gmall.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(229, 0, 'personalbanker', 'Personal Banker', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(230, 0, 'dainikbhaskurgmail.com', 'dainikbhaskur@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(231, 0, 'civil', 'Civil', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(232, 0, 'individualhome', 'Individual home', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(233, 0, 'technican', 'technican', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(234, 0, 'assisstantmanager', 'Assisstant Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(235, 0, 'itsupporter', 'IT-Supporter', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(236, 0, 'sme', 'SME', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(237, 0, 'buisness', 'buisness', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(238, 0, 'sakrediffmail.com', 'sa_k@rediffmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(239, 0, 'director', 'Director', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(240, 0, 'ms.jenifer96gmail.com', 'ms.jenifer96@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(241, 0, 'engineeringdepartment', 'Engineering department', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(242, 0, 'super', 'Super', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(243, 0, 'ashu', 'Ashu', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(244, 0, 'merchandiser', 'Merchandiser', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(245, 0, 'hrtalentmanager', 'HR Talent Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(246, 0, 'b.comcomplied', 'B.com complied', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(247, 0, 'freelancer', 'Freelancer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(248, 0, 'businessheadtechnology', 'Business Head - Technology', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(249, 0, 'businesses', 'businesses', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(250, 0, 'v.r.vishnuecsgmail.com', 'v.r.vishnuecs@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(251, 0, 'nazi2nazi2002gmail.com', 'nazi2nazi2002@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(252, 0, 'santhosh.balaramanyahoo.com', 'santhosh.balaraman@yahoo.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(253, 0, 'jothipriyakarthikeyangmail.com', 'jothipriyakarthikeyan@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(254, 0, 'pleasegetquoteforindependentvillaincoimbatore', 'Please get quote for independent villa in Coimbatore', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(255, 0, 'networking', 'Networking', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(256, 0, 'desginengineer', 'Desgin Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(257, 0, 'seniorterritorysalesexecutive', 'Senior Territory Sales Executive', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(258, 0, 'electricalengineer', 'electrical engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(259, 0, 'divisionalmanagerproduction', 'Divisional Manager Production', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(260, 0, 'individualhomesunder30lacs', 'Individual homes under 30 lacs', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(261, 0, 'sales', 'sales', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(262, 0, 'processassociate', 'Process Associate', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(263, 0, 'sendcatalogue', 'Send catalogue', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(264, 0, 'associatemanager', 'Associate Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(265, 0, 'estudante', 'Estudante', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(266, 0, 'centralgovt', 'central govt', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(267, 0, 'airlines', 'Airlines', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(268, 0, 'principalarchitectpartner', 'Principal Architect/Partner', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(269, 0, 'physiotherapist', 'Physiotherapist', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(270, 0, 'teamleader', 'Team Leader', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(271, 0, 'softwareengineeruiuxdesigner', 'Software Engineer UI UX Designer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(272, 0, 'applestore', 'Apple Store', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(273, 0, 'testprofessional', 'Test professional', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(274, 0, 'hotel', 'hotel', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(275, 0, 'bb', 'Bb', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(276, 0, 'start', 'start', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(277, 0, 'own', 'Own', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(278, 0, 'vjns', 'v j n s', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(279, 0, 'teamlead', 'Team lead', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(280, 0, 'whichareainpodanur', 'which area in podanur', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(281, 0, 'nagarajan', 'Nagarajan', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(282, 0, 'hemavasu415gmail.com', 'hemavasu415@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(283, 0, 'sakthishivaligmail.com', 'sakthishivali@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(284, 0, 'indianairforce', 'indian air Force', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(285, 0, 'telungupalayam', 'Telungupalayam', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(286, 0, 'mobileshop', 'Mobile Shop', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(287, 0, 'seniornetworksecurityengineer', 'Senior Network Security Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(288, 0, 'functionalconsultant', 'Functional Consultant', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(289, 0, 'need', 'need', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(290, 0, 'assistantmanagerproduction', 'Assistant Manager - Production', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(291, 0, 'expectation', 'expectation', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(292, 0, 'statestreethclservices', 'Statestreet Hcl services', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(293, 0, 'cloudengineer', 'Cloud Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(294, 0, 'finance', 'Finance', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(295, 0, '3bhkhone', '3 bhk hone', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(296, 0, 'southernrailway', 'Southern Railway', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(297, 0, 'required3bhkluxuryvilla', 'Required 3 bhk luxury villa', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(298, 0, 'gc', 'GC', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(299, 0, 'agent', 'Agent', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(300, 0, '3bhk', '3bhk', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(301, 0, 'mr', 'Mr', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(302, 0, 'professional', 'Professional', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(303, 0, 'coimbatoreinstituteoftechnology', 'coimbatore institute of technology', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(304, 0, 'techniciantrainee', 'Technician Trainee', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(305, 0, 'agri', 'agri', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(306, 0, 'bankofficer', 'bank officer', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(307, 0, 'modulelead', 'Module Lead', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(308, 0, 'susintechnologypltd', 'susin technology p ltd', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(309, 0, 'agaramagencies', 'Agaram Agencies', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(310, 0, 'callme', 'call me', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(311, 0, 'locationpricelayoutetcrequired', 'Location price layout etc required', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(312, 0, 'seniorsystemsexecutive', 'Senior Systems Executive', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(313, 0, 'officerdigitalmarketing', 'Officer - Digital Marketing', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(314, 0, 'areasalesmanager', 'Area Sales Manager', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(315, 0, 'ssdgfhmail.com', 'ssdgf@hmail.com', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(316, 0, 'sreena.gopishgmail.com', 'sreena.gopish@gmail.com', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(317, 0, 'detailsof3bhkvilla', 'Details of 3BHK VILLA', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(318, 0, 'generalsurgeon', 'General surgeon', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(319, 0, 'photoeditioranddesigner', 'Photo Editior and designer', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(320, 0, 'financemanager', 'Finance manager', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(321, 0, 'byjus', 'Byjus', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(322, 0, 'articleship', 'Articleship', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(323, 0, 'agencies', 'agencies', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(324, 0, 'corporate', 'Corporate', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(325, 0, 'assistantmanager', 'Assistant Manager', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(326, 0, 'acco', 'acco', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(327, 0, 'temple', 'Temple', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(328, 0, 'proprietorship', 'Proprietorship', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(329, 0, 'developer', 'Developer', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(330, 0, 'automotivetestengineer', 'Automotive Test Engineer', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(331, 0, 'stroes', 'Stroes', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(332, 0, 'iamintrested', 'I am intrested', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(333, 0, 'akmedicalcenter', 'Ak medical center', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(334, 0, 'architect', 'Architect', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(335, 0, 'photographer', 'photographer', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(336, 0, 'interested3bhkvillain36l', 'Interested 3bhk villa in 36L', 1, 1, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(337, 0, 'foreman', 'Foreman', 1, 1, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(338, 0, 'dringschool', 'Dring school', 1, 1, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(339, 0, 'logisticsincharge', 'Logistics Incharge', 1, 1, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(340, 0, 'homewife', 'home wife', 1, 1, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(341, 0, 'salesmanagerforindiabangladeshitandsrilankaatbuckman', 'Sales Manager for India, Bangladesh it and Sri Lanka at Buckman', 1, 1, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(342, 0, 'agri.officer', 'Agri.Officer', 1, 1, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(343, 0, 'snacks', 'Snacks', 1, 1, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(344, 0, 'businessdevelopmentexecutive', 'business development executive', 1, 1, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(345, 0, 'processlead', 'Process Lead', 1, 1, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(346, 0, 'testingexecutive', 'Testing Executive', 1, 1, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(347, 0, 'chatbot', 'Chatbot', 1, 1, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(348, 0, 'seniorengineer', 'Senior Engineer', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(349, 0, 'ceo', 'ceo', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(350, 0, 'merchandisingmanager', 'Merchandising Manager', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(351, 0, 'rsm', 'RSM', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(352, 0, 'hrbp', 'HRBP', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(353, 0, 'businessconsultant', 'Business consultant', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(354, 0, 'pastrychef', 'Pastry Chef', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(355, 0, 'academician', 'Academician', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(356, 0, 'se', 'SE', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(357, 0, 'assistantgeneralmanager', 'Assistant General Manager', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(358, 0, 'engineertechnician', 'Engineer Technician', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(359, 0, 'juniorengineer', 'Junior Engineer', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(360, 0, 'airconditioningservicecenter', 'Air-conditioning service center', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(361, 0, 'webdeveloper', 'Web Developer', 1, 1, '2021-08-30 21:21:31', '2021-08-30 21:21:31');

-- --------------------------------------------------------

--
-- Table structure for table `customer_tbl`
--

DROP TABLE IF EXISTS `customer_tbl`;
CREATE TABLE IF NOT EXISTS `customer_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `import_token` varchar(200) DEFAULT NULL,
  `token` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `lead_id` int(11) DEFAULT 0,
  `uid` varchar(200) DEFAULT NULL,
  `priority` varchar(200) DEFAULT NULL,
  `property_id` int(11) DEFAULT 0,
  `allotment_date` varchar(200) DEFAULT NULL,
  `assign_remarks` longtext DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `gender` varchar(200) DEFAULT NULL,
  `dob` varchar(200) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `has_psw` varchar(200) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `nationality` varchar(200) DEFAULT NULL,
  `pincode` varchar(200) DEFAULT NULL,
  `ad_contact_person` varchar(200) DEFAULT NULL,
  `ad_relationship` varchar(200) DEFAULT NULL,
  `ad_mobile` varchar(200) DEFAULT NULL,
  `ad_email` varchar(200) DEFAULT NULL,
  `ad_address` varchar(200) DEFAULT NULL,
  `ad_city` varchar(200) DEFAULT NULL,
  `ad_state` varchar(200) DEFAULT NULL,
  `ad_pincode` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `kyc_status` int(11) DEFAULT 0,
  `approved_on` varchar(200) DEFAULT NULL,
  `kyc_submittedon` varchar(200) DEFAULT NULL,
  `approved_by` int(11) DEFAULT 0,
  `kyc_remarks` longtext DEFAULT NULL,
  `assign_status` int(11) DEFAULT 0,
  `resetpassword` int(11) DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_tbl`
--

INSERT INTO `customer_tbl` (`id`, `branch_id`, `import_token`, `token`, `type`, `lead_id`, `uid`, `priority`, `property_id`, `allotment_date`, `assign_remarks`, `name`, `gender`, `dob`, `mobile`, `email`, `password`, `has_psw`, `address`, `city`, `state`, `nationality`, `pincode`, `ad_contact_person`, `ad_relationship`, `ad_mobile`, `ad_email`, `ad_address`, `ad_city`, `ad_state`, `ad_pincode`, `description`, `status`, `kyc_status`, `approved_on`, `kyc_submittedon`, `approved_by`, `kyc_remarks`, `assign_status`, `resetpassword`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'cust-1-', NULL, 0, 'CU0001', 'low', 0, NULL, NULL, 'Cust 1', 'male', NULL, '9942387100', 'koshik@webykart.com', '11232477605d2c775dc406a951f1bcd61cd1bcaa', 'fTeEHyvh', '49wmani nagar', 'coimbatore', 'tamil nadu', NULL, '641108', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, '', 0, 0, 1, '2021-09-01 09:40:31', '2021-09-01 09:40:31'),
(2, 1, 'jvc8BMcRSOVQb2Z', 'test', NULL, 0, 'CU0002', 'low', 0, NULL, NULL, 'Test', 'male', NULL, '1234567890', 'test@mail.com', 'e17ba1cbc425533d69d8bf505b383cd82176333e', 'gsTsbVWa', 'sample', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, '', 0, 0, 1, '2021-09-01 10:21:34', '2021-09-01 10:22:03'),
(3, 2, NULL, 'salem-cus', NULL, 0, 'CU0003', 'low', 0, NULL, NULL, 'Salem cus', 'male', NULL, '153', 'cust@salem.com', 'c39b4ae99db9e24a3b98605fdef54fa4b70a58ab', 'HrzVggJH', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, '', 0, 0, 1, '2021-09-01 11:28:23', '2021-09-01 11:28:23');

-- --------------------------------------------------------

--
-- Table structure for table `datasheet_tbl`
--

DROP TABLE IF EXISTS `datasheet_tbl`;
CREATE TABLE IF NOT EXISTS `datasheet_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `import_token` varchar(50) DEFAULT NULL,
  `uploaded_sheet` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `datasheet_tbl`
--

INSERT INTO `datasheet_tbl` (`id`, `branch_id`, `import_token`, `uploaded_sheet`, `created_at`, `updated_at`, `status`) VALUES
(4, 1, 'pcnt1UmnDzTmDug', '01-09-2021_3TpBo7zx_sample_data.xlsx', '2021-09-01 11:10:04', '2021-09-01 11:10:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `deposite_master_tbl`
--

DROP TABLE IF EXISTS `deposite_master_tbl`;
CREATE TABLE IF NOT EXISTS `deposite_master_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `item_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposite_master_tbl`
--

INSERT INTO `deposite_master_tbl` (`id`, `branch_id`, `token`, `item_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '1l', '1L', 1, 1, '2021-07-26 13:33:49', '2021-07-26 13:35:20'),
(2, 0, '2l', '2L', 1, 1, '2021-07-26 13:35:24', '2021-07-26 13:35:24'),
(3, 0, '5l', '5L', 1, 1, '2021-07-26 13:35:32', '2021-07-26 13:35:32'),
(4, 0, '10l', '10L', 1, 1, '2021-07-26 13:35:41', '2021-07-26 13:35:41'),
(5, 0, '15l', '15L', 1, 1, '2021-08-30 21:21:31', '2021-08-30 21:21:31'),
(6, 0, '20l', '20L', 1, 1, '2021-09-01 11:10:08', '2021-09-01 11:10:08');

-- --------------------------------------------------------

--
-- Table structure for table `employee_permission_tbl`
--

DROP TABLE IF EXISTS `employee_permission_tbl`;
CREATE TABLE IF NOT EXISTS `employee_permission_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `employee_id` varchar(200) DEFAULT NULL,
  `customers` int(11) NOT NULL DEFAULT 0,
  `manage_customer` int(11) NOT NULL DEFAULT 0,
  `import_customer` int(11) NOT NULL DEFAULT 0,
  `lead` int(11) NOT NULL DEFAULT 0,
  `manage_lead` int(11) NOT NULL DEFAULT 0,
  `lead_type` int(11) NOT NULL DEFAULT 0,
  `activity_type` int(11) NOT NULL DEFAULT 0,
  `lead_source` int(11) NOT NULL DEFAULT 0,
  `customer_profile` int(11) NOT NULL DEFAULT 0,
  `import_lead` int(11) NOT NULL DEFAULT 0,
  `employee` int(11) NOT NULL DEFAULT 0,
  `manageemployee` int(11) NOT NULL DEFAULT 0,
  `reports` int(11) NOT NULL DEFAULT 0,
  `lead_report` int(11) NOT NULL DEFAULT 0,
  `customer_report` int(11) NOT NULL DEFAULT 0,
  `settings` int(11) NOT NULL DEFAULT 0,
  `chit_master` int(11) NOT NULL DEFAULT 0,
  `deposit_master` int(11) NOT NULL DEFAULT 0,
  `vam_master` int(11) NOT NULL DEFAULT 0,
  `branch_master` int(1) DEFAULT 0,
  `employee_role_master` int(1) DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_permission_tbl`
--

INSERT INTO `employee_permission_tbl` (`id`, `branch_id`, `employee_id`, `customers`, `manage_customer`, `import_customer`, `lead`, `manage_lead`, `lead_type`, `activity_type`, `lead_source`, `customer_profile`, `import_lead`, `employee`, `manageemployee`, `reports`, `lead_report`, `customer_report`, `settings`, `chit_master`, `deposit_master`, `vam_master`, `branch_master`, `employee_role_master`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '1', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2020-06-06 15:06:09', '2020-06-06 15:12:37'),
(78, 0, '3', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-09-01 11:17:04', '2021-09-01 11:17:43'),
(79, 0, '4', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-09-01 11:19:46', '2021-09-01 11:19:46'),
(80, 0, '5', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-09-01 11:20:17', '2021-09-01 11:20:17'),
(81, 0, '6', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-09-01 11:20:46', '2021-09-01 11:20:46'),
(82, 0, '7', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-09-01 11:21:19', '2021-09-01 11:21:19'),
(83, 0, '8', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-09-01 11:21:52', '2021-09-01 11:21:52'),
(84, 0, '9', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, '2021-09-01 11:24:20', '2021-09-01 11:24:20'),
(85, 0, '10', 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 5, '2021-09-01 11:25:52', '2021-09-01 11:26:40');

-- --------------------------------------------------------

--
-- Table structure for table `employee_role_master_tbl`
--

DROP TABLE IF EXISTS `employee_role_master_tbl`;
CREATE TABLE IF NOT EXISTS `employee_role_master_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(200) DEFAULT NULL,
  `employee_role` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_role_master_tbl`
--

INSERT INTO `employee_role_master_tbl` (`id`, `token`, `employee_role`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'partner', 'Partner', 1, 1, '2021-08-24 11:23:41', '2021-08-24 11:23:41'),
(2, 'generalmanager', 'General Manager', 1, 1, '2021-08-24 15:28:24', '2021-08-24 16:14:15'),
(3, 'branchmanager', 'Branch Manager', 1, 1, '2021-08-24 16:14:30', '2021-08-24 16:14:30'),
(4, 'sr.executivesales', 'Sr. Executive Sales', 1, 1, '2021-08-24 11:20:33', '2021-08-24 11:20:33'),
(5, 'executiveoffice', 'Executive Office', 1, 1, '2021-08-24 11:21:09', '2021-08-24 11:21:09'),
(6, 'telecaller', 'Telecaller', 1, 1, '2021-08-24 11:23:01', '2021-08-24 11:23:01');

-- --------------------------------------------------------

--
-- Table structure for table `employee_tbl`
--

DROP TABLE IF EXISTS `employee_tbl`;
CREATE TABLE IF NOT EXISTS `employee_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `employee_uid` varchar(200) DEFAULT NULL,
  `token` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `role` int(1) DEFAULT 0,
  `gender` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `pincode` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `assigned_branch` varchar(100) DEFAULT NULL,
  `assigned_employee` varchar(200) DEFAULT NULL,
  `super_admin` int(11) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_tbl`
--

INSERT INTO `employee_tbl` (`id`, `branch_id`, `employee_uid`, `token`, `name`, `role`, `gender`, `password`, `email`, `mobile`, `address`, `city`, `state`, `pincode`, `status`, `assigned_branch`, `assigned_employee`, `super_admin`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'EMP0001', 'admin', 'Admin', 2, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Admin@mail.com', '9942387100', '', 'coimbatore', '', '6441103', 1, NULL, '0', 1, 1, '2020-06-06 15:06:09', '2021-09-01 09:39:35'),
(3, 1, 'EMP0003', 'gm-cbe-salem', 'GM CBE SALEM', 2, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'GM@cbesalem.com', '123', '', '', '', '', 1, '1,2', '', 0, 1, '2021-09-01 11:17:04', '2021-09-01 11:17:52'),
(4, 1, 'EMP0004', 'gm-erode-madurai', 'GM Erode Madurai', 2, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'gm@erode.com', '12', '', '', '', '', 1, '3,4', '', 0, 1, '2021-09-01 11:19:46', '2021-09-01 11:19:46'),
(5, 1, 'EMP0005', 'gm-cbe', 'GM CBE', 2, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Gm@cbe.com', '987', '', '', '', '', 1, '1', '', 0, 1, '2021-09-01 11:20:17', '2021-09-01 11:30:31'),
(6, 1, 'EMP0006', 'gm-salem', 'GM salem', 2, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'gm@salem.com', '985', '', '', '', '', 1, '2', '', 0, 1, '2021-09-01 11:20:46', '2021-09-01 11:20:46'),
(7, 1, 'EMP0007', 'gm-erode', 'GM Erode', 2, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'gm@erode1.com', '45', '', '', '', '', 1, '3', '', 0, 1, '2021-09-01 11:21:19', '2021-09-01 11:21:19'),
(8, 1, 'EMP0008', 'gm-madurai', 'GM madurai', 2, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'gm@madurai', '34', '', '', '', '', 1, '4', '', 0, 1, '2021-09-01 11:21:52', '2021-09-01 11:21:52'),
(9, 1, 'EMP0009', 'branch-1', 'Branch 1', 3, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'branch@cbe.com', '1234', '', '', '', '', 1, '1', '5', 0, 5, '2021-09-01 11:24:19', '2021-09-01 11:24:19'),
(10, 1, 'EMP0010', 'emplo-cbe-1', 'Emplo cbe 1', 4, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'empl1@cbe.com', '4562', '', '', '', '', 1, '', '9', 0, 5, '2021-09-01 11:25:52', '2021-09-01 11:26:08');

-- --------------------------------------------------------

--
-- Table structure for table `flatvilla_documenttype_list_tbl`
--

DROP TABLE IF EXISTS `flatvilla_documenttype_list_tbl`;
CREATE TABLE IF NOT EXISTS `flatvilla_documenttype_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `document_id` int(11) NOT NULL DEFAULT 0,
  `doc_name` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` int(1) DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `flatvilla_documenttype_tbl`
--

DROP TABLE IF EXISTS `flatvilla_documenttype_tbl`;
CREATE TABLE IF NOT EXISTS `flatvilla_documenttype_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `doc_title` varchar(200) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `publish_status` int(11) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `flat_type_master`
--

DROP TABLE IF EXISTS `flat_type_master`;
CREATE TABLE IF NOT EXISTS `flat_type_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `flat_variant` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `flat_type_masterlist`
--

DROP TABLE IF EXISTS `flat_type_masterlist`;
CREATE TABLE IF NOT EXISTS `flat_type_masterlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `flattype_id` int(11) DEFAULT 0,
  `room_type` varchar(200) DEFAULT NULL,
  `sqft` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` int(1) DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `floor_tbl`
--

DROP TABLE IF EXISTS `floor_tbl`;
CREATE TABLE IF NOT EXISTS `floor_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `floor` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_image_tbl`
--

DROP TABLE IF EXISTS `gallery_image_tbl`;
CREATE TABLE IF NOT EXISTS `gallery_image_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `gallery_id` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_tbl`
--

DROP TABLE IF EXISTS `gallery_tbl`;
CREATE TABLE IF NOT EXISTS `gallery_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `album_title` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `sort_order` int(11) DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gendral_documents_tbl`
--

DROP TABLE IF EXISTS `gendral_documents_tbl`;
CREATE TABLE IF NOT EXISTS `gendral_documents_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `documents` varchar(200) DEFAULT NULL,
  `document_type` varchar(200) DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_tbl`
--

DROP TABLE IF EXISTS `invoice_tbl`;
CREATE TABLE IF NOT EXISTS `invoice_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `inv_uid` varchar(200) DEFAULT NULL,
  `customer_id` int(11) DEFAULT 0,
  `inv_number` varchar(200) DEFAULT NULL,
  `inv_date` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `raised_to` varchar(200) DEFAULT NULL,
  `documents` varchar(200) DEFAULT NULL,
  `document_type` varchar(200) DEFAULT NULL,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kyc_list_tbl`
--

DROP TABLE IF EXISTS `kyc_list_tbl`;
CREATE TABLE IF NOT EXISTS `kyc_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `kyctype_id` int(11) NOT NULL DEFAULT 0,
  `image_name` varchar(200) DEFAULT NULL,
  `file_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `approval_status` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kyc_tbl`
--

DROP TABLE IF EXISTS `kyc_tbl`;
CREATE TABLE IF NOT EXISTS `kyc_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `kyc_uid` varchar(200) DEFAULT NULL,
  `property_id` int(11) DEFAULT 0,
  `customer_id` int(11) DEFAULT 0,
  `id_type` varchar(200) DEFAULT NULL,
  `firstname` varchar(200) DEFAULT NULL,
  `lastname` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `dob` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `address1` varchar(200) DEFAULT NULL,
  `address2` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `nationiality` varchar(200) DEFAULT NULL,
  `pincode` varchar(200) DEFAULT NULL,
  `approval_status` int(11) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kyc_type_tbl`
--

DROP TABLE IF EXISTS `kyc_type_tbl`;
CREATE TABLE IF NOT EXISTS `kyc_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `kyc_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_activity_tbl`
--

DROP TABLE IF EXISTS `lead_activity_tbl`;
CREATE TABLE IF NOT EXISTS `lead_activity_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `date` varchar(200) DEFAULT NULL,
  `lead_id` varchar(200) DEFAULT NULL,
  `employee_id` int(11) NOT NULL DEFAULT 0,
  `activity_id` int(11) NOT NULL DEFAULT 0,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_activity_tbl`
--

INSERT INTO `lead_activity_tbl` (`id`, `branch_id`, `date`, `lead_id`, `employee_id`, `activity_id`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, '2021-09-01', '1', 1, 2, 'sample d', 1, '2021-09-01 11:10:43', '2021-09-01 11:11:21');

-- --------------------------------------------------------

--
-- Table structure for table `lead_gallery_tbl`
--

DROP TABLE IF EXISTS `lead_gallery_tbl`;
CREATE TABLE IF NOT EXISTS `lead_gallery_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_id` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_gallery_tbl`
--

INSERT INTO `lead_gallery_tbl` (`id`, `lead_id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'SN47AH3MtgdIweTDzlYyRKmEJX2FOL.jpg', 1, '2021-09-01 11:11:06', '2021-09-01 11:11:06'),
(2, '1', 'Ezo_0pbL3msgITuS8aAYkexWZ4OBVK.jpg', 1, '2021-09-01 11:11:06', '2021-09-01 11:11:06');

-- --------------------------------------------------------

--
-- Table structure for table `lead_logs_tbl`
--

DROP TABLE IF EXISTS `lead_logs_tbl`;
CREATE TABLE IF NOT EXISTS `lead_logs_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `type` varchar(200) DEFAULT NULL,
  `lead_type` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `created_to` int(11) NOT NULL DEFAULT 0,
  `ref_id` int(11) NOT NULL DEFAULT 0,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_source_tbl`
--

DROP TABLE IF EXISTS `lead_source_tbl`;
CREATE TABLE IF NOT EXISTS `lead_source_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `lead_source` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_source_tbl`
--

INSERT INTO `lead_source_tbl` (`id`, `branch_id`, `token`, `lead_source`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'fb', 'Fb', 1, 1, '2020-09-22 11:39:38', '2020-09-22 11:39:38'),
(2, 0, 'instagram', 'Instagram', 1, 1, '2020-09-22 11:40:01', '2020-09-22 11:40:01'),
(3, 0, 'goggle-ads', 'goggle Ads', 1, 1, '2020-09-22 11:40:24', '2020-09-22 11:40:24'),
(4, 0, 'facebook', 'Facebook', 1, 1, '2020-10-01 16:29:40', '2020-10-01 16:29:40'),
(5, 0, 'google', 'Google', 1, 1, '2020-10-01 16:29:40', '2020-10-01 16:29:40'),
(6, 0, 'directcall', 'Direct Call', 1, 5, '2020-10-08 12:33:07', '2021-09-01 11:46:36'),
(7, 0, 'ig', 'ig', 1, 1, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(9, 0, 'fairyland', 'Fairyland', 1, 1, '2021-02-04 16:44:54', '2021-02-04 16:44:54'),
(10, 0, 'chatbot', 'Chatbot', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(11, 0, 'land', 'Land', 1, 1, '2021-08-30 21:21:31', '2021-09-01 10:27:55'),
(12, 1, 'sss', 'sss', 1, 1, '2021-09-01 10:27:59', '2021-09-01 10:27:59');

-- --------------------------------------------------------

--
-- Table structure for table `lead_tbl`
--

DROP TABLE IF EXISTS `lead_tbl`;
CREATE TABLE IF NOT EXISTS `lead_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `import_token` varchar(200) DEFAULT NULL,
  `uid` varchar(200) DEFAULT NULL,
  `auth_type` varchar(200) DEFAULT NULL,
  `property_id` int(11) DEFAULT 0,
  `customer_id` int(11) DEFAULT 0,
  `lead_date` varchar(200) DEFAULT NULL,
  `enquiry_date` varchar(100) DEFAULT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `lname` varchar(200) DEFAULT NULL,
  `leadsource` varchar(200) DEFAULT NULL,
  `designation` varchar(200) DEFAULT NULL,
  `has_chit` int(11) NOT NULL DEFAULT 0,
  `has_deposite` int(11) NOT NULL DEFAULT 0,
  `has_vas` int(11) NOT NULL DEFAULT 0,
  `chit_id` int(11) NOT NULL DEFAULT 0,
  `deposite_id` int(11) NOT NULL DEFAULT 0,
  `vas_id` int(11) NOT NULL DEFAULT 0,
  `lead_status` varchar(150) DEFAULT NULL,
  `flattype_id` int(11) DEFAULT 0,
  `site_visit_status` varchar(200) DEFAULT NULL,
  `site_visit_date` varchar(100) DEFAULT NULL,
  `customer_profile_id` int(11) DEFAULT 0,
  `gender` varchar(200) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `secondary_mobile` varchar(200) DEFAULT NULL,
  `secondary_email` varchar(200) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pincode` varchar(250) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `lead_status_id` int(11) NOT NULL DEFAULT 0,
  `employee_id` int(11) NOT NULL DEFAULT 0,
  `assign_status` int(11) DEFAULT 0,
  `closed_status` int(11) DEFAULT 0,
  `moved_status` int(1) NOT NULL DEFAULT 0,
  `move_remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_on` varchar(250) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(250) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_tbl`
--

INSERT INTO `lead_tbl` (`id`, `branch_id`, `token`, `import_token`, `uid`, `auth_type`, `property_id`, `customer_id`, `lead_date`, `enquiry_date`, `fname`, `lname`, `leadsource`, `designation`, `has_chit`, `has_deposite`, `has_vas`, `chit_id`, `deposite_id`, `vas_id`, `lead_status`, `flattype_id`, `site_visit_status`, `site_visit_date`, `customer_profile_id`, `gender`, `mobile`, `email`, `secondary_mobile`, `secondary_email`, `address`, `city`, `state`, `pincode`, `description`, `lead_status_id`, `employee_id`, `assign_status`, `closed_status`, `moved_status`, `move_remarks`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'arun', 'pcnt1UmnDzTmDug', 'LE0001', 'excel', 0, 0, '2021-09-01', '2021-08-30', 'Arun', 'kumar', '1', '', 1, 1, 0, 1, 6, 0, 'hot leads', 0, '', '', 1, 'male', '9944123022', 'koshik@webykart.com', '', '', '49w mani nagar', 'coimbatore', 'tamill nadu', '641104', 'sample', 4, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2021-09-01 11:10:08', '2021-09-01 11:11:31');

-- --------------------------------------------------------

--
-- Table structure for table `lead_type_tbl`
--

DROP TABLE IF EXISTS `lead_type_tbl`;
CREATE TABLE IF NOT EXISTS `lead_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `uid` varchar(200) DEFAULT NULL,
  `token` varchar(200) DEFAULT NULL,
  `lead_type` varchar(200) DEFAULT NULL,
  `default_settings` int(1) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_type_tbl`
--

INSERT INTO `lead_type_tbl` (`id`, `branch_id`, `uid`, `token`, `lead_type`, `default_settings`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'LT0001', 'will-think-and-get-back', 'Will think and get back', 0, 1, 1, '2020-08-16 22:22:58', '2020-08-17 19:35:34'),
(2, 0, 'LT0002', 'hot-lead', 'Hot Lead', 1, 1, 1, '2020-08-16 22:23:09', '2020-08-17 19:35:22'),
(3, 0, 'LT0003', 'will-discuss-with-family', 'Will discuss with family', 0, 1, 1, '2020-08-16 22:26:03', '2020-08-17 19:35:41'),
(4, 0, 'LT0004', 'just-for-collecting-info', 'Just for collecting info', 0, 1, 1, '2020-08-17 19:35:53', '2020-08-17 19:35:53'),
(5, 0, 'LT0005', 'do-not-follow-up', 'Do not follow up', 0, 1, 1, '2020-08-17 19:35:59', '2020-08-17 19:35:59'),
(6, 0, 'LT0006', 'interested', 'Interested', 0, 1, 1, '2020-10-08 12:34:27', '2020-10-08 12:34:27'),
(7, 0, 'LT0007', 'notinterested', 'Not Interested', 0, 1, 1, '2020-10-08 12:34:39', '2020-10-08 12:34:39'),
(9, 0, 'LT0009', 'notattnthecall', 'Not attn the call', 0, 1, 1, '2020-10-09 15:12:55', '2020-10-09 15:12:55'),
(10, 0, 'LT0010', 'lowbudget', 'Low budget', 0, 1, 1, '2020-10-09 16:54:31', '2020-10-09 16:54:31'),
(25, 0, NULL, 'hotleads', 'hot leads', 0, 1, 1, '2021-09-01 11:10:08', '2021-09-01 11:10:08');

-- --------------------------------------------------------

--
-- Table structure for table `news_tbl`
--

DROP TABLE IF EXISTS `news_tbl`;
CREATE TABLE IF NOT EXISTS `news_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `date` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `email_notification` int(11) NOT NULL DEFAULT 0,
  `post_to` varchar(200) DEFAULT NULL,
  `customer_ids` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `show_popup` int(11) NOT NULL DEFAULT 0,
  `popup_till` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `send_mail_status` int(11) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification_email_tbl`
--

DROP TABLE IF EXISTS `notification_email_tbl`;
CREATE TABLE IF NOT EXISTS `notification_email_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `adhoc_permission` int(1) NOT NULL DEFAULT 0,
  `kyc_permission` int(1) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paymentinfo_tbl`
--

DROP TABLE IF EXISTS `paymentinfo_tbl`;
CREATE TABLE IF NOT EXISTS `paymentinfo_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `name` varchar(200) DEFAULT NULL,
  `account_number` varchar(200) DEFAULT NULL,
  `bank` varchar(200) DEFAULT NULL,
  `ifcs_code` varchar(200) DEFAULT NULL,
  `branch` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `pincode` int(11) DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project_type_tbl`
--

DROP TABLE IF EXISTS `project_type_tbl`;
CREATE TABLE IF NOT EXISTS `project_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `project_name` varchar(200) DEFAULT NULL,
  `status` int(1) DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_broucher_tbl`
--

DROP TABLE IF EXISTS `property_broucher_tbl`;
CREATE TABLE IF NOT EXISTS `property_broucher_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `description` longtext DEFAULT NULL,
  `brochures` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_document_list_items`
--

DROP TABLE IF EXISTS `property_document_list_items`;
CREATE TABLE IF NOT EXISTS `property_document_list_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `document_id` int(11) NOT NULL DEFAULT 0,
  `document_list_id` int(11) NOT NULL DEFAULT 0,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `doc_name` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `document_file` varchar(200) DEFAULT NULL,
  `doc_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `doc_status` int(11) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_document_list_tbl`
--

DROP TABLE IF EXISTS `property_document_list_tbl`;
CREATE TABLE IF NOT EXISTS `property_document_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `documents_id` int(11) NOT NULL DEFAULT 0,
  `type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_gallery_tbl`
--

DROP TABLE IF EXISTS `property_gallery_tbl`;
CREATE TABLE IF NOT EXISTS `property_gallery_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `images` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_gendral_documents_tbl`
--

DROP TABLE IF EXISTS `property_gendral_documents_tbl`;
CREATE TABLE IF NOT EXISTS `property_gendral_documents_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `title` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `documents` varchar(200) DEFAULT NULL,
  `document_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_rooms_list_tbl`
--

DROP TABLE IF EXISTS `property_rooms_list_tbl`;
CREATE TABLE IF NOT EXISTS `property_rooms_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `roomlist_id` int(11) NOT NULL DEFAULT 0,
  `image` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_tbl`
--

DROP TABLE IF EXISTS `property_tbl`;
CREATE TABLE IF NOT EXISTS `property_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `customer_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `house_facing` varchar(200) DEFAULT NULL,
  `projectsize` varchar(200) DEFAULT NULL,
  `projectarea` varchar(200) DEFAULT NULL,
  `cost` varchar(200) DEFAULT NULL,
  `room_id` int(11) NOT NULL DEFAULT 0,
  `block_id` int(11) NOT NULL DEFAULT 0,
  `floor_id` int(11) NOT NULL DEFAULT 0,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `pincode` varchar(200) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `assign_status` int(11) DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `deleted_status` int(11) NOT NULL DEFAULT 0,
  `deleted_by` int(11) NOT NULL DEFAULT 0,
  `deleted_on` varchar(200) DEFAULT NULL,
  `restored_by` int(11) NOT NULL DEFAULT 0,
  `restored_on` varchar(200) DEFAULT NULL,
  `restored_remarks` longtext DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `request_type_tbl`
--

DROP TABLE IF EXISTS `request_type_tbl`;
CREATE TABLE IF NOT EXISTS `request_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(200) DEFAULT NULL,
  `request_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `session_tbl`
--

DROP TABLE IF EXISTS `session_tbl`;
CREATE TABLE IF NOT EXISTS `session_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `logged_id` int(11) NOT NULL DEFAULT 0,
  `user_type` varchar(50) DEFAULT NULL,
  `portal_type` varchar(100) DEFAULT NULL,
  `auth_referer` varchar(50) DEFAULT NULL,
  `auth_medium` varchar(50) DEFAULT NULL,
  `auth_user_agent` text DEFAULT NULL,
  `auth_ip_address` varchar(250) DEFAULT NULL,
  `session_in` datetime NOT NULL,
  `session_out` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session_tbl`
--

INSERT INTO `session_tbl` (`id`, `branch_id`, `logged_id`, `user_type`, `portal_type`, `auth_referer`, `auth_medium`, `auth_user_agent`, `auth_ip_address`, `session_in`, `session_out`) VALUES
(1, 0, 5, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', '127.0.0.1', '2021-09-01 11:22:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `temp_customer_excel_tbl`
--

DROP TABLE IF EXISTS `temp_customer_excel_tbl`;
CREATE TABLE IF NOT EXISTS `temp_customer_excel_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `upload_token` varchar(50) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `addresss` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_customer_excel_tbl`
--

INSERT INTO `temp_customer_excel_tbl` (`id`, `branch_id`, `upload_token`, `name`, `addresss`, `mobile`, `email`, `created_at`, `updated_at`, `status`) VALUES
(7, 1, 'jvc8BMcRSOVQb2Z', 'Test', 'sample', '1234567890', 'test@mail.com', '2021-09-01 10:21:26', '2021-09-01 10:21:26', 0);

-- --------------------------------------------------------

--
-- Table structure for table `temp_excel_tbl`
--

DROP TABLE IF EXISTS `temp_excel_tbl`;
CREATE TABLE IF NOT EXISTS `temp_excel_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `upload_token` varchar(50) DEFAULT NULL,
  `uid` varchar(80) DEFAULT NULL,
  `enquiry_date` varchar(50) DEFAULT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `lname` varchar(200) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `primary_mobile` varchar(200) DEFAULT NULL,
  `primary_email` varchar(200) DEFAULT NULL,
  `profile` varchar(200) DEFAULT NULL,
  `leadsource` varchar(200) DEFAULT NULL,
  `lead_status` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `pincode` varchar(150) DEFAULT NULL,
  `chit` varchar(100) DEFAULT NULL,
  `deposite` varchar(100) DEFAULT NULL,
  `vas` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_excel_tbl`
--

INSERT INTO `temp_excel_tbl` (`id`, `branch_id`, `upload_token`, `uid`, `enquiry_date`, `fname`, `lname`, `gender`, `primary_mobile`, `primary_email`, `profile`, `leadsource`, `lead_status`, `address`, `city`, `state`, `description`, `pincode`, `chit`, `deposite`, `vas`, `created_at`, `updated_at`, `status`) VALUES
(4, 1, 'pcnt1UmnDzTmDug', NULL, '2021-08-30', 'Arun', 'kumar', 'male', '9944123022', 'koshik@webykart.com', 'doctor', 'fb', 'hot leads', '49w mani nagar', 'coimbatore', 'tamill nadu', 'sample', '641104', '1L', '20L', '', '2021-09-01 11:10:04', '2021-09-01 11:10:04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vas_master_tbl`
--

DROP TABLE IF EXISTS `vas_master_tbl`;
CREATE TABLE IF NOT EXISTS `vas_master_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(200) DEFAULT NULL,
  `item_type` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vas_master_tbl`
--

INSERT INTO `vas_master_tbl` (`id`, `token`, `item_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'insurance', 'Insurance', 1, 1, '2021-07-26 13:53:15', '2021-07-26 13:54:51'),
(2, 'finance', 'Finance', 1, 1, '2021-07-26 13:55:00', '2021-07-26 13:55:00'),
(3, 'housingloan', 'Housing Loan', 1, 1, '2021-08-30 21:21:31', '2021-08-30 21:21:31'),
(4, '', '', 1, 1, '2021-09-01 11:10:08', '2021-09-01 11:10:08');

-- --------------------------------------------------------

--
-- Table structure for table `virtualtour_menusettings_tbl`
--

DROP TABLE IF EXISTS `virtualtour_menusettings_tbl`;
CREATE TABLE IF NOT EXISTS `virtualtour_menusettings_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0,
  `menu` varchar(200) NOT NULL DEFAULT '0',
  `tour_code` longtext DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `virtualtour_menusettings_tbl`
--

INSERT INTO `virtualtour_menusettings_tbl` (`id`, `branch_id`, `menu`, `tour_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'hide', '', 1, '2020-10-06 15:13:33', '2020-10-06 15:13:33');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
