-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 27, 2021 at 08:52 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ss_chit_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_type_tbl`
--

CREATE TABLE `activity_type_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `activity_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_type_tbl`
--

INSERT INTO `activity_type_tbl` (`id`, `branch_id`, `token`, `activity_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'coldcall', 'Cold Call', 1, 1, '2020-08-12 22:00:16', '2020-10-08 12:32:18'),
(2, 1, 'follow-up-1', 'Follow Up 1', 1, 1, '2020-08-13 09:50:59', '2020-08-17 19:40:01'),
(3, 1, 'follow-up-2', 'Follow Up 2', 1, 1, '2020-08-17 18:41:51', '2020-08-17 19:40:06'),
(4, 1, 'inprocess', 'In Process', 1, 1, '2020-08-17 18:41:57', '2020-10-08 12:32:33'),
(5, 1, 'completed', 'completed', 1, 1, '2020-08-17 18:42:02', '2020-08-17 19:40:54'),
(6, 1, 'notattnthecall', 'Not attn the call', 1, 8, '2020-10-09 15:01:23', '2020-10-09 15:01:23'),
(7, 1, 'lowbudget', 'Low budget', 1, 8, '2020-10-09 16:46:14', '2020-10-09 16:46:14');

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_image_tbl`
--

CREATE TABLE `adhoc_image_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `ref_id` int(11) NOT NULL,
  `adoc_id` int(11) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `image_name` varchar(250) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_log_tbl`
--

CREATE TABLE `adhoc_log_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `type` varchar(255) NOT NULL,
  `request_type` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_request_tbl`
--

CREATE TABLE `adhoc_request_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `ticket_uid` varchar(255) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `request_type` varchar(250) NOT NULL,
  `requesttype_id` int(11) NOT NULL,
  `subject` varchar(250) NOT NULL,
  `remarks` longtext NOT NULL,
  `request_status` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_tbl`
--

CREATE TABLE `admin_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `ad_token` varchar(250) NOT NULL,
  `ad_name` varchar(250) NOT NULL,
  `ad_mobile` varchar(50) NOT NULL,
  `ad_email` varchar(250) NOT NULL,
  `ad_dob` varchar(250) NOT NULL,
  `address1` varchar(250) NOT NULL,
  `address2` varchar(250) NOT NULL,
  `ad_city` varchar(250) NOT NULL,
  `ad_pincode` varchar(250) NOT NULL,
  `ad_password` varchar(250) NOT NULL,
  `ad_type` int(1) NOT NULL,
  `ad_super_admin` int(1) NOT NULL,
  `ad_status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_tbl`
--

INSERT INTO `admin_tbl` (`id`, `branch_id`, `ad_token`, `ad_name`, `ad_mobile`, `ad_email`, `ad_dob`, `address1`, `address2`, `ad_city`, `ad_pincode`, `ad_password`, `ad_type`, `ad_super_admin`, `ad_status`, `created_at`, `updated_at`) VALUES
(1, 0, 'admin', 'Admin', '987654321', 'admin@leadcrm.com', '2020-05-30', 'xxxxxx', 'yyyyy', 'cbe', 'tamil nadu', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 1, 1, 1, '2016-06-01 10:00:00', '2020-05-28 09:33:18');

-- --------------------------------------------------------

--
-- Table structure for table `assignlead_tbl`
--

CREATE TABLE `assignlead_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `type` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `remarks` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `remove_status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assignlead_tbl`
--

INSERT INTO `assignlead_tbl` (`id`, `branch_id`, `type`, `created_by`, `created_to`, `ref_id`, `priority`, `remarks`, `status`, `remove_status`, `created_at`, `updated_at`) VALUES
(1, 0, 'assign', 1, 6, 1, 'low', 'sample', 1, 0, '2021-07-29 17:43:22', '2021-07-29 17:43:22'),
(2, 0, 'assign', 1, 7, 862, 'urgent', 'urgent please follow up', 1, 0, '2021-08-23 15:44:41', '2021-08-23 15:44:41'),
(3, 0, 'assign', 1, 7, 2, 'high', 'Test', 0, 0, '2021-08-24 15:13:23', '2021-08-24 16:53:48'),
(4, 0, 'reassign', 1, 8, 2, 'high', 'S', 1, 0, '2021-08-24 16:53:48', '2021-08-24 16:53:48'),
(5, 0, 'assign', 1, 6, 3, 'medium', 'Test', 1, 0, '2021-08-26 11:23:42', '2021-08-26 11:23:42'),
(6, 0, 'assign', 1, 6, 26, 'high', 'Test', 1, 0, '2021-08-26 14:08:58', '2021-08-26 14:08:58'),
(7, 0, 'assign', 1, 6, 864, 'high', 'test', 1, 0, '2021-08-26 14:11:31', '2021-08-26 14:11:31');

-- --------------------------------------------------------

--
-- Table structure for table `block_tbl`
--

CREATE TABLE `block_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `block` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `block_tbl`
--

INSERT INTO `block_tbl` (`id`, `branch_id`, `token`, `block`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'block-1', 'Block 1', 1, 1, '2020-05-28 16:09:22', '2020-09-09 13:40:13'),
(2, 0, 'block-b', 'Block B', 1, 1, '2020-06-03 09:06:06', '2020-09-08 12:20:32'),
(3, 0, 'block-c', 'Block C', 1, 1, '2020-06-19 12:32:52', '2020-06-19 12:32:52'),
(4, 0, 'block-d', 'Block D', 1, 1, '2020-07-03 21:06:49', '2020-07-03 21:06:49'),
(5, 0, 'block-e', 'Block E', 0, 1, '2020-08-21 12:22:39', '2020-08-21 12:22:39'),
(6, 0, 'block-f', 'Block F', 0, 1, '2020-09-08 12:26:16', '2020-09-08 12:26:16'),
(7, 0, 'block-g', 'Block G', 0, 1, '2020-09-08 12:26:28', '2020-09-08 12:26:28'),
(8, 0, 'block-h', 'Block H', 0, 1, '2020-09-08 12:26:36', '2020-09-08 12:26:36'),
(9, 0, 'block-i', 'Block I', 0, 1, '2020-09-08 12:26:46', '2020-09-08 12:26:57'),
(10, 0, 'block-j', 'Block J', 0, 1, '2020-09-08 12:27:04', '2020-09-08 12:27:04'),
(11, 0, 'block-k', 'Block K', 1, 1, '2020-09-08 12:27:14', '2020-09-08 12:27:14'),
(12, 0, 'block-l', 'Block L', 1, 1, '2020-09-08 12:27:31', '2020-09-08 12:27:31');

-- --------------------------------------------------------

--
-- Table structure for table `branch_master_tbl`
--

CREATE TABLE `branch_master_tbl` (
  `id` int(11) NOT NULL,
  `token` varchar(250) NOT NULL,
  `branch` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch_master_tbl`
--

INSERT INTO `branch_master_tbl` (`id`, `token`, `branch`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'coimbatore', 'Coimbatore', 1, 1, '2021-08-24 10:05:49', '2021-08-24 10:05:49'),
(2, 'salem', 'Salem', 1, 1, '2021-08-24 13:31:10', '2021-08-24 13:31:10');

-- --------------------------------------------------------

--
-- Table structure for table `chit_master_tbl`
--

CREATE TABLE `chit_master_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `item_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chit_master_tbl`
--

INSERT INTO `chit_master_tbl` (`id`, `branch_id`, `token`, `item_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '1l', '1L', 1, 1, '2021-07-26 12:02:45', '2021-07-26 12:58:37'),
(2, 0, '2l', '2L', 1, 1, '2021-07-26 12:04:06', '2021-07-26 12:04:06'),
(3, 0, '5l', '5L', 1, 1, '2021-07-26 12:04:12', '2021-07-26 12:04:12'),
(4, 0, '10l', '10L', 1, 1, '2021-07-26 12:04:24', '2021-07-26 12:04:24');

-- --------------------------------------------------------

--
-- Table structure for table `company_settings_tbl`
--

CREATE TABLE `company_settings_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `phone` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `website_address` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_settings_tbl`
--

INSERT INTO `company_settings_tbl` (`id`, `branch_id`, `phone`, `email`, `website_address`, `address`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '9942387100', 'admin@rtohome.com', 'admin@rtohome.com', '99A vyasuva building', 1, 1, '2020-07-22 21:52:59', '2020-07-22 21:52:59');

-- --------------------------------------------------------

--
-- Table structure for table `contactinfo_list_tbl`
--

CREATE TABLE `contactinfo_list_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `contact_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `designation` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contactinfo_tbl`
--

CREATE TABLE `contactinfo_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `role` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contactinfo_tbl`
--

INSERT INTO `contactinfo_tbl` (`id`, `branch_id`, `token`, `name`, `role`, `email`, `mobile`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(2, 0, 'admin', 'admin', 'Admin', 'admin@rtohomes.com', '1234567890', 1, 1, '2020-06-06 10:35:33', '2020-10-06 15:11:16');

-- --------------------------------------------------------

--
-- Table structure for table `customerdatasheet_tbl`
--

CREATE TABLE `customerdatasheet_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `import_token` varchar(50) NOT NULL,
  `uploaded_sheet` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_profile_tbl`
--

CREATE TABLE `customer_profile_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `customer_profile` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_profile_tbl`
--

INSERT INTO `customer_profile_tbl` (`id`, `branch_id`, `token`, `customer_profile`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'doctor', 'Doctor', 1, 1, '2020-09-22 15:38:24', '2020-09-28 18:13:32'),
(2, 0, 'engineer', 'Engineer', 1, 1, '2020-09-28 18:13:38', '2020-09-28 18:13:38'),
(3, 0, 'business', 'Business', 1, 1, '2020-09-28 18:13:46', '2020-09-28 18:13:46'),
(4, 0, 'employee', 'Employee', 1, 1, '2020-09-28 18:14:05', '2020-09-28 18:14:05'),
(5, 0, 'lawyer', 'Lawyer', 1, 1, '2020-09-28 18:14:17', '2020-09-28 18:14:17'),
(6, 0, 'gg', 'Gg', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(7, 0, 'govtservices', 'govt services', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(8, 0, 'proprietor', 'proprietor', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(9, 0, 'manager', 'Manager', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(10, 0, 'ownuse', 'Own use', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(11, 0, 'guard', 'Guard', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(12, 0, 'chairmanmanagingdirectorandceo', 'Chairman Managing Director and CEO', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(13, 0, 'student', 'Student', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(14, 0, 'healthconsultant', 'Health Consultant', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(15, 0, 'assistantprofessor', 'Assistant Professor', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(16, 0, 'housewife', 'House wife', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(17, 0, 'ownerphotographer', 'Owner/Photographer', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(18, 0, 'h', 'h', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(19, 0, 'seniormanager', 'Senior Manager', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(20, 0, 'owner', 'Owner', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(21, 0, 'lecturer', 'Lecturer', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(22, 0, 'villa', 'Villa', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(23, 0, 'directorofphysicaleducation', 'Director of Physical Education', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(24, 0, 'freelancewriter', 'Freelance writer', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(25, 0, 'buyingvillaatcbe', 'buying villa at cbe', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(26, 0, 'soleproprietorship', 'Sole proprietorship', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(27, 0, '', '', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(28, 0, 'nb', 'nb', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(29, 0, 'md', 'Md', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(30, 0, 'flatdetails', 'flat details', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(31, 0, 'villaneeded', 'villa,needed', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(32, 0, 'selfemployed', 'Self-employed', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(33, 0, 'villas', 'villas', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(34, 0, 'commerciallineagency', 'Commercial Line - Agency', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(35, 0, 'regionalhead', 'Regional head', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(36, 0, 'ownbuisness', 'own buisness', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(37, 0, 'relationshipmanager', 'Relationship Manager', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(38, 0, 'workinginpsgcollege', 'Working in PSG college', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(39, 0, 'wealreadyvisitedandsatisfied.', 'We already visited and satisfied.', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(40, 0, 'pastorandevanglist', 'pastor and evanglist', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(41, 0, 'tamilnadugenerationanddistributioncorporationlimited', 'Tamil nadu Generation and Distribution corporation Limited', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(42, 0, 'buildingcontractor', 'building contractor', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(43, 0, 'private', 'Private', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(44, 0, '4bhkandamenities', '4bhk and amenities', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(45, 0, 'branchhead', 'Branch Head', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(46, 0, 'house', 'House', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(47, 0, 'headofthedepartment', 'Head of the department', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(48, 0, 'software', 'software', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(49, 0, 'mobileappdeveloper', 'Mobile App Developer', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37'),
(50, 0, 'section-officer', 'Section officer', 1, 8, '2020-10-20 15:31:12', '2020-10-20 15:31:12'),
(51, 0, 'associate', 'Associate', 1, 8, '2020-10-20 16:12:42', '2020-10-20 16:12:42'),
(52, 0, 'project-manager', 'Project manager', 1, 8, '2020-10-20 16:22:41', '2020-10-20 16:22:41'),
(53, 0, 'consultant', 'Consultant', 1, 8, '2020-10-20 16:29:05', '2020-10-20 16:29:05'),
(54, 0, 'currentlyworking', 'Currently Working', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(55, 0, 'boss', 'Boss', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(56, 0, 'was', 'was', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(57, 0, 'hi', 'hi', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(58, 0, 'designengineer', 'Design Engineer', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(59, 0, 'gmfinance', 'GM Finance', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(60, 0, 'salesmanager', 'sales manager', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(61, 0, 'foundermanagingdirector', 'Founder & Managing Director', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(62, 0, 'wherethialocationinpodanur3bedroomappramnetorvilla', 'Where thia location in podanur 3 bed room appramnet or villA', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(63, 0, 'founderandmanagingdirector', 'Founder and Managing Director', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(64, 0, 'ownbusiness', 'Own-Business', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(65, 0, 'technicalsalesengineer', 'Technical Sales Engineer', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(66, 0, 'charteredaccountant', 'Chartered accountant', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(67, 0, 'bpo', 'BPO', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(68, 0, 'humanresourcescoordinatorhrcoordinator', 'Human Resources Coordinator (HR Coordinator)', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(69, 0, 'managingdirector', 'Managing Director', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(70, 0, 'momscateringparisutham', 'Moms Catering Parisutham', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(71, 0, 'home', 'Home', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(72, 0, 'area', 'area', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(73, 0, 'grouplead', 'GroupLead', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(74, 0, 'distributionhead', 'distribution head', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(75, 0, 'madrashighcourt', 'Madras high court', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(76, 0, 'lead', 'lead', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(77, 0, 'ownerandfounder', 'Owner and Founder', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(78, 0, 'directcall', 'Direct call', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(79, 0, 'softwareengineer', 'Software engineer', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(80, 0, '.location', '.Location ???', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(81, 0, '2bhk', '2bhk', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(82, 0, 'teacher', 'Teacher', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(83, 0, 'teaching', 'teaching', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(84, 0, 'system-admin', 'System Admin', 1, 8, '2020-11-06 10:58:10', '2020-11-06 10:58:10'),
(85, 0, 'bde', 'Bde', 1, 8, '2020-11-06 10:58:41', '2020-11-06 10:58:41'),
(86, 0, 'it', 'IT', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(87, 0, 'seniorsystemsengineer', 'senior systems engineer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(88, 0, 'homemaker', 'homemaker', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(89, 0, 'plsendmethedetailspricesforreadytooccupy', 'Pl send me the details, prices for ready to occupy', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(90, 0, 'seniorsystemexecutive', 'Senior System Executive', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(91, 0, 'agiiitech', 'AG-III(Tech)', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(92, 0, 'subregister', 'sub register', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(93, 0, 'officer', 'officer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(94, 0, 'earthmovingspats', 'Earth Moving Spats', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(95, 0, 'cloudmigrationarchitect', 'Cloud Migration Architect', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(96, 0, 'salesofficer', 'SALES OFFICER', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(97, 0, 'phdcandidateresearcher', 'PhD Candidate/Researcher', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(98, 0, 'salaried', 'salaried', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(99, 0, 'assistant', 'Assistant', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(100, 0, 'newhome', 'New Home', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(101, 0, 'govtemployee', 'Govt employee', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(102, 0, 'purchase', 'purchase', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(103, 0, 'busness', 'Busness', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(104, 0, 'interested', 'interested', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(105, 0, 'driving', 'Driving', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(106, 0, 'proprietorofagcinterior', 'proprietor of Agcinterior', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(107, 0, 'need3bhkindividualhomesvillas', 'Need 3 BHK Individual homes villas', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(108, 0, 'owneroperator', 'Owner Operator', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(109, 0, 'marketingmanager', 'marketing manager', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(110, 0, 'territorymanager', 'Territory Manager', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(111, 0, 's', 's', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(112, 0, 'governmentorganization', 'Government organization', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(113, 0, 'qualityassuranceexecutive', 'Quality assurance executive', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(114, 0, 'specialist', 'Specialist', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(115, 0, 'managerqualityservices', 'Manager - Quality Services', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(116, 0, 'generalmanageroperationsaccounts', 'General Manager - Operations & Accounts', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(117, 0, 'professor', 'Professor', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(118, 0, 'securityconsultant', 'security consultant', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(119, 0, 'marketing', 'Marketing', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(120, 0, 'seniorqaengineer', 'Senior QA Engineer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(121, 0, 'trader', 'Trader', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(122, 0, 'enguir', 'enguir', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(123, 0, 'enigineer', 'Enigineer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(124, 0, 'independenthouse3bedroomsatcoimbatore', 'Independent house - 3 bedrooms at Coimbatore', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(125, 0, 'needkeralatypefrontsidetiltedroofhousers40lto50l.housingloanelegible.', 'Need Kerala type (front side tilted roof) house, Rs 40L to 50L. Housing loan elegible.', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(126, 0, 'lookingfor3bhkvillas', 'Looking for 3bhk villas', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(127, 0, 'pleasecallby3clk', 'Please call by 3clk', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(128, 0, 'bussiness', 'Bussiness', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(129, 0, 'camengineer', 'CAM Engineer', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(130, 0, 'no', 'no', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(131, 0, 'insurance', 'insurance', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(132, 0, 'mycustomerisinbombaywantsvillawith3bhkincoimbatorepleaseshareit', 'My customer is in Bombay wants villa with 3 bhk in coimbatore please share it', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(133, 0, 'registeredmassagetherapist', 'Registered Massage Therapist', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(134, 0, 'accounts', 'accounts', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(135, 0, 'army', 'army', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(136, 0, 'buy', 'Buy', 1, 1, '2021-01-19 09:29:00', '2021-01-19 09:29:00'),
(137, 0, 'music', 'music', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(138, 0, 'siteengineer', 'Site Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(139, 0, 'tobuy', 'To buy', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(140, 0, 'analyst', 'Analyst', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(141, 0, 'upvcwindowsfabricator', 'upvc windows fabricator', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(142, 0, 'executive', 'executive', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(143, 0, 'itprofessional', 'IT professional', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(144, 0, 'executiveofficer', 'Executive Officer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(145, 0, 'hr', 'HR', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(146, 0, 'plcengineer', 'PLC engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(147, 0, 'storemanager', 'Store Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(148, 0, 'singanallur', 'singanallur', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(149, 0, 'underwritingmanager', 'Underwriting Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(150, 0, 'seniorgraphicdesigner', 'Senior Graphic Designer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(151, 0, 'regionalsalesmanager', 'Regional Sales Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(152, 0, 'managingpartner', 'Managing Partner', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(153, 0, 'computeroperator', 'Computer Operator', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(154, 0, 'seniorofficer', 'Senior Officer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(155, 0, 'materialsale', 'material sale', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(156, 0, 'managerfinance', 'Manager - Finance', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(157, 0, 'miss', 'miss', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(158, 0, 'trainer', 'trainer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(159, 0, 'na', 'Na', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(160, 0, 'working', 'working', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(161, 0, 'projectengineer', 'Project Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(162, 0, 'govt', 'govt', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(163, 0, 'readytooccupyvilla', 'Ready to occupy villa', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(164, 0, '3bk', '3 bk', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(165, 0, 'lookingforvillabelow50lakhs', 'Looking for villa below 50 lakhs', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(166, 0, 'pricesfor3bhkvilla', 'Prices for 3bhk villa', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(167, 0, 'bussenes', 'Bussenes', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(168, 0, 'insidesalesatcameronmanufacturingltd', 'Inside sales at Cameron Manufacturing LTD', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(169, 0, 'projectleader', 'Project Leader', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(170, 0, 'consignment', 'Consignment', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(171, 0, 'jr.managermarketing', 'Jr. Manager Marketing', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(172, 0, 'lookingforproperty', 'looking for property', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(173, 0, 'wanttobuy', 'Want to buy', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(174, 0, 'taxconsultant', 'Tax consultant', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(175, 0, 'productdesigner', 'Product Designer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(176, 0, 'physicaleducationteacherpeteacher', 'Physical Education Teacher (PE Teacher)', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(177, 0, 'softwareassociate', 'Software associate', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(178, 0, 'itemployee', 'IT employee', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(179, 0, 'b', 'B', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(180, 0, 'banker', 'Banker', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(181, 0, 'needquotationfor3bhkand2bhk', 'Need quotation for 3bhk and 2bhk', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(182, 0, 'a', 'A', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(183, 0, 'purchaseofhouse', 'purchase of house', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(184, 0, 'seniorassociate', 'Senior Associate', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(185, 0, 'employed', 'Employed', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(186, 0, '36lakhs', '36 lakhs', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(187, 0, 'iaminterestedingatedcommunityindependentvillainandaroundramanathapuramcoimbatore', 'I am interested in gated community independent villa in and around Ramanathapuram, Coimbatore', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(188, 0, 'plscall', 'Pls call', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(189, 0, 'cncmachineoperatorcomputernumericallycontrolledmachineoperator', 'CNC Machine Operator (Computer Numerically Controlled Machine Operator)', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(190, 0, 'productionengineer', 'Production engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(191, 0, 'chuma', 'Chuma', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(192, 0, 'individual', 'individual', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(193, 0, 'branchmanager', 'Branch manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(194, 0, 'programmeranalyst', 'Programmer Analyst', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(195, 0, 'govt.', 'Govt.', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(196, 0, 'creditseniormanager', 'Credit Senior Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(197, 0, 'interiordesigner', 'interior designer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(198, 0, 'bcom', 'bcom', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(199, 0, 'softwareprofessional', 'Software professional', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(200, 0, '.', '.', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(201, 0, 'obgyndoctorobstetricsgynecologydoctor', 'OB/GYN Doctor (Obstetrics/Gynecology Doctor)', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(202, 0, 'seniormanagerbusinessdevelopment', 'Senior Manager - Business Development', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(203, 0, 'yes', 'yes', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(204, 0, 'assistantmanagerhr', 'Assistant Manager HR', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(205, 0, 'enterprisesystemanalyst', 'Enterprise System Analyst', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(206, 0, 'qualitycontrolanalyst', 'Quality Control Analyst', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(207, 0, 'google', 'Google', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(208, 0, 'government', 'Government', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(209, 0, 'lookingfor3bhkvillanearbyavinashird', 'Looking for 3BHK VILLA NEAR BY AVINASHI RD', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(210, 0, 'customerrelationshipofficer', 'Customer relationship officer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(211, 0, 'lookingfor3or4bhkvillasorindependenthousesincoimbatore.', 'Looking for 3 or 4 BHK villas or independent houses in Coimbatore.', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(212, 0, 'needsumdetails', 'Need sum DETAILS', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(213, 0, 'seniorprocessexecutive', 'Senior Process Executive', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(214, 0, 'seniorengineerplanning', 'Senior Engineer - Planning', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(215, 0, 'staff', 'staff', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(216, 0, 'bmp', 'BMP', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(217, 0, 'headfinance', 'Head Finance', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(218, 0, 'sakthivel', 'Sakthivel', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(219, 0, 'buss', 'buss', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(220, 0, 'servicedeliverycoordinator', 'service delivery coordinator', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(221, 0, 'banking', 'Banking', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(222, 0, 'staffnurse', 'Staff Nurse', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(223, 0, 'engineersustainabledesign', 'Engineer-sustainable design', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(224, 0, 'projectmanager', 'Project Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(225, 0, 'ownersoleproprietor', 'Owner/Sole Proprietor', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(226, 0, 'djproducer', 'DJ/Producer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(227, 0, 'kotesan', 'kotesan', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(228, 0, 'rm4683159gmall.com', 'rm4683159@gmall.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(229, 0, 'personalbanker', 'Personal Banker', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(230, 0, 'dainikbhaskurgmail.com', 'dainikbhaskur@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(231, 0, 'civil', 'Civil', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(232, 0, 'individualhome', 'Individual home', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(233, 0, 'technican', 'technican', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(234, 0, 'assisstantmanager', 'Assisstant Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(235, 0, 'itsupporter', 'IT-Supporter', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(236, 0, 'sme', 'SME', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(237, 0, 'buisness', 'buisness', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(238, 0, 'sakrediffmail.com', 'sa_k@rediffmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(239, 0, 'director', 'Director', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(240, 0, 'ms.jenifer96gmail.com', 'ms.jenifer96@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(241, 0, 'engineeringdepartment', 'Engineering department', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(242, 0, 'super', 'Super', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(243, 0, 'ashu', 'Ashu', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(244, 0, 'merchandiser', 'Merchandiser', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(245, 0, 'hrtalentmanager', 'HR Talent Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(246, 0, 'b.comcomplied', 'B.com complied', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(247, 0, 'freelancer', 'Freelancer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(248, 0, 'businessheadtechnology', 'Business Head - Technology', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(249, 0, 'businesses', 'businesses', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(250, 0, 'v.r.vishnuecsgmail.com', 'v.r.vishnuecs@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(251, 0, 'nazi2nazi2002gmail.com', 'nazi2nazi2002@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(252, 0, 'santhosh.balaramanyahoo.com', 'santhosh.balaraman@yahoo.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(253, 0, 'jothipriyakarthikeyangmail.com', 'jothipriyakarthikeyan@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(254, 0, 'pleasegetquoteforindependentvillaincoimbatore', 'Please get quote for independent villa in Coimbatore', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(255, 0, 'networking', 'Networking', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(256, 0, 'desginengineer', 'Desgin Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(257, 0, 'seniorterritorysalesexecutive', 'Senior Territory Sales Executive', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(258, 0, 'electricalengineer', 'electrical engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(259, 0, 'divisionalmanagerproduction', 'Divisional Manager Production', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(260, 0, 'individualhomesunder30lacs', 'Individual homes under 30 lacs', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(261, 0, 'sales', 'sales', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(262, 0, 'processassociate', 'Process Associate', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(263, 0, 'sendcatalogue', 'Send catalogue', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(264, 0, 'associatemanager', 'Associate Manager', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(265, 0, 'estudante', 'Estudante', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(266, 0, 'centralgovt', 'central govt', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(267, 0, 'airlines', 'Airlines', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(268, 0, 'principalarchitectpartner', 'Principal Architect/Partner', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(269, 0, 'physiotherapist', 'Physiotherapist', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(270, 0, 'teamleader', 'Team Leader', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(271, 0, 'softwareengineeruiuxdesigner', 'Software Engineer UI UX Designer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(272, 0, 'applestore', 'Apple Store', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(273, 0, 'testprofessional', 'Test professional', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(274, 0, 'hotel', 'hotel', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(275, 0, 'bb', 'Bb', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(276, 0, 'start', 'start', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(277, 0, 'own', 'Own', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(278, 0, 'vjns', 'v j n s', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(279, 0, 'teamlead', 'Team lead', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(280, 0, 'whichareainpodanur', 'which area in podanur', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(281, 0, 'nagarajan', 'Nagarajan', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(282, 0, 'hemavasu415gmail.com', 'hemavasu415@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(283, 0, 'sakthishivaligmail.com', 'sakthishivali@gmail.com', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(284, 0, 'indianairforce', 'indian air Force', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(285, 0, 'telungupalayam', 'Telungupalayam', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(286, 0, 'mobileshop', 'Mobile Shop', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(287, 0, 'seniornetworksecurityengineer', 'Senior Network Security Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(288, 0, 'functionalconsultant', 'Functional Consultant', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(289, 0, 'need', 'need', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(290, 0, 'assistantmanagerproduction', 'Assistant Manager - Production', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(291, 0, 'expectation', 'expectation', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(292, 0, 'statestreethclservices', 'Statestreet Hcl services', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(293, 0, 'cloudengineer', 'Cloud Engineer', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(294, 0, 'finance', 'Finance', 1, 1, '2021-01-19 09:29:01', '2021-01-19 09:29:01'),
(295, 0, '3bhkhone', '3 bhk hone', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(296, 0, 'southernrailway', 'Southern Railway', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(297, 0, 'required3bhkluxuryvilla', 'Required 3 bhk luxury villa', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(298, 0, 'gc', 'GC', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(299, 0, 'agent', 'Agent', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(300, 0, '3bhk', '3bhk', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(301, 0, 'mr', 'Mr', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(302, 0, 'professional', 'Professional', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(303, 0, 'coimbatoreinstituteoftechnology', 'coimbatore institute of technology', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(304, 0, 'techniciantrainee', 'Technician Trainee', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(305, 0, 'agri', 'agri', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(306, 0, 'bankofficer', 'bank officer', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(307, 0, 'modulelead', 'Module Lead', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(308, 0, 'susintechnologypltd', 'susin technology p ltd', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(309, 0, 'agaramagencies', 'Agaram Agencies', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(310, 0, 'callme', 'call me', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(311, 0, 'locationpricelayoutetcrequired', 'Location price layout etc required', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(312, 0, 'seniorsystemsexecutive', 'Senior Systems Executive', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(313, 0, 'officerdigitalmarketing', 'Officer - Digital Marketing', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(314, 0, 'areasalesmanager', 'Area Sales Manager', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(315, 0, 'ssdgfhmail.com', 'ssdgf@hmail.com', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(316, 0, 'sreena.gopishgmail.com', 'sreena.gopish@gmail.com', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(317, 0, 'detailsof3bhkvilla', 'Details of 3BHK VILLA', 1, 1, '2021-01-19 09:29:02', '2021-01-19 09:29:02'),
(318, 0, 'generalsurgeon', 'General surgeon', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(319, 0, 'photoeditioranddesigner', 'Photo Editior and designer', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(320, 0, 'financemanager', 'Finance manager', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(321, 0, 'byjus', 'Byjus', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(322, 0, 'articleship', 'Articleship', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(323, 0, 'agencies', 'agencies', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(324, 0, 'corporate', 'Corporate', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(325, 0, 'assistantmanager', 'Assistant Manager', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(326, 0, 'acco', 'acco', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(327, 0, 'temple', 'Temple', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(328, 0, 'proprietorship', 'Proprietorship', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(329, 0, 'developer', 'Developer', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(330, 0, 'automotivetestengineer', 'Automotive Test Engineer', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(331, 0, 'stroes', 'Stroes', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(332, 0, 'iamintrested', 'I am intrested', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(333, 0, 'akmedicalcenter', 'Ak medical center', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(334, 0, 'architect', 'Architect', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(335, 0, 'photographer', 'photographer', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(336, 0, 'interested3bhkvillain36l', 'Interested 3bhk villa in 36L', 1, 8, '2021-01-21 14:33:46', '2021-01-21 14:33:46'),
(337, 0, 'foreman', 'Foreman', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(338, 0, 'dringschool', 'Dring school', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(339, 0, 'logisticsincharge', 'Logistics Incharge', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(340, 0, 'homewife', 'home wife', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(341, 0, 'salesmanagerforindiabangladeshitandsrilankaatbuckman', 'Sales Manager for India, Bangladesh it and Sri Lanka at Buckman', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(342, 0, 'agri.officer', 'Agri.Officer', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(343, 0, 'snacks', 'Snacks', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(344, 0, 'businessdevelopmentexecutive', 'business development executive', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(345, 0, 'processlead', 'Process Lead', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(346, 0, 'testingexecutive', 'Testing Executive', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(347, 0, 'chatbot', 'Chatbot', 1, 12, '2021-02-04 16:44:56', '2021-02-04 16:44:56'),
(348, 0, 'seniorengineer', 'Senior Engineer', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(349, 0, 'ceo', 'ceo', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(350, 0, 'merchandisingmanager', 'Merchandising Manager', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(351, 0, 'rsm', 'RSM', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(352, 0, 'hrbp', 'HRBP', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(353, 0, 'businessconsultant', 'Business consultant', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(354, 0, 'pastrychef', 'Pastry Chef', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(355, 0, 'academician', 'Academician', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(356, 0, 'se', 'SE', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(357, 0, 'assistantgeneralmanager', 'Assistant General Manager', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(358, 0, 'engineertechnician', 'Engineer Technician', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(359, 0, 'juniorengineer', 'Junior Engineer', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04'),
(360, 0, 'airconditioningservicecenter', 'Air-conditioning service center', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04');

-- --------------------------------------------------------

--
-- Table structure for table `customer_tbl`
--

CREATE TABLE `customer_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `import_token` varchar(250) DEFAULT NULL,
  `token` varchar(250) NOT NULL,
  `type` varchar(250) DEFAULT NULL,
  `lead_id` int(11) DEFAULT NULL,
  `uid` varchar(250) DEFAULT NULL,
  `priority` varchar(250) NOT NULL,
  `property_id` int(11) DEFAULT NULL,
  `allotment_date` varchar(250) DEFAULT NULL,
  `assign_remarks` longtext DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `mobile` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `has_psw` varchar(250) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `nationality` varchar(250) DEFAULT NULL,
  `pincode` varchar(250) DEFAULT NULL,
  `ad_contact_person` varchar(250) DEFAULT NULL,
  `ad_relationship` varchar(250) DEFAULT NULL,
  `ad_mobile` varchar(250) DEFAULT NULL,
  `ad_email` varchar(250) DEFAULT NULL,
  `ad_address` varchar(250) DEFAULT NULL,
  `ad_city` varchar(250) DEFAULT NULL,
  `ad_state` varchar(250) DEFAULT NULL,
  `ad_pincode` varchar(250) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` int(1) NOT NULL,
  `kyc_status` int(11) NOT NULL,
  `approved_on` varchar(250) DEFAULT NULL,
  `kyc_submittedon` varchar(250) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `kyc_remarks` longtext NOT NULL,
  `assign_status` int(11) DEFAULT NULL,
  `resetpassword` int(11) DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_tbl`
--

INSERT INTO `customer_tbl` (`id`, `branch_id`, `import_token`, `token`, `type`, `lead_id`, `uid`, `priority`, `property_id`, `allotment_date`, `assign_remarks`, `name`, `gender`, `dob`, `mobile`, `email`, `password`, `has_psw`, `address`, `city`, `state`, `nationality`, `pincode`, `ad_contact_person`, `ad_relationship`, `ad_mobile`, `ad_email`, `ad_address`, `ad_city`, `ad_state`, `ad_pincode`, `description`, `status`, `kyc_status`, `approved_on`, `kyc_submittedon`, `approved_by`, `kyc_remarks`, `assign_status`, `resetpassword`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, 'venkatesh', 'moved', 1, 'CU0001', 'low', NULL, NULL, NULL, 'Venkatesh', 'male', NULL, '9003634508', 'koshik2@webykart.com', '5ad49af4eee44e3b6c243dc71e36bc26d7de7435', 'gIflTAJN', '', 'Coimbatore', 'Tamil Nadu', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 12, '2021-02-04 16:58:41', '2021-08-24 16:51:28'),
(2, 1, NULL, 'test', NULL, NULL, 'CU0002', 'low', NULL, NULL, NULL, 'test', 'male', NULL, '1111', 'koshik1@webykart.com', '0363cef9db7c7d7cbd9ff75c708e04857c8c2e88', 'sbmFWKvV', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2021-07-27 11:30:53', '2021-07-29 17:55:53'),
(3, 2, NULL, 'venkatesh-', 'moved', 1, 'CU0003', 'low', NULL, NULL, NULL, 'Venkatesh', 'male', NULL, '9003146833', 'admin@postcrm.com', 'e787d3dfd4c8cd4afcfcd79c314aed00587eefca', 'FCBEhsYo', '', 'Coimbatore', 'Tamil Nadu', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2021-07-29 17:56:29', '2021-07-29 17:56:29'),
(4, 2, NULL, 'viki-', 'moved', 862, 'CU0004', 'low', NULL, NULL, NULL, 'Viki', 'male', NULL, '9942387101', 'viki@venpep.net', 'f38e4abdcb5db8ee2bc48a0fa1fc42df5b146d12', 'iPCmPvro', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2021-08-23 15:47:19', '2021-08-23 15:47:19'),
(5, 2, NULL, 'jagdhesh-', 'moved', 2, 'CU0005', 'low', NULL, NULL, NULL, 'Jagdhesh', 'male', NULL, '9994551832', 'jaga@test.com', 'aaa57b7c4d6cba87acd82b3808b6c4d5c382b19f', 'cwlLLDsq', '', 'Coimbatore', 'Tamil Nadu', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2021-08-26 16:56:13', '2021-08-26 16:56:13'),
(6, 2, NULL, 'praveen-kumar-', 'moved', 4, 'CU0006', 'low', NULL, NULL, NULL, 'Praveen Kumar', 'M', NULL, '9894588627', '', '4f110e8db084b6afec04347db9875212364f2893', 'NUtaVOZj', '', 'Coimbatore', 'Tamil Nadu', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2021-08-26 16:57:49', '2021-08-26 16:57:49'),
(7, 1, NULL, 'munish-', 'moved', 26, 'CU0007', 'low', NULL, NULL, NULL, 'Munish', 'male', NULL, '9842543552', 'munish@gmail.com', 'cdcf12167b4a98079a8f93d9cf1098b84072c327', 'vsMsbUBA', '', 'Coimbatore', 'Tamil Nadu', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2021-08-26 17:00:15', '2021-08-26 17:00:15'),
(12, 1, NULL, 'vigneswaran', NULL, NULL, 'CU0012', 'high', NULL, NULL, NULL, 'Vigneswaran', 'male', NULL, '9878987678', 'vignesh@gmail.com', '3a268c731af0bb65b23e21b039d0c7402dca498d', 'pMdKQUiX', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2021-08-27 12:17:34', '2021-08-27 12:17:34'),
(13, 2, NULL, 'anand', NULL, NULL, 'CU0013', 'medium', NULL, NULL, NULL, 'Anand', 'male', NULL, '7094265321', 'anandbechris@gmail.com', 'f885062ceb398e97a16906b31ffc468c5d385dfd', 'LivgtVAz', '272/22, Ayynarappan kovil near , v.v.c.r nager , Erode', 'Coimbatore', 'Tamil Nadu', NULL, '641018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2021-08-27 12:18:24', '2021-08-27 12:18:24');

-- --------------------------------------------------------

--
-- Table structure for table `datasheet_tbl`
--

CREATE TABLE `datasheet_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `import_token` varchar(50) NOT NULL,
  `uploaded_sheet` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `datasheet_tbl`
--

INSERT INTO `datasheet_tbl` (`id`, `branch_id`, `import_token`, `uploaded_sheet`, `created_at`, `updated_at`, `status`) VALUES
(1, 0, 'n7cZkv5FGeiv7zx', '24-02-2021_sE2U2jKi_leadreport.xlsx', '2021-02-24 14:46:51', '2021-02-24 14:47:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `deposite_master_tbl`
--

CREATE TABLE `deposite_master_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `item_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposite_master_tbl`
--

INSERT INTO `deposite_master_tbl` (`id`, `branch_id`, `token`, `item_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '1l', '1L', 1, 1, '2021-07-26 13:33:49', '2021-07-26 13:35:20'),
(2, 0, '2l', '2L', 1, 1, '2021-07-26 13:35:24', '2021-07-26 13:35:24'),
(3, 0, '5l', '5L', 1, 1, '2021-07-26 13:35:32', '2021-07-26 13:35:32'),
(4, 0, '10l', '10L', 1, 1, '2021-07-26 13:35:41', '2021-07-26 13:35:41');

-- --------------------------------------------------------

--
-- Table structure for table `employee_permission_tbl`
--

CREATE TABLE `employee_permission_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `employee_id` varchar(250) DEFAULT NULL,
  `customers` int(11) NOT NULL DEFAULT 0,
  `manage_customer` int(11) NOT NULL DEFAULT 0,
  `import_customer` int(11) NOT NULL DEFAULT 0,
  `lead` int(11) NOT NULL DEFAULT 0,
  `manage_lead` int(11) NOT NULL DEFAULT 0,
  `lead_type` int(11) NOT NULL DEFAULT 0,
  `activity_type` int(11) NOT NULL DEFAULT 0,
  `lead_source` int(11) NOT NULL DEFAULT 0,
  `customer_profile` int(11) NOT NULL DEFAULT 0,
  `import_lead` int(11) NOT NULL DEFAULT 0,
  `employee` int(11) NOT NULL DEFAULT 0,
  `manageemployee` int(11) NOT NULL DEFAULT 0,
  `reports` int(11) NOT NULL DEFAULT 0,
  `lead_report` int(11) NOT NULL DEFAULT 0,
  `customer_report` int(11) NOT NULL DEFAULT 0,
  `settings` int(11) NOT NULL DEFAULT 0,
  `chit_master` int(11) NOT NULL DEFAULT 0,
  `deposit_master` int(11) NOT NULL DEFAULT 0,
  `vam_master` int(11) NOT NULL DEFAULT 0,
  `branch_master` int(1) DEFAULT 0,
  `employee_role_master` int(1) DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_permission_tbl`
--

INSERT INTO `employee_permission_tbl` (`id`, `branch_id`, `employee_id`, `customers`, `manage_customer`, `import_customer`, `lead`, `manage_lead`, `lead_type`, `activity_type`, `lead_source`, `customer_profile`, `import_lead`, `employee`, `manageemployee`, `reports`, `lead_report`, `customer_report`, `settings`, `chit_master`, `deposit_master`, `vam_master`, `branch_master`, `employee_role_master`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '1', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, '2020-06-06 15:06:09', '2020-06-06 15:12:37'),
(6, 0, '6', 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, '2020-10-08 10:35:26', '2021-08-26 11:37:56'),
(7, 0, '7', 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, '2020-10-08 12:26:30', '2021-08-24 15:14:20'),
(8, 0, '8', 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, '2020-10-08 12:28:36', '2020-11-03 14:54:53'),
(9, 0, '9', 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, '2020-10-08 12:29:38', '2020-10-08 13:24:35'),
(10, 0, '10', 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, '2020-11-03 17:06:46', '2020-11-03 17:07:11'),
(11, 0, '11', 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, '2020-11-03 17:17:30', '2020-11-03 17:17:37'),
(12, 0, '12', 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, '2021-02-02 12:51:18', '2021-02-02 12:51:47'),
(13, 0, '13', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, '2021-07-27 10:59:09', '2021-07-27 11:00:11'),
(14, 0, '14', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, '2021-08-24 18:06:22', '2021-08-24 18:06:22'),
(15, 0, '15', 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, '2021-08-24 18:12:28', '2021-08-24 18:12:28');

-- --------------------------------------------------------

--
-- Table structure for table `employee_role_master_tbl`
--

CREATE TABLE `employee_role_master_tbl` (
  `id` int(11) NOT NULL,
  `token` varchar(250) NOT NULL,
  `employee_role` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_role_master_tbl`
--

INSERT INTO `employee_role_master_tbl` (`id`, `token`, `employee_role`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'partner', 'Partner', 1, 1, '2021-08-24 11:23:41', '2021-08-24 11:23:41'),
(2, 'generalmanager', 'General Manager', 1, 1, '2021-08-24 15:28:24', '2021-08-24 16:14:15'),
(3, 'branchmanager', 'Branch Manager', 1, 1, '2021-08-24 16:14:30', '2021-08-24 16:14:30'),
(4, 'sr.executivesales', 'Sr. Executive Sales', 1, 1, '2021-08-24 11:20:33', '2021-08-24 11:20:33'),
(5, 'executiveoffice', 'Executive Office', 1, 1, '2021-08-24 11:21:09', '2021-08-24 11:21:09'),
(6, 'telecaller', 'Telecaller', 1, 1, '2021-08-24 11:23:01', '2021-08-24 11:23:01');

-- --------------------------------------------------------

--
-- Table structure for table `employee_tbl`
--

CREATE TABLE `employee_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `employee_uid` varchar(250) DEFAULT NULL,
  `token` varchar(250) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `role` int(1) DEFAULT 0,
  `gender` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `pincode` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `super_admin` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_tbl`
--

INSERT INTO `employee_tbl` (`id`, `branch_id`, `employee_uid`, `token`, `name`, `role`, `gender`, `password`, `email`, `mobile`, `address`, `city`, `state`, `pincode`, `status`, `super_admin`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 2, 'EMP0001', 'admin', 'Admin', 2, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Admin@mail.com', '9942387100', '', 'coimbatore', '', '6441103', 1, 1, 1, '2020-06-06 15:06:09', '2021-07-27 11:34:35'),
(6, 1, 'EMP0006', 'koushik', 'koushik', 1, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'koshikprabhur@venpep.com', '9944123022', '', '', '', '', 1, 0, 6, '2020-10-08 10:35:26', '2021-08-24 17:42:41'),
(7, 2, 'EMP0007', 'savitha', 'Savitha', 3, 'female', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'savithapnrhousing@gmail.com', '9940940904', 'Cbe', 'Cbe', 'TN', '', 1, 0, 1, '2020-10-08 12:26:30', '2021-08-24 11:40:37'),
(8, 1, 'EMP0008', 'latha', 'Latha', 2, 'female', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'admin@pnrhousing.com', '7094038921', 'Cbe', 'Cbe', 'TN', '', 1, 0, 1, '2020-10-08 12:28:36', '2020-10-08 12:28:36'),
(9, 2, 'EMP0009', 'suren', 'Suren', 1, 'male', '50ddd3ef48102f66c6df2a6a5b638ca1d07c57e5', 'suren.vittel@gmail.com', '9442626677', 'Cbe', 'Cbe', 'TN', '', 1, 0, 1, '2020-10-08 12:29:38', '2021-08-24 11:40:43'),
(10, 2, 'EMP0010', 'abhishek-', 'Abhishek', 2, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'archiabi@gmail.com', '9841081600', '', '', '', '', 1, 0, 1, '2020-11-03 17:06:46', '2020-11-07 20:22:38'),
(11, 2, 'EMP0011', 'shyam', 'Shyam', 2, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'shyam@venpep.com', '9894090038', '', '', '', '', 1, 0, 1, '2020-11-03 17:17:30', '2020-11-03 17:17:30'),
(12, 1, 'EMP0012', 'jenifer', 'Jenifer', 2, 'female', 'b80619e95f50f9ce4e3b4b8d1ce250e4b0d8aaca', 'jenifer.i@venpep.net', '9600452341', 'CBE', 'CBE', 'TN', '641023', 1, 0, 1, '2021-02-02 12:51:18', '2021-02-02 14:29:43'),
(13, 2, 'EMP0013', 'test', 'test', 2, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'koushik1@mail.com', '994412303', '', '', '', '', 1, 0, 1, '2021-07-27 10:59:09', '2021-07-27 10:59:09'),
(14, 2, 'EMP0014', 'anand', 'Anand', 4, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'anandbechris@gmail.com', '7094265321', '272/22, Ayynarappan kovil near , v.v.c.r nager , Erode', 'Coimbatore', 'Tamil Nadu', '641018', 1, 0, 1, '2021-08-24 18:06:22', '2021-08-24 18:06:22'),
(15, 2, 'EMP0015', 'anand', 'Anand', 1, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'anandbechris1@gmail.com', '7094265323', '272/22, Ayynarappan kovil near , v.v.c.r nager , Erode', 'Coimbatore', 'Tamil Nadu', '641018', 1, 0, 1, '2021-08-24 18:12:28', '2021-08-24 18:12:28');

-- --------------------------------------------------------

--
-- Table structure for table `flatvilla_documenttype_list_tbl`
--

CREATE TABLE `flatvilla_documenttype_list_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `document_id` int(11) NOT NULL,
  `doc_name` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flatvilla_documenttype_list_tbl`
--

INSERT INTO `flatvilla_documenttype_list_tbl` (`id`, `branch_id`, `document_id`, `doc_name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'EB Form', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 1, '2020-06-16 09:54:06', '2020-06-16 09:54:06'),
(2, 0, 1, 'EB Bill', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 1, '2020-06-16 09:54:06', '2020-06-16 09:54:06'),
(3, 0, 2, 'Building Ouside View', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ', 1, '2020-06-16 09:55:52', '2020-06-16 09:55:52'),
(4, 0, 2, 'Building inner View', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 1, '2020-06-16 09:55:52', '2020-06-16 09:55:52'),
(5, 0, 4, 'Land Sub Document ', 'Add Document', 1, '2020-06-19 12:32:01', '2020-06-19 12:32:01'),
(6, 0, 5, 'building approval document', 'sample content', 1, '2020-08-21 11:58:53', '2020-08-21 11:58:53'),
(7, 0, 6, 'Booking Form', 'Booking Form', 1, '2020-09-04 18:34:11', '2020-09-04 18:34:11'),
(8, 0, 7, 'Allotment Letter', 'Allotment Letter', 1, '2020-09-04 18:34:42', '2020-09-04 18:34:42'),
(9, 0, 8, 'Sale Agreement', 'Sale Agreement', 1, '2020-09-04 18:35:07', '2020-09-04 18:35:07'),
(10, 0, 9, 'Construction Agreement', 'Construction Agreement', 1, '2020-09-04 18:35:33', '2020-09-04 18:35:33'),
(11, 0, 10, 'Tripartite Agreement', 'Tripartite Agreement', 1, '2020-09-04 18:36:05', '2020-09-04 18:36:05'),
(12, 0, 11, 'Sale Deed', 'Sale Deed', 1, '2020-09-04 18:36:29', '2020-09-04 18:36:29'),
(13, 0, 12, 'Builder Demand', 'Builder Demand', 1, '2020-09-04 18:36:55', '2020-09-04 18:36:55'),
(14, 0, 13, 'Property Tax Book', 'Property Tax Book', 1, '2020-09-04 18:37:21', '2020-09-04 18:37:21');

-- --------------------------------------------------------

--
-- Table structure for table `flatvilla_documenttype_tbl`
--

CREATE TABLE `flatvilla_documenttype_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `doc_title` varchar(250) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `publish_status` int(11) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flatvilla_documenttype_tbl`
--

INSERT INTO `flatvilla_documenttype_tbl` (`id`, `branch_id`, `token`, `doc_title`, `sort_order`, `status`, `publish_status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'eb-documents', 'EB Documents', 1, 0, 0, 0, 0, '', 0, '', '', 1, '2020-06-16 09:54:06', '2020-06-16 09:54:06'),
(2, 0, 'building-blue-print', 'Building Blue Print', 2, 0, 0, 0, 0, '', 0, '', '', 1, '2020-06-16 09:55:52', '2020-07-08 01:37:33'),
(3, 0, 'land-property-document', 'Land Property Document', 3, 0, 0, 0, 0, '', 0, '', '', 1, '2020-06-19 11:35:15', '2020-06-19 11:35:15'),
(4, 0, 'land-approval-document-', 'Land Approval Document', 2, 0, 0, 0, 0, '', 0, '', '', 1, '2020-06-19 12:32:01', '2020-06-19 12:32:01'),
(5, 0, 'building-approval-document', 'Building approval document', 4, 0, 0, 0, 0, '', 0, '', '', 1, '2020-08-21 11:58:53', '2020-08-21 11:58:53'),
(6, 0, 'booking-form', 'Booking Form', 6, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:34:11', '2020-09-04 18:34:11'),
(7, 0, 'allotment-letter', 'Allotment Letter', 7, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:34:42', '2020-09-04 18:34:42'),
(8, 0, 'sale-agreement', 'Sale Agreement', 8, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:35:07', '2020-09-04 18:35:07'),
(9, 0, 'construction-agreement', 'Construction Agreement', 9, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:35:33', '2020-09-04 18:35:33'),
(10, 0, 'tripartite-agreement', 'Tripartite Agreement', 10, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:36:05', '2020-09-04 18:36:05'),
(11, 0, 'sale-deed', 'Sale Deed', 11, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:36:29', '2020-09-04 18:36:29'),
(12, 0, 'builder-demand', 'Builder Demand', 12, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:36:55', '2020-09-04 18:36:55'),
(13, 0, 'property-tax-book', 'Property Tax Book', 13, 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-04 18:37:21', '2020-09-04 18:37:21');

-- --------------------------------------------------------

--
-- Table structure for table `flat_type_master`
--

CREATE TABLE `flat_type_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `flat_variant` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flat_type_master`
--

INSERT INTO `flat_type_master` (`id`, `branch_id`, `token`, `flat_variant`, `description`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, '1bhk', '1BHK', 'Sample', 1, 1, '2020-06-13 14:03:37', '2020-06-13 14:32:24'),
(2, 0, '2bhk', '2BHK', 'sample', 1, 1, '2020-06-13 14:04:06', '2020-06-13 14:04:06'),
(3, 0, '3bhk', '3BHK', 'sample', 1, 1, '2020-06-13 14:04:21', '2020-06-13 14:04:21'),
(4, 0, '4bhk', '4BHK', '', 1, 1, '2020-06-19 13:33:35', '2020-06-19 13:33:35'),
(6, 0, '5bhk', '5BHK', '', 0, 1, '2020-06-22 18:36:31', '2020-06-22 18:36:31'),
(8, 0, '', '', '', 1, 1, '2020-10-08 12:55:37', '2020-10-08 12:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `flat_type_masterlist`
--

CREATE TABLE `flat_type_masterlist` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `flattype_id` int(11) NOT NULL,
  `room_type` varchar(250) NOT NULL,
  `sqft` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flat_type_masterlist`
--

INSERT INTO `flat_type_masterlist` (`id`, `branch_id`, `flattype_id`, `room_type`, `sqft`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Bed Room', '256', 'sample', 1, '2020-06-13 14:03:37', '2020-06-13 14:03:37'),
(2, 0, 1, 'Kitchen', '100', 'sample', 1, '2020-06-13 14:03:37', '2020-06-13 14:03:37'),
(3, 0, 2, 'Bed Room', '256', 'sample', 1, '2020-06-13 14:04:06', '2020-06-13 14:04:06'),
(4, 0, 2, 'Kitchen', '100', 'sampl 1', 1, '2020-06-13 14:04:06', '2020-06-13 14:04:06'),
(5, 0, 3, 'Bed Room', '123', 'sample', 1, '2020-06-13 14:04:21', '2020-06-13 14:04:21'),
(7, 0, 4, '4 BHK', '220 Sqft', 'Appartment', 1, '2020-06-19 13:33:35', '2020-06-19 13:33:35'),
(8, 0, 5, '1 BHK', '290', 'Add Flat Type Master', 1, '2020-06-19 19:00:40', '2020-06-19 19:00:40');

-- --------------------------------------------------------

--
-- Table structure for table `floor_tbl`
--

CREATE TABLE `floor_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `floor` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `floor_tbl`
--

INSERT INTO `floor_tbl` (`id`, `branch_id`, `token`, `floor`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(2, 0, '1-floor', '1 Floor', 1, 1, '2020-05-28 16:42:59', '2020-07-08 01:39:59'),
(3, 0, '2-floor', '2 Floor', 1, 1, '2020-06-03 09:07:41', '2020-06-03 09:07:41'),
(4, 0, '3-floor', '3 Floor', 1, 1, '2020-06-13 17:29:44', '2020-06-13 17:29:44'),
(5, 0, '4-floor', '4 Floor', 1, 1, '2020-06-19 13:28:32', '2020-06-19 13:28:32'),
(6, 0, '5-floor', '5 Floor', 1, 1, '2020-07-03 21:18:43', '2020-07-03 21:18:43'),
(7, 0, '6-floor', '6 Floor', 1, 1, '2020-08-31 11:18:37', '2020-08-31 11:18:37'),
(8, 0, '7-floor', '7 Floor', 1, 1, '2020-08-31 11:18:45', '2020-08-31 11:18:45'),
(9, 0, '8-floor', '8 Floor', 1, 1, '2020-08-31 11:18:56', '2020-08-31 11:18:56');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_image_tbl`
--

CREATE TABLE `gallery_image_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `gallery_id` varchar(250) DEFAULT NULL,
  `image` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_image_tbl`
--

INSERT INTO `gallery_image_tbl` (`id`, `branch_id`, `gallery_id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 0, '1', 'Yg7h12qOima_lH4p6W8QoTZUkf5NCc.jpg', 1, '2021-07-26 22:27:38', '2021-07-26 22:27:38'),
(4, 0, '1', 'pxN3SiQdwch916TyGzCXnPDeBIkHlm.jpg', 1, '2021-07-26 22:46:25', '2021-07-26 22:46:25'),
(5, 0, '1', 'gN0S1nxs8eXT_CWVfDmkboZMrhyGpw.jpg', 1, '2021-07-26 22:46:25', '2021-07-26 22:46:25');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_tbl`
--

CREATE TABLE `gallery_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) DEFAULT NULL,
  `album_title` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(250) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_tbl`
--

INSERT INTO `gallery_tbl` (`id`, `branch_id`, `token`, `album_title`, `description`, `image`, `sort_order`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'test', 'test', '', 'xaZeFgFJ5426072021102722img2-middle.jpg', 1, 1, 0, 0, '', 0, '', '', 1, '2021-07-26 22:27:22', '2021-07-26 22:27:22');

-- --------------------------------------------------------

--
-- Table structure for table `gendral_documents_tbl`
--

CREATE TABLE `gendral_documents_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `documents` varchar(250) NOT NULL,
  `document_type` varchar(250) NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_tbl`
--

CREATE TABLE `invoice_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `inv_uid` varchar(255) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `inv_number` varchar(255) DEFAULT NULL,
  `inv_date` varchar(250) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `raised_to` varchar(250) DEFAULT NULL,
  `documents` varchar(250) DEFAULT NULL,
  `document_type` varchar(250) NOT NULL,
  `remarks` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kyc_list_tbl`
--

CREATE TABLE `kyc_list_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `customer_id` int(11) NOT NULL,
  `kyctype_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `file_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `approval_status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kyc_tbl`
--

CREATE TABLE `kyc_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `kyc_uid` varchar(250) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `id_type` varchar(250) NOT NULL,
  `firstname` varchar(250) NOT NULL,
  `lastname` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `dob` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `address1` varchar(250) NOT NULL,
  `address2` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `nationiality` varchar(250) NOT NULL,
  `pincode` varchar(250) NOT NULL,
  `approval_status` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kyc_type_tbl`
--

CREATE TABLE `kyc_type_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `kyc_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kyc_type_tbl`
--

INSERT INTO `kyc_type_tbl` (`id`, `branch_id`, `token`, `kyc_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'aadhaar-card', 'Aadhaar Card', 1, 1, '2020-07-02 22:58:39', '2020-07-02 22:58:39'),
(2, 0, 'pan-card', 'PAN Card', 1, 1, '2020-07-02 22:59:04', '2020-07-02 22:59:04'),
(3, 0, 'ration-card', 'Ration Card', 1, 1, '2020-07-02 22:59:12', '2020-07-02 22:59:12'),
(4, 0, 'communication-address', 'Communication address', 1, 1, '2020-07-02 22:59:20', '2020-09-09 14:46:37'),
(5, 0, 'aadhar-card-witness-details-for-documentation', 'Aadhar card -Witness details for documentation', 1, 1, '2020-07-02 22:59:27', '2020-09-09 14:47:15'),
(6, 0, 'photo', 'Photo', 1, 1, '2020-07-03 22:13:12', '2020-09-09 14:46:13'),
(7, 0, 'bank-loan-sanction-letter', 'Bank Loan Sanction Letter', 1, 1, '2020-09-09 14:45:55', '2020-09-09 14:45:55');

-- --------------------------------------------------------

--
-- Table structure for table `lead_activity_tbl`
--

CREATE TABLE `lead_activity_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `date` varchar(250) NOT NULL,
  `lead_id` varchar(250) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `remarks` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_activity_tbl`
--

INSERT INTO `lead_activity_tbl` (`id`, `branch_id`, `date`, `lead_id`, `employee_id`, `activity_id`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, '2021-02-25', '625', 12, 1, 'No answer', 1, '2021-02-25 10:45:19', '2021-02-25 10:45:19'),
(2, 0, '2021-02-25', '624', 12, 1, 'No answer', 1, '2021-02-25 10:45:51', '2021-02-25 10:45:51'),
(3, 0, '2021-02-25', '631', 12, 1, 'Call back later', 1, '2021-02-25 10:47:21', '2021-02-25 10:47:21'),
(4, 0, '2021-02-25', '624', 12, 1, 'Site visit on 28/2/21 afternoon', 1, '2021-02-25 10:53:09', '2021-02-25 10:53:09'),
(5, 0, '2021-02-25', '635', 12, 1, 'Busy', 1, '2021-02-25 10:54:59', '2021-02-25 10:54:59'),
(6, 0, '2021-02-25', '636', 12, 1, 'Looking for house in vadavalli side, budget-40L', 1, '2021-02-25 10:57:46', '2021-02-25 10:57:46'),
(7, 0, '2021-02-25', '637', 12, 1, 'No answer', 1, '2021-02-25 10:59:12', '2021-02-25 10:59:12'),
(8, 0, '2021-02-25', '639', 12, 1, 'Not interested( Need RTO home)', 1, '2021-02-25 11:02:38', '2021-02-25 11:02:38'),
(9, 0, '2021-02-25', '641', 12, 1, 'Will call back later', 1, '2021-02-25 11:05:59', '2021-02-25 11:05:59'),
(10, 0, '2021-02-25', '642', 12, 1, 'No answer', 1, '2021-02-25 11:09:43', '2021-02-25 11:09:43'),
(11, 0, '2021-02-25', '644', 12, 1, 'Call back later', 1, '2021-02-25 11:25:51', '2021-02-25 11:25:51'),
(12, 0, '2021-02-25', '646', 12, 1, 'No answer', 1, '2021-02-25 11:31:29', '2021-02-25 11:31:29'),
(13, 0, '2021-02-25', '647', 12, 1, 'Call @ 6.15', 1, '2021-02-25 11:32:14', '2021-02-25 11:32:14'),
(14, 0, '2021-02-25', '650', 12, 1, 'No answer', 1, '2021-02-25 11:34:09', '2021-02-25 11:34:09'),
(15, 0, '2021-02-25', '651', 12, 1, 'No answer', 1, '2021-02-25 11:35:43', '2021-02-25 11:35:43'),
(16, 0, '2021-02-25', '652', 12, 1, 'Wrong number', 1, '2021-02-25 11:37:08', '2021-02-25 11:37:08'),
(17, 0, '2021-02-25', '653', 12, 1, 'No answer', 1, '2021-02-25 11:38:47', '2021-02-25 11:38:47'),
(18, 0, '2021-02-25', '655', 12, 1, 'No answer', 1, '2021-02-25 12:09:03', '2021-02-25 12:09:03'),
(19, 0, '2021-02-25', '657', 12, 1, 'Switched off', 1, '2021-02-25 12:14:40', '2021-02-25 12:14:40'),
(20, 0, '2021-02-25', '658', 12, 1, 'Call back later', 1, '2021-02-25 12:19:25', '2021-02-25 12:19:25'),
(21, 0, '2021-02-25', '659', 12, 1, 'Plan changed, now not interested', 1, '2021-02-25 12:21:46', '2021-02-25 12:21:46'),
(22, 0, '2021-02-25', '660', 12, 1, 'No answer', 1, '2021-02-25 12:26:06', '2021-02-25 12:26:06'),
(23, 0, '2021-02-25', '661', 12, 1, 'Call back later', 1, '2021-02-25 12:27:21', '2021-02-25 12:27:21'),
(24, 0, '2021-02-25', '662', 12, 1, 'Plan postponed because of medical expenses', 1, '2021-02-25 12:30:33', '2021-02-25 12:30:33'),
(25, 0, '2021-02-25', '664', 12, 1, 'Busy', 1, '2021-02-25 12:38:19', '2021-02-25 12:38:19'),
(26, 0, '2021-02-25', '666', 12, 1, 'Not reachable', 1, '2021-02-25 12:40:52', '2021-02-25 12:40:52'),
(27, 0, '2021-02-25', '668', 12, 1, 'No answer', 1, '2021-02-25 12:42:06', '2021-02-25 12:42:06'),
(28, 0, '2021-02-25', '671', 12, 1, 'No answer', 1, '2021-02-25 12:43:31', '2021-02-25 12:43:31'),
(29, 0, '2021-02-25', '672', 12, 1, 'Busy', 1, '2021-02-25 12:44:54', '2021-02-25 12:44:54'),
(30, 0, '2021-02-25', '673', 12, 1, 'Busy', 1, '2021-02-25 12:46:00', '2021-02-25 12:46:00'),
(31, 0, '2021-02-25', '674', 12, 1, 'No answer', 1, '2021-02-25 12:47:29', '2021-02-25 12:47:29'),
(32, 0, '2021-02-25', '675', 12, 1, 'Not reachable', 1, '2021-02-25 12:49:40', '2021-02-25 12:49:40'),
(33, 0, '2021-02-25', '679', 12, 1, 'Switched off', 1, '2021-02-25 12:53:56', '2021-02-25 12:53:56'),
(34, 0, '2021-02-25', '675', 12, 1, 'Busy', 1, '2021-02-25 12:55:46', '2021-02-25 12:55:46'),
(35, 0, '2021-02-25', '681', 12, 1, 'Plan dropped', 1, '2021-02-25 12:57:38', '2021-02-25 12:57:38'),
(36, 0, '2021-02-25', '682', 12, 1, 'Wrong number', 1, '2021-02-25 12:59:34', '2021-02-25 12:59:34'),
(37, 0, '2021-02-25', '671', 12, 1, 'No answer', 1, '2021-02-25 13:02:05', '2021-02-25 13:02:05'),
(38, 0, '2021-02-25', '683', 12, 1, 'No network', 1, '2021-02-25 13:03:46', '2021-02-25 13:03:46'),
(39, 0, '2021-02-25', '684', 12, 1, 'Call @ 2.30', 1, '2021-02-25 13:06:20', '2021-02-25 13:06:20'),
(40, 0, '2021-02-25', '685', 12, 1, 'No answer', 1, '2021-02-25 13:07:35', '2021-02-25 13:07:35'),
(41, 0, '2021-02-25', '686', 12, 1, 'Call back later', 1, '2021-02-25 13:10:59', '2021-02-25 13:10:59'),
(42, 0, '2021-02-25', '668', 12, 1, 'Want minimum 3 cents, budget-60L', 1, '2021-02-25 14:29:00', '2021-02-25 14:29:00'),
(43, 0, '2021-02-25', '301', 12, 1, 'Not interested, disconnected the call', 1, '2021-02-25 18:23:45', '2021-02-25 18:23:45'),
(44, 0, '2021-02-26', '76', 12, 1, 'Call back on 5/3/21', 1, '2021-02-26 11:38:38', '2021-02-26 11:38:38'),
(45, 0, '2021-02-26', '87', 12, 1, 'Not interested', 1, '2021-02-26 11:39:44', '2021-02-26 11:39:44'),
(46, 0, '2021-02-26', '89', 12, 1, 'Switched off', 1, '2021-02-26 11:40:49', '2021-02-26 11:40:49'),
(47, 0, '2021-02-26', '103', 12, 1, 'No answer', 1, '2021-02-26 11:42:51', '2021-02-26 11:42:51'),
(48, 0, '2021-02-26', '109', 12, 1, 'No answer', 1, '2021-02-26 11:46:58', '2021-02-26 11:46:58'),
(49, 0, '2021-02-26', '129', 12, 1, 'Call back in the afternoon', 1, '2021-02-26 11:55:26', '2021-02-26 11:55:26'),
(50, 0, '2021-02-26', '175', 12, 1, 'No answer', 1, '2021-02-26 12:15:53', '2021-02-26 12:15:53'),
(51, 0, '2021-02-26', '202', 12, 1, 'No answer', 1, '2021-02-26 12:31:24', '2021-02-26 12:31:24'),
(52, 0, '2021-02-26', '219', 12, 1, 'No answer', 1, '2021-02-26 12:32:04', '2021-02-26 12:32:04'),
(53, 0, '2021-02-26', '166', 12, 1, 'Call back on 27/2/21', 1, '2021-02-26 12:32:42', '2021-02-26 12:32:42'),
(54, 0, '2021-02-26', '175', 12, 1, 'No answer', 1, '2021-02-26 12:33:12', '2021-02-26 12:33:12'),
(55, 0, '2021-02-26', '253', 12, 1, 'Call back on 1/3/21', 1, '2021-02-26 12:47:16', '2021-02-26 12:47:16'),
(56, 0, '2021-02-26', '267', 12, 1, 'No answer', 1, '2021-02-26 12:49:02', '2021-02-26 12:49:02'),
(57, 0, '2021-02-26', '273', 12, 1, 'Busy', 1, '2021-02-26 12:50:07', '2021-02-26 12:50:07'),
(58, 0, '2021-02-26', '275', 12, 1, 'Busy', 1, '2021-02-26 12:51:26', '2021-02-26 12:51:26'),
(59, 0, '2021-02-26', '283', 12, 1, 'No answer', 1, '2021-02-26 12:53:06', '2021-02-26 12:53:06'),
(60, 0, '2021-02-26', '343', 12, 1, 'No answer', 1, '2021-02-26 12:55:01', '2021-02-26 12:55:01'),
(61, 0, '2021-02-26', '349', 12, 1, 'Need individual house in saravanampatti side', 1, '2021-02-26 13:01:08', '2021-02-26 13:01:08'),
(62, 0, '2021-02-26', '352', 12, 1, 'Not interested (Family is not satisfied with the site)', 1, '2021-02-26 13:01:47', '2021-02-26 13:01:47'),
(63, 0, '2021-02-26', '212', 12, 1, 'No answer', 1, '2021-02-26 14:46:47', '2021-02-26 14:46:47'),
(64, 0, '2021-02-26', '388', 12, 1, 'No network', 1, '2021-02-26 14:49:29', '2021-02-26 14:49:29'),
(65, 0, '2021-02-26', '388', 12, 1, 'No network', 1, '2021-02-26 14:51:10', '2021-02-26 14:51:10'),
(66, 0, '2021-02-26', '412', 12, 1, ' Budget is high. Budget-35 L\r\n', 1, '2021-02-26 14:57:01', '2021-02-26 14:57:01'),
(67, 0, '2021-02-26', '413', 12, 1, 'No answer', 1, '2021-02-26 14:59:12', '2021-02-26 14:59:12'),
(68, 0, '2021-02-26', '423', 12, 1, 'No answer', 1, '2021-02-26 15:00:44', '2021-02-26 15:00:44'),
(69, 0, '2021-02-26', '444', 12, 1, 'No answer', 1, '2021-02-26 15:07:41', '2021-02-26 15:07:41'),
(70, 0, '2021-02-26', '446', 12, 1, 'No answer', 1, '2021-02-26 15:10:44', '2021-02-26 15:10:44'),
(71, 0, '2021-02-26', '444', 12, 1, 'Call back later', 1, '2021-02-26 15:11:49', '2021-02-26 15:11:49'),
(72, 0, '2021-02-26', '462', 12, 1, 'Not interested', 1, '2021-02-26 15:15:49', '2021-02-26 15:15:49'),
(73, 0, '2021-02-26', '463', 12, 1, 'Not interested (Budet is high)\r\n', 1, '2021-02-26 15:20:34', '2021-02-26 15:20:34'),
(74, 0, '2021-02-26', '62', 12, 1, 'Busy', 1, '2021-02-26 15:27:27', '2021-02-26 15:27:27'),
(75, 0, '2021-02-26', '62', 12, 1, 'Waiting for the vaasthu clearance, will call back \r\n', 1, '2021-02-26 15:31:22', '2021-02-26 15:31:22'),
(76, 0, '2021-02-26', '476', 12, 1, 'Disconnected the call\r\n', 1, '2021-02-26 15:51:11', '2021-02-26 15:51:11'),
(77, 0, '2021-02-26', '480', 12, 1, 'Bought another property', 1, '2021-02-26 15:56:10', '2021-02-26 15:56:10'),
(78, 0, '2021-02-26', '488', 12, 1, 'Call back on 3/3/21', 1, '2021-02-26 16:01:08', '2021-02-26 16:01:08'),
(79, 0, '2021-02-26', '490', 12, 1, 'Not interested', 1, '2021-02-26 16:03:32', '2021-02-26 16:03:32'),
(80, 0, '2021-02-26', '493', 12, 1, 'Plan dropped', 1, '2021-02-26 16:05:35', '2021-02-26 16:05:35'),
(81, 0, '2021-02-26', '410', 12, 1, 'Plan postponed', 1, '2021-02-26 16:10:58', '2021-02-26 16:10:58'),
(82, 0, '2021-02-26', '516', 12, 1, 'satisfied with apatment villa\r\n', 1, '2021-02-26 16:14:55', '2021-02-26 16:14:55'),
(83, 0, '2021-02-26', '519', 12, 1, 'Call back later', 1, '2021-02-26 16:18:00', '2021-02-26 16:18:00'),
(84, 0, '2021-02-26', '374', 12, 1, 'No answer', 1, '2021-02-26 16:19:47', '2021-02-26 16:19:47'),
(85, 0, '2021-02-26', '359', 12, 1, 'Not interested ( Not satisfied with the location)\r\n', 1, '2021-02-26 16:25:51', '2021-02-26 16:25:51'),
(86, 0, '2021-02-26', '532', 12, 1, 'No answer\r\n', 1, '2021-02-26 16:27:26', '2021-02-26 16:27:26'),
(87, 0, '2021-02-26', '537', 12, 1, 'Bought another property\r\n', 1, '2021-02-26 16:30:51', '2021-02-26 16:30:51'),
(88, 0, '2021-02-26', '565', 12, 1, 'Need 4 cents, budget-45L, outside of the city also will do\r\n', 1, '2021-02-26 16:38:11', '2021-02-26 16:38:11'),
(89, 0, '2021-02-26', '577', 12, 1, 'Switched off', 1, '2021-02-26 16:55:05', '2021-02-26 16:55:05'),
(90, 0, '2021-02-26', '601', 12, 1, 'Bought another property\r\n', 1, '2021-02-26 16:57:18', '2021-02-26 16:57:18'),
(91, 0, '2021-02-26', '602', 12, 1, 'Plan dropped', 1, '2021-02-26 16:58:55', '2021-02-26 16:58:55'),
(92, 0, '2021-02-26', '219', 12, 1, 'Bought another property\r\n', 1, '2021-02-26 17:02:20', '2021-02-26 17:02:20'),
(93, 0, '2021-02-27', '3', 12, 1, 'Call back later', 1, '2021-02-27 11:28:58', '2021-02-27 11:28:58'),
(94, 0, '2021-02-27', '26', 12, 1, 'Need individual house in perur, budget-1 cr', 1, '2021-02-27 11:29:50', '2021-02-27 11:29:50'),
(95, 0, '2021-02-27', '59', 12, 1, 'Call on 3/3/21', 1, '2021-02-27 11:30:20', '2021-02-27 11:30:20'),
(96, 0, '2021-02-27', '88', 12, 1, 'Call back later', 1, '2021-02-27 11:32:06', '2021-02-27 11:32:06'),
(97, 0, '2021-02-27', '94', 12, 1, 'Call back later', 1, '2021-02-27 11:32:33', '2021-02-27 11:32:33'),
(98, 0, '2021-02-27', '114', 12, 1, 'Call back on 16/3/21', 1, '2021-02-27 11:33:14', '2021-02-27 11:33:14'),
(99, 0, '2021-02-27', '167', 12, 1, 'Plan dropped, call after 3 months( may month)', 1, '2021-02-27 11:35:31', '2021-02-27 11:35:31'),
(100, 0, '2021-02-27', '207', 12, 1, 'After 3 months(In april)', 1, '2021-02-27 11:36:13', '2021-02-27 11:36:13'),
(101, 0, '2021-02-27', '300', 12, 1, 'Probably after 1 0r 2 months later(April or may)', 1, '2021-02-27 11:38:01', '2021-02-27 11:38:01'),
(102, 0, '2021-02-27', '391', 12, 1, 'Call in march month', 1, '2021-02-27 11:38:53', '2021-02-27 11:38:53'),
(103, 0, '2021-02-27', '391', 12, 1, 'Call in march month', 1, '2021-02-27 11:39:40', '2021-02-27 11:39:40'),
(104, 0, '2021-02-27', '639', 12, 1, 'Not interested( Need RTO home)', 1, '2021-02-27 11:41:15', '2021-02-27 11:41:15'),
(105, 0, '2021-02-27', '662', 12, 1, 'Plan postponed because of medical expenses', 1, '2021-02-27 11:41:59', '2021-02-27 11:41:59'),
(106, 0, '2021-02-27', '125', 12, 1, 'Busy', 1, '2021-02-27 11:43:42', '2021-02-27 11:43:42'),
(107, 0, '2021-02-27', '147', 12, 1, 'Not reachable', 1, '2021-02-27 11:46:17', '2021-02-27 11:46:17'),
(108, 0, '2021-02-27', '208', 12, 1, 'Busy', 1, '2021-02-27 11:49:36', '2021-02-27 11:49:36'),
(109, 0, '2021-02-27', '232', 12, 1, 'Call back later', 1, '2021-02-27 11:53:39', '2021-02-27 11:53:39'),
(110, 0, '2021-02-27', '255', 12, 1, 'Not reachable', 1, '2021-02-27 11:54:57', '2021-02-27 11:54:57'),
(111, 0, '2021-02-27', '588', 12, 1, 'Call on 3/3/21', 1, '2021-02-27 11:56:05', '2021-02-27 11:56:05'),
(112, 0, '2021-02-27', '274', 12, 1, 'No answer', 1, '2021-02-27 11:57:45', '2021-02-27 11:57:45'),
(113, 0, '2021-02-27', '278', 12, 1, 'No answer', 1, '2021-02-27 12:01:44', '2021-02-27 12:01:44'),
(114, 0, '2021-02-27', '286', 12, 1, 'Call on 3/3/21', 1, '2021-02-27 12:02:32', '2021-02-27 12:02:32'),
(115, 0, '2021-02-27', '308', 12, 1, 'No answer', 1, '2021-02-27 12:05:22', '2021-02-27 12:05:22'),
(116, 0, '2021-02-27', '309', 12, 1, 'Call on 3/3/21', 1, '2021-02-27 12:06:27', '2021-02-27 12:06:27'),
(117, 0, '2021-02-27', '588', 12, 1, 'Call on 3/3/21', 1, '2021-02-27 12:22:47', '2021-02-27 12:22:47'),
(118, 0, '2021-02-27', '274', 12, 1, 'No answer', 1, '2021-02-27 12:23:24', '2021-02-27 12:23:24'),
(119, 0, '2021-02-27', '278', 12, 1, 'No answer', 1, '2021-02-27 12:24:23', '2021-02-27 12:24:23'),
(120, 0, '2021-02-27', '286', 12, 1, 'Call on 3/3/21', 1, '2021-02-27 12:24:55', '2021-02-27 12:24:55'),
(121, 0, '2021-02-27', '308', 12, 1, 'No answer', 1, '2021-02-27 12:25:58', '2021-02-27 12:25:58'),
(122, 0, '2021-02-27', '309', 12, 1, 'Call on 3/3/21', 1, '2021-02-27 12:26:51', '2021-02-27 12:26:51'),
(123, 0, '2021-02-27', '692', 12, 1, 'No answer', 1, '2021-02-27 12:43:11', '2021-02-27 12:43:11'),
(124, 0, '2021-02-27', '326', 12, 1, 'Once the construction is done he will come ', 1, '2021-02-27 12:43:40', '2021-02-27 12:43:40'),
(125, 0, '2021-02-27', '357', 12, 1, 'No answer', 1, '2021-02-27 12:45:55', '2021-02-27 12:45:55'),
(126, 0, '2021-02-27', '366', 12, 2, 'Need individual house, Budget-30 L', 1, '2021-02-27 12:50:05', '2021-02-27 12:50:05'),
(127, 0, '2021-02-27', '393', 12, 1, 'Switched off', 1, '2021-02-27 12:51:43', '2021-02-27 12:51:43'),
(128, 0, '2021-03-01', '653', 12, 1, 'Site visit done on 28/2/21. In discussion', 1, '2021-03-01 10:11:20', '2021-03-01 10:11:20'),
(129, 0, '2021-03-01', '389', 12, 1, 'No answer', 1, '2021-03-01 10:12:14', '2021-03-01 10:12:14'),
(130, 0, '2021-03-01', '427', 12, 1, 'He may visit on 6th or 7th', 1, '2021-03-01 10:13:54', '2021-03-01 10:13:54'),
(131, 0, '2021-03-01', '590', 12, 1, 'No answer', 1, '2021-03-01 10:14:25', '2021-03-01 10:14:25'),
(132, 0, '2021-03-01', '624', 12, 1, 'No answer', 1, '2021-03-01 10:15:06', '2021-03-01 10:15:06'),
(133, 0, '2021-03-01', '582', 12, 1, 'Will visit once he gets well', 1, '2021-03-01 10:15:48', '2021-03-01 10:15:48'),
(134, 0, '2021-03-01', '102', 12, 1, 'Will visit on 6th or 7th march', 1, '2021-03-01 10:16:47', '2021-03-01 10:16:47'),
(135, 0, '2021-03-01', '653', 12, 1, 'Site visit done on 28/2/21. In discussion', 1, '2021-03-01 10:17:52', '2021-03-01 10:17:52'),
(136, 0, '2021-03-01', '557', 12, 1, 'Site visit done on 28/2/21. In discussion', 1, '2021-03-01 10:18:51', '2021-03-01 10:18:51'),
(137, 0, '2021-03-01', '693', 12, 1, 'No answer', 1, '2021-03-01 10:20:30', '2021-03-01 10:20:30'),
(138, 0, '2021-03-01', '3', 12, 1, ' No answer', 1, '2021-03-01 12:41:06', '2021-03-01 12:41:06'),
(139, 0, '2021-03-01', '88', 12, 1, 'No answer', 1, '2021-03-01 12:45:05', '2021-03-01 12:45:05'),
(140, 0, '2021-03-01', '94', 12, 1, 'No answer', 1, '2021-03-01 12:48:39', '2021-03-01 12:48:39'),
(141, 0, '2021-03-01', '147', 12, 1, 'No answer', 1, '2021-03-01 12:53:12', '2021-03-01 12:53:12'),
(142, 0, '2021-03-01', '208', 12, 1, 'No answer', 1, '2021-03-01 12:54:48', '2021-03-01 12:54:48'),
(144, 0, '2021-03-01', '232', 12, 1, 'santhosh-Twin & duplex villa details sent thr whatsapp, call back on 3/3/21\r\n', 1, '2021-03-01 13:08:43', '2021-03-01 13:11:33'),
(145, 0, '2021-03-01', '255', 12, 3, 'Bought another property', 1, '2021-03-01 13:15:27', '2021-03-01 13:15:27'),
(146, 0, '2021-03-01', '274', 12, 1, 'No answer', 1, '2021-03-01 13:17:06', '2021-03-01 13:17:06'),
(147, 0, '2021-03-01', '278', 12, 1, 'Busy', 1, '2021-03-01 13:20:27', '2021-03-01 13:20:27'),
(148, 0, '2021-03-01', '125', 12, 1, 'Bought another property', 1, '2021-03-01 13:22:45', '2021-03-01 13:22:45'),
(149, 0, '2021-03-01', '308', 12, 1, 'No answer', 1, '2021-03-01 13:24:30', '2021-03-01 13:24:30'),
(150, 0, '2021-03-01', '692', 12, 1, 'No answer', 1, '2021-03-01 13:27:20', '2021-03-01 13:27:20'),
(151, 0, '2021-03-01', '357', 12, 1, 'Busy', 1, '2021-03-01 13:31:29', '2021-03-01 13:31:29'),
(152, 0, '2021-03-01', '393', 12, 1, 'Switched off', 1, '2021-03-01 13:33:46', '2021-03-01 13:33:46'),
(153, 0, '2021-03-01', '403', 12, 1, 'Not reachable', 1, '2021-03-01 13:37:36', '2021-03-01 13:37:36'),
(154, 0, '2021-03-01', '411', 12, 1, 'Call back @ 4.15', 1, '2021-03-01 15:39:59', '2021-03-01 15:39:59'),
(155, 0, '2021-03-01', '415', 12, 1, 'Call back in the evening', 1, '2021-03-01 15:40:31', '2021-03-01 15:40:31'),
(156, 0, '2021-03-01', '417', 12, 1, 'No answer', 1, '2021-03-01 15:40:54', '2021-03-01 15:40:54'),
(157, 0, '2021-03-01', '371', 12, 1, 'If interested he will call back', 1, '2021-03-01 15:45:03', '2021-03-01 15:45:03'),
(158, 0, '2021-03-01', '443', 12, 1, 'Busy', 1, '2021-03-01 15:47:30', '2021-03-01 15:47:30'),
(159, 0, '2021-03-01', '465', 12, 1, 'Call back later', 1, '2021-03-01 15:49:52', '2021-03-01 15:49:52'),
(160, 0, '2021-03-01', '470', 12, 1, 'Not reachable', 1, '2021-03-01 15:51:56', '2021-03-01 15:51:56'),
(161, 0, '2021-03-01', '478', 12, 1, 'Call back on 3/3/21', 1, '2021-03-01 15:52:38', '2021-03-01 15:52:38'),
(162, 0, '2021-03-01', '481', 12, 1, 'Call back on 3/3/21', 1, '2021-03-01 15:55:12', '2021-03-01 15:55:12'),
(163, 0, '2021-03-01', '509', 12, 1, 'Busy', 1, '2021-03-01 16:01:15', '2021-03-01 16:01:15'),
(164, 0, '2021-03-01', '512', 12, 1, 'Call back on 2/3/21', 1, '2021-03-01 16:03:04', '2021-03-01 16:03:04'),
(165, 0, '2021-03-01', '515', 12, 1, 'Busy', 1, '2021-03-01 16:05:20', '2021-03-01 16:05:20'),
(166, 0, '2021-03-01', '422', 12, 1, 'Bought another property', 1, '2021-03-01 16:07:14', '2021-03-01 16:07:14'),
(167, 0, '2021-03-01', '455', 12, 1, 'Call back @ 6', 1, '2021-03-01 16:09:47', '2021-03-01 16:09:47'),
(168, 0, '2021-03-01', '531', 12, 2, 'Not reachable', 1, '2021-03-01 16:10:51', '2021-03-01 16:10:51'),
(169, 0, '2021-03-01', '541', 12, 1, 'No answer', 1, '2021-03-01 16:12:29', '2021-03-01 16:12:29'),
(170, 0, '2021-03-01', '399', 12, 1, 'Call back later', 1, '2021-03-01 16:14:42', '2021-03-01 16:14:42'),
(171, 0, '2021-03-01', '542', 12, 1, 'Not reachable', 1, '2021-03-01 16:36:46', '2021-03-01 16:36:46'),
(172, 0, '2021-03-01', '555', 12, 1, 'Call back on 5/3/21 for site visit', 1, '2021-03-01 16:46:11', '2021-03-01 16:46:11'),
(173, 0, '2021-03-01', '561', 12, 1, 'Call back on 3/3/21', 1, '2021-03-01 17:10:16', '2021-03-01 17:10:16'),
(174, 0, '2021-03-01', '572', 12, 1, 'Not interested', 1, '2021-03-01 17:16:29', '2021-03-01 17:16:29'),
(175, 0, '2021-03-01', '695', 12, 1, 'Call back later', 1, '2021-03-01 17:38:52', '2021-03-01 17:38:52'),
(176, 0, '2021-03-08', '579', 12, 1, 'No answer', 1, '2021-03-01 17:46:18', '2021-03-01 17:46:18'),
(177, 0, '2021-03-01', '579', 12, 1, 'He will call back', 1, '2021-03-01 17:48:39', '2021-03-01 17:48:39'),
(178, 0, '2021-03-01', '590', 12, 1, 'Call back on 20/3/21', 1, '2021-03-01 18:01:35', '2021-03-01 18:01:35'),
(179, 0, '2021-03-01', '598', 12, 1, 'Busy\r\n', 1, '2021-03-01 18:09:04', '2021-03-01 18:09:04'),
(180, 0, '2021-03-01', '598', 12, 1, 'Call back on 20/3/21', 1, '2021-03-01 18:11:14', '2021-03-01 18:11:14'),
(181, 0, '2021-03-01', '603', 12, 1, 'No answer', 1, '2021-03-01 18:36:24', '2021-03-01 18:36:24'),
(182, 0, '2021-03-01', '603', 12, 1, 'Not interested (Disconnected the call)', 1, '2021-03-01 18:39:39', '2021-03-01 18:39:39'),
(183, 0, '2021-03-01', '350', 12, 1, 'Call back on 3/3/21', 1, '2021-03-01 18:41:33', '2021-03-01 18:41:33'),
(184, 0, '2021-03-04', '690', 12, 1, 'Out of station', 1, '2021-03-04 11:51:31', '2021-03-04 11:51:31'),
(185, 0, '2021-03-04', '3', 12, 1, 'No answer', 1, '2021-03-04 11:52:34', '2021-03-04 11:52:34'),
(186, 0, '2021-03-04', '59', 12, 1, 'Bought another property', 1, '2021-03-04 11:53:20', '2021-03-04 11:53:20'),
(187, 0, '2021-03-04', '691', 12, 1, 'Waiting for the payment, will visit once the payment is received', 1, '2021-03-04 11:53:46', '2021-03-04 11:53:46'),
(188, 0, '2021-03-04', '88', 12, 1, 'He will call back', 1, '2021-03-04 11:54:23', '2021-03-04 11:54:23'),
(189, 0, '2021-03-04', '94', 12, 1, 'Will visit on 7/3/21', 1, '2021-03-04 11:54:51', '2021-03-04 11:54:51'),
(190, 0, '2021-03-04', '147', 12, 1, 'No answer', 1, '2021-03-04 11:55:21', '2021-03-04 11:55:21'),
(191, 0, '2021-03-04', '208', 12, 1, 'Busy', 1, '2021-03-04 11:55:47', '2021-03-04 11:55:47'),
(192, 0, '2021-03-04', '232', 12, 1, 'No answer', 1, '2021-03-04 11:59:05', '2021-03-04 11:59:05'),
(193, 0, '2021-03-04', '260', 12, 1, 'Will visit site on 7th or 14th', 1, '2021-03-04 12:45:06', '2021-03-04 12:45:06'),
(194, 0, '2021-03-04', '588', 12, 1, 'No answer', 1, '2021-03-04 12:52:26', '2021-03-04 12:52:26'),
(195, 0, '2021-03-04', '274', 12, 1, 'Plan postponed because of marriage, call back on 10/4/21', 1, '2021-03-04 12:58:40', '2021-03-04 12:58:40'),
(196, 0, '2021-03-04', '620', 12, 1, 'Out of station', 1, '2021-03-04 14:59:52', '2021-03-04 14:59:52'),
(197, 0, '2021-03-04', '624', 12, 1, 'No answer', 1, '2021-03-04 15:00:16', '2021-03-04 15:00:16'),
(198, 0, '2021-03-04', '625', 12, 1, 'No answer', 1, '2021-03-04 15:00:58', '2021-03-04 15:00:58'),
(199, 0, '2021-03-04', '631', 12, 1, 'Call back later', 1, '2021-03-04 15:03:19', '2021-03-04 15:03:19'),
(200, 0, '2021-03-04', '637', 12, 1, 'Not yet decided,If interested he will call back', 1, '2021-03-04 15:09:15', '2021-03-04 15:09:15'),
(201, 0, '2021-03-04', '640', 12, 1, 'Busy', 1, '2021-03-04 15:10:50', '2021-03-04 15:10:50'),
(202, 0, '2021-03-04', '641', 12, 1, 'No answer', 1, '2021-03-04 15:13:11', '2021-03-04 15:13:11'),
(203, 0, '2021-03-04', '642', 12, 1, 'Disconnected the call\r\n', 1, '2021-03-04 15:36:28', '2021-03-04 15:36:28'),
(204, 0, '2021-03-04', '646', 12, 1, 'No answer', 1, '2021-03-04 15:38:40', '2021-03-04 15:38:40'),
(205, 0, '2021-03-04', '651', 12, 1, 'Will call back after 2 days', 1, '2021-03-04 15:46:53', '2021-03-04 15:46:53'),
(206, 0, '2021-03-04', '652', 12, 1, 'Wrong number', 1, '2021-03-04 16:13:43', '2021-03-04 16:13:43'),
(207, 0, '2021-03-04', '655', 12, 1, 'Site visit done on 4/3/21', 1, '2021-03-04 16:26:00', '2021-03-04 16:26:00'),
(208, 0, '2021-03-05', '661', 12, 1, 'If interested he will call back', 1, '2021-03-05 12:28:00', '2021-03-05 12:28:00'),
(209, 0, '2021-03-05', '657', 12, 1, 'Switched off', 1, '2021-03-05 12:28:49', '2021-03-05 12:28:49'),
(210, 0, '2021-03-05', '688', 12, 1, 'Busy', 1, '2021-03-05 12:29:12', '2021-03-05 12:29:12'),
(211, 0, '2021-03-05', '658', 12, 1, 'Busy', 1, '2021-03-05 12:29:34', '2021-03-05 12:29:34'),
(212, 0, '2021-03-05', '660', 12, 1, 'No answer', 1, '2021-03-05 12:30:09', '2021-03-05 12:30:09'),
(213, 0, '2021-03-05', '664', 12, 1, 'No answer', 1, '2021-03-05 12:33:14', '2021-03-05 12:33:14'),
(214, 0, '2021-03-05', '666', 12, 1, 'No answer', 1, '2021-03-05 12:49:19', '2021-03-05 12:49:19'),
(215, 0, '2021-03-05', '666', 12, 1, 'Not interested', 1, '2021-03-05 12:50:53', '2021-03-05 12:50:53'),
(216, 0, '2021-03-05', '671', 12, 1, 'Details shared through whatsapp, call back on 8/3/21\r\n', 1, '2021-03-05 13:03:55', '2021-03-05 13:03:55'),
(217, 0, '2021-03-05', '672', 12, 1, 'Switched off', 1, '2021-03-05 14:51:43', '2021-03-05 14:51:43'),
(218, 0, '2021-03-05', '673', 12, 1, 'He will call back', 1, '2021-03-05 14:54:17', '2021-03-05 14:54:17'),
(219, 0, '2021-03-05', '675', 12, 1, 'In chennai, call back on 22/3/21', 1, '2021-03-05 14:58:27', '2021-03-05 14:58:27'),
(220, 0, '2021-03-05', '679', 12, 1, 'Call back later', 1, '2021-03-05 15:00:03', '2021-03-05 15:00:03'),
(221, 0, '2021-03-05', '683', 12, 1, 'Need RTO homes in saravanampatti, thudiyalur side, budget-40L to 50L', 1, '2021-03-05 15:11:10', '2021-03-05 15:11:10'),
(222, 0, '2021-03-05', '684', 12, 1, 'No answer', 1, '2021-03-05 15:12:30', '2021-03-05 15:12:30'),
(223, 0, '2021-03-05', '685', 12, 1, 'No answer', 1, '2021-03-05 15:14:43', '2021-03-05 15:14:43'),
(224, 0, '2021-03-05', '689', 12, 1, 'Call back on 10/3/21', 1, '2021-03-05 15:17:03', '2021-03-05 15:17:03'),
(225, 0, '2021-03-05', '350', 12, 1, 'Busy', 1, '2021-03-05 15:26:23', '2021-03-05 15:26:23'),
(226, 0, '2021-03-05', '684', 12, 1, 'Call back on 8/3/21', 1, '2021-03-05 15:28:07', '2021-03-05 15:28:07'),
(227, 0, '2021-03-05', '696', 12, 1, 'No answer', 1, '2021-03-05 16:31:27', '2021-03-05 16:31:27'),
(228, 0, '2021-03-05', '598', 12, 1, 'Call back on 20/3/21', 1, '2021-03-05 16:33:32', '2021-03-05 16:33:32'),
(229, 0, '2021-03-05', '590', 12, 1, 'No answer', 1, '2021-03-05 16:34:51', '2021-03-05 16:34:51'),
(230, 0, '2021-03-05', '586', 12, 1, 'Will call back later', 1, '2021-03-05 16:36:31', '2021-03-05 16:36:31'),
(231, 0, '2021-03-05', '579', 12, 1, 'He will call back', 1, '2021-03-05 16:40:04', '2021-03-05 16:40:04'),
(232, 0, '2021-03-05', '561', 12, 1, 'Busy', 1, '2021-03-05 16:50:29', '2021-03-05 16:50:29'),
(233, 0, '2021-03-05', '555', 12, 1, 'Site visit on 6/3/21', 1, '2021-03-05 17:02:22', '2021-03-05 17:02:22'),
(234, 0, '2021-03-09', '88', 12, 1, 'Call back later', 1, '2021-03-09 10:55:28', '2021-03-09 10:55:28'),
(235, 0, '2021-03-09', '94', 12, 1, 'No answer', 1, '2021-03-09 10:57:03', '2021-03-09 10:57:03'),
(236, 0, '2021-03-09', '147', 12, 1, 'Call back on 10/3/21', 1, '2021-03-09 10:59:25', '2021-03-09 10:59:25'),
(237, 0, '2021-03-09', '208', 12, 1, 'Busy', 1, '2021-03-09 11:00:54', '2021-03-09 11:00:54'),
(238, 0, '2021-03-09', '212', 12, 1, 'In chennai, will return to cbe in 10 days, call back on 22/3/21', 1, '2021-03-09 11:03:35', '2021-03-09 11:03:35'),
(239, 0, '2021-03-09', '232', 12, 1, 'No answer', 1, '2021-03-09 11:05:36', '2021-03-09 11:05:36'),
(240, 0, '2021-03-09', '588', 12, 1, 'No answer', 1, '2021-03-09 11:07:45', '2021-03-09 11:07:45'),
(241, 0, '2021-03-09', '278', 12, 1, 'No answer', 1, '2021-03-09 11:10:16', '2021-03-09 11:10:16'),
(242, 0, '2021-03-09', '286', 12, 1, 'Call back on 15/3/21', 1, '2021-03-09 11:12:21', '2021-03-09 11:12:21'),
(243, 0, '2021-03-09', '308', 12, 1, 'No answer', 1, '2021-03-09 11:14:14', '2021-03-09 11:14:14'),
(244, 0, '2021-03-09', '692', 12, 1, 'He will call back later', 1, '2021-03-09 11:17:04', '2021-03-09 11:17:04'),
(245, 0, '2021-03-09', '357', 12, 1, 'Details sent thr whatsapp, plan x', 1, '2021-03-09 11:24:14', '2021-03-09 11:24:14'),
(246, 0, '2021-03-09', '391', 12, 1, 'Call back on 10/3/21', 1, '2021-03-09 11:24:56', '2021-03-09 11:24:56'),
(247, 0, '2021-03-09', '403', 12, 1, 'Looking in kancheepuram side', 1, '2021-03-09 11:28:43', '2021-03-09 11:28:43'),
(248, 0, '2021-03-09', '417', 12, 1, 'Call back later', 1, '2021-03-09 12:11:53', '2021-03-09 12:11:53'),
(249, 0, '2021-03-09', '427', 12, 1, 'Will visit on 13th or 14th of march, call and remind him on 13th', 1, '2021-03-09 12:18:52', '2021-03-09 12:18:52'),
(250, 0, '2021-03-09', '443', 12, 1, 'Call back later', 1, '2021-03-09 12:21:47', '2021-03-09 12:21:47'),
(251, 0, '2021-03-09', '465', 12, 1, 'Will visit on 13th or 14th, budget-45L', 1, '2021-03-09 12:26:41', '2021-03-09 12:26:41'),
(252, 0, '2021-03-09', '470', 12, 1, 'No answer', 1, '2021-03-09 12:32:22', '2021-03-09 12:32:22'),
(253, 0, '2021-03-09', '478', 12, 1, 'Brouchure sent thr whatsapp, call back on 15/3/21', 1, '2021-03-09 12:36:56', '2021-03-09 12:36:56'),
(254, 0, '2021-03-09', '481', 12, 1, 'Not interested (Disconnected the call)', 1, '2021-03-09 12:39:38', '2021-03-09 12:39:38'),
(255, 0, '2021-03-09', '509', 12, 1, 'No answer', 1, '2021-03-09 12:42:22', '2021-03-09 12:42:22'),
(256, 0, '2021-03-09', '512', 12, 1, 'No answer', 1, '2021-03-09 12:44:05', '2021-03-09 12:44:05'),
(257, 0, '2021-03-09', '455', 12, 1, 'Switched off', 1, '2021-03-09 12:54:39', '2021-03-09 12:54:39'),
(258, 0, '2021-03-09', '531', 12, 1, 'Not reachable', 1, '2021-03-09 12:58:14', '2021-03-09 12:58:14'),
(259, 0, '2021-03-09', '541', 12, 1, 'No answer', 1, '2021-03-09 13:04:01', '2021-03-09 13:04:01'),
(260, 0, '2021-03-09', '399', 12, 1, 'No answer', 1, '2021-03-09 13:13:55', '2021-03-09 13:13:55'),
(261, 0, '2021-03-09', '542', 12, 1, 'No answer', 1, '2021-03-09 13:15:28', '2021-03-09 13:15:28'),
(262, 0, '2021-03-09', '561', 12, 1, 'Not reachable', 1, '2021-03-09 13:17:26', '2021-03-09 13:17:26'),
(263, 0, '2021-03-09', '579', 12, 1, 'Should share TWV, DV pics, if interested he will call back, was searching house in madhampatti', 1, '2021-03-09 13:25:25', '2021-03-09 13:25:25'),
(264, 0, '2021-03-09', '585', 12, 1, 'In Erode, call back on 10/3/21', 1, '2021-03-09 15:13:01', '2021-03-09 15:13:01'),
(265, 0, '2021-03-09', '586', 12, 1, 'No answer', 1, '2021-03-09 15:14:24', '2021-03-09 15:14:24'),
(266, 0, '2021-03-09', '696', 12, 1, 'Not seen the details yet, will call back later', 1, '2021-03-09 15:17:08', '2021-03-09 15:17:08'),
(267, 0, '2021-03-09', '350', 12, 1, 'Busy', 1, '2021-03-09 15:18:05', '2021-03-09 15:18:05'),
(268, 0, '2021-03-09', '610', 12, 1, 'No answer', 1, '2021-03-09 15:19:58', '2021-03-09 15:19:58'),
(269, 0, '2021-03-09', '615', 12, 1, 'Call back on 17/3/21', 1, '2021-03-09 15:22:24', '2021-03-09 15:22:24'),
(270, 0, '2021-03-09', '620', 12, 1, 'Not reachable', 1, '2021-03-09 15:24:31', '2021-03-09 15:24:31'),
(271, 0, '2021-03-09', '624', 12, 1, 'Bought another property', 1, '2021-03-09 15:32:32', '2021-03-09 15:32:32'),
(272, 0, '2021-03-09', '625', 12, 1, 'Plan dropped, not interested', 1, '2021-03-09 15:35:33', '2021-03-09 15:35:33'),
(273, 0, '2021-03-09', '631', 12, 1, 'Not interested in Podanur, Budget-50L', 1, '2021-03-09 15:42:03', '2021-03-09 15:42:03'),
(274, 0, '2021-03-09', '633', 12, 1, 'Plan dropped, not interested', 1, '2021-03-09 15:50:10', '2021-03-09 15:50:10'),
(275, 0, '2021-03-09', '641', 12, 1, 'Busy', 1, '2021-03-09 16:06:36', '2021-03-09 16:06:36'),
(276, 0, '2021-03-09', '650', 12, 1, 'No answer', 1, '2021-03-09 16:37:23', '2021-03-09 16:37:23'),
(277, 0, '2021-03-09', '651', 12, 1, 'No answer', 1, '2021-03-09 16:39:19', '2021-03-09 16:39:19'),
(278, 0, '2021-03-09', '688', 12, 1, 'Will visit on 14/3/21', 1, '2021-03-09 16:41:32', '2021-03-09 16:41:32'),
(279, 0, '2021-03-09', '658', 12, 1, 'Busy', 1, '2021-03-09 17:27:47', '2021-03-09 17:27:47'),
(280, 0, '2021-03-09', '660', 12, 1, 'Switched off', 1, '2021-03-09 17:29:34', '2021-03-09 17:29:34'),
(281, 0, '2021-03-09', '664', 12, 1, 'Switched off', 1, '2021-03-09 17:31:22', '2021-03-09 17:31:22'),
(282, 0, '2021-03-09', '671', 12, 1, 'Plan dropped, not interested', 1, '2021-03-09 17:35:18', '2021-03-09 17:35:18'),
(283, 0, '2021-03-09', '672', 12, 1, 'No answer', 1, '2021-03-09 17:36:59', '2021-03-09 17:36:59'),
(284, 0, '2021-03-09', '673', 12, 1, 'No answer', 1, '2021-03-09 17:40:26', '2021-03-09 17:40:26'),
(285, 0, '2021-03-09', '679', 12, 1, 'Not reachable', 1, '2021-03-09 17:41:53', '2021-03-09 17:41:53'),
(286, 0, '2021-03-09', '651', 12, 1, 'Site visit done on 4/3/21, She is not satisfied with the villa', 1, '2021-03-09 18:34:26', '2021-03-09 18:37:48'),
(287, 0, '2021-03-10', '684', 12, 1, 'Call back @ 4.30', 1, '2021-03-10 12:03:36', '2021-03-10 12:03:36'),
(288, 0, '2021-03-10', '685', 12, 1, 'No answer', 1, '2021-03-10 12:06:01', '2021-03-10 12:06:01'),
(289, 0, '2021-03-10', '689', 12, 1, 'No answer', 1, '2021-03-10 12:07:28', '2021-03-10 12:07:28'),
(290, 0, '2021-03-10', '721', 12, 1, 'No answer', 1, '2021-03-10 12:19:07', '2021-03-10 12:19:07'),
(291, 0, '2021-03-10', '724', 12, 1, 'Switched off', 1, '2021-03-10 12:19:59', '2021-03-10 12:19:59'),
(292, 0, '2021-03-10', '726', 12, 1, 'Bought another property', 1, '2021-03-10 12:21:46', '2021-03-10 12:21:46'),
(293, 0, '2021-03-10', '730', 12, 1, 'Call back @ 2.30(Bala)', 1, '2021-03-10 13:02:22', '2021-03-10 13:02:22'),
(294, 0, '2021-03-10', '492', 12, 1, 'Disconnected the call, not interested', 1, '2021-03-10 13:10:35', '2021-03-10 13:10:35'),
(295, 0, '2021-03-10', '732', 12, 1, 'No answer', 1, '2021-03-10 13:12:57', '2021-03-10 13:12:57'),
(296, 0, '2021-03-10', '741', 12, 1, 'Clicked the ad by mistake', 1, '2021-03-10 13:15:40', '2021-03-10 13:15:40'),
(297, 0, '2021-03-10', '742', 12, 1, 'wrong number', 1, '2021-03-10 13:19:32', '2021-03-10 13:19:32'),
(298, 0, '2021-03-10', '201', 12, 1, ' No answer', 1, '2021-03-10 13:22:54', '2021-03-10 13:22:54'),
(299, 0, '2021-03-10', '655', 12, 1, 'Interested in duplex villa, his father will confirm it after visiting the site', 1, '2021-03-10 15:05:50', '2021-03-10 15:05:50'),
(300, 0, '2021-03-10', '744', 12, 1, 'Switched off', 1, '2021-03-10 15:19:22', '2021-03-10 15:19:22'),
(301, 0, '2021-03-10', '745', 12, 1, 'Will visit on 13th or 14th ', 1, '2021-03-10 15:25:20', '2021-03-10 15:25:20'),
(302, 0, '2021-03-10', '746', 12, 1, 'No answer', 1, '2021-03-10 15:27:45', '2021-03-10 15:27:45'),
(303, 0, '2021-03-10', '747', 12, 1, 'No answer', 1, '2021-03-10 15:29:39', '2021-03-10 15:29:39'),
(304, 0, '2021-03-10', '748', 12, 1, 'Call barred', 1, '2021-03-10 15:49:47', '2021-03-10 15:49:47'),
(305, 0, '2021-03-10', '754', 12, 1, 'Call back later', 1, '2021-03-10 15:54:59', '2021-03-10 15:54:59'),
(306, 0, '2021-03-10', '746', 12, 1, 'If interested he will call back', 1, '2021-03-10 16:36:48', '2021-03-10 16:36:48'),
(307, 0, '2021-03-11', '94', 12, 1, 'Call back on 13/3/21 for site visit', 1, '2021-03-11 10:44:31', '2021-03-11 10:44:31'),
(308, 0, '2021-03-11', '147', 12, 1, 'He will call back if interested', 1, '2021-03-11 10:45:05', '2021-03-11 10:45:05'),
(309, 0, '2021-03-11', '208', 12, 1, 'No answer', 1, '2021-03-11 10:45:29', '2021-03-11 10:45:29'),
(310, 0, '2021-03-11', '232', 12, 1, 'No answer', 1, '2021-03-11 10:47:18', '2021-03-11 10:47:18'),
(311, 0, '2021-03-11', '588', 12, 1, 'Busy', 1, '2021-03-11 10:48:26', '2021-03-11 10:48:26'),
(312, 0, '2021-03-11', '308', 12, 1, 'No answer', 1, '2021-03-11 10:50:10', '2021-03-11 10:50:10'),
(313, 0, '2021-03-11', '389', 12, 1, 'Busy', 1, '2021-03-11 11:21:20', '2021-03-11 11:21:20'),
(314, 0, '2021-03-11', '391', 12, 1, 'No answer', 1, '2021-03-11 11:24:30', '2021-03-11 11:24:30'),
(315, 0, '2021-03-11', '470', 12, 1, 'No answer', 1, '2021-03-11 11:29:03', '2021-03-11 11:29:03'),
(316, 0, '2021-03-11', '509', 12, 1, 'Busy', 1, '2021-03-11 11:30:18', '2021-03-11 11:30:18'),
(317, 0, '2021-03-11', '512', 12, 1, 'Not interested', 1, '2021-03-11 11:33:37', '2021-03-11 11:33:37'),
(318, 0, '2021-03-11', '455', 12, 1, 'Busy', 1, '2021-03-11 11:34:47', '2021-03-11 11:34:47'),
(319, 0, '2021-03-11', '541', 12, 1, 'No answer', 1, '2021-03-11 11:36:10', '2021-03-11 11:36:10'),
(320, 0, '2021-03-11', '399', 12, 1, 'Details sent thr whatsapp, call back later', 1, '2021-03-11 11:43:16', '2021-03-11 11:43:16'),
(321, 0, '2021-03-11', '542', 12, 1, 'No answer', 1, '2021-03-11 11:45:27', '2021-03-11 11:45:27'),
(322, 0, '2021-03-11', '561', 12, 1, 'No answer', 1, '2021-03-11 11:47:56', '2021-03-11 11:47:56'),
(323, 0, '2021-03-11', '585', 12, 1, 'No answer', 1, '2021-03-11 12:09:04', '2021-03-11 12:09:04'),
(324, 0, '2021-03-11', '586', 12, 1, 'Call back later', 1, '2021-03-11 12:09:31', '2021-03-11 12:09:31'),
(325, 0, '2021-03-11', '350', 12, 1, 'Busy', 1, '2021-03-11 12:10:41', '2021-03-11 12:10:41'),
(326, 0, '2021-03-11', '610', 12, 1, 'No answer', 1, '2021-03-11 12:13:15', '2021-03-11 12:13:15'),
(327, 0, '2021-03-11', '620', 12, 1, 'Not interested', 1, '2021-03-11 12:15:44', '2021-03-11 12:15:44'),
(328, 0, '2021-03-11', '641', 12, 1, 'No answer\r\n', 1, '2021-03-11 12:19:16', '2021-03-11 12:19:16'),
(329, 0, '2021-03-11', '650', 12, 1, 'No answer\r\n', 1, '2021-03-11 12:43:21', '2021-03-11 12:43:21'),
(330, 0, '2021-03-11', '658', 12, 1, 'No answer', 1, '2021-03-11 13:18:28', '2021-03-11 13:18:28'),
(331, 0, '2021-03-11', '672', 12, 1, 'No answer', 1, '2021-03-11 13:19:51', '2021-03-11 13:19:51'),
(332, 0, '2021-03-11', '673', 12, 1, 'No answer', 1, '2021-03-11 13:41:42', '2021-03-11 13:41:42'),
(333, 0, '2021-03-11', '679', 12, 1, 'Not reachable', 1, '2021-03-11 13:44:05', '2021-03-11 13:44:05'),
(334, 0, '2021-03-11', '685', 12, 1, 'No answer', 1, '2021-03-11 15:40:42', '2021-03-11 15:40:42'),
(335, 0, '2021-03-11', '689', 12, 1, 'Call back on 12/3/21', 1, '2021-03-11 15:42:20', '2021-03-11 15:42:20'),
(336, 0, '2021-03-11', '721', 12, 1, 'Busy', 1, '2021-03-11 15:43:27', '2021-03-11 15:43:27'),
(337, 0, '2021-03-11', '721', 12, 1, 'Busy', 1, '2021-03-11 15:44:31', '2021-03-11 15:44:31'),
(338, 0, '2021-03-11', '732', 12, 1, 'No answer', 1, '2021-03-11 15:46:46', '2021-03-11 15:46:46'),
(339, 0, '2021-03-17', '778', 12, 1, 'Site visit done on 16/3/21, will visit again on 18/3/21 to do legal work\r\n', 1, '2021-03-17 11:28:45', '2021-03-17 11:28:45'),
(340, 0, '2021-03-22', '427', 12, 1, 'Site visit done on 21/3/21. He says that the room sizes are small. Will call on 22/3/21 .', 1, '2021-03-22 10:37:40', '2021-03-22 10:37:40'),
(341, 0, '2021-03-23', '809', 12, 1, 'Discussion going on', 1, '2021-03-23 10:31:03', '2021-03-23 11:49:08'),
(342, 0, '2021-03-23', '427', 12, 1, 'discussion going on', 1, '2021-03-23 10:32:33', '2021-03-23 11:49:36'),
(343, 0, '2021-03-23', '624', 12, 1, 'Interested in twin villa he need two days time to take his decision', 1, '2021-03-24 10:17:42', '2021-03-24 10:17:42'),
(344, 0, '2021-03-25', '810', 12, 1, 'again coming site visit on Sunday.', 1, '2021-03-25 11:48:21', '2021-03-25 11:48:21'),
(345, 0, '2021-03-25', '817', 12, 1, 'he said that he will discuss and come back.', 1, '2021-03-25 11:51:29', '2021-03-25 11:51:29'),
(346, 0, '2021-03-25', '801', 12, 1, 'interested in Twin villa.', 1, '2021-03-25 11:52:06', '2021-03-25 11:52:06'),
(347, 0, '2021-03-31', '201', 12, 1, 'He is in madurai now, he will be back on 5/4/21. Call him on 5/4/21', 1, '2021-03-31 11:22:21', '2021-03-31 11:22:21'),
(348, 0, '2021-03-31', '744', 12, 1, 'Switched off', 1, '2021-03-31 11:27:57', '2021-03-31 11:27:57'),
(349, 0, '2021-03-31', '745', 12, 1, 'He may visit on 4/4/21', 1, '2021-03-31 11:34:46', '2021-03-31 11:34:46'),
(350, 0, '2021-03-31', '747', 12, 1, 'Call back in the evening\r\n', 1, '2021-03-31 11:37:41', '2021-03-31 11:37:41'),
(351, 0, '2021-03-31', '748', 12, 1, 'Details sent through whatsapp, call back on 2/4/21', 1, '2021-03-31 11:46:09', '2021-03-31 11:49:14'),
(352, 0, '2021-03-31', '755', 12, 1, 'No answer', 1, '2021-03-31 11:57:02', '2021-03-31 11:57:02'),
(353, 0, '2021-03-31', '759', 12, 1, 'Call back on 15/4/21', 1, '2021-03-31 12:01:02', '2021-03-31 12:01:02'),
(354, 0, '2021-03-31', '760', 12, 1, 'Looking for another RTO house', 1, '2021-03-31 12:04:40', '2021-03-31 12:04:40'),
(355, 0, '2021-03-31', '762', 12, 1, 'He needs minimum of 6 cents,  his budget is 85 L - 1 cr, should call back on 3/3/21 for site visit', 1, '2021-03-31 12:18:00', '2021-03-31 12:18:00'),
(356, 0, '2021-03-31', '755', 12, 1, 'Plan dropped', 1, '2021-03-31 12:28:55', '2021-03-31 12:28:55'),
(357, 0, '2021-03-31', '763', 12, 1, 'Location sent through whatsapp, will visit once she is free', 1, '2021-03-31 12:36:11', '2021-03-31 12:36:11'),
(358, 0, '2021-04-01', '764', 12, 1, 'Not reachable\r\n', 1, '2021-04-01 14:00:51', '2021-04-01 14:00:51'),
(359, 0, '2021-04-01', '765', 12, 1, 'No answer', 1, '2021-04-01 14:02:26', '2021-04-01 14:02:26'),
(360, 0, '2021-04-01', '769', 12, 1, 'No answer', 1, '2021-04-01 14:04:08', '2021-04-01 14:04:08'),
(361, 0, '2021-04-01', '771', 12, 1, 'Details sent through whatsapp, should call back on 5/4/21', 1, '2021-04-01 14:28:02', '2021-04-01 14:28:02'),
(362, 0, '2021-04-01', '774', 12, 1, 'Twin villa details sent through whatsapp. if interested he will call back', 1, '2021-04-01 19:20:44', '2021-04-01 19:20:44'),
(363, 0, '2021-04-01', '775', 12, 1, 'He is in chennai now, he will be back on 15th, should call back on 16/4/21', 1, '2021-04-01 19:21:21', '2021-04-01 19:21:21'),
(364, 0, '2021-04-01', '766', 12, 1, 'No answer', 1, '2021-04-01 19:22:16', '2021-04-01 19:22:16'),
(365, 0, '2021-04-01', '767', 12, 1, 'Not interesred in podanur side , Looking for site in or near singanallur', 1, '2021-04-01 19:22:58', '2021-04-01 19:22:58'),
(366, 0, '2021-04-01', '768', 12, 1, 'Not seen the details yet, will call back later', 1, '2021-04-01 19:23:31', '2021-04-01 19:23:31'),
(367, 0, '2021-04-01', '777', 12, 1, 'Site visit done ', 1, '2021-04-01 19:23:59', '2021-04-01 19:23:59'),
(368, 0, '2021-04-01', '778', 12, 1, 'No answer', 1, '2021-04-01 19:24:31', '2021-04-01 19:24:31'),
(369, 0, '2021-04-01', '778', 12, 1, 'No answer', 1, '2021-04-01 19:26:24', '2021-04-01 19:26:24'),
(370, 0, '2021-04-01', '779', 12, 1, 'He wants to  buy only land', 1, '2021-04-01 19:27:50', '2021-04-01 19:27:50'),
(371, 0, '2021-04-01', '780', 12, 1, 'No answer', 1, '2021-04-01 19:28:31', '2021-04-01 19:28:31'),
(372, 0, '2021-04-01', '782', 12, 1, 'Busy', 1, '2021-04-01 19:29:05', '2021-04-01 19:29:05'),
(373, 0, '2021-04-01', '785', 12, 1, 'Details sent through whatsapp, if interested he will call back', 1, '2021-04-01 19:31:02', '2021-04-01 19:31:02'),
(374, 0, '2021-04-01', '787', 12, 1, 'Busy', 1, '2021-04-01 19:31:41', '2021-04-01 19:31:41'),
(375, 0, '2021-04-01', '788', 12, 1, 'No answer', 1, '2021-04-01 19:32:17', '2021-04-01 19:32:17'),
(376, 0, '2021-04-01', '789', 12, 1, 'No answer', 1, '2021-04-01 19:32:48', '2021-04-01 19:32:48'),
(377, 0, '2021-04-01', '123', 12, 1, 'Not reachable', 1, '2021-04-01 19:33:25', '2021-04-01 19:33:25'),
(378, 0, '2021-04-01', '794', 12, 1, 'No idea for buying a house', 1, '2021-04-01 19:33:59', '2021-04-01 19:33:59'),
(379, 0, '2021-04-01', '795', 12, 1, 'Out of service', 1, '2021-04-01 19:34:29', '2021-04-01 19:34:29'),
(380, 0, '2021-04-01', '589', 12, 1, 'Will visit on 2/4/21 or 3/4/21', 1, '2021-04-01 19:36:36', '2021-04-01 19:36:36'),
(381, 0, '2021-04-01', '743', 12, 1, 'Budget problem, not interested', 1, '2021-04-01 19:37:19', '2021-04-01 19:37:19'),
(382, 0, '2021-04-01', '592', 12, 1, 'Details sent thrrough whatsapp, call back on 3/4/21', 1, '2021-04-01 19:38:02', '2021-04-01 19:38:02'),
(383, 0, '2021-04-01', '796', 12, 1, 'Not reachable', 1, '2021-04-01 19:38:33', '2021-04-01 19:38:33'),
(384, 0, '2021-04-01', '796', 12, 1, 'Not reachable', 1, '2021-04-01 19:39:15', '2021-04-01 19:39:15'),
(385, 0, '2021-04-01', '797', 12, 1, 'Will visit the site after the election', 1, '2021-04-01 19:40:13', '2021-04-01 19:40:13'),
(386, 0, '2021-04-01', '800', 12, 1, 'No answer', 1, '2021-04-01 19:41:08', '2021-04-01 19:41:08'),
(387, 0, '2021-04-01', '803', 12, 1, 'No answer', 1, '2021-04-01 19:41:47', '2021-04-01 19:41:47'),
(388, 0, '2021-04-01', '806', 12, 1, 'Not seen the details yet, will call back later', 1, '2021-04-01 19:42:30', '2021-04-01 19:42:30'),
(389, 0, '2021-04-01', '813', 12, 1, 'No answer', 1, '2021-04-01 19:43:04', '2021-04-01 19:43:04'),
(390, 0, '2021-04-01', '814', 12, 1, 'Busy', 1, '2021-04-01 19:43:41', '2021-04-01 19:43:41'),
(391, 0, '2021-04-01', '815', 12, 1, 'Will visit within 2 or 3 days', 1, '2021-04-01 19:44:29', '2021-04-01 19:44:29'),
(392, 0, '2021-04-01', '816', 12, 1, 'Switched off', 1, '2021-04-01 19:45:28', '2021-04-01 19:45:28'),
(393, 0, '2021-04-01', '817', 12, 1, 'No answer', 1, '2021-04-01 19:46:02', '2021-04-01 19:46:02'),
(394, 0, '2021-04-01', '818', 12, 1, 'Call back later', 1, '2021-04-01 19:46:41', '2021-04-01 19:46:41'),
(395, 0, '2021-04-01', '820', 12, 1, 'In meeting, will call back later', 1, '2021-04-01 19:47:18', '2021-04-01 19:47:18'),
(396, 0, '2021-04-01', '822', 12, 1, 'No answer', 1, '2021-04-01 19:47:53', '2021-04-01 19:47:53'),
(397, 0, '2021-04-01', '823', 12, 1, 'Will call back later', 1, '2021-04-01 19:48:30', '2021-04-01 19:48:30'),
(398, 0, '2021-04-01', '824', 12, 1, 'Wrong number', 1, '2021-04-01 19:49:02', '2021-04-01 19:49:02'),
(399, 0, '2021-04-01', '826', 12, 1, 'Busy', 1, '2021-04-01 19:49:54', '2021-04-01 19:49:54'),
(400, 0, '2021-04-01', '827', 12, 1, 'Looking for individual villa ( 5 cents )', 1, '2021-04-01 19:50:31', '2021-04-01 19:50:31'),
(401, 0, '2021-04-05', '689', 12, 1, 'No answer\r\n', 1, '2021-04-05 12:16:01', '2021-04-05 12:16:01'),
(402, 0, '2021-04-05', '721', 12, 1, 'No answer', 1, '2021-04-05 12:17:44', '2021-04-05 12:17:44'),
(403, 0, '2021-04-05', '724', 12, 1, 'Switched off\r\n', 1, '2021-04-05 12:19:05', '2021-04-05 12:19:05'),
(404, 0, '2021-04-05', '746', 12, 1, 'Site visit done on 4/4/21, discussion going on', 1, '2021-04-05 12:24:09', '2021-04-05 12:24:09'),
(405, 0, '2021-04-05', '730', 12, 1, 'No answer', 1, '2021-04-05 12:26:32', '2021-04-05 12:26:32'),
(406, 0, '2021-04-05', '732', 12, 1, 'No answer\r\n', 1, '2021-04-05 12:30:03', '2021-04-05 12:30:03'),
(407, 0, '2021-04-05', '744', 12, 1, 'Switched off\r\n', 1, '2021-04-05 12:34:19', '2021-04-05 12:34:19'),
(408, 0, '2021-04-05', '745', 12, 1, 'Plan dropped\r\n', 1, '2021-04-05 13:04:48', '2021-04-05 13:04:48'),
(409, 0, '2021-04-05', '747', 12, 1, 'No answer\r\n', 1, '2021-04-05 13:05:13', '2021-04-05 13:05:13'),
(410, 0, '2021-04-05', '754', 12, 1, 'Call back next week\r\n', 1, '2021-04-05 13:12:00', '2021-04-05 13:12:00'),
(411, 0, '2021-04-05', '765', 12, 1, 'No answer', 1, '2021-04-05 14:04:50', '2021-04-05 14:04:50'),
(412, 0, '2021-04-05', '769', 12, 1, 'Will visit after 10 days(i.e 16/4/21) Call back on 15/4/21', 1, '2021-04-05 14:08:48', '2021-04-05 14:08:48'),
(413, 0, '2021-04-05', '771', 12, 1, 'No answer', 1, '2021-04-05 14:10:15', '2021-04-05 14:10:15'),
(414, 0, '2021-04-05', '773', 12, 1, 'Not reachable', 1, '2021-04-05 14:11:04', '2021-04-05 14:11:04'),
(415, 0, '2021-04-05', '774', 12, 1, 'Busy', 1, '2021-04-05 14:11:52', '2021-04-05 14:11:52'),
(416, 0, '2021-04-05', '778', 12, 1, 'No answer', 1, '2021-04-05 14:42:01', '2021-04-05 14:42:01'),
(417, 0, '2021-04-05', '780', 12, 1, 'Not reachable', 1, '2021-04-05 14:42:36', '2021-04-05 14:42:36'),
(418, 0, '2021-04-05', '782', 12, 1, 'Busy\r\n', 1, '2021-04-05 14:43:07', '2021-04-05 14:43:07'),
(419, 0, '2021-04-05', '787', 12, 1, 'Busy', 1, '2021-04-05 14:43:43', '2021-04-05 14:43:43'),
(420, 0, '2021-04-05', '788', 12, 1, 'Brochure and location sent through whatsapp, will call back', 1, '2021-04-05 14:44:22', '2021-04-05 14:44:22'),
(421, 0, '2021-04-05', '789', 12, 1, 'Need villa or apartment in 30 L', 1, '2021-04-05 14:45:00', '2021-04-05 14:45:00'),
(422, 0, '2021-04-05', '123', 12, 1, 'Busy', 1, '2021-04-05 14:45:30', '2021-04-05 14:45:30'),
(423, 0, '2021-04-05', '795', 12, 1, 'Out of service', 1, '2021-04-05 17:07:30', '2021-04-05 17:07:30'),
(424, 0, '2021-04-05', '848', 12, 1, 'Out of service', 1, '2021-04-05 17:08:01', '2021-04-05 17:08:01'),
(425, 0, '2021-04-05', '589', 12, 1, 'No answer', 1, '2021-04-05 17:08:33', '2021-04-05 17:08:33'),
(426, 0, '2021-04-05', '592', 12, 1, 'Will call back later', 1, '2021-04-05 17:10:44', '2021-04-05 17:10:44'),
(427, 0, '2021-04-05', '796', 12, 1, 'Busy', 1, '2021-04-05 17:15:11', '2021-04-05 17:15:11'),
(428, 0, '2021-04-05', '800', 12, 1, 'Not reachable', 1, '2021-04-05 17:16:50', '2021-04-05 17:16:50'),
(429, 0, '2021-04-05', '803', 12, 1, 'No answer', 1, '2021-04-05 17:18:11', '2021-04-05 17:18:11'),
(430, 0, '2021-04-05', '813', 12, 1, 'No answer', 1, '2021-04-05 17:19:59', '2021-04-05 17:19:59'),
(431, 0, '2021-04-05', '814', 12, 1, 'No answer', 1, '2021-04-05 17:21:46', '2021-04-05 17:21:46'),
(432, 0, '2021-04-05', '815', 12, 1, 'He and his friend will visit after the election', 1, '2021-04-05 17:24:10', '2021-04-05 17:24:10'),
(433, 0, '2021-04-05', '816', 12, 1, 'Not reachable\r\n', 1, '2021-04-05 17:29:43', '2021-04-05 17:29:43'),
(434, 0, '2021-04-05', '818', 12, 1, 'No answer\r\n', 1, '2021-04-05 17:51:07', '2021-04-05 17:51:07'),
(435, 0, '2021-04-05', '820', 12, 1, 'Will call back later', 1, '2021-04-05 17:52:47', '2021-04-05 17:52:47'),
(436, 0, '2021-04-05', '822', 12, 1, 'Busy', 1, '2021-04-05 17:53:43', '2021-04-05 17:53:43'),
(437, 0, '2021-04-05', '823', 12, 1, 'No idea\r\n', 1, '2021-04-05 17:55:49', '2021-04-05 17:55:49'),
(438, 0, '2021-04-05', '824', 12, 1, 'Wrong number', 1, '2021-04-05 18:20:08', '2021-04-05 18:20:08'),
(439, 0, '2021-04-05', '826', 12, 1, 'Switched off', 1, '2021-04-05 18:20:39', '2021-04-05 18:20:39'),
(440, 0, '2021-04-05', '833', 12, 1, 'No idea, need RTO home', 1, '2021-04-05 18:21:34', '2021-04-05 18:21:34'),
(441, 0, '2021-04-05', '841', 12, 1, 'Clicked it by mistake', 1, '2021-04-05 18:22:26', '2021-04-05 18:22:44'),
(442, 0, '2021-04-05', '490', 12, 1, 'Call back on 10th or 11th for site visit', 1, '2021-04-05 18:23:24', '2021-04-05 18:23:24'),
(443, 0, '2021-04-05', '844', 12, 1, 'Call back next week ', 1, '2021-04-05 18:24:11', '2021-04-05 18:24:11'),
(444, 0, '2021-04-05', '845', 12, 1, 'Site visit done on 3/4/21, will come again tomorrow', 1, '2021-04-05 18:24:53', '2021-04-05 18:24:53'),
(445, 0, '2021-04-05', '363', 12, 1, 'Not reachable\r\n', 1, '2021-04-05 18:26:47', '2021-04-05 18:26:47'),
(446, 0, '2021-04-05', '846', 12, 1, 'Will call back later', 1, '2021-04-05 18:29:08', '2021-04-05 18:29:08'),
(447, 0, '2021-04-05', '464', 12, 1, 'Brochure and location sent through whatsapp, will call back', 1, '2021-04-05 18:35:06', '2021-04-05 18:35:06'),
(448, 0, '2021-04-05', '518', 12, 1, 'Busy', 1, '2021-04-05 18:40:01', '2021-04-05 18:40:01'),
(449, 0, '2021-04-07', '478', 12, 1, 'Will visit the site when he is free', 1, '2021-04-07 16:52:41', '2021-04-07 16:52:41'),
(450, 0, '2021-04-07', '846', 12, 1, 'No answer', 1, '2021-04-07 16:56:03', '2021-04-07 16:58:48'),
(451, 0, '2021-04-07', '363', 12, 1, 'Will visit on 13th or 14th', 1, '2021-04-07 16:57:48', '2021-04-07 16:57:48'),
(452, 0, '2021-04-07', '844', 12, 1, 'Call back on 12/4/21', 1, '2021-04-07 16:59:55', '2021-04-07 16:59:55'),
(453, 0, '2021-04-07', '845', 12, 1, 'Busy', 1, '2021-04-07 17:37:29', '2021-04-07 17:37:29'),
(454, 0, '2021-04-07', '830', 12, 1, 'If interested, will call back', 1, '2021-04-07 17:38:29', '2021-04-07 17:38:29'),
(455, 0, '2021-04-07', '827', 12, 1, 'Looking for another property', 1, '2021-04-07 17:39:08', '2021-04-07 17:39:08'),
(456, 0, '2021-04-07', '826', 12, 1, 'Call back later', 1, '2021-04-07 17:46:35', '2021-04-07 17:46:35'),
(457, 0, '2021-04-07', '822', 12, 1, 'Switched off', 1, '2021-04-07 17:53:14', '2021-04-07 17:53:14'),
(458, 0, '2021-04-07', '820', 12, 1, 'Will call back later', 1, '2021-04-07 18:09:04', '2021-04-07 18:09:04'),
(459, 0, '2021-04-07', '819', 12, 1, 'No answer', 1, '2021-04-07 18:09:31', '2021-04-07 18:09:31'),
(460, 0, '2021-04-07', '818', 12, 1, 'No answer', 1, '2021-04-07 18:10:45', '2021-04-07 18:10:45'),
(461, 0, '2021-04-08', '764', 12, 1, 'Out of service', 1, '2021-04-08 13:18:47', '2021-04-08 13:18:47'),
(462, 0, '2021-04-08', '762', 12, 1, 'Will visit on 10th or 11th\r\n', 1, '2021-04-08 13:19:22', '2021-04-08 13:19:22');
INSERT INTO `lead_activity_tbl` (`id`, `branch_id`, `date`, `lead_id`, `employee_id`, `activity_id`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(463, 0, '2021-04-08', '748', 12, 1, 'Not reachable\r\n', 1, '2021-04-08 13:19:53', '2021-04-08 13:19:53'),
(464, 0, '2021-04-08', '747', 12, 1, 'No answer', 1, '2021-04-08 13:21:13', '2021-04-08 13:21:13'),
(465, 0, '2021-04-08', '745', 12, 1, 'Busy', 1, '2021-04-08 13:27:25', '2021-04-08 13:27:25'),
(466, 0, '2021-04-08', '744', 12, 1, 'Switched off', 1, '2021-04-08 14:05:34', '2021-04-08 14:05:34'),
(467, 0, '2021-04-08', '201', 12, 1, 'No answer', 1, '2021-04-08 14:12:17', '2021-04-08 14:12:17'),
(468, 0, '2021-04-08', '732', 12, 1, 'Details sent through whatsapp, will call back later', 1, '2021-04-08 14:17:50', '2021-04-08 14:23:08'),
(469, 0, '2021-04-08', '730', 12, 1, 'Busy', 1, '2021-04-08 14:30:31', '2021-04-08 14:30:31'),
(470, 0, '2021-04-08', '724', 12, 1, 'Switched off', 1, '2021-04-08 14:31:21', '2021-04-08 14:31:21'),
(471, 0, '2021-04-08', '721', 12, 1, 'Call back later', 1, '2021-04-08 14:32:48', '2021-04-08 14:32:48'),
(472, 0, '2021-04-08', '685', 12, 1, 'Busy', 1, '2021-04-08 14:33:47', '2021-04-08 14:33:47'),
(473, 0, '2021-04-08', '757', 12, 1, 'No answer', 1, '2021-04-08 14:34:53', '2021-04-08 14:34:53'),
(474, 0, '2021-04-08', '679', 12, 1, 'Call back in the evening', 1, '2021-04-08 14:36:34', '2021-04-08 14:36:34'),
(475, 0, '2021-04-08', '509', 12, 1, 'Busy', 1, '2021-04-08 17:35:39', '2021-04-08 17:35:39'),
(476, 0, '2021-04-08', '478', 12, 1, 'No answer', 1, '2021-04-08 17:38:22', '2021-04-08 17:38:22'),
(477, 0, '2021-04-08', '470', 12, 1, 'Busy', 1, '2021-04-08 17:43:24', '2021-04-08 17:43:24'),
(478, 0, '2021-04-08', '465', 12, 1, 'Bought another property', 1, '2021-04-08 17:45:01', '2021-04-08 17:45:01'),
(479, 0, '2021-04-08', '650', 12, 1, 'Want land only', 1, '2021-04-08 17:50:57', '2021-04-08 17:50:57'),
(480, 0, '2021-04-08', '443', 12, 1, 'Want land only', 1, '2021-04-08 17:51:36', '2021-04-08 17:51:36'),
(481, 0, '2021-04-08', '417', 12, 1, 'No answer', 1, '2021-04-08 17:52:56', '2021-04-08 17:52:56'),
(482, 0, '2021-04-08', '411', 12, 1, 'Busy', 1, '2021-04-08 17:53:33', '2021-04-08 17:53:33'),
(483, 0, '2021-04-08', '404', 12, 1, 'Busy', 1, '2021-04-08 17:54:13', '2021-04-08 17:54:13'),
(484, 0, '2021-04-08', '391', 12, 1, 'No answer', 1, '2021-04-08 17:56:02', '2021-04-08 17:56:02'),
(485, 0, '2021-04-08', '357', 12, 1, 'His friend Sivashankar is interested in twin and duplex villa, site visit done ', 1, '2021-04-08 17:59:48', '2021-04-08 17:59:48'),
(486, 0, '2021-04-08', '309', 12, 1, 'Busy', 1, '2021-04-08 18:00:11', '2021-04-08 18:00:11'),
(487, 0, '2021-04-08', '308', 12, 1, 'Busy', 1, '2021-04-08 18:02:18', '2021-04-08 18:02:18'),
(488, 0, '2021-04-08', '291', 12, 1, 'Call barred', 1, '2021-04-08 18:03:12', '2021-04-08 18:03:12'),
(489, 0, '2021-04-08', '588', 12, 1, 'No answer', 1, '2021-04-08 18:04:14', '2021-04-08 18:04:14'),
(490, 0, '2021-04-08', '260', 12, 1, 'No answer', 1, '2021-04-08 18:05:17', '2021-04-08 18:05:17'),
(491, 0, '2021-04-08', '232', 12, 1, 'No answer\r\n', 1, '2021-04-08 18:06:14', '2021-04-08 18:06:14'),
(492, 0, '2021-04-08', '208', 12, 1, 'No answer', 1, '2021-04-08 18:09:43', '2021-04-08 18:09:43'),
(493, 0, '2021-04-08', '208', 12, 1, 'Bought another property', 1, '2021-04-08 18:13:11', '2021-04-08 18:13:11'),
(494, 0, '2021-04-08', '147', 12, 1, 'No answer', 1, '2021-04-08 18:24:17', '2021-04-08 18:24:17'),
(495, 0, '2021-04-08', '114', 12, 1, 'His budget is 20-30L', 1, '2021-04-08 18:30:54', '2021-04-08 18:30:54'),
(496, 0, '2021-04-08', '94', 12, 1, 'Call back on 10/4/21 for site visit', 1, '2021-04-08 18:31:24', '2021-04-08 18:31:24'),
(497, 0, '2021-04-09', '147', 12, 1, 'Not reachable\r\n', 1, '2021-04-09 15:36:09', '2021-04-09 15:36:09'),
(498, 0, '2021-04-09', '208', 12, 1, 'Switched off', 1, '2021-04-09 15:39:08', '2021-04-09 15:39:08'),
(499, 0, '2021-04-09', '232', 12, 1, 'No answer', 1, '2021-04-09 15:41:52', '2021-04-09 15:41:52'),
(500, 0, '2021-04-09', '260', 12, 1, 'No answer\r\n', 1, '2021-04-09 15:43:00', '2021-04-09 15:43:00'),
(501, 0, '2021-04-09', '588', 12, 1, 'No answer', 1, '2021-04-09 15:44:13', '2021-04-09 15:44:13'),
(502, 0, '2021-04-09', '274', 12, 1, 'Call on 12/4/21', 1, '2021-04-09 15:45:53', '2021-04-09 15:46:22'),
(503, 0, '2021-04-09', '291', 12, 1, 'Call barred', 1, '2021-04-09 15:46:51', '2021-04-09 15:46:51'),
(504, 0, '2021-04-09', '308', 12, 1, 'Switched off', 1, '2021-04-09 15:57:07', '2021-04-09 15:57:07'),
(505, 0, '2021-04-09', '309', 12, 1, 'Need individual house in 30-35L', 1, '2021-04-09 16:00:16', '2021-04-09 16:00:16'),
(506, 0, '2021-04-09', '391', 12, 1, 'Busy', 1, '2021-04-09 16:01:21', '2021-04-09 16:01:21'),
(507, 0, '2021-04-09', '404', 12, 1, 'Busy', 1, '2021-04-09 16:04:07', '2021-04-09 16:04:07'),
(508, 0, '2021-04-09', '411', 12, 1, 'Brochure and location sent through whatsapp, call on  10/4/21', 1, '2021-04-09 16:09:08', '2021-04-09 16:09:08'),
(509, 0, '2021-04-09', '417', 12, 1, 'Not reachable', 1, '2021-04-09 16:10:26', '2021-04-09 16:10:26'),
(510, 0, '2021-04-09', '470', 12, 1, 'No answer', 1, '2021-04-09 16:12:45', '2021-04-09 16:12:45'),
(511, 0, '2021-04-09', '478', 12, 1, 'Will call and come', 1, '2021-04-09 16:25:22', '2021-04-09 16:25:22'),
(512, 0, '2021-04-09', '486', 12, 1, 'Busy', 1, '2021-04-09 16:26:14', '2021-04-09 16:26:14'),
(513, 0, '2021-04-09', '509', 12, 1, 'Busy', 1, '2021-04-09 16:35:55', '2021-04-09 16:35:55'),
(514, 0, '2021-04-09', '455', 12, 1, 'No answer', 1, '2021-04-09 16:37:17', '2021-04-09 16:37:17'),
(515, 0, '2021-04-09', '541', 12, 1, 'No answer', 1, '2021-04-09 16:38:49', '2021-04-09 16:38:49'),
(516, 0, '2021-04-09', '542', 12, 1, 'No answer', 1, '2021-04-09 16:40:00', '2021-04-09 16:40:00'),
(517, 0, '2021-04-09', '585', 12, 1, 'Will call back on 12/4/21', 1, '2021-04-09 16:41:29', '2021-04-09 16:41:29'),
(518, 0, '2021-04-09', '586', 12, 1, 'No answer', 1, '2021-04-09 16:43:32', '2021-04-09 16:43:32'),
(519, 0, '2021-04-09', '598', 12, 1, 'No answer', 1, '2021-04-09 16:44:43', '2021-04-09 16:44:43'),
(520, 0, '2021-04-09', '696', 12, 1, 'No answer', 1, '2021-04-09 16:47:00', '2021-04-09 16:47:00'),
(521, 0, '2021-04-09', '350', 12, 1, 'Call back on 10/4/21', 1, '2021-04-09 16:50:06', '2021-04-09 16:50:06'),
(522, 0, '2021-04-09', '123', 12, 1, 'Network issue', 1, '2021-04-09 16:51:37', '2021-04-09 16:51:37'),
(523, 0, '2021-04-09', '640', 12, 1, 'No answer\r\n', 1, '2021-04-09 16:56:29', '2021-04-09 16:56:29'),
(524, 0, '2021-04-09', '641', 12, 1, 'No answer', 1, '2021-04-09 16:58:35', '2021-04-09 16:58:35'),
(525, 0, '2021-04-09', '688', 12, 1, 'Not reachable', 1, '2021-04-09 16:59:36', '2021-04-09 16:59:36'),
(526, 0, '2021-04-09', '757', 12, 1, 'No answer', 1, '2021-04-09 17:01:31', '2021-04-09 17:01:31'),
(527, 0, '2021-04-09', '685', 12, 1, 'No answer', 1, '2021-04-09 17:04:39', '2021-04-09 17:04:39'),
(528, 0, '2021-04-09', '724', 12, 1, 'Switched off', 1, '2021-04-09 17:05:42', '2021-04-09 17:05:42'),
(529, 0, '2021-04-09', '730', 12, 1, 'Call back on 10/4/21', 1, '2021-04-09 17:06:57', '2021-04-09 17:06:57'),
(530, 0, '2021-04-09', '744', 12, 1, 'Switched off', 1, '2021-04-09 17:09:15', '2021-04-09 17:09:15'),
(531, 0, '2021-04-09', '745', 12, 1, 'Busy', 1, '2021-04-09 17:09:58', '2021-04-09 17:09:58'),
(532, 0, '2021-04-09', '747', 12, 1, 'Busy', 1, '2021-04-09 17:10:40', '2021-04-09 17:10:40'),
(533, 0, '2021-04-09', '748', 12, 1, 'Not satisfied with house size', 1, '2021-04-09 17:16:04', '2021-04-09 17:16:04'),
(534, 0, '2021-04-09', '764', 12, 1, 'Out of service', 1, '2021-04-09 17:16:31', '2021-04-09 17:16:31'),
(535, 0, '2021-04-09', '773', 12, 1, 'Not reachable', 1, '2021-04-09 17:18:07', '2021-04-09 17:18:07'),
(536, 0, '2021-04-09', '766', 12, 1, 'Network issue', 1, '2021-04-09 17:20:08', '2021-04-09 17:20:08'),
(537, 0, '2021-04-10', '691', 12, 1, 'Will visit the site once the amont is ready', 1, '2021-04-10 13:24:07', '2021-04-10 13:24:07'),
(538, 0, '2021-04-10', '94', 12, 1, 'Will call back in the evening for site visit', 1, '2021-04-10 13:24:35', '2021-04-10 13:24:35'),
(539, 0, '2021-04-10', '147', 12, 1, 'Not available', 1, '2021-04-10 13:25:00', '2021-04-10 13:25:00'),
(540, 0, '2021-04-07', '167', 12, 1, 'Will visit on 14/4/21', 1, '2021-04-10 13:53:26', '2021-04-10 13:53:26'),
(541, 0, '2021-04-07', '515', 12, 1, 'Will call back next month', 1, '2021-04-10 13:59:55', '2021-04-10 13:59:55'),
(542, 0, '2021-04-07', '399', 12, 1, 'Call back next week ', 1, '2021-04-10 14:01:08', '2021-04-10 14:01:08'),
(543, 0, '2021-04-07', '561', 12, 1, 'Will call baack in 2 days for site visit', 1, '2021-04-10 14:01:57', '2021-04-10 14:01:57'),
(544, 0, '2021-04-07', '590', 12, 1, 'Call back after diwali', 1, '2021-04-10 14:04:05', '2021-04-10 14:04:05'),
(545, 0, '2021-04-09', '610', 12, 1, 'No answer', 1, '2021-04-10 14:05:35', '2021-04-10 14:05:35'),
(546, 0, '2021-04-09', '615', 12, 1, 'Will call back later', 1, '2021-04-10 14:06:01', '2021-04-10 14:06:01'),
(547, 0, '2021-04-08', '649', 12, 1, 'Call back later', 1, '2021-04-10 14:06:52', '2021-04-10 14:06:52'),
(548, 0, '2021-04-08', '672', 12, 1, 'Details sent through whatsapp, will call back', 1, '2021-04-10 14:07:33', '2021-04-10 14:07:33'),
(549, 0, '2021-04-09', '673', 12, 1, 'Call back on 10th for site visit', 1, '2021-04-10 14:08:15', '2021-04-10 14:08:15'),
(550, 0, '2021-04-09', '675', 12, 1, 'Call back later ', 1, '2021-04-10 14:08:52', '2021-04-10 14:08:52'),
(551, 0, '2021-04-08', '759', 12, 1, 'Call back on 15/4/21', 1, '2021-04-10 14:27:02', '2021-04-10 14:27:02'),
(552, 0, '2021-04-08', '771', 12, 1, 'Will visit next week, call on 12/4/21', 1, '2021-04-10 14:28:51', '2021-04-10 14:28:51'),
(553, 0, '2021-04-09', '778', 12, 1, 'Network issue', 1, '2021-04-10 14:30:55', '2021-04-10 14:30:55'),
(554, 0, '2021-04-09', '780', 12, 1, 'No answer', 1, '2021-04-10 14:31:28', '2021-04-10 14:31:28'),
(555, 0, '2021-04-08', '787', 12, 1, 'Call back later', 1, '2021-04-10 14:32:05', '2021-04-10 14:32:05'),
(556, 0, '2021-04-09', '795', 12, 1, 'Out of service', 1, '2021-04-10 14:32:44', '2021-04-10 14:32:44'),
(557, 0, '2021-04-09', '848', 12, 1, 'Out of service', 1, '2021-04-10 14:33:14', '2021-04-10 14:33:14'),
(558, 0, '2021-04-09', '589', 12, 1, 'He want a house in 4 cents', 1, '2021-04-10 14:33:59', '2021-04-10 14:33:59'),
(559, 0, '2021-04-09', '796', 12, 1, 'Busy', 1, '2021-04-10 14:34:53', '2021-04-10 14:34:53'),
(560, 0, '2021-04-09', '800', 12, 1, 'Not reachable', 1, '2021-04-10 14:35:18', '2021-04-10 14:35:18'),
(561, 0, '2021-04-09', '803', 12, 1, 'No answer', 1, '2021-04-10 14:35:46', '2021-04-10 14:35:46'),
(562, 0, '2021-04-09', '814', 12, 1, 'Will call back later', 1, '2021-04-10 14:36:15', '2021-04-10 14:36:15'),
(563, 0, '2021-04-09', '815', 12, 1, 'Will call and visit when he is free', 1, '2021-04-10 14:37:00', '2021-04-10 14:37:00'),
(564, 0, '2021-04-09', '818', 12, 1, 'Details sent through whatsapp, will call back', 1, '2021-04-10 14:37:29', '2021-04-10 14:37:29'),
(565, 0, '2021-04-09', '819', 12, 1, 'No answer', 1, '2021-04-10 14:37:55', '2021-04-10 14:37:55'),
(566, 0, '2021-04-09', '822', 12, 1, 'Not reachable', 1, '2021-04-10 14:38:46', '2021-04-10 14:38:46'),
(567, 0, '2021-04-09', '845', 12, 1, 'Not reachable', 1, '2021-04-10 14:40:42', '2021-04-10 14:40:42'),
(568, 0, '2021-04-08', '464', 12, 1, 'Call on 10/4/21', 1, '2021-04-10 14:42:42', '2021-04-10 14:42:42'),
(569, 0, '2021-04-09', '849', 12, 1, 'No answer', 1, '2021-04-10 14:43:14', '2021-04-10 14:43:14'),
(570, 0, '2021-04-09', '850', 12, 1, 'Call back next week', 1, '2021-04-10 14:43:43', '2021-04-10 14:43:43'),
(571, 0, '2021-04-09', '854', 12, 1, 'Will visit in this week end', 1, '2021-04-10 14:44:13', '2021-04-10 14:44:13'),
(572, 0, '2021-04-07', '528', 12, 1, 'Call back on 8/4/21', 1, '2021-04-10 14:48:26', '2021-04-10 14:48:26'),
(573, 0, '2021-04-12', '1', 1, 3, 'test follow up 2', 1, '2021-04-12 11:16:34', '2021-04-12 11:16:34'),
(574, 0, '2021-07-27', '1', 1, 2, 's', 1, '2021-07-27 11:31:41', '2021-07-27 11:31:41'),
(575, 0, '2021-08-23', '1', 1, 2, 'test', 1, '2021-08-23 15:39:21', '2021-08-23 15:39:21'),
(576, 0, '2021-08-22', '862', 1, 2, 'follow up no response', 1, '2021-08-23 15:45:21', '2021-08-23 15:45:21'),
(577, 0, '2021-08-26', '1', 1, 3, 'test', 1, '2021-08-26 10:24:47', '2021-08-26 10:24:47'),
(578, 0, '2021-08-19', '1', 6, 4, 'test', 1, '2021-08-26 10:25:49', '2021-08-26 12:23:06');

-- --------------------------------------------------------

--
-- Table structure for table `lead_gallery_tbl`
--

CREATE TABLE `lead_gallery_tbl` (
  `id` int(11) NOT NULL,
  `lead_id` varchar(250) DEFAULT NULL,
  `image` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_gallery_tbl`
--

INSERT INTO `lead_gallery_tbl` (`id`, `lead_id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'xmQqtDUXM1hEuYLjdaTrP0zkfpOAni.jpg', 1, '2021-07-26 22:49:22', '2021-07-26 22:49:22'),
(2, '1', 'CpWQ0gGA35reEDoLFVZT42MwP68y_n.jpg', 1, '2021-07-26 22:49:36', '2021-07-26 22:49:36'),
(5, '1', 'p26U3HBcdnb7f0DCKxY9gJE58oqFeW.jpg', 1, '2021-07-26 22:49:36', '2021-07-26 22:49:36'),
(8, '1', 'EZJhrzxHGOt9n7iBMkbKaC2swpmuRL.jpg', 1, '2021-07-27 11:32:00', '2021-07-27 11:32:00');

-- --------------------------------------------------------

--
-- Table structure for table `lead_logs_tbl`
--

CREATE TABLE `lead_logs_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `type` varchar(250) NOT NULL,
  `lead_type` varchar(255) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_logs_tbl`
--

INSERT INTO `lead_logs_tbl` (`id`, `branch_id`, `type`, `lead_type`, `created_by`, `created_to`, `ref_id`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'created', 'manual', 12, 0, 687, '', 1, '2021-02-25 11:59:13', '2021-02-25 11:59:13'),
(2, 0, 'created', 'manual', 12, 0, 688, '', 1, '2021-02-25 12:17:29', '2021-02-25 12:17:29'),
(3, 0, 'created', 'manual', 12, 0, 689, '', 1, '2021-02-25 13:09:37', '2021-02-25 13:09:37'),
(4, 0, 'created', 'manual', 12, 0, 690, '', 1, '2021-02-27 11:28:33', '2021-02-27 11:28:33'),
(5, 0, 'created', 'manual', 12, 0, 691, '', 1, '2021-02-27 11:31:41', '2021-02-27 11:31:41'),
(6, 0, 'created', 'manual', 12, 0, 692, '', 1, '2021-02-27 12:42:11', '2021-02-27 12:42:11'),
(7, 0, 'created', 'manual', 12, 0, 693, '', 1, '2021-02-27 14:17:51', '2021-02-27 14:17:51'),
(8, 0, 'created', 'manual', 12, 0, 694, '', 1, '2021-03-01 17:15:47', '2021-03-01 17:15:47'),
(9, 0, 'created', 'manual', 12, 0, 695, '', 1, '2021-03-01 17:20:15', '2021-03-01 17:20:15'),
(10, 0, 'created', 'manual', 12, 0, 696, '', 1, '2021-03-01 18:13:08', '2021-03-01 18:13:08'),
(11, 0, 'created', 'manual', 12, 0, 697, '', 1, '2021-03-08 10:38:00', '2021-03-08 10:38:00'),
(12, 0, 'created', 'manual', 12, 0, 698, '', 1, '2021-03-08 10:42:47', '2021-03-08 10:42:47'),
(13, 0, 'created', 'manual', 12, 0, 699, '', 1, '2021-03-08 16:28:25', '2021-03-08 16:28:25'),
(14, 0, 'created', 'manual', 12, 0, 700, '', 1, '2021-03-08 16:29:24', '2021-03-08 16:29:24'),
(15, 0, 'created', 'manual', 12, 0, 701, '', 1, '2021-03-08 16:31:13', '2021-03-08 16:31:13'),
(16, 0, 'created', 'manual', 12, 0, 702, '', 1, '2021-03-08 16:32:46', '2021-03-08 16:32:46'),
(17, 0, 'created', 'manual', 12, 0, 703, '', 1, '2021-03-08 16:33:55', '2021-03-08 16:33:55'),
(18, 0, 'created', 'manual', 12, 0, 704, '', 1, '2021-03-08 16:36:35', '2021-03-08 16:36:35'),
(19, 0, 'created', 'manual', 12, 0, 705, '', 1, '2021-03-08 16:40:25', '2021-03-08 16:40:25'),
(20, 0, 'created', 'manual', 12, 0, 706, '', 1, '2021-03-08 16:43:07', '2021-03-08 16:43:07'),
(21, 0, 'created', 'manual', 12, 0, 707, '', 1, '2021-03-08 16:45:46', '2021-03-08 16:45:46'),
(22, 0, 'created', 'manual', 12, 0, 708, '', 1, '2021-03-08 17:16:58', '2021-03-08 17:16:58'),
(23, 0, 'created', 'manual', 12, 0, 709, '', 1, '2021-03-08 17:17:42', '2021-03-08 17:17:42'),
(24, 0, 'created', 'manual', 12, 0, 710, '', 1, '2021-03-08 17:18:41', '2021-03-08 17:18:41'),
(25, 0, 'created', 'manual', 12, 0, 711, '', 1, '2021-03-08 17:19:48', '2021-03-08 17:19:48'),
(26, 0, 'created', 'manual', 12, 0, 712, '', 1, '2021-03-08 17:22:15', '2021-03-08 17:22:15'),
(27, 0, 'created', 'manual', 12, 0, 713, '', 1, '2021-03-08 18:13:16', '2021-03-08 18:13:16'),
(28, 0, 'created', 'manual', 12, 0, 714, '', 1, '2021-03-08 18:15:39', '2021-03-08 18:15:39'),
(29, 0, 'created', 'manual', 12, 0, 715, '', 1, '2021-03-08 18:23:26', '2021-03-08 18:23:26'),
(30, 0, 'created', 'manual', 12, 0, 716, '', 1, '2021-03-08 18:30:26', '2021-03-08 18:30:26'),
(31, 0, 'created', 'manual', 12, 0, 717, '', 1, '2021-03-09 10:18:12', '2021-03-09 10:18:12'),
(32, 0, 'created', 'manual', 12, 0, 718, '', 1, '2021-03-09 11:33:59', '2021-03-09 11:33:59'),
(33, 0, 'created', 'manual', 12, 0, 719, '', 1, '2021-03-09 15:29:32', '2021-03-09 15:29:32'),
(34, 0, 'created', 'manual', 12, 0, 720, '', 1, '2021-03-10 10:30:06', '2021-03-10 10:30:06'),
(35, 0, 'created', 'manual', 12, 0, 721, '', 1, '2021-03-10 10:31:57', '2021-03-10 10:31:57'),
(36, 0, 'created', 'manual', 12, 0, 722, '', 1, '2021-03-10 10:33:51', '2021-03-10 10:33:51'),
(37, 0, 'created', 'manual', 12, 0, 723, '', 1, '2021-03-10 10:35:25', '2021-03-10 10:35:25'),
(38, 0, 'created', 'manual', 12, 0, 724, '', 1, '2021-03-10 10:37:28', '2021-03-10 10:37:28'),
(39, 0, 'created', 'manual', 12, 0, 725, '', 1, '2021-03-10 10:39:39', '2021-03-10 10:39:39'),
(40, 0, 'created', 'manual', 12, 0, 726, '', 1, '2021-03-10 10:41:04', '2021-03-10 10:41:04'),
(41, 0, 'created', 'manual', 12, 0, 727, '', 1, '2021-03-10 10:45:19', '2021-03-10 10:45:19'),
(42, 0, 'created', 'manual', 12, 0, 728, '', 1, '2021-03-10 10:48:03', '2021-03-10 10:48:03'),
(43, 0, 'created', 'manual', 12, 0, 729, '', 1, '2021-03-10 10:52:24', '2021-03-10 10:52:24'),
(44, 0, 'created', 'manual', 12, 0, 730, '', 1, '2021-03-10 10:55:01', '2021-03-10 10:55:01'),
(45, 0, 'created', 'manual', 12, 0, 731, '', 1, '2021-03-10 10:56:33', '2021-03-10 10:56:33'),
(46, 0, 'created', 'manual', 12, 0, 732, '', 1, '2021-03-10 10:58:19', '2021-03-10 10:58:19'),
(47, 0, 'created', 'manual', 12, 0, 733, '', 1, '2021-03-10 10:59:41', '2021-03-10 10:59:41'),
(48, 0, 'created', 'manual', 12, 0, 734, '', 1, '2021-03-10 11:01:01', '2021-03-10 11:01:01'),
(49, 0, 'created', 'manual', 12, 0, 735, '', 1, '2021-03-10 11:02:36', '2021-03-10 11:02:36'),
(50, 0, 'created', 'manual', 12, 0, 736, '', 1, '2021-03-10 11:03:42', '2021-03-10 11:03:42'),
(51, 0, 'created', 'manual', 12, 0, 737, '', 1, '2021-03-10 11:04:59', '2021-03-10 11:04:59'),
(52, 0, 'created', 'manual', 12, 0, 738, '', 1, '2021-03-10 11:06:37', '2021-03-10 11:06:37'),
(53, 0, 'created', 'manual', 12, 0, 739, '', 1, '2021-03-10 11:08:12', '2021-03-10 11:08:12'),
(54, 0, 'created', 'manual', 12, 0, 740, '', 1, '2021-03-10 11:10:33', '2021-03-10 11:10:33'),
(55, 0, 'created', 'manual', 12, 0, 741, '', 1, '2021-03-10 11:12:38', '2021-03-10 11:12:38'),
(56, 0, 'created', 'manual', 12, 0, 742, '', 1, '2021-03-10 11:14:18', '2021-03-10 11:14:18'),
(57, 0, 'created', 'manual', 12, 0, 743, '', 1, '2021-03-10 11:17:10', '2021-03-10 11:17:10'),
(58, 0, 'created', 'manual', 12, 0, 744, '', 1, '2021-03-10 11:20:32', '2021-03-10 11:20:32'),
(59, 0, 'created', 'manual', 12, 0, 745, '', 1, '2021-03-10 11:22:29', '2021-03-10 11:22:29'),
(60, 0, 'created', 'manual', 12, 0, 746, '', 1, '2021-03-10 11:23:33', '2021-03-10 11:23:33'),
(61, 0, 'created', 'manual', 12, 0, 747, '', 1, '2021-03-10 11:24:31', '2021-03-10 11:24:31'),
(62, 0, 'created', 'manual', 12, 0, 748, '', 1, '2021-03-10 11:39:54', '2021-03-10 11:39:54'),
(63, 0, 'created', 'manual', 12, 0, 749, '', 1, '2021-03-10 11:41:16', '2021-03-10 11:41:16'),
(64, 0, 'created', 'manual', 12, 0, 750, '', 1, '2021-03-10 11:43:56', '2021-03-10 11:43:56'),
(65, 0, 'created', 'manual', 12, 0, 751, '', 1, '2021-03-10 11:46:46', '2021-03-10 11:46:46'),
(66, 0, 'created', 'manual', 12, 0, 752, '', 1, '2021-03-10 11:49:01', '2021-03-10 11:49:01'),
(67, 0, 'created', 'manual', 12, 0, 753, '', 1, '2021-03-10 11:50:41', '2021-03-10 11:50:41'),
(68, 0, 'created', 'manual', 12, 0, 754, '', 1, '2021-03-10 11:54:14', '2021-03-10 11:54:14'),
(69, 0, 'created', 'manual', 12, 0, 755, '', 1, '2021-03-10 11:55:26', '2021-03-10 11:55:26'),
(70, 0, 'created', 'manual', 12, 0, 756, '', 1, '2021-03-10 11:56:33', '2021-03-10 11:56:33'),
(71, 0, 'created', 'manual', 12, 0, 757, '', 1, '2021-03-10 12:01:22', '2021-03-10 12:01:22'),
(72, 0, 'created', 'manual', 12, 0, 758, '', 1, '2021-03-10 14:58:21', '2021-03-10 14:58:21'),
(73, 0, 'created', 'manual', 12, 0, 759, '', 1, '2021-03-10 15:10:51', '2021-03-10 15:10:51'),
(74, 0, 'created', 'manual', 12, 0, 760, '', 1, '2021-03-10 15:17:44', '2021-03-10 15:17:44'),
(75, 0, 'created', 'manual', 12, 0, 761, '', 1, '2021-03-11 10:23:31', '2021-03-11 10:23:31'),
(76, 0, 'created', 'manual', 12, 0, 762, '', 1, '2021-03-11 10:27:34', '2021-03-11 10:27:34'),
(77, 0, 'created', 'manual', 12, 0, 763, '', 1, '2021-03-11 10:29:10', '2021-03-11 10:29:10'),
(78, 0, 'created', 'manual', 12, 0, 764, '', 1, '2021-03-11 10:30:58', '2021-03-11 10:30:58'),
(79, 0, 'created', 'manual', 12, 0, 765, '', 1, '2021-03-11 11:02:06', '2021-03-11 11:02:06'),
(80, 0, 'created', 'manual', 12, 0, 766, '', 1, '2021-03-12 15:34:23', '2021-03-12 15:34:23'),
(81, 0, 'created', 'manual', 12, 0, 767, '', 1, '2021-03-12 15:35:33', '2021-03-12 15:35:33'),
(82, 0, 'created', 'manual', 12, 0, 768, '', 1, '2021-03-12 15:36:47', '2021-03-12 15:36:47'),
(83, 0, 'created', 'manual', 12, 0, 769, '', 1, '2021-03-16 11:40:18', '2021-03-16 11:40:18'),
(84, 0, 'created', 'manual', 12, 0, 770, '', 1, '2021-03-16 11:44:22', '2021-03-16 11:44:22'),
(85, 0, 'created', 'manual', 12, 0, 771, '', 1, '2021-03-16 11:46:47', '2021-03-16 11:46:47'),
(86, 0, 'created', 'manual', 12, 0, 772, '', 1, '2021-03-16 11:51:54', '2021-03-16 11:51:54'),
(87, 0, 'created', 'manual', 12, 0, 773, '', 1, '2021-03-16 11:53:50', '2021-03-16 11:53:50'),
(88, 0, 'created', 'manual', 12, 0, 774, '', 1, '2021-03-16 11:55:50', '2021-03-16 11:55:50'),
(89, 0, 'created', 'manual', 12, 0, 775, '', 1, '2021-03-16 11:59:25', '2021-03-16 11:59:25'),
(90, 0, 'created', 'manual', 12, 0, 776, '', 1, '2021-03-16 12:01:53', '2021-03-16 12:01:53'),
(91, 0, 'created', 'manual', 12, 0, 777, '', 1, '2021-03-16 12:04:21', '2021-03-16 12:04:21'),
(92, 0, 'created', 'manual', 12, 0, 778, '', 1, '2021-03-16 12:06:23', '2021-03-16 12:06:23'),
(93, 0, 'created', 'manual', 12, 0, 779, '', 1, '2021-03-16 12:07:43', '2021-03-16 12:07:43'),
(94, 0, 'created', 'manual', 12, 0, 780, '', 1, '2021-03-16 12:09:59', '2021-03-16 12:09:59'),
(95, 0, 'created', 'manual', 12, 0, 781, '', 1, '2021-03-16 12:12:15', '2021-03-16 12:12:15'),
(96, 0, 'created', 'manual', 12, 0, 782, '', 1, '2021-03-16 12:13:22', '2021-03-16 12:13:22'),
(97, 0, 'created', 'manual', 12, 0, 783, '', 1, '2021-03-16 12:16:14', '2021-03-16 12:16:14'),
(98, 0, 'created', 'manual', 12, 0, 784, '', 1, '2021-03-16 12:17:37', '2021-03-16 12:17:37'),
(99, 0, 'created', 'manual', 12, 0, 785, '', 1, '2021-03-16 12:19:54', '2021-03-16 12:19:54'),
(100, 0, 'created', 'manual', 12, 0, 786, '', 1, '2021-03-16 12:21:12', '2021-03-16 12:21:12'),
(101, 0, 'created', 'manual', 12, 0, 787, '', 1, '2021-03-16 12:26:12', '2021-03-16 12:26:12'),
(102, 0, 'created', 'manual', 12, 0, 788, '', 1, '2021-03-16 12:27:40', '2021-03-16 12:27:40'),
(103, 0, 'created', 'manual', 12, 0, 789, '', 1, '2021-03-16 12:31:48', '2021-03-16 12:31:48'),
(104, 0, 'created', 'manual', 12, 0, 790, '', 1, '2021-03-16 12:35:05', '2021-03-16 12:35:05'),
(105, 0, 'created', 'manual', 12, 0, 791, '', 1, '2021-03-16 12:36:41', '2021-03-16 12:36:41'),
(106, 0, 'created', 'manual', 12, 0, 792, '', 1, '2021-03-16 12:38:06', '2021-03-16 12:38:06'),
(107, 0, 'created', 'manual', 12, 0, 793, '', 1, '2021-03-16 12:40:30', '2021-03-16 12:40:30'),
(108, 0, 'created', 'manual', 12, 0, 794, '', 1, '2021-03-16 12:42:24', '2021-03-16 12:42:24'),
(109, 0, 'created', 'manual', 12, 0, 795, '', 1, '2021-03-16 12:44:17', '2021-03-16 12:44:17'),
(110, 0, 'created', 'manual', 12, 0, 796, '', 1, '2021-03-22 12:34:30', '2021-03-22 12:34:30'),
(111, 0, 'created', 'manual', 12, 0, 797, '', 1, '2021-03-22 12:38:39', '2021-03-22 12:38:39'),
(112, 0, 'created', 'manual', 12, 0, 798, '', 1, '2021-03-22 12:40:16', '2021-03-22 12:40:16'),
(113, 0, 'created', 'manual', 12, 0, 799, '', 1, '2021-03-22 12:52:50', '2021-03-22 12:52:50'),
(114, 0, 'created', 'manual', 12, 0, 800, '', 1, '2021-03-22 12:54:28', '2021-03-22 12:54:28'),
(115, 0, 'created', 'manual', 12, 0, 801, '', 1, '2021-03-22 12:55:40', '2021-03-22 12:55:40'),
(116, 0, 'created', 'manual', 12, 0, 802, '', 1, '2021-03-22 12:56:46', '2021-03-22 12:56:46'),
(117, 0, 'created', 'manual', 12, 0, 803, '', 1, '2021-03-22 12:57:44', '2021-03-22 12:57:44'),
(118, 0, 'created', 'manual', 12, 0, 804, '', 1, '2021-03-22 12:58:51', '2021-03-22 12:58:51'),
(119, 0, 'created', 'manual', 12, 0, 805, '', 1, '2021-03-22 13:00:06', '2021-03-22 13:00:06'),
(120, 0, 'created', 'manual', 12, 0, 806, '', 1, '2021-03-22 13:01:10', '2021-03-22 13:01:10'),
(121, 0, 'created', 'manual', 12, 0, 807, '', 1, '2021-03-22 13:02:22', '2021-03-22 13:02:22'),
(122, 0, 'created', 'manual', 12, 0, 808, '', 1, '2021-03-22 13:03:35', '2021-03-22 13:03:35'),
(123, 0, 'created', 'manual', 12, 0, 809, '', 1, '2021-03-22 13:04:53', '2021-03-22 13:04:53'),
(124, 0, 'created', 'manual', 12, 0, 810, '', 1, '2021-03-23 11:29:14', '2021-03-23 11:29:14'),
(125, 0, 'created', 'manual', 12, 0, 811, '', 1, '2021-03-23 11:30:42', '2021-03-23 11:30:42'),
(126, 0, 'created', 'manual', 12, 0, 812, '', 1, '2021-03-23 11:44:18', '2021-03-23 11:44:18'),
(127, 0, 'created', 'manual', 12, 0, 813, '', 1, '2021-03-23 11:47:23', '2021-03-23 11:47:23'),
(128, 0, 'created', 'manual', 12, 0, 814, '', 1, '2021-03-25 10:29:48', '2021-03-25 10:29:48'),
(129, 0, 'created', 'manual', 12, 0, 815, '', 1, '2021-03-25 10:31:07', '2021-03-25 10:31:07'),
(130, 0, 'created', 'manual', 12, 0, 816, '', 1, '2021-03-25 10:32:07', '2021-03-25 10:32:07'),
(131, 0, 'created', 'manual', 12, 0, 817, '', 1, '2021-03-25 10:34:15', '2021-03-25 10:34:15'),
(132, 0, 'created', 'manual', 12, 0, 818, '', 1, '2021-03-25 10:35:51', '2021-03-25 10:35:51'),
(133, 0, 'created', 'manual', 12, 0, 819, '', 1, '2021-03-25 10:36:52', '2021-03-25 10:36:52'),
(134, 0, 'created', 'manual', 12, 0, 820, '', 1, '2021-03-25 10:38:01', '2021-03-25 10:38:01'),
(135, 0, 'created', 'manual', 12, 0, 821, '', 1, '2021-03-25 10:39:04', '2021-03-25 10:39:04'),
(136, 0, 'created', 'manual', 12, 0, 822, '', 1, '2021-03-25 10:41:38', '2021-03-25 10:41:38'),
(137, 0, 'created', 'manual', 12, 0, 823, '', 1, '2021-03-25 10:42:44', '2021-03-25 10:42:44'),
(138, 0, 'created', 'manual', 12, 0, 824, '', 1, '2021-03-25 10:44:08', '2021-03-25 10:44:08'),
(139, 0, 'created', 'manual', 12, 0, 825, '', 1, '2021-03-25 10:45:12', '2021-03-25 10:45:12'),
(140, 0, 'created', 'manual', 12, 0, 826, '', 1, '2021-03-25 10:46:12', '2021-03-25 10:46:12'),
(141, 0, 'created', 'manual', 12, 0, 827, '', 1, '2021-03-25 10:47:18', '2021-03-25 10:47:18'),
(142, 0, 'created', 'manual', 12, 0, 828, '', 1, '2021-03-25 11:49:50', '2021-03-25 11:49:50'),
(143, 0, 'created', 'manual', 12, 0, 829, '', 1, '2021-03-25 11:53:30', '2021-03-25 11:53:30'),
(144, 0, 'created', 'manual', 12, 0, 830, '', 1, '2021-03-29 10:25:53', '2021-03-29 10:25:53'),
(145, 0, 'created', 'manual', 12, 0, 831, '', 1, '2021-03-29 10:27:00', '2021-03-29 10:27:00'),
(146, 0, 'created', 'manual', 12, 0, 832, '', 1, '2021-03-29 10:27:52', '2021-03-29 10:27:52'),
(147, 0, 'created', 'manual', 12, 0, 833, '', 1, '2021-03-29 10:29:25', '2021-03-29 10:29:25'),
(148, 0, 'created', 'manual', 12, 0, 834, '', 1, '2021-03-29 10:30:30', '2021-03-29 10:30:30'),
(149, 0, 'created', 'manual', 12, 0, 835, '', 1, '2021-03-29 10:31:46', '2021-03-29 10:31:46'),
(150, 0, 'created', 'manual', 12, 0, 836, '', 1, '2021-03-29 10:32:55', '2021-03-29 10:32:55'),
(151, 0, 'created', 'manual', 12, 0, 837, '', 1, '2021-03-29 10:33:54', '2021-03-29 10:33:54'),
(152, 0, 'created', 'manual', 12, 0, 838, '', 1, '2021-03-29 10:35:34', '2021-03-29 10:35:34'),
(153, 0, 'created', 'manual', 12, 0, 839, '', 1, '2021-03-29 10:36:27', '2021-03-29 10:36:27'),
(154, 0, 'created', 'manual', 12, 0, 840, '', 1, '2021-03-29 10:37:25', '2021-03-29 10:37:25'),
(155, 0, 'created', 'manual', 12, 0, 841, '', 1, '2021-03-29 10:39:59', '2021-03-29 10:39:59'),
(156, 0, 'created', 'manual', 12, 0, 842, '', 1, '2021-03-29 10:41:05', '2021-03-29 10:41:05'),
(157, 0, 'created', 'manual', 12, 0, 843, '', 1, '2021-03-29 10:42:32', '2021-03-29 10:42:32'),
(158, 0, 'created', 'manual', 12, 0, 844, '', 1, '2021-03-29 10:43:32', '2021-03-29 10:43:32'),
(159, 0, 'created', 'manual', 12, 0, 845, '', 1, '2021-03-29 10:44:49', '2021-03-29 10:44:49'),
(160, 0, 'created', 'manual', 12, 0, 846, '', 1, '2021-03-29 10:45:57', '2021-03-29 10:45:57'),
(161, 0, 'created', 'manual', 12, 0, 847, '', 1, '2021-04-01 14:17:04', '2021-04-01 14:17:04'),
(162, 0, 'created', 'manual', 12, 0, 848, '', 1, '2021-04-01 19:36:01', '2021-04-01 19:36:01'),
(163, 0, 'created', 'manual', 12, 0, 849, '', 1, '2021-04-05 18:38:00', '2021-04-05 18:38:00'),
(164, 0, 'created', 'manual', 12, 0, 850, '', 1, '2021-04-05 18:42:29', '2021-04-05 18:42:29'),
(165, 0, 'created', 'manual', 12, 0, 851, '', 1, '2021-04-07 14:29:26', '2021-04-07 14:29:26'),
(166, 0, 'created', 'manual', 12, 0, 852, '', 1, '2021-04-07 16:12:44', '2021-04-07 16:12:44'),
(167, 0, 'created', 'manual', 12, 0, 853, '', 1, '2021-04-07 16:18:47', '2021-04-07 16:18:47'),
(168, 0, 'created', 'manual', 12, 0, 854, '', 1, '2021-04-07 16:30:12', '2021-04-07 16:30:12'),
(169, 0, 'created', 'manual', 12, 0, 855, '', 1, '2021-04-07 16:36:04', '2021-04-07 16:36:04'),
(170, 0, 'created', 'manual', 12, 0, 856, '', 1, '2021-04-07 16:43:13', '2021-04-07 16:43:13'),
(171, 0, 'created', 'manual', 12, 0, 857, '', 1, '2021-04-10 14:47:09', '2021-04-10 14:47:09'),
(172, 0, 'created', 'manual', 12, 0, 858, '', 1, '2021-04-10 14:49:19', '2021-04-10 14:49:19'),
(173, 0, 'created', 'manual', 1, 0, 860, '', 1, '2021-07-26 17:10:18', '2021-07-26 17:10:18'),
(174, 0, 'created', 'manual', 1, 0, 861, '', 1, '2021-07-27 11:31:30', '2021-07-27 11:31:30'),
(175, 0, 'assigned', 'manual', 1, 6, 1, 'sample', 1, '2021-07-29 17:43:22', '2021-07-29 17:43:22'),
(176, 0, 'created', 'manual', 1, 0, 862, '', 1, '2021-08-23 15:42:54', '2021-08-23 15:42:54'),
(177, 0, 'assigned', 'manual', 1, 7, 862, 'urgent please follow up', 1, '2021-08-23 15:44:41', '2021-08-23 15:44:41'),
(178, 0, 'assigned', 'manual', 1, 7, 2, 'Test', 1, '2021-08-24 15:13:23', '2021-08-24 15:13:23'),
(179, 0, 'reassigned', 'manual', 1, 8, 2, 'S', 1, '2021-08-24 16:53:48', '2021-08-24 16:53:48'),
(180, 0, 'created', 'manual', 1, 0, 863, '', 1, '2021-08-26 10:10:05', '2021-08-26 10:10:05'),
(181, 0, 'assigned', 'manual', 1, 6, 3, 'Test', 1, '2021-08-26 11:23:42', '2021-08-26 11:23:42'),
(182, 0, 'assigned', 'manual', 1, 6, 26, 'Test', 1, '2021-08-26 14:08:59', '2021-08-26 14:08:59'),
(183, 0, 'created', 'manual', 1, 0, 864, '', 1, '2021-08-26 14:11:11', '2021-08-26 14:11:11'),
(184, 0, 'assigned', 'manual', 1, 6, 864, 'test', 1, '2021-08-26 14:11:31', '2021-08-26 14:11:31');

-- --------------------------------------------------------

--
-- Table structure for table `lead_source_tbl`
--

CREATE TABLE `lead_source_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `lead_source` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_source_tbl`
--

INSERT INTO `lead_source_tbl` (`id`, `branch_id`, `token`, `lead_source`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'fb', 'Fb', 1, 1, '2020-09-22 11:39:38', '2020-09-22 11:39:38'),
(2, 0, 'instagram', 'Instagram', 1, 1, '2020-09-22 11:40:01', '2020-09-22 11:40:01'),
(3, 0, 'goggle-ads', 'goggle Ads', 1, 1, '2020-09-22 11:40:24', '2020-09-22 11:40:24'),
(4, 0, 'facebook', 'Facebook', 1, 1, '2020-10-01 16:29:40', '2020-10-01 16:29:40'),
(5, 0, 'google', 'Google', 1, 1, '2020-10-01 16:29:40', '2020-10-01 16:29:40'),
(6, 0, 'directcall', 'Direct Call', 1, 1, '2020-10-08 12:33:07', '2020-10-08 12:33:07'),
(7, 0, 'ig', 'ig', 1, 8, '2020-11-03 17:08:45', '2020-11-03 17:08:45'),
(9, 0, 'fairyland', 'Fairyland', 1, 12, '2021-02-04 16:44:54', '2021-02-04 16:44:54'),
(10, 0, 'chatbot', 'Chatbot', 1, 1, '2021-02-24 14:43:04', '2021-02-24 14:43:04');

-- --------------------------------------------------------

--
-- Table structure for table `lead_tbl`
--

CREATE TABLE `lead_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `import_token` varchar(255) DEFAULT NULL,
  `uid` varchar(250) DEFAULT NULL,
  `auth_type` varchar(250) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `lead_date` varchar(250) DEFAULT NULL,
  `enquiry_date` varchar(100) DEFAULT NULL,
  `fname` varchar(250) NOT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `leadsource` varchar(250) NOT NULL,
  `designation` varchar(200) NOT NULL,
  `has_chit` int(11) NOT NULL,
  `has_deposite` int(11) NOT NULL,
  `has_vas` int(11) NOT NULL,
  `chit_id` int(11) NOT NULL,
  `deposite_id` int(11) NOT NULL,
  `vas_id` int(11) NOT NULL,
  `lead_status` varchar(150) DEFAULT NULL,
  `flattype_id` int(11) DEFAULT NULL,
  `site_visit_status` varchar(250) DEFAULT NULL,
  `site_visit_date` varchar(100) DEFAULT NULL,
  `customer_profile_id` int(11) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `mobile` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `secondary_mobile` varchar(255) DEFAULT NULL,
  `secondary_email` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pincode` varchar(250) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `lead_status_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `assign_status` int(11) DEFAULT NULL,
  `closed_status` int(11) DEFAULT NULL,
  `moved_status` int(1) NOT NULL,
  `move_remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_tbl`
--

INSERT INTO `lead_tbl` (`id`, `branch_id`, `token`, `import_token`, `uid`, `auth_type`, `property_id`, `customer_id`, `lead_date`, `enquiry_date`, `fname`, `lname`, `leadsource`, `designation`, `has_chit`, `has_deposite`, `has_vas`, `chit_id`, `deposite_id`, `vas_id`, `lead_status`, `flattype_id`, `site_visit_status`, `site_visit_date`, `customer_profile_id`, `gender`, `mobile`, `email`, `secondary_mobile`, `secondary_email`, `address`, `city`, `state`, `pincode`, `description`, `lead_status_id`, `employee_id`, `assign_status`, `closed_status`, `moved_status`, `move_remarks`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'venkatesh', 'n7cZkv5FGeiv7zx', 'LE0001', 'excel', 0, 3, '2021-02-24', '2020-09-21', 'Venkatesh', '', '9', '', 1, 1, 0, 2, 1, 0, 'Not Interested', 0, 'Yes', '2020-09-21', 27, 'male', '9003146833', 'admin@postcrm.com', '', '', '', 'Coimbatore', 'Tamil Nadu', '', 'Not Interested', 2, 6, 1, 1, 1, 'sample', 1, 0, 0, '', 0, '', '', 1, '2021-02-24 14:47:05', '2021-08-27 12:14:41'),
(2, 2, 'jagdhesh', 'n7cZkv5FGeiv7zx', 'LE0002', 'excel', 0, 5, '2021-02-24', '2020-09-21', 'Jagdhesh', '', '9', '', 1, 1, 1, 1, 2, 1, 'Other', 0, '', '1899-12-30', 27, 'male', '9994551832', 'jaga@test.com', '', '', '', 'Coimbatore', 'Tamil Nadu', '', 'Budget 25L', 17, 8, 1, 1, 1, 'dsf', 1, 0, 0, '', 0, '', '', 1, '2021-02-24 14:47:05', '2021-08-26 16:55:59'),
(3, 1, 'jayakumar', 'n7cZkv5FGeiv7zx', 'LE0003', 'excel', 0, 0, '2021-02-24', '2020-09-21', 'Jayakumar', '', '9', '', 0, 0, 0, 0, 0, 0, 'Follow up', 0, 'Yes', '2020-09-21', 27, 'M', '9843624243', '', '', '', '', 'Coimbatore', 'Tamil Nadu', '', 'Not reachable', 16, 6, 1, 0, 0, '', 1, 0, 0, '', 1, '2021-08-09 11:59:15', 'sa', 1, '2021-02-24 14:47:05', '2021-08-09 11:59:15'),
(4, 2, 'praveen-kumar', 'n7cZkv5FGeiv7zx', 'LE0004', 'excel', 0, 6, '2021-02-24', '2020-09-25', 'Praveen Kumar', '', '1', '', 0, 0, 0, 0, 0, 0, 'Not Interested', 0, 'Yes', '2020-09-21', 27, 'M', '9894588627', '', '', '', '', 'Coimbatore', 'Tamil Nadu', '', 'Not attending the call', 7, 0, 0, 1, 1, 'DF', 1, 0, 0, '', 0, '', '', 1, '2021-02-24 14:47:05', '2021-02-24 14:47:05'),
(5, 2, 'annadurai-durai', 'n7cZkv5FGeiv7zx', 'LE0005', 'excel', 0, 0, '2021-02-24', '2020-09-25', 'Annadurai Durai', '', '1', '', 0, 0, 0, 0, 0, 0, 'Not Interested', 0, '', '1899-12-30', 27, 'M', '9566666432', '', '', '', '', 'Coimbatore', 'Tamil Nadu', '', 'Not interested', 7, 0, 0, 1, 0, '', 1, 0, 0, '', 0, '', '', 1, '2021-02-24 14:47:05', '2021-02-24 14:47:05'),
(26, 1, 'munish', 'n7cZkv5FGeiv7zx', 'LE0026', 'excel', 0, 7, '2021-02-24', '2020-09-30', 'Munish', '', '1', '', 1, 0, 0, 3, 0, 0, 'Follow up', 0, '', '1899-12-30', 33, 'male', '9842543552', 'munish@gmail.com', '', '', '', 'Coimbatore', 'Tamil Nadu', '', 'Call back later', 2, 6, 1, 1, 1, 'asd', 1, 0, 0, '', 0, '', '', 1, '2021-02-24 14:47:05', '2021-08-27 12:15:17'),
(860, 2, 'sample-test-case', NULL, 'LE0860', NULL, 0, 0, '2021-07-26', '2021-07-26', 'sample test case', '', '2', 'test', 0, 1, 0, 0, 1, 0, NULL, 0, 'yes', NULL, 4, 'male', '9942387100', 'koshik@webykart.com', '', '', '', '', '', '', '', 2, 0, 0, 0, 0, NULL, 1, 0, 0, '', 0, '', '', 1, '2021-07-26 17:10:18', '2021-07-26 20:07:29'),
(861, 1, 'sample-test-case', NULL, 'LE0861', NULL, 0, 0, '2021-07-27', '2021-07-27', 'sample test case', '', '1', '', 0, 0, 0, 0, 0, 0, NULL, 0, '', NULL, 1, 'male', '99423871002', 'koshik2@webykart.com', '', '', '', '', '', '', '', 2, 0, 0, 0, 0, NULL, 1, 1, 1, '2021-07-27 11:32:11', 0, '', '', 1, '2021-07-27 11:31:30', '2021-07-27 11:31:30'),
(862, 1, 'viki', NULL, 'LE0862', NULL, 0, 4, '2021-08-23', '2021-08-23', 'Viki', '', '1', 'IT', 1, 0, 0, 2, 0, 0, NULL, 0, '', NULL, 2, 'male', '9942387101', 'viki@venpep.net', '', '', '', '', '', '', '', 9, 7, 1, 1, 1, 'new customer joined', 1, 0, 0, '', 0, '', '', 1, '2021-08-23 15:42:54', '2021-08-23 15:46:12'),
(863, 0, 'anand', NULL, 'LE0863', NULL, 0, 0, '2021-08-26', '2021-08-26', 'Anand', '', '3', 'Coimbatore', 1, 0, 0, 3, 0, 0, NULL, 0, '', NULL, 79, 'male', '7094265321', 'anandbechris@gmail.com', '7094265321', 'anandbechris@gmail.com', '272/22, Ayynarappan kovil near , v.v.c.r nager , Erode', 'Coimbatore', 'Tamil Nadu', '641018', 'He intrested to join our 5L chit', 2, 0, 0, 0, 0, NULL, 1, 0, 0, '', 0, '', '', 1, '2021-08-26 10:10:04', '2021-08-26 10:10:04'),
(864, 1, 'anand', NULL, 'LE0864', NULL, 0, 0, '2021-08-26', '2021-08-26', 'Anand', '', '1', 'Coimbatore', 1, 0, 0, 3, 0, 0, NULL, 0, '', NULL, 5, 'male', '7094265332', 'anandbechris2@gmail.com', '7094265321', 'anandbechris@gmail.com', '272/22, Ayynarappan kovil near , v.v.c.r nager , Erode', 'Coimbatore', 'Tamil Nadu', '641018', 'zxc', 2, 6, 1, 0, 0, NULL, 1, 0, 0, '', 0, '', '', 1, '2021-08-26 14:11:11', '2021-08-26 14:11:11');

-- --------------------------------------------------------

--
-- Table structure for table `lead_type_tbl`
--

CREATE TABLE `lead_type_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `uid` varchar(250) DEFAULT NULL,
  `token` varchar(250) NOT NULL,
  `lead_type` varchar(250) NOT NULL,
  `default_settings` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_type_tbl`
--

INSERT INTO `lead_type_tbl` (`id`, `branch_id`, `uid`, `token`, `lead_type`, `default_settings`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'LT0001', 'will-think-and-get-back', 'Will think and get back', 0, 1, 1, '2020-08-16 22:22:58', '2020-08-17 19:35:34'),
(2, 0, 'LT0002', 'hot-lead', 'Hot Lead', 1, 1, 1, '2020-08-16 22:23:09', '2020-08-17 19:35:22'),
(3, 0, 'LT0003', 'will-discuss-with-family', 'Will discuss with family', 0, 1, 1, '2020-08-16 22:26:03', '2020-08-17 19:35:41'),
(4, 0, 'LT0004', 'just-for-collecting-info', 'Just for collecting info', 0, 1, 1, '2020-08-17 19:35:53', '2020-08-17 19:35:53'),
(5, 0, 'LT0005', 'do-not-follow-up', 'Do not follow up', 0, 1, 1, '2020-08-17 19:35:59', '2020-08-17 19:35:59'),
(6, 0, 'LT0006', 'interested', 'Interested', 0, 1, 1, '2020-10-08 12:34:27', '2020-10-08 12:34:27'),
(7, 0, 'LT0007', 'notinterested', 'Not Interested', 0, 1, 1, '2020-10-08 12:34:39', '2020-10-08 12:34:39'),
(8, 0, 'LT0008', 'interestedinsomeotherlocation', 'Interested in Some other Location', 0, 1, 1, '2020-10-08 12:35:06', '2020-10-08 12:35:31'),
(9, 0, 'LT0009', 'notattnthecall', 'Not attn the call', 0, 1, 8, '2020-10-09 15:12:55', '2020-10-09 15:12:55'),
(10, 0, 'LT0010', 'lowbudget', 'Low budget', 0, 1, 8, '2020-10-09 16:54:31', '2020-10-09 16:54:31'),
(11, 0, 'LT0011', 'detailsshared', 'Details shared', 0, 1, 8, '2020-10-09 17:14:09', '2020-10-09 17:14:09'),
(12, 0, 'LT0012', 'callback', 'Call back', 0, 1, 8, '2020-10-12 10:49:23', '2020-10-12 10:49:23'),
(13, 0, 'LT0013', 'linebusy', 'Line busy', 0, 1, 8, '2020-10-12 11:05:01', '2020-10-12 11:05:01'),
(14, 0, 'LT0014', 'sitevisitdone', 'Site visit done', 0, 1, 8, '2020-10-15 11:31:19', '2020-10-15 11:31:19'),
(15, 0, 'LT0015', 'notreachable', 'Not reachable', 0, 1, 8, '2020-10-20 16:07:20', '2020-10-20 16:07:20'),
(16, 0, 'LT0016', 'followup', 'Follow up', 0, 1, 1, '2021-02-04 19:36:08', '2021-02-04 19:36:08'),
(17, 0, 'LT0017', 'other', 'Other', 0, 1, 1, '2021-02-04 19:36:08', '2021-02-04 19:36:08'),
(20, 0, NULL, '', '', 0, 1, 1, '2021-02-24 14:47:08', '2021-02-24 14:47:08'),
(21, 0, 'LT0021', '1l', '1L', 0, 1, 1, '2021-07-26 11:58:39', '2021-07-26 11:58:39');

-- --------------------------------------------------------

--
-- Table structure for table `news_tbl`
--

CREATE TABLE `news_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `date` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `email_notification` int(11) NOT NULL,
  `post_to` varchar(250) NOT NULL,
  `customer_ids` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `show_popup` int(11) NOT NULL,
  `popup_till` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `send_mail_status` int(11) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification_email_tbl`
--

CREATE TABLE `notification_email_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `adhoc_permission` int(1) NOT NULL,
  `kyc_permission` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paymentinfo_tbl`
--

CREATE TABLE `paymentinfo_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `name` varchar(250) NOT NULL,
  `account_number` varchar(250) NOT NULL,
  `bank` varchar(250) NOT NULL,
  `ifcs_code` varchar(250) NOT NULL,
  `branch` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `pincode` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project_type_tbl`
--

CREATE TABLE `project_type_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `project_name` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_broucher_tbl`
--

CREATE TABLE `property_broucher_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `property_id` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `brochures` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_document_list_items`
--

CREATE TABLE `property_document_list_items` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `document_id` int(11) NOT NULL,
  `document_list_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `doc_name` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `document_file` varchar(255) NOT NULL,
  `doc_type` varchar(250) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `doc_status` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_document_list_tbl`
--

CREATE TABLE `property_document_list_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `property_id` int(11) NOT NULL,
  `documents_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_gallery_tbl`
--

CREATE TABLE `property_gallery_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `property_id` int(11) NOT NULL,
  `images` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_gendral_documents_tbl`
--

CREATE TABLE `property_gendral_documents_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) DEFAULT NULL,
  `property_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `documents` varchar(250) NOT NULL,
  `document_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_rooms_list_tbl`
--

CREATE TABLE `property_rooms_list_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `property_id` int(11) NOT NULL,
  `roomlist_id` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_tbl`
--

CREATE TABLE `property_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `customer_id` int(11) DEFAULT NULL,
  `token` varchar(250) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `house_facing` varchar(250) NOT NULL,
  `projectsize` varchar(250) NOT NULL,
  `projectarea` varchar(250) NOT NULL,
  `cost` varchar(250) NOT NULL,
  `room_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `pincode` varchar(250) NOT NULL,
  `content` longtext NOT NULL,
  `image` varchar(250) NOT NULL,
  `assign_status` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `request_type_tbl`
--

CREATE TABLE `request_type_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `token` varchar(250) NOT NULL,
  `request_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_type_tbl`
--

INSERT INTO `request_type_tbl` (`id`, `branch_id`, `token`, `request_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'general', 'General', 1, 1, '2020-06-10 11:34:39', '2020-07-08 01:35:33'),
(2, 0, 'report-a-issue', 'Report a issue', 1, 1, '2020-06-19 14:18:14', '2020-06-19 14:18:14'),
(3, 0, 'other', 'Other', 1, 1, '2020-06-19 15:10:34', '2020-06-19 15:10:34'),
(4, 0, 'work-status-update', 'Work Status Update', 1, 1, '2020-06-19 15:17:48', '2020-06-19 15:17:48');

-- --------------------------------------------------------

--
-- Table structure for table `session_tbl`
--

CREATE TABLE `session_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `logged_id` int(11) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `portal_type` varchar(100) NOT NULL,
  `auth_referer` varchar(50) NOT NULL,
  `auth_medium` varchar(50) NOT NULL,
  `auth_user_agent` text NOT NULL,
  `auth_ip_address` varchar(250) NOT NULL,
  `session_in` datetime NOT NULL,
  `session_out` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session_tbl`
--

INSERT INTO `session_tbl` (`id`, `branch_id`, `logged_id`, `user_type`, `portal_type`, `auth_referer`, `auth_medium`, `auth_user_agent`, `auth_ip_address`, `session_in`, `session_out`) VALUES
(1, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '106.222.116.83', '2020-10-06 15:09:01', NULL),
(2, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '49.205.56.60', '2020-10-06 15:39:26', NULL),
(3, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '157.46.108.197', '2020-10-08 13:14:47', NULL),
(4, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '157.46.108.197', '2020-10-08 13:16:14', NULL),
(5, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '157.46.108.197', '2020-10-08 13:18:20', NULL),
(6, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '106.222.96.54', '2020-10-08 13:21:30', NULL),
(7, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '106.222.96.54', '2020-10-08 13:22:46', NULL),
(8, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '106.222.96.54', '2020-10-08 13:24:56', NULL),
(9, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '106.222.96.54', '2020-10-08 13:30:34', NULL),
(10, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '106.222.96.54', '2020-10-08 13:34:32', NULL),
(11, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '49.37.185.119', '2020-10-08 15:35:09', NULL),
(12, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '49.37.199.218', '2020-10-09 14:40:07', NULL),
(13, 0, 7, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', '49.37.199.218', '2020-10-09 14:41:43', NULL),
(14, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '49.37.185.214', '2020-10-12 10:42:48', NULL),
(15, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '49.37.188.237', '2020-10-15 11:17:27', NULL),
(16, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', '49.37.188.126', '2020-10-20 15:23:00', NULL),
(17, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.190.126', '2020-10-27 11:43:35', NULL),
(18, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.187.25', '2020-11-03 14:02:53', NULL),
(19, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', '103.114.209.31', '2020-11-03 14:48:39', NULL),
(20, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.187.25', '2020-11-03 14:53:04', NULL),
(21, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.187.25', '2020-11-03 14:54:20', NULL),
(22, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.187.25', '2020-11-03 14:56:43', NULL),
(23, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.187.25', '2020-11-03 17:08:16', NULL),
(24, 0, 10, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '122.178.175.229', '2020-11-04 10:18:28', NULL),
(25, 0, 10, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0 Mobile/15E148 Safari/604.1', '122.178.175.229', '2020-11-04 10:20:05', NULL),
(26, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', '49.37.184.154', '2020-11-06 10:29:21', NULL),
(27, 0, 10, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.1 Mobile/15E148 Safari/604.1', '122.178.116.241', '2020-11-07 20:21:01', NULL),
(28, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '49.37.217.56', '2020-11-17 10:37:53', NULL),
(29, 0, 9, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '49.37.217.56', '2020-11-17 10:39:28', NULL),
(30, 0, 9, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.3 Safari/605.1.15', '49.37.217.56', '2020-11-17 10:49:13', NULL),
(31, 0, 9, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.3 Safari/605.1.15', '49.37.217.56', '2020-11-17 10:50:27', NULL),
(32, 0, 9, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.3 Safari/605.1.15', '49.37.217.56', '2020-11-17 14:59:27', NULL),
(33, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', '49.206.171.194', '2020-11-17 17:14:26', NULL),
(34, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '106.222.99.119', '2020-11-18 10:56:41', NULL),
(35, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '106.222.99.119', '2020-11-18 10:59:01', NULL),
(36, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '106.222.99.119', '2020-11-18 11:07:06', NULL),
(37, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', '49.206.170.155', '2020-11-18 11:16:52', NULL),
(38, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '49.37.217.80', '2020-11-18 14:25:46', NULL),
(39, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', '49.37.217.80', '2020-11-18 17:00:40', NULL),
(40, 0, 32, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '49.206.163.187', '2020-11-18 17:29:52', NULL),
(41, 0, 40, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '157.46.65.81', '2020-11-18 18:50:05', NULL),
(42, 0, 47, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0 Mobile/15E148 Safari/604.1', '49.205.101.60', '2020-11-18 22:16:49', NULL),
(43, 0, 24, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0', '157.46.122.170', '2020-11-21 18:29:49', NULL),
(44, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '49.206.160.52', '2020-12-01 10:13:57', NULL),
(45, 0, 10, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.1 Mobile/15E148 Safari/604.1', '223.182.201.47', '2020-12-12 16:30:07', NULL),
(46, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '49.37.217.164', '2020-12-14 12:33:00', NULL),
(47, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '106.222.121.184', '2020-12-14 14:36:00', NULL),
(48, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '106.222.121.184', '2020-12-14 14:38:02', NULL),
(49, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '103.114.209.31', '2020-12-14 14:45:05', NULL),
(50, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '49.37.217.164', '2020-12-14 14:47:49', NULL),
(51, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '103.114.209.31', '2020-12-14 14:54:20', NULL),
(52, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '106.222.99.113', '2020-12-15 08:30:10', NULL),
(53, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '49.37.219.226', '2020-12-19 14:28:59', NULL),
(54, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '49.37.217.56', '2020-12-29 16:56:26', NULL),
(55, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '42.106.84.38', '2020-12-29 20:04:42', NULL),
(56, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '49.37.221.112', '2020-12-30 11:33:12', NULL),
(57, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '106.222.121.186', '2020-12-30 11:36:39', NULL),
(58, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '106.222.113.189', '2021-01-08 14:58:13', NULL),
(59, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '49.37.221.220', '2021-01-09 10:59:41', NULL),
(60, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', '49.37.217.80', '2021-01-12 11:10:16', NULL),
(61, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', '49.37.223.186', '2021-01-18 14:46:58', NULL),
(62, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', '106.222.113.181', '2021-01-19 09:21:05', NULL),
(63, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', '49.37.221.220', '2021-01-20 10:01:09', NULL),
(64, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', '49.37.221.232', '2021-01-21 11:08:08', NULL),
(65, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '106.222.113.190', '2021-02-02 14:46:31', NULL),
(66, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '43.225.165.49', '2021-02-02 14:47:30', NULL),
(67, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '106.222.121.187', '2021-02-02 22:12:30', NULL),
(68, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '43.225.167.38', '2021-02-03 10:13:39', NULL),
(69, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '43.225.167.38', '2021-02-03 15:30:56', NULL),
(70, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '106.222.121.187', '2021-02-03 17:06:40', NULL),
(71, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '103.21.79.24', '2021-02-04 10:37:21', NULL),
(72, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '106.222.121.176', '2021-02-04 10:52:22', NULL),
(73, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '103.21.79.24', '2021-02-04 11:03:59', NULL),
(74, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '103.21.79.24', '2021-02-04 11:04:52', NULL),
(75, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '103.21.79.24', '2021-02-04 11:07:01', NULL),
(76, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '103.21.79.24', '2021-02-04 11:15:01', NULL),
(77, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '106.222.121.176', '2021-02-04 11:17:03', NULL),
(78, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', '103.21.79.24', '2021-02-04 12:08:56', NULL),
(79, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.146 Safari/537.36', '103.224.32.255', '2021-02-05 10:03:24', NULL),
(80, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '49.206.172.20', '2021-02-11 10:10:38', NULL),
(81, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '49.206.168.4', '2021-02-12 10:05:54', NULL),
(82, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '49.206.168.4', '2021-02-12 15:15:39', NULL),
(83, 0, 8, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Linux; Android 8.0.0; AUM-AL20) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.96 Mobile Safari/537.36', '42.106.87.163', '2021-02-12 19:58:09', NULL),
(84, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '49.206.185.12', '2021-02-15 13:55:39', NULL),
(85, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '49.206.184.118', '2021-02-16 10:30:49', NULL),
(86, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '117.98.176.106', '2021-02-17 10:10:47', NULL),
(87, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', '117.98.176.106', '2021-02-18 11:27:23', NULL),
(88, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', '122.178.82.69', '2021-02-22 10:11:48', NULL),
(89, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', '122.178.82.69', '2021-02-23 10:00:22', NULL),
(90, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '122.178.82.69', '2021-02-24 09:54:45', NULL),
(91, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', '106.222.99.96', '2021-02-24 12:46:11', NULL),
(92, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '122.178.82.69', '2021-02-25 10:28:26', NULL),
(93, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '49.206.160.74', '2021-02-26 10:16:48', NULL),
(94, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.34.227', '2021-02-27 09:57:27', NULL),
(95, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.21.79.20', '2021-03-01 09:46:44', NULL),
(96, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.21.79.20', '2021-03-01 13:06:19', NULL),
(97, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '106.222.121.185', '2021-03-01 13:07:16', NULL),
(98, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.33.87', '2021-03-04 10:17:52', NULL),
(99, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.33.87', '2021-03-04 10:17:52', NULL),
(100, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.33.88', '2021-03-05 09:55:29', NULL),
(101, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.34.234', '2021-03-08 10:21:37', NULL),
(102, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '122.178.195.58', '2021-03-09 10:07:24', NULL),
(103, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '43.225.167.197', '2021-03-10 10:00:14', NULL),
(104, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '43.225.165.51', '2021-03-11 10:13:50', NULL),
(105, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.33.68', '2021-03-12 10:11:51', NULL),
(106, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.32.254', '2021-03-13 10:16:00', NULL),
(107, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '103.224.32.254', '2021-03-13 12:30:26', NULL),
(108, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', '103.224.33.73', '2021-03-15 10:36:32', NULL),
(109, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', '103.224.33.73', '2021-03-16 10:25:44', NULL),
(110, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.32.245', '2021-03-17 10:35:57', NULL),
(111, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '43.225.165.50', '2021-03-18 10:09:54', NULL),
(112, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.34.248', '2021-03-19 10:07:44', NULL),
(113, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.21.79.6', '2021-03-22 10:15:38', NULL),
(114, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.33.95', '2021-03-23 10:22:36', NULL),
(115, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '43.225.165.52', '2021-03-24 10:15:15', NULL),
(116, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.21.79.11', '2021-03-25 10:16:39', NULL),
(117, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.21.79.11', '2021-03-25 18:15:56', NULL),
(118, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.33.92', '2021-03-26 10:03:33', NULL),
(119, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.33.76', '2021-03-27 10:01:16', NULL),
(120, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.32.234', '2021-03-29 10:23:47', NULL),
(121, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '106.222.113.186', '2021-03-29 13:09:37', NULL),
(122, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.224.32.250', '2021-03-30 10:00:57', NULL),
(123, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.21.79.21', '2021-03-31 09:58:12', NULL),
(124, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '103.21.79.21', '2021-03-31 10:07:41', NULL),
(125, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', '157.49.81.8', '2021-04-01 08:52:31', NULL),
(126, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', '106.211.210.45', '2021-04-05 12:13:33', NULL),
(127, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', '106.211.210.48', '2021-04-07 14:27:03', NULL),
(128, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', '157.51.44.201', '2021-04-08 13:14:37', NULL),
(129, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', '157.51.165.95', '2021-04-09 15:35:02', NULL),
(130, 0, 12, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', '157.51.188.246', '2021-04-10 10:52:13', NULL),
(131, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0', '127.0.0.1', '2021-04-12 14:40:21', NULL),
(132, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0', '127.0.0.1', '2021-07-08 23:12:13', NULL),
(133, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-07-27 00:34:13', NULL),
(134, 0, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-07-27 01:23:34', NULL),
(135, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-04 10:11:39', NULL),
(136, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-04 10:13:28', NULL),
(137, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-04 10:21:32', NULL),
(138, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-04 10:22:21', NULL),
(139, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-04 11:10:35', NULL),
(140, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-09 10:16:51', NULL),
(141, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '::1', '2021-08-09 10:22:34', NULL),
(142, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '::1', '2021-08-09 10:22:58', NULL),
(143, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '::1', '2021-08-09 10:25:39', NULL),
(144, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-09 10:58:50', NULL),
(145, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-09 11:00:19', NULL),
(146, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '::1', '2021-08-09 11:01:06', NULL),
(147, 0, 7, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '::1', '2021-08-09 11:04:36', NULL),
(148, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-09 11:13:24', NULL),
(149, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-09 11:14:58', NULL),
(150, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '::1', '2021-08-09 11:15:32', NULL),
(151, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0', '127.0.0.1', '2021-08-09 11:58:50', NULL),
(152, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', '127.0.0.1', '2021-08-14 13:32:12', NULL),
(153, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', '127.0.0.1', '2021-08-14 19:40:26', NULL),
(154, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', '127.0.0.1', '2021-08-23 15:32:30', NULL),
(155, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', '127.0.0.1', '2021-08-23 15:36:09', NULL),
(156, 0, 13, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', '127.0.0.1', '2021-08-23 15:54:03', NULL),
(157, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 08:33:25', NULL),
(158, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 08:47:55', NULL),
(159, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 08:48:57', NULL),
(160, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 08:50:51', NULL),
(161, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 09:12:43', NULL),
(162, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 09:59:32', NULL),
(163, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 10:45:49', NULL),
(164, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:32:19', NULL),
(165, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:32:45', NULL),
(166, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:33:20', NULL),
(167, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:33:57', NULL),
(168, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:34:23', NULL),
(169, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:34:48', NULL),
(170, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:35:32', NULL),
(171, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:35:49', NULL),
(172, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:36:30', NULL),
(173, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:40:28', NULL),
(174, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 14:57:52', NULL),
(175, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:00:53', NULL),
(176, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:01:47', NULL),
(177, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:09:36', NULL),
(178, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:09:57', NULL),
(179, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:12:59', NULL),
(180, 0, 7, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:13:53', NULL),
(181, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:24:48', NULL),
(182, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:44:40', NULL),
(183, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:50:41', NULL),
(184, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:51:03', NULL),
(185, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:54:33', NULL),
(186, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:55:57', NULL),
(187, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:56:30', NULL),
(188, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 15:56:42', NULL),
(189, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 16:25:42', NULL),
(190, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 16:25:55', NULL),
(191, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 16:29:08', NULL),
(192, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 16:29:30', NULL),
(193, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 16:30:21', NULL),
(194, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:06:17', NULL),
(195, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:08:55', NULL),
(196, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:16:35', NULL),
(197, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:42:30', NULL),
(198, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:42:50', NULL),
(199, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:44:24', NULL),
(200, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:50:22', NULL),
(201, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:55:17', NULL),
(202, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 17:59:24', NULL),
(203, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:00:05', NULL),
(204, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:05:35', NULL),
(205, 0, 14, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:06:56', NULL),
(206, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:10:58', NULL),
(207, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:11:50', NULL),
(208, 0, 15, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:13:31', NULL),
(209, 0, 14, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:13:47', NULL),
(210, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:14:46', NULL),
(211, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:21:13', NULL),
(212, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:28:58', NULL),
(213, 0, 14, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:29:16', NULL),
(214, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-24 18:29:23', NULL),
(215, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-25 09:24:30', NULL),
(216, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-25 09:25:25', NULL),
(217, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-25 09:57:35', NULL),
(218, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:05:22', NULL),
(219, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:07:40', NULL),
(220, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:08:11', NULL),
(221, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:25:32', NULL),
(222, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:26:10', NULL),
(223, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:36:41', NULL),
(224, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 10:37:53', NULL),
(225, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 11:42:03', NULL),
(226, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 13:17:04', NULL),
(227, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-26 16:19:49', NULL),
(228, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-27 09:50:44', NULL),
(229, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-27 10:44:39', NULL),
(230, 0, 6, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-27 11:00:15', NULL),
(231, 0, 1, 'admin', 'SS CHIT', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '::1', '2021-08-27 11:14:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `temp_customer_excel_tbl`
--

CREATE TABLE `temp_customer_excel_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `upload_token` varchar(50) NOT NULL,
  `name` varchar(250) NOT NULL,
  `addresss` varchar(255) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `email` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_excel_tbl`
--

CREATE TABLE `temp_excel_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `upload_token` varchar(50) NOT NULL,
  `uid` varchar(80) DEFAULT NULL,
  `enquiry_date` varchar(50) NOT NULL,
  `fname` varchar(250) NOT NULL,
  `lname` varchar(250) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `primary_mobile` varchar(200) NOT NULL,
  `primary_email` varchar(200) NOT NULL,
  `profile` varchar(250) NOT NULL,
  `leadsource` varchar(200) NOT NULL,
  `lead_status` varchar(50) NOT NULL,
  `flat_type` varchar(250) NOT NULL,
  `site_visited` varchar(250) NOT NULL,
  `site_visit_date` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `pincode` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_excel_tbl`
--

INSERT INTO `temp_excel_tbl` (`id`, `branch_id`, `upload_token`, `uid`, `enquiry_date`, `fname`, `lname`, `gender`, `primary_mobile`, `primary_email`, `profile`, `leadsource`, `lead_status`, `flat_type`, `site_visited`, `site_visit_date`, `address`, `city`, `state`, `description`, `pincode`, `created_at`, `updated_at`, `status`) VALUES
(1, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-21', 'Venkatesh', '', 'M', '9003146833', '', '', 'Fairyland', 'Not Interested', '', 'Yes', '2020-09-21', '', 'Coimbatore', 'Tamil Nadu', 'Not Interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(2, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-21', 'Jagdhesh', '', 'M', '9994551832', '', '', 'Fairyland', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Budget 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(3, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-21', 'Jayakumar', '', 'M', '9843624243', '', '', 'Fairyland', 'Follow up', '', 'Yes', '2020-09-21', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(4, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-25', 'Praveen Kumar', '', 'M', '9894588627', '', '', 'fb', 'Not Interested', '', 'Yes', '2020-09-21', '', 'Coimbatore', 'Tamil Nadu', 'Not attending the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(5, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-25', 'Annadurai Durai', '', 'M', '9566666432', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(6, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-27', 'Chella Ananthanarayanan', '', 'M', '7397395417', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in Trichy Road Location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(7, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-27', 'Nandhini Ramaraj', '', 'M', '9025300016', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(8, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-27', 'chandrasekar', '', 'M', '9790270167', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in Perur Location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(9, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-27', 'Selvam', '', 'M', '9487702679', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He said he had no intention of buying a house now', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(10, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-27', 'Balasubramani', '', 'M', '9080110323', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(11, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-27', 'samyuktha karthikeyan', '', 'F', '6381705166', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in Singanallur, ondipudur Locations', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(12, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-28', 'Pushpalatha Venkatapathy', '', 'F', '8072641567', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in Saravanampatti', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(13, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-28', 'Nithya Ramasamy', '', 'F', '9902216007', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(14, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-28', 'Saranya Priyadharshini', '', 'F', '9894813328', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in avinash road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(15, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-28', 'Annadurai Durai', '', 'M', '9566666432', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(16, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-28', 'Arul Raj', '', 'M', '9843111200', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(17, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-28', 'Subathirai Mani', '', 'F', '9894340500', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Her budget 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(18, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-28', 'Mani maran', '', 'M', '9003790601', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(19, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-29', 'Pramod Kanakillam', '', 'M', '9382127272', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(20, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-29', 'Ragupathy Varatharajulu', '', 'M', '9443550688', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(21, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-29', 'Antony Raj', '', 'M', '9003769480', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(22, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-29', 'Abdhulkadhar', '', 'M', '9843019123', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(23, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-29', 'Dr. B. Sangeetha Priya', '', 'F', '7010991879', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(24, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-29', 'Nithya Bharathi', '', 'F', '9788362445', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(25, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-30', 'Asha Nair', '', 'F', '9843521828', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(26, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-30', 'S Raju', '', 'M', '9360720405', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(27, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-30', 'Munish', '', 'M', '9842543552', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(28, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-09-30', 'Radhakrishana', '', 'M', '9790000086', '', '', 'fb', 'Other', '', 'Yes', '2020-10-06', '', 'Coimbatore', 'Tamil Nadu', 'Location not good, prefer some other location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(29, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-01', 'SaviDinesh', '', 'M', '9843513315', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested(expected price 30L)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(30, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-01', 'Kamalakkannan Ramakrishnan', '', 'M', '9894030057', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(31, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-05', 'Girija Jaganathan', '', 'F', '9043764630', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in Ramanathapuram', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(32, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-05', 'vetri', '', 'M', '7904531604', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Busy', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(33, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-04', 'Sundarrajan Vijaykumar', '', 'M', '9976008840', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Need rental house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(34, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-04', 'Priya ShivakumarjbdW..', '', 'F', '9551270901', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'she want to ask land only.', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(35, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-04', 'Rupa Menghraj', '', 'F', '9080731034', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in avinash road, trichy road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(36, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-04', 'manohar lal', '', 'M', '9344826080', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(37, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-03', 'rahul', '', 'M', '8012977079', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in Saibaba colony side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(38, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-03', 'Selvakumar Sankarraj', '', 'M', '9843523313', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Expected price 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(39, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-03', 'Shamuvel Jaisanker', '', 'M', '9840066814', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in Trichy road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(40, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-03', 'Ankit Kumar Nair', '', 'M', '9843578487', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(41, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-03', 'Suki', '', 'M', '9843231913', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in avinash road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(42, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-02', 'Manoj', '', 'M', '9786353000', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Now, no idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(43, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-02', 'Asha Nair', '', 'F', '9843521828', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(44, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-02', 'Baskar Boopathy', '', 'M', '9791942007', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in Saravanampatti', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(45, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-02', 'Punitha Amalaraj', '', 'F', '9943331964', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan changed - Currently Not Interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(46, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-02', 'Latha Bhuvaneshwari', '', 'F', '9566158574', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Her budget is 35 L(Not interested)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(47, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-01', 'Donald Abraham', '', 'M', '9842219223', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not response properly', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(48, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-01', 'Radhakrishnan Thangavelu', '', 'M', '9842192715', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(49, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-01', 'Jagadesh K', '', 'M', '9994551832', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget 15 L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(50, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-05', 'Lakshmi', '', 'F', '9500636481', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in Vadavalli', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(51, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-05', 'umamaheswari', '', 'F', '9095206555', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in vadavalli', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(52, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-05', 'Haridev Gokulchandran', '', 'M', '9894089334', '', '', 'fb', 'Interested', '', 'Yes', '2020-10-11', '', 'Coimbatore', 'Tamil Nadu', 'Advanced received', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(53, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-05', 'Helen Cathrine', '', 'F', '9962433311', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interesed in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(54, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-05', 'Rathi Gopal', '', 'M', '9443682700', '', '', 'fb', 'Other', '', 'Yes', '2020-10-06', '', 'Coimbatore', 'Tamil Nadu', 'Not interested(vastu problem)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(55, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-06', 'à®šà¯†à®¨à¯à®¤à®¿à®²à¯à®¨à®¾à®¤à®©à¯', '', 'M', '9488924550', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in thondamputhur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(56, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-06', 'SnehaSivasubramanian', '', 'F', '8754734342', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(57, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-07', 'Bhuvaneswari', '', 'F', '9994023741', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(58, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-07', 'Sathya Priya', '', 'F', '9597821781', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She expected price 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(59, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-08', 'P Rajavelu', '', 'M', '9842249009', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in sowripalayam', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(60, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-08', 'Jayachandran Perumal', '', 'M', '9486244100', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expected price 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(61, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-08', 'Vijay Velu', '', 'M', '9994279923', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested(Not interested in pothanur area)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(62, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-09', 'Ashok Kumar', '', 'M', '8130407654', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will call back if needed', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(63, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-09', 'Malathi', '', 'F', '7540034090', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in ramanatha puram', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(64, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-09', 'Jagdhesh', '', 'M', '9994551832', '', '', 'Fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(65, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-10', 'Karthikeyan Bhaskaran', '', 'M', '9489609158', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(66, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-11', 'CA Shyam Sundar', '', 'M', '9843342044', '', '', 'fb', 'Other', '', 'Yes', '2021-01-03', '', 'Coimbatore', 'Tamil Nadu', 'Interested in singanallur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(67, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-12', 'Balasubramani', '', 'M', '9080110323', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(68, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-13', 'Priscilla Esther', '', 'F', '9894157611', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(69, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-13', 'pandu rangan', '', 'M', '8973106490', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Switchoff', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(70, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-15', 'Sunoj Adil', '', 'M', '9513323335', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(71, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-16', 'Vinoth Kumar', '', 'M', '9994605999', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in ganapathy,sarvanamapatti(Details shared)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(72, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-16', 'SuchitraUmamahesh', '', 'F', '7418585959', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in Vadavalli area', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(73, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-16', 'Sarath Joshua Revival', '', 'M', '7708939358', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Now, no idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(74, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-16', 'Shabrin', '', 'M', '9790382375', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested(He want more amenities)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(75, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-16', 'vigneshwaran harish', '', 'M', '8508083379', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong Number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(76, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-17', 'Sridhar V', '', 'M', '9087179652', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call barred', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(77, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-17', 'Vivek Kumar', '', 'M', '7904684325', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(78, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-18', 'Sarah', '', 'F', '9597823558', '', '', 'fb', 'Other', '', 'Yes', '2020-11-01', '', 'Coimbatore', 'Tamil Nadu', 'Her Budget is 30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(79, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-18', 'Rajesh Kathirvel', '', 'M', '9566581975', '', '', 'fb', 'Other', '', 'Yes', '2020-10-18', '', 'Coimbatore', 'Tamil Nadu', 'Need a Luxury villa', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(80, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-18', 'Muthu Raman', '', 'M', '8344488877', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested (He is not intending to buy house now)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(81, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-18', 'Ooty Arun', '', 'M', '9442563387', '', '', 'ig', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Need to send Aprtment villa details, details sent', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(82, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-18', 'Sajeev Nair', '', 'M', '9442064912', '', '', 'fb', 'Other', '', 'Yes', '2020-11-02', '', 'Coimbatore', 'Tamil Nadu', 'Area not good', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(83, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-18', 'Shahul hameed', '', 'M', '9952307551', '', '', 'Direct call', 'Not Interested', '', 'Yes', '2020-10-18', '', 'Coimbatore', 'Tamil Nadu', 'Not interested( price is high) - Loan issue', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(84, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-18', 'nandhini', '', 'F', '8056890388', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Already purchased in Ganapathy area', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(85, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-18', 'GJ', '', 'M', '9080936845', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(86, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-19', 'Radha Krishnan', '', 'M', '9942419218', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not response properly', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(87, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-21', 'Jemima', '', 'F', '9843811043', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(88, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-21', 'suresh', '', 'M', '9500516615', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Now no idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(89, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-21', 'Velmurugan', '', 'M', '9865054538', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Busy', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(90, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-21', 'Karthiga Nithya', '', 'F', '9566618226', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She expected price 25L to 30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(91, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-21', 'shanthi', '', 'F', '9952210010', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(92, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-21', 'Gopish Krishnan', '', 'M', '9585445555', '', '', 'ig', 'Interested', '', 'Yes', '2020-12-02', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(93, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-22', 'Anees', '', 'M', '7867823042', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(94, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-22', 'MJ Rahman', '', 'M', '9092242780', '', '', 'ig', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will visit Next week', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(95, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-22', 'Anwar Sanafar', '', 'M', '9344228276', '', '', 'fb', 'Other', '', 'Yes', '2020-10-27', '', 'Coimbatore', 'Tamil Nadu', 'Not interested( Low budget 30L)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(96, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-22', 'Nishanth Acoustic', '', 'M', '9789477303', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Switched off', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(97, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-23', 'Rajesh Vignesh', '', 'M', '7373475835', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not answering', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(98, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-23', 'AnandaKumar Krishnasamy', '', 'M', '9159996995', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not answering', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(99, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-23', 'Yuvaraj', '', 'M', '9943943196', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(100, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-23', 'Zulfikhar Ahamed', '', 'M', '9790290004', '', '', 'ig', 'Other', '', 'Yes', '2020-10-27', '', 'Coimbatore', 'Tamil Nadu', 'Not interested( Low budget 30L)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(101, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-23', 'Sai Dinesh', '', 'M', '98435133315', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(102, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-24', 'Nandha Kumar', '', 'M', '8940004643', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that add(PMP BANK)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(103, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-24', 'Gajendran Yadhav', '', 'M', '9788150986', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call not connected', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(104, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-25', 'Manikumar', '', 'M', '9790189869', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not Interested in Podanur area', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(105, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-25', 'Kiruthikanjali', '', 'F', '9940018447', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in kovai pudur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(106, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-25', 'Avani Shukla', '', 'F', '9978637683', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Low Budget', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(107, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-25', 'Jegan Mohan R.', '', 'M', '8754466459', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Jegan Mohan R.will come and site visit by any time', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(108, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-25', 'Sathya', '', 'F', '9123545957', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done. Interested in plot no 18 & 19 (Duplex villa). Discussion going on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(109, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-25', 'Harish', '', 'M', '9994370677', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ads', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(110, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-26', 'SK Shivakumaar', '', 'M', '9600382227', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(111, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-26', 'Raghvendra Goswami', '', 'M', '8127675377', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(112, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-26', 'Parthiban', '', 'M', '9789290629', '', '', 'ig', 'Other', '', 'Yes', '2020-11-08', '', 'Coimbatore', 'Tamil Nadu', 'Very low budget', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(113, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-26', 'Selvamani', '', 'M', '9789779785', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(114, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-26', 'Sathya', '', 'F', '9566320667', '', '', 'ig', 'Interested', '', 'Yes', '2021-01-31', '', 'Coimbatore', 'Tamil Nadu', 'Discussion going on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(115, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-27', 'Karthik Ganesh Karthik', '', 'M', '8531048868', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Switched off', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(116, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-27', 'Vivek Vijayakumar', '', 'M', '9894936886', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 40L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(117, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-27', 'Satheesh Kannan', '', 'M', '9585516034', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in trichy road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(118, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-27', 'Moms Catering Parisutham', '', 'F', '9843035877', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(119, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-28', 'Saheer Habeb', '', 'M', '9894888536', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back on 16/3/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(120, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-28', 'Bernard John', '', 'M', '9363247780', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(121, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-28', 'Ambili Ajayan', '', 'M', '9400997169', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(122, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-28', 'Sheikh Dhavudh', '', 'M', '9894337321', '', '', 'ig', 'Not Interested', '', 'Yes', '2020-11-01', '', 'Coimbatore', 'Tamil Nadu', 'Not interested ( He is not intending to buy house now)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(123, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-28', 'Praveen Kinayath', '', 'M', '8606598606', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(124, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-28', 'Vinoth Kannan', '', 'M', '9894834939', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(125, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-29', 'Nandini Bhattacharjee', '', 'F', '9952175281', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back @ 4.30', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(126, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-29', 'Famitha', '', 'F', '7339544303', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Financial problem, probably next year', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(127, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-29', 'Vasvim', '', 'M', '9600719593', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not Interested in Podanur area', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(128, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-29', 'Manikandan Rangasamy', '', 'M', '9786693223', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expected price 30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(129, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-29', 'SuÌˆpriÌˆyaÌƒ RiÌˆyaÌƒ', '', 'F', '9600553953', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She expected price 30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(130, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-29', 'Arun Kumar', '', 'M', '9025960756', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Switched off', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(131, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-29', 'Revathi', '', 'F', '9941311486', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(132, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-30', 'ibrahim', '', 'M', '9843126345', '', '', 'fb', 'Other', '', 'Yes', '2020-11-01', '', 'Coimbatore', 'Tamil Nadu', 'He said the house is too small - Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(133, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-30', 'Raadikaa', '', 'F', '9003528829', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(134, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-30', 'Karthik Ramalingam', '', 'M', '9952145700', '', '', 'fb', 'Interested', '', 'No', '2020-11-08', '', 'Coimbatore', 'Tamil Nadu', 'Call after 3o clock', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(135, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-30', 'Kalai Radhi', '', 'F', '8754031210', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(136, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-30', 'GaneshKumar', '', 'M', '9894012535', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(137, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-30', 'Mangaiyarkarasi Umamaheshwaran', '', 'F', '9789412627', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(138, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-31', 'S Ramkanth', '', 'M', '9618312122', '', '', 'fb', 'Not Interested', '', 'Yes', '2020-11-01', '', 'Coimbatore', 'Tamil Nadu', 'Not responding', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(139, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-10-31', 'Natarajan Venkiteswaran', '', 'M', '9176039230', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Disconnected the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(140, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-04', 'Jahir Hussain C', '', 'M', '9840414214', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in singanallur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(141, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-04', 'Selvam Saravana', '', 'M', '9845431761', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in ganapathy', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(142, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-05', 'Meenakshi Shridhar', '', 'F', '9167066543', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(143, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-05', 'Thanabal Nagu', '', 'M', '9442638607', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(144, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-05', 'à®°à¯†à®¾à®®à®¾à®©à¯à®¸à¯ à®²à¯‚à®°à¯à®¤à¯', '', 'M', '9940023410', '', '', 'ig', 'Other', '', 'Yes', '2020-11-09', '', 'Coimbatore', 'Tamil Nadu', 'His budget within 35 lakhs', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(145, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-05', 'Mohamed Irfan', '', 'M', '9597869696', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(146, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-05', 'Akila R', '', 'F', '9952599940', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in Singallur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(147, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-05', 'Lakshyavendhan', '', 'M', '9715540603', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in vadavalli', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(148, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-05', 'R. P. Thiyaghakumar', '', 'M', '7825989007', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan drop', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(149, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-05', 'Remesh Nair', '', 'M', '9841300472', '', '', 'fb', 'Not Interested', '', 'Yes', '2020-11-22', '', 'Coimbatore', 'Tamil Nadu', 'Bought another plot', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(150, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-05', 'assdss', '', 'M', '9999999999', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Number incorrect', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(151, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-06', 'Junaitha Banu', '', 'F', '9789670971', '', '', 'fb', 'Not Interested', '', 'Yes', '2021-02-17', '', 'Coimbatore', 'Tamil Nadu', 'Not satisfied with the area', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(152, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-06', 'Babu Subramaniam', '', 'M', '9025219330', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(153, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-06', 'Sharmila Michael', '', 'F', '9843011411', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur area)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(154, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-06', 'Anoop Vattathuvalappil', '', 'M', '9894890530', '', '', 'fb', 'Other', '', 'Yes', '2020-09-11', '', 'Coimbatore', 'Tamil Nadu', 'He expect minimum 5 cents', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(155, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-07', 'Nandhini Vijayakumar', '', 'F', '9789731072', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in avinash road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(156, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-07', 'à®šà®¨à¯à®¤à®¾à®©à®ªà®¾à®°à®¤à®¿. à®šà¯Œ.', '', 'M', '9500747095', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Need individual villa, budget- 35-20 L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(157, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-07', 'Sandeep Kumar', '', 'M', '9003701200', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Transfered to Rajasthan, not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(158, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-07', 'Thiyakarajan Balavenketraman', '', 'M', '8220954964', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(159, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-07', 'Sirisha Duvvuri', '', 'F', '9789420004', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in avinash road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(160, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-07', 'Beaulahgrace Joseph', '', 'F', '9677778354', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in peelamedu', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(161, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-08', 'Janani Jagadeeswaran', '', 'F', '9677442325', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(162, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-08', 'Samant', '', 'M', '9443108788', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Touched it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(163, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-06', 'Jayakumar', '', 'M', '8838729201', '', '', 'Google', 'Not Interested', '', 'Yes', '2020-09-23', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(164, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-09', 'Manoj Kumar', '', 'M', '9003454681', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in other place', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(165, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-09', 'Saravana Kumar', '', 'M', '9994826461', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Out of service', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(166, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-09', 'Suriya Narayanan', '', 'M', '9842256256', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(167, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-09', 'Kumar', '', 'M', '9443025959', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(168, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-11', 'Michael', '', 'M', '9994147012', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call disconnected', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(169, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-11', 'Pavithra Vikash', '', 'F', '9600486482', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another villa', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(170, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-11', 'Murali N', '', 'M', '9842859233', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(171, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-11', 'Sumaiya Begum', '', 'F', '9790011654', '', '', 'fb', 'Interested', '', 'Yes', '2021-01-04', '', 'Coimbatore', 'Tamil Nadu', 'Discussion gng on with family', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(172, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-12', 'Manoj', '', 'M', '9994821111', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped, call after 3 months', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(173, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-12', 'R Sreekumar', '', 'M', '8870881177', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(174, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-12', 'Boopathy', '', 'M', '9994672701', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Need individual villa,Budget 60 L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(175, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-11', 'Varadharajan', '', 'M', '9444867228', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ads', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(176, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-12', 'Jayaraman Ramasamy', '', 'M', '8778426852', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(177, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-14', 'Ark Mbbs', '', 'M', '7502399666', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect minimum 5 cents', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(178, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-15', 'Praveen Kumar', '', 'M', '9894765423', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(179, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-15', 'Prabhath Siddu', '', 'M', '9944715625', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in Eittimalai', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(180, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-15', 'Malinie ViJra', '', 'F', '9952305166', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(181, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-16', 'Aravindkumar Arumugam', '', 'M', '9787551926', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 36L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(182, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-16', 'Rosee Joseph', '', 'F', '9367444027', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ads', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(183, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-16', 'Bhadoriya', '', 'M', '7795608631', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Disconnected the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(184, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-16', 'A C Umamaheswari', '', 'F', '9486100263', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Now not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(185, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-15', 'Balaji', '', 'M', '9989783851', '', '', 'Google', 'Other', '', 'Yes', '2020-11-22', '', 'Coimbatore', 'Tamil Nadu', 'Looking for some other location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(186, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-15', 'Vasanthlakshmi', '', 'F', '9869071952', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in saravanampatti', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(187, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-14', 'Nimeshika', '', 'F', '9080426992', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not response properly', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(188, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-14', 'Ganesh', '', 'M', '9306188679', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in puliyakulam side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(189, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-14', 'G NANDHINI', '', 'F', '9585441997', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ads', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(190, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-14', 'Poovendran', '', 'M', '9487222286', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(191, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-14', 'RAMAMURTHY', '', 'M', '8072413592', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in RS Puram', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(192, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-16', 'Sandhya Karthikeyan', '', 'F', '9043462635', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(193, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-16', 'Ammadurai Sakthivel', '', 'M', '9750854582', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(194, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-16', 'Sadiq', '', 'M', '9846518558', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(195, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-16', 'Akilandeswari Gurunathan', '', 'F', '9597317047', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in Ganapathu', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(196, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-16', 'Rasetha Riyas', '', 'F', '9944573663', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(197, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-16', 'Stephen Christopher', '', 'M', '9843353030', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(198, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-17', 'Santhosh Kumar', '', 'M', '9842217794', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0);
INSERT INTO `temp_excel_tbl` (`id`, `branch_id`, `upload_token`, `uid`, `enquiry_date`, `fname`, `lname`, `gender`, `primary_mobile`, `primary_email`, `profile`, `leadsource`, `lead_status`, `flat_type`, `site_visited`, `site_visit_date`, `address`, `city`, `state`, `description`, `pincode`, `created_at`, `updated_at`, `status`) VALUES
(199, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-17', 'U.A. Siddhiqe', '', 'M', '9677798455', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call barred', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(200, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-17', 'John Bosco', '', 'M', '9345218839', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No budget', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(201, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-18', 'Ashiq Ismail', '', 'M', '9994873659', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Her budget 30 L(Kovil palayam)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(202, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-18', 'Bala', '', 'M', '9994733433', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in Avinash road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(203, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-19', 'Kalai Arasan', '', 'M', '9655287343', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(204, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-18', 'Deepa Gupta', '', 'F', '9360680181', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in Rs puram area', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(205, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-19', 'Tami Zharasan', '', 'M', '9443301881', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in Mathukari area', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(206, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-19', 'Dinesh', '', 'M', '9900635584', '', '', 'ig', 'Other', '', 'Yes', '2020-11-22', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(207, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-19', 'anandan', '', 'M', '8112286206', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Need east faced individual villa, budget-50L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(208, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-18', 'Sharathy Mathavan', '', 'M', '9790019100', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(209, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-18', 'Karthick Catik', '', 'M', '9944941370', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped, If interested call back', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(210, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-18', 'Santhoshkumar Vijayan', '', 'M', '7530025352', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in thudiyalur and saravanampatti', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(211, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-20', 'Rajap Deen', '', 'M', '9042611273', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Need individual villa, budget-35 L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(212, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-20', 'Sasi Kumar', '', 'M', '9944560002', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'After 3 months(In april)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(213, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-20', 'Babu', '', 'M', '9344335706', '', '', 'Fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'May visit on 23/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(214, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-20', 'swathy', '', 'F', '8754012784', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Cordon blue', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(215, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-20', 'Nishath', '', 'M', '9843378110', '', '', 'Fb', 'Other', '', 'Yes', '2020-11-20', '', 'Coimbatore', 'Tamil Nadu', 'He expect budget in 30 lakhs', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(216, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-20', 'Mohammed Rameez', '', 'M', '9944466636', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Now, no idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(217, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-21', 'Vignesh Kumararaja', '', 'M', '9894669648', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back on 27/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(218, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-21', 'Akilasunil Akilasunil', '', 'M', '7667430072', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Low budget', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(219, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-21', 'Karthik Sankar', '', 'M', '9994150475', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(220, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-21', 'Raji G', '', 'M', '8098851027', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(221, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-21', 'Babu Rasheed', '', 'M', '9663310197', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(222, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-21', 'SamPraveen Kumar', '', 'M', '9940643269', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(223, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-21', 'Luv tO mA luV', '', 'M', '9629926759', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in saravanampatti, Villangkurichi', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(224, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-21', 'C P Vinesh', '', 'M', '7200558818', '', '', 'fb', 'Interested', '', 'Yes', '2020-11-29', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done (29.11.20) he expect budget in 50 lakhs', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(225, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-21', 'Sunder', '', 'M', '9003686112', '', '', 'fb', 'Other', '', 'Yes', '2020-11-22', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(226, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-22', 'Sneha Sreedharan', '', 'F', '9445825930', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in avinash road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(227, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-22', 'dalvin12292@gmail.com', '', 'M', '7639495818', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interessted in saravanampatti', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(228, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-22', 'riyazmohamed', '', 'M', '9500997462', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in thudiyalurand Kuniyamuthur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(229, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-23', 'Majestic Mani', '', 'M', '9159937394', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan postponed', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(230, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-23', 'Yuvaraj Raj', '', 'M', '9791716251', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(231, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-23', 'Gnanaguru Samy', '', 'M', '8124355292', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared ,He expect minimum 5 cents', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(232, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-23', 'Usha Sivaraj', '', 'F', '8015333364', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Now, no idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(233, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-24', 'Sankar', '', 'M', '9942283396', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Need individual villa, budget-40 L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(234, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-24', 'S Dhanasekaran', '', 'M', '8610268073', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(235, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-24', 'Latha', '', 'F', '9840054859', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(236, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-24', 'sharmele', '', 'F', '6594264950', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(237, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-25', 'Santhosh nair', '', 'M', '9901388606', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(238, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-25', 'Senthil Nathan', '', 'M', '7358922555', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(239, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-25', 'Raj Kumar', '', 'M', '9944238858', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Budget is too high', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(240, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-25', 'muthukumar', '', 'M', '9842773693', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in Avinash road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(241, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-25', 'Narmadha Ragu', '', 'M', '9043362200', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in Mathukari and Eachanari side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(242, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-25', 'Abilash', '', 'M', '8122122212', '', '', 'fb', 'Other', '', 'Yes', '2020-11-28', '', 'Coimbatore', 'Tamil Nadu', 'He searching land only', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(243, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-26', 'Sanchu', '', 'M', '9715272001', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Out of service', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(244, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-26', 'Aravind', '', 'M', '8903436102', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for rent', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(245, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-26', 'Akhil Dev', '', 'M', '9786294084', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Budget problem. buget-30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(246, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-26', 'Yuvaraj', '', 'M', '8760263637', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested(He expect price 35L)and expect area Nanjundapuram', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(247, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-26', 'Saravanan Sarvan', '', 'M', '9786898933', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Need in Mettupalayam, budget-30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(248, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-27', 'Chitra Karthi', '', 'F', '9176660111', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in kalapatti area', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(249, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-27', '@jOcKeR1411', '', 'M', '9585963830', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Disconnected the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(250, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-27', 'Ramya Christi', '', 'F', '7829049449', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(251, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-27', 'Gowtham', '', 'M', '8072692950', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(252, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-27', 'Mvenkatraman', '', 'M', '9361140001', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in avinash road(Budget 45L)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(253, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-27', 'Muthumanivel', '', 'M', '9600015586', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will visit on 20/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(254, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-27', 'PURUSOTHAMAN', '', 'M', '9791431537', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Now, no idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(255, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-27', 'Vasanth Vasie Raj', '', 'M', '8667856423', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in thudiyalur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(256, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-27', 'Sureshkumar', '', 'M', '9677631666', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Budget problem', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(257, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-28', 'aiju s', '', 'M', '9092391925', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(258, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-28', 'Punitha Sheela', '', 'F', '9629996747', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will visit soon', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(259, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-28', 'Bharath Talavat', '', 'M', '9677333626', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Budget 15 L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(260, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-30', 'Neema Jacob', '', 'F', '7373707047', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back in the evening', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(261, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-28', 'SureshKumar', '', 'M', '9443819878', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(262, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-28', 'Mr_Prabhakar', '', 'M', '9597787835', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 35L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(263, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-29', 'selva', '', 'M', '7708383567', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(264, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-29', 'maheswari', '', 'F', '9944359945', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Out of service', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(265, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-29', 'Kingsly Mathews', '', 'M', '9944994689', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back @ 4', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(266, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-29', 'Siddik Gulam', '', 'M', '9362411998', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(267, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-29', 'Sudish Kumar Acharya', '', 'M', '7402184684', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Already purchased', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(268, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-30', 'Karthikeyan. V. V', '', 'M', '9994964050', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expected price 25 L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(269, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-30', 'Nirmal Kumar Ramamoorthy', '', 'M', '9150286245', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in peelamedu side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(270, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-30', 'Umamaheswari', '', 'F', '9940798845', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in Thudiyalur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(271, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-30', 'Prakash Om', '', 'M', '8524826511', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in Kovaipudur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(272, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-30', 'sowmya', '', 'F', '8122364455', '', '', 'fb', 'Interested', '', 'Yes', '2020-12-13', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done, She prefered site no 5,30th coming for discussion', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(273, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-30', 'Ramaprabha Sundarraj', '', 'M', '9790383240', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She expect price 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(274, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-11-30', 'Nirmal Prabhu', '', 'M', '9894656452', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Already purchased', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(275, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-01', 'Saloni Priyadarshini', '', 'F', '9958580525', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in title park', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(276, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-01', 'Nanda', '', 'M', '9159325946', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(277, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-01', 'Mohamed Noorul Firdouse L', '', 'M', '9677017472', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in Avinash road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(278, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-01', 'Mani Kandan', '', 'M', '9600625851', '', '', 'fb', 'Interested', '', 'Yes', '2021-01-30', '', 'Coimbatore', 'Tamil Nadu', 'Discussion going on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(279, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-01', 'Mohan', '', 'M', '8072643395', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(280, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-01', 'Naveen Kumar', '', 'M', '9047320132', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Should share the details', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(281, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-01', 'Vijayalakshmi Balakrishnan', '', 'F', '9445466070', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', '', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(282, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-01', 'Poorni Anand', '', 'F', '9500670616', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Seen another site', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(283, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-02', 'Afsal', '', 'M', '9037514181', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(284, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-02', 'Jayanthi', '', 'F', '8825809002', '', '', 'Google', 'Not Interested', '', 'Yes', '2020-12-06', '', 'Coimbatore', 'Tamil Nadu', 'Bought some other property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(285, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-02', 'a.s.senthilkumar', '', 'M', '9443128850', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He said that heis looking for individual house for 30L not interested in twin villa and apartment villa', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(286, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-02', 'Christy', '', 'F', '9943629296', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(287, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-02', 'prabhu', '', 'M', '7558123456', '', '', 'fb', 'Interested', '', 'Yes', '2021-01-30', '', 'Coimbatore', 'Tamil Nadu', 'GM coordinated for loan process', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(288, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-03', 'Pranesh P', '', 'M', '9500391067', '', '', 'ig', 'Interested', '', 'Yes', '2020-11-04', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(289, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-03', 'Gokul Hari Prasanth', '', 'M', '7639990015', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in kovilpalayam', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(290, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-03', 'Nithya Lakshmi', '', 'F', '9025519944', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(291, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-03', 'sai_charan', '', 'M', '7708886993', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will decide and call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(292, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-03', 'Logesh Kumar', '', 'M', '9943718262', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(293, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-03', 'Vikram Kumar', '', 'M', '9176771155', '', '', 'fb', 'Interested', '', 'Yes', '2020-12-13', '', 'Coimbatore', 'Tamil Nadu', 'Advance received', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(294, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-03', 'Elavarasan', '', 'M', '9003913394', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(295, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-03', 'Yuva Raj', '', 'M', '8072282063', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(296, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-03', 'Akash', '', 'M', '8220865940', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Cordon', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(297, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-04', 'Veerappa Samy', '', 'M', '7667376343', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(298, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-04', 'Aleem Basha', '', 'M', '9994868629', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expected price 30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(299, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-04', 'Mani Bobby', '', 'M', '8122101948', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in Avinash road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(300, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-04', 'Monisha Mahendran', '', 'F', '8220796916', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'just visit in fb', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(301, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-04', 'Ruban Prasanth', '', 'M', '7845238699', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location, budget-75L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(302, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-04', 'Jeevan Ramachandran Jeevan', '', 'M', '9843488258', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(303, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-04', 'Nantha Hari Paramasivan', '', 'M', '8939929909', '', '', 'fb', 'Other', '', 'Yes', '2020-12-07', '', 'Coimbatore', 'Tamil Nadu', 'Not Interested in Podanur area', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(304, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-05', 'Sakthivel', '', 'M', '9043733102', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared but he interested in singanallur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(305, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-05', 'Aishwarya Murali', '', 'F', '9597345973', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Probably after 1 0r 2 months later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(306, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-05', 'Vishwanath Ramakrishnan', '', 'M', '8072669237', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(307, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-05', 'Logesh Babu', '', 'M', '8220386512', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested right now', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(308, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-06', 'Shikha Purushothaman', '', 'M', '990438383', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared but he interested in singanallur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(309, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-06', 'à®šà®¿à®µà®¾à®©à®¨à¯à®¤à®®à¯ à®¨à®¾à®•à®²à®¿à®™à¯à®•à®®à¯', '', 'M', '9786847979', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in avinash road', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(310, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-06', 'Vignesh Mahendran', '', 'M', '9585614383', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Individual villa in 4 cents, budget 40L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(311, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-06', 'Vibu Thangavel', '', 'M', '8344410000', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in peelamedu', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(312, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-06', 'Karthi Keyan', '', 'M', '9488449388', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 35L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(313, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'Ã‘Ã­yÃ¢z', '', 'M', '8682018640', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(314, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'Shameer Sha', '', 'M', '7708083112', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(315, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'Aarya Ramanan', '', 'M', '7708992019', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared but he interested in kovai pudur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(316, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'S Narayanasamy', '', 'M', '9443721270', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in ramanathapuram', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(317, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'Barath', '', 'M', '8680066676', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in cheenai', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(318, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'Ramkumar', '', 'M', '9894197888', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Budget is too high, Plan dropped', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(319, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'Thiyaku Dx', '', 'M', '9790651427', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(320, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'Krishna kumar', '', 'M', '8526761517', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared but he expected price 35L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(321, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'Sangeetha Sadhasivam', '', 'F', '9994623002', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(322, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'Jeevan Lal', '', 'M', '9367151067', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(323, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'rizwanaanwarbatcha', '', 'F', '940909625', '', '', 'ig', 'Not Interested', '', 'Yes', '2020-12-09', '', 'Coimbatore', 'Tamil Nadu', 'Busy', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(324, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'Praveen RK', '', 'M', '9944911382', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested(No idea for buying house)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(325, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-07', 'Vandhana Elangovan', '', 'M', '9585644938', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call barred', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(326, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-08', 'Raj Kumar', '', 'M', '9566231611', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She expect price 35L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(327, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-08', 'Ramalakshman Kumar', '', 'M', '9894264400', '', '', 'fb', 'Other', '', 'Yes', '2020-12-09', '', 'Coimbatore', 'Tamil Nadu', 'Discussion gng on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(328, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-08', 'Gopala Krishnan', '', 'M', '9443898768', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She expect price 35L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(329, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-08', 'Saranya Gokul', '', 'F', '7010202969', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not Interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(330, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-08', 'Udhaya Kumar M', '', 'M', '9738234523', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in Podanur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(331, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-09', 'Samsuthen', '', 'M', '9786172952', '', '', 'Direct call', 'Follow up', '', 'Yes', '2020-12-12', '', 'Coimbatore', 'Tamil Nadu', 'Once the construction is done he will come', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(332, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-08', 'Andrew Anand', '', 'M', '9600281758', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Switched off', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(333, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-08', 'Abirami', '', 'F', '9578566529', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Booked another house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(334, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-09', 'balaji', '', 'M', '8344433053', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call after 6.30', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(335, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-09', 'Vignesh', '', 'M', '9894943403', '', '', 'ig', 'Not Interested', '', 'Yes', '2020-12-09', '', 'Coimbatore', 'Tamil Nadu', 'Bought some other property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(336, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-09', 'Apkumarr Arr', '', 'M', '9445255448', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(337, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-10', 'Sriram', '', 'M', '9789179306', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She expect price 35L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(338, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-10', 'Govarthanan', '', 'M', '9095433499', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(339, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-10', 'Ravindran J', '', 'M', '9663705300', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(340, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-10', 'GoCool GS', '', 'M', '9003682953', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She expect price 30L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(341, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-11', 'Sudharsan T', '', 'M', '9884883107', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He searching land only', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(342, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-11', 'Gowtham', '', 'M', '9566679251', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget is 30L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(343, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-11', 'Saheel', '', 'M', '9843919982', '', '', 'fb', 'Follow up', '', 'Yes', '2020-11-25', '', 'Coimbatore', 'Tamil Nadu', 'Discussion gng on, Loan problem', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(344, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-11', 'Pradeesh78896', '', 'M', '9944471486', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back @6', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(345, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-11', 'PS Jayachandran', '', 'M', '9047007078', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(346, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-12', 'Thon_Gabriel', '', 'M', '7708121399', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(347, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-12', 'Velayuthan Vinoth', '', 'M', '7845423247', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(348, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-13', 'Gnanm', '', 'M', '', '', '', 'Direct call', 'Follow up', '', 'Yes', '2020-12-13', '', 'Coimbatore', 'Tamil Nadu', 'Discussion gng on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(349, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-13', 'Jadhesh', '', 'M', '7418896495', '', '', 'Direct call', 'Interested', '', 'Yes', '2020-12-13', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(350, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Ramesh Ramakrishnan', '', 'M', '65 93878962', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(351, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Vignesh', '', 'M', '9003556855', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(352, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Sathish', '', 'M', '6484676570', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(353, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Suraj', '', 'M', '8220896965', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Purchased house other side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(354, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Ashwin J', '', 'M', '9677344404', '', '', 'fb', 'Not Interested', '', 'Yes', '2020-12-28', '', 'Coimbatore', 'Tamil Nadu', 'Plan Dropped', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(355, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'suriya', '', 'M', '8870697986', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Busy', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(356, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Abdul Rahuman..', '', 'M', '9750035005', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Need individual villa in 4 cents, budget-70L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(357, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Jayachandran Murugesh', '', 'M', '8489211134', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(358, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Raja Gp', '', 'M', '9789111311', '', '', 'ig', 'Interested', '', 'Yes', '2020-12-27', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done by 27/11/2020', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(359, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Paiya GK', '', 'M', '9080653949', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Already bought another site', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(360, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Vinith_Nambiar', '', 'M', '9994070823', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just clieck that ads', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(361, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Saranya Gokul', '', 'F', '8124996386', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Mobileno incorrect', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(362, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Mani Manivannan', '', 'M', '7708908135', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(363, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Sakthivel S', '', 'M', '9942838444', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(364, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'manikandan , g', '', 'M', '8220645919', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in vadavalli', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(365, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Shareef Hinayathullah', '', 'M', '8870888613', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(366, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Vignesh Kumararaja', '', 'M', '9894669648', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(367, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Gopish Krishnan', '', 'M', '9585445555', '', '', 'ig', 'Not Interested', '', 'Yes', '2020-12-02', '', 'Coimbatore', 'Tamil Nadu', 'His wife Not Interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(368, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'KARTHIKEYAN', '', 'M', '8807175805', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just clieck that ads', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(369, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Rajap Deen', '', 'M', '9042611273', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget 30L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(370, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Abi Raja', '', 'M', '9994758866', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(371, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Parameswari Velmurugan', '', 'F', '9944473677', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She expect price 35 L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(372, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Christy Mekala', '', 'F', '9894061928', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not Interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(373, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Vineesh Thekkat', '', 'M', '9944032171', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in saravanampatti, Villangurichi', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(374, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Navash Khan', '', 'M', '9543043734', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Purchased house other side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(375, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Muruganandtham Ips', '', 'M', '7373411332', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(376, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Vibu Thangavel', '', 'M', '8344410000', '', '', 'fb', 'Other', '', 'Yes', '2020-12-09', '', 'Coimbatore', 'Tamil Nadu', 'Not interested(He looking other side)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(377, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Rosee Joseph', '', 'F', '9367444027', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She expect price 20 to 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(378, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Sarah', '', 'F', '9597823558', '', '', 'fb', 'Other', '', 'Yes', '2020-11-01', '', 'Coimbatore', 'Tamil Nadu', 'Her budget 30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(379, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-15', 'Prasanna Jack', '', 'M', '9688896383', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(380, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-15', 'Kalinga', '', 'M', '9790426029', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared with another person, she is not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(381, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-12', 'Velayuthan Vinoth', '', 'M', '7845423247', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Switched off', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(382, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-14', 'Vishwanath Ramakrishnan', '', 'M', '8072669237', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will visit on 28/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(383, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-15', 'kotesangivemel, balakrishnarao', '', 'M', '6302900321', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(384, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-15', 'Modi Rohit Kumar Govind Bhai', '', 'M', '9979299522', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(385, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-15', 'Prasanth Kavin', '', 'M', '9095464436', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 35L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(386, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-15', 'Anantha murugan', '', 'M', '9790612280', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 40L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(387, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-15', 'Chandraprakash Upadhyay', '', 'M', '9532354356', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(388, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-16', 'Jayachandran Murugesh', '', 'M', '8489211134', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(389, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-16', 'Rp', '', 'M', '7373727278', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(390, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-16', 'Tharani Tapes', '', 'M', '9842811386', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'just click that ads', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(391, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-16', 'suresh', '', 'M', '9787779666', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget 35L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(392, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-16', 'Nishar', '', 'M', '9942847865', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for apatment/villa in 35-40L in vadavalli', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(393, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-16', 'Suresh Kumar', '', 'M', '9443436203', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(394, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-16', 'Siva Kumar', '', 'M', '9626166102', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call not connected', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(395, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-16', 'Êœá´€Ê€Éª á´˜Ê€á´€êœ±á´€á´…', '', 'M', '9791630204', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(396, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-16', 'Vikram Jai', '', 'M', '9087052052', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped(Budget problem)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0);
INSERT INTO `temp_excel_tbl` (`id`, `branch_id`, `upload_token`, `uid`, `enquiry_date`, `fname`, `lname`, `gender`, `primary_mobile`, `primary_email`, `profile`, `leadsource`, `lead_status`, `flat_type`, `site_visited`, `site_visit_date`, `address`, `city`, `state`, `description`, `pincode`, `created_at`, `updated_at`, `status`) VALUES
(397, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-16', 'Anand Kumar S', '', 'M', '9943265968', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in Podanur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(398, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-16', 'Arunkumar', '', 'M', '7373544225', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(399, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-16', 'Nidhi Mahadevan', '', 'F', '9480355353', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', '2000 sqft', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(400, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-17', 'Jenifer', '', 'F', '9384151707', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She expect price 25L to 30L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(401, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-17', 'Vasanthlakshmi', '', 'F', '9869071952', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(402, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-17', 'Nishanth Acoustic', '', 'M', '9789477303', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(403, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-17', 'Saravanakumar Ganeshan', '', 'M', '9003358743', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(404, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-17', 'Darshini M', '', 'F', '9047013463', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Her budget 20L to 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(405, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-17', 'Santhoshkumar Vijayan', '', 'M', '7530025352', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for villa in thudiyalur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(406, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-17', 'v i g n e s h', '', 'M', '9894438794', '', '', 'ig', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Should share the details', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(407, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-17', 'Shanthi', '', 'F', '9942805770', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will visit on 28/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(408, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-18', 'Satheesh', '', 'M', '8925176360', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(409, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-18', 'Bala Divya Dharshana B', '', 'M', '9597601718', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call in march month', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(410, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-18', 'Rajkumar', '', 'M', '7397744700', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(411, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-18', 'Satheesh', '', 'M', '9952433322', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Switched off', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(412, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-18', 'Imran Khan', '', 'M', '9865090821', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(413, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-19', 'A.salomiya', '', 'F', '9629586282', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(414, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-19', 'Vaishaly', '', 'F', '9787138911', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(415, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-19', 'Abdul Rahuman..', '', 'M', '9698795033', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(416, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-19', 'Santhosh Ramsingh', '', 'M', '9686726222', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in kalapatti, saravanampatti', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(417, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-20', 'Lhm Akram', '', 'M', '9443230127', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He said that pothanur area is full mosquito area (Not interested)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(418, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-20', 'krishna', '', 'M', '9043040888', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No idea in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(419, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-20', 'Vishnu', '', 'M', '9487414338', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ads', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(420, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-20', 'Anoop Vattathuvalappil', '', 'M', '9894890530', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Budget problem', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(421, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-20', 'Nazini Shafi', '', 'F', '9894778781', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget 30L to 35L (Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(422, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-20', 'Santhosh', '', 'M', '8148556780', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(423, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-21', 'Darshan. K', '', 'M', '7373362949', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back on 10/3/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(424, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-21', 'Sellamuthu', '', 'M', '8012580967', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ads', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(425, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-21', 'Rajesh G', '', 'M', '9843618385', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Individual villa, budget <40L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(426, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-21', 'Neethi Minu', '', 'M', '7598491286', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'rate is too high', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(427, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-21', 'Balaji', '', 'M', '8754490919', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in vadavalli', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(428, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-21', 'Sundaram Ayyannan', '', 'M', '9487025435', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Need individual villa in 30 L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(429, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-21', 'Vairaprakash Ramakrishnan', '', 'M', '9965580064', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Budget too high', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(430, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-21', 'Karan Bala', '', 'M', '9092058145', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(431, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-21', 'Nanda Kumar', '', 'M', '9994230626', '', '', 'fb', 'Interested', '', 'Yes', '2021-02-17', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done, discussion going on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(432, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-22', 'Thoufeek Rahman', '', 'M', '8438388803', '', '', 'ig', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(433, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-22', 'D. Bharathi', '', 'F', '9843419175', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in gowndampalayam', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(434, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-22', 'Manicka Manjula', '', 'F', '8072904707', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(435, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-22', 'Chellapandi', '', 'M', '9585604521', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(436, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-22', 'Deepak Ganeshan', '', 'M', '8248780465', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(437, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-22', 'Venkatesh Govindaraju', '', 'M', '9965078456', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in Podanur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(438, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-23', 'Prethika', '', 'F', '9003580455', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(439, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-23', 'Arunsamy', '', 'M', '9842409805', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(440, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-23', 'N JAYANTHI', '', 'F', '9025128745', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad.', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(441, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-23', 'Kittu Smiley', '', 'M', '9626696662', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(442, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-23', 'Farzeena Farz', '', 'F', '7708544839', '', '', 'ig', 'Interested', '', 'Yes', '2020-12-27', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done 27/12/2020, Discussion gng on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(443, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-23', 'Iswarya Ramesh', '', 'F', '9791907618', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Her budget 30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(444, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-23', 'Praveen DV', '', 'M', '8072059276', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Disconnected the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(445, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-23', 'Ãƒshwinth', '', 'M', '9095317757', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in perur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(446, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-23', 'Abhishek Shankar', '', 'M', '9994168473', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He may visit on 28/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(447, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-24', 'Prince D Peter', '', 'M', '9789500581', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(448, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-25', 'à®ªà®¿à®°à®•à®¾à®·à¯ à®²à®Ÿà¯à®šà¯à®®à®£à®©à¯', '', 'M', '9952339499', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Need individual villa, budget 30 L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(449, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-26', 'KM Rajkumar', '', 'M', '9445953880', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget 30L to 35L (Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(450, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-26', 'Balaji Hema', '', 'M', '9047321123', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in saravanampatti, Villangurichi', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(451, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-26', 'vijayamohan', '', 'M', '9894705543', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(452, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-26', 'siraj_dio_rider', '', 'M', '9042897300', '', '', 'ig', 'Interested', '', 'Yes', '2020-12-28', '', 'Coimbatore', 'Tamil Nadu', 'Coming tomorrow for discussion (30/12/2020)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(453, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Uma Sakthivel', '', 'M', '9566834387', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Budget 30 to 35L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(454, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Mercy Patrick', '', 'F', '9952142842', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not response properly', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(455, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Ravi', '', 'M', '9994060896', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(456, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Manikandan', '', 'M', '9943562923', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call not connected', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(457, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-26', 'Balaji Krishnan', '', 'M', '8900000001', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(458, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-26', 'Venkat', '', 'M', '9003334556', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He searching rental purpose', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(459, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-27', 'Mohamed thowfeeq', '', 'M', '9677939554', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in saravanampatti', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(460, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-27', 'sdhamodharan266@gmail.com', '', 'M', '9042658468', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He searching land only', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(461, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-27', 'Sowmya Hari', '', 'F', '9566413406', '', '', 'fb', 'Interested', '', 'Yes', '2021-01-03', '', 'Coimbatore', 'Tamil Nadu', 'Advance Received', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(462, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-28', 'Indoshitoschools', '', 'M', '9994449572', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(463, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-28', 'Anish Panicker', '', 'M', '8667707805', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Visited', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(464, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-28', 'Salma boutique coimbatore', '', 'F', '9994860621', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan Dropped, Money Problem', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(465, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-28', 'Balaji Krishnan', '', 'M', '8900000001', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(466, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-28', 'Bala Krishnan', '', 'M', '8848004978', '', '', 'ig', 'Interested', '', 'Yes', '2020-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He like twin villa house, loan process gng on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(467, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-28', 'Sasi Kumar', '', 'M', '9500670037', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget 25L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(468, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-28', 'Magaraj Joshi', '', 'M', '9600787547', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in TVS Nagar', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(469, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-28', 'Vivek Jayaram', '', 'M', '7975763758', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call not connect', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(470, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-28', 'Ruban Prasanth', '', 'M', '7845238699', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', '', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(471, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'G. Ganesh', '', 'M', '9677449107', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(472, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'G.Nagarajan', '', 'M', '9865372325', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Touched it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(473, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Hemavathi V', '', 'F', '9442533977', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(474, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Karthikeyan', '', 'M', '9789468921', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong Number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(475, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Pandava Prasad', '', 'M', '7696735932', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong Number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(476, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Rajesh Kumar', '', 'M', '9894964312', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget 40L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(477, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Kathiresan Kdm', '', 'M', '9444126841', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in Saravanampatti', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(478, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Rajesh Kumar', '', 'M', '9894964312', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(479, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Sheriffa Farook', '', 'F', '8608630924', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(480, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Kethrapal Sethuramalingam', '', 'M', '9566669981', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Booked another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(481, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Sudarsan Ice', '', 'M', '9944540816', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in perur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(482, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Prem Kumar', '', 'M', '9443706727', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(483, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Nallendra Prakash', '', 'M', '9894351638', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget is 30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(484, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Latha Senthil', '', 'F', '7708911611', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She will come and site visit by sat(02/01/2021)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(485, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Kaveri Prakash', '', 'F', '8220052538', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She will come and site visit by sunday(03/01/20210', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(486, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-29', 'Arun S', '', 'M', '7760344816', '', '', 'ig', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He will come and site visit by Jan 2 week', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(487, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-30', 'CA Shyam Sundar', '', 'M', '9843342044', '', '', 'fb', 'Interested', '', 'Yes', '2021-01-03', '', 'Coimbatore', 'Tamil Nadu', 'He will come and site visit by Sat(02/01/2021)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(488, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-30', 'kanagu', '', 'M', '9047492955', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(489, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-30', '@ab_hi', '', 'M', '8778134045', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(490, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-30', 'Naveen Ragavan', '', 'M', '8778753109', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another area', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(491, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-30', '', '', 'M', '9597548543', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(492, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-30', 'Anitha', '', 'F', '9542123438', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(493, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-30', 'julfia', '', 'F', '9894051320', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(494, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-31', 'Algesh Abhimanyu', '', 'M', '9003969141', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Site visit on 18/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(495, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-31', 'Arun', '', 'M', '9943847788', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call barred', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(496, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-31', 'Bikram singh', '', 'M', '8056036753', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in peelamedu', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(497, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-31', 'Vijay', '', 'M', '9811830446', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(498, 0, 'n7cZkv5FGeiv7zx', NULL, '2020-12-31', 'Karuppasamy Kingmaker', '', 'M', '9600502346', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(499, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-01', 'Humaira Imran', '', 'M', '9677876347', '', '', 'ig', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'he will come and site visit by sunday(03/01/20210', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(500, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-01', 'Gopalakrishnan Kandasamy', '', 'M', '9487873981', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(501, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-01', 'Udaya Kumar', '', 'M', '9171117576', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back after 28/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(502, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-01', 'Nagaraj', '', 'M', '8973354321', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He seaarching only land', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(503, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-01', 'SAI LAXMAN H', '', 'M', '9978984737', '', '', 'ig', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'vhe will come and site visit by sunday(03/01/20210', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(504, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-01', 'DJ', '', 'M', '8608311522', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(505, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-01', 'Sandy', '', 'M', '8946041104', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(506, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-01', 'Gowri Shankar', '', 'M', '8754349233', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(507, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-01', 'Ambika Mahesh', '', 'F', '9442093246', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget is 40L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(508, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-02', 'Balakumar Rangaraj', '', 'M', '8667644290', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(509, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-02', 'D Muthu Kumar', '', 'M', '9994425205', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call on 20/3/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(510, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-02', 'Karthi Keyan', '', 'M', '9003463166', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(511, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-02', 'Shanthi', '', 'F', '9942805770', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call on 1/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(512, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-03', 'Durai Ramachandran.B', '', 'M', '9894715437', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call on 1/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(513, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-03', 'Balaji Rajagopal', '', 'M', '9443910511', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Financial problem', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(514, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-03', 'Mohammed Shibli Sheriff', '', 'M', '9150685177', '', '', 'ig', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'he will come and site visit by sunday(10/01/20210', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(515, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-03', 'Venkatesh Govindaraju', '', 'M', '9965078456', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in thudiyalur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(516, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-03', 'arunkumar', '', 'M', '9843961102', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(517, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-03', 'Ponkarthikeyan Ponraj', '', 'M', '9381735007', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(518, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-04', 'Vijay', '', 'M', '9811830446', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call not connected', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(519, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-04', 'Syed', '', 'M', '7708992264', '', '', 'Google', 'Interested', '', 'Yes', '2021-01-25', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done, discusion gng on, interested in twin villa type', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(520, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-04', 'Krishnasamy Chinnadurai', '', 'M', '9442070045', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He searching land only', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(521, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-04', 'Sowjanya', '', 'F', '7708343910', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in thudiyalur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(522, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-04', 'Amal Paul', '', 'M', '9994452496', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(523, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-04', 'shobana kannan', '', 'M', '9500323847', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Purchased another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(524, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-04', 'Muraliraj Rathinam', '', 'M', '9786679014', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Busy', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(525, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-05', 'Sathish', '', 'M', '9791333393', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(526, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-27', 'Nijam', '', 'M', '9994299914', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget is 25L(Not interested)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(527, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-27', 'Hasan Fathima', '', 'F', '9080294438', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Already purchase', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(528, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-27', 'Nandhini Vijayakumar', '', 'F', '9789731072', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(529, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-27', 'Shri Navamani', '', 'M', '9677580642', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He looking other side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(530, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-27', 'A. V. R', '', 'M', '9486628205', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(531, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-05', 'Vignesh Raj', '', 'M', '8148407465', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(532, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-05', 'Lhm Akram', '', 'M', '9443230127', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(533, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-05', 'sreena', '', 'F', '7012215442', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Now, no idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(534, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-06', 'Sridharan venkatachalam', '', 'M', '9865440614', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(535, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-18', 'Senthil Prabu S P', '', 'M', '9842755444', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Already purchase', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(536, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-18', 'Vineeth', '', 'M', '9841278595', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(537, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-18', 'B@3...^', '', 'M', '7708918951', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back after 2 days', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(538, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-18', 'Bala Gopalan', '', 'M', '8807529014', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Asking 2 months time', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(539, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-17', 'Vairaprakash Ramakrishnan', '', 'M', '9965580064', '', '', 'fb', 'Interested', '', 'Yes', '2021-01-24', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done , discussion gng on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(540, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-17', 'Thiruarsh', '', 'M', '9600249627', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Out of service', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(541, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-17', 'VM Kutty', '', 'M', '7598215278', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(542, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-17', 'VIGNESHWARAN', '', 'M', '9003888386', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call @ 2.30', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(543, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-17', 'Sharmi John', '', 'F', '7667217466', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(544, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-17', 'Aifas Henna Studio coimbatore', '', 'M', '9894575576', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(545, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-17', 'Saravana kumar', '', 'M', '9884151558', '', '', 'ig', 'Interested', '', 'Yes', '2021-01-22', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done, discussion gng on, individual villa', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(546, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-17', 'Vibu Thangavel', '', 'M', '8300074466', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(547, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-17', 'Sathish Kumar A', '', 'M', '9791333393', '', '', 'ig', 'Not Interested', '', 'Yes', '2021-01-11', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done by 11/01/2021, not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(548, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-16', 'Vetharaj Vetharaj', '', 'M', '9443081722', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget is 40L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(549, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-16', 'Nicholas Hendry', '', 'M', '8056666742', '', '', 'ig', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He may visit anytime', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(550, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-16', 'RAJKUMAR', '', 'M', '9711247175', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not intersted', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(551, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-16', 'Raj', '', 'M', '7373727278', '', '', 'ig', 'Interested', '', 'Yes', '2021-01-19', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done by 19/01/2021,Dissusion gng on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(552, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-16', 'Shareef Hinayathullah', '', 'M', '8870888613', '', '', 'ig', 'Interested', '', 'Yes', '2021-01-21', '', 'Coimbatore', 'Tamil Nadu', 'In discussion', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(553, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-15', 'shanmugam', '', 'M', '7871933391', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(554, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-15', 'Philip Raja', '', 'M', '9843999784', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(555, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-15', 'Kittu Smiley', '', 'M', '9626696662', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Callback @2.30', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(556, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-14', 'Ramya Christi', '', 'F', '7829049449', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Busy', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(557, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-14', 'Rajesh Kumar', '', 'M', '9894964312', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back in the evening', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(558, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-14', 'Rajeev', '', 'M', '9360513138', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Switched off', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(559, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-14', 'ALVYN ABRAHAM', '', 'M', '8838249450', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget is 40L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(560, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-14', 'Semmal', '', 'M', '9842650626', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(561, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-13', 'Anand Rajan', '', 'M', '7736841770', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Individual villa, budget-40L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(562, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-13', 'Rahul Karthick', '', 'M', '9677734765', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call not connected', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(563, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-13', 'Sasi Kumar', '', 'M', '9500670037', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Repeat call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(564, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-13', 'Saheer Habeb', '', 'M', '9894888536', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(565, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-13', 'Krishna Badkrishna', '', 'M', '9894822697', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Repeat call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(566, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-13', 'Shareef', '', 'M', '8870888613', '', '', 'Google', 'Interested', '', 'Yes', '2021-01-21', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done, Discussion gng on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(567, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-13', 'Rajesh', '', 'M', '7373727278', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Repeat call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(568, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-13', 'Nicholas', '', 'M', '8056666742', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Repeat call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(569, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-13', 'Vetharaj A', '', 'M', '9443081722', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Repeat call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(570, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-13', 'Sathish', '', 'M', '9791333393', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Repeat call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(571, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-18', 'Rosee Joseph', '', 'F', '9367444027', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in 25L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(572, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-18', 'Omar Faroque', '', 'M', '7904598123', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Repeat call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(573, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-18', 'Vishwanath Ramakrishnan', '', 'M', '7904598123', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Busy', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(574, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-18', 'Irfan Qureishi.S', '', 'M', '8056475684', '', '', 'ig', 'Interested', '', 'Yes', '2021-01-25', '', 'Coimbatore', 'Tamil Nadu', 'Finilazed appartment villa, still in discussion', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(575, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-19', 'Pon Kiruthika', '', 'F', '8903403589', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call not connected', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(576, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-21', 'OMAR Faroque', '', 'M', '9486476071', '', '', 'Google', 'Interested', '', 'Yes', '2021-01-24', '', 'Coimbatore', 'Tamil Nadu', 'Today again coming to the site for finalizing', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(577, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-21', 'Arun', '', 'M', '98843 44772', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(578, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-21', 'Baskaran Bas', '', 'M', '9597635173', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(579, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-21', 'Mohammed', '', 'M', '6379245058', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He searching land only', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(580, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-21', 'Raji Rajidhyalan', '', 'F', '9841275049', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(581, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-21', 'Deleepan Marx(senthil)', '', 'M', '8939888935', '', '', 'fb', 'Interested', '', 'Yes', '2021-01-24', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done, discussion gng on plot no 19', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(582, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-21', 'Asif Iqbal', '', 'M', '8428887866', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(583, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-22', 'Mohamed Irfan', '', 'M', '9597869696', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Old client', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(584, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-22', 'Anu Merciya Sebastian', '', 'F', '8825955392', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(585, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-22', 'Raji', '', 'F', '9841725549', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Busy', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(586, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-22', 'RadhikaDevi', '', 'M', '8547683621', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No network', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(587, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-22', 'Akram', '', 'M', '9443230127', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(588, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-22', 'Xavier Deepak', '', 'M', '9488361245', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(589, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-22', 'Sathya', '', 'F', '9123545957', '', '', 'fb', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will visit on 14/2/21or 15/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(590, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-22', 'bhhshs', '', 'M', '9655589669', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(591, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-23', 'Sumitha', '', 'F', '9940940904', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(592, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-23', 'live_free_love.celina', '', 'M', '9597380329', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(593, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-23', 'Marlyn Rose', '', 'F', '9791800391', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the call', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(594, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-24', 'Anoop Rao', '', 'M', '9952023555', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Old client', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(595, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-25', 'Santhosh', '', 'M', '9840899609', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in saravanampatti(budget 40L)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0);
INSERT INTO `temp_excel_tbl` (`id`, `branch_id`, `upload_token`, `uid`, `enquiry_date`, `fname`, `lname`, `gender`, `primary_mobile`, `primary_email`, `profile`, `leadsource`, `lead_status`, `flat_type`, `site_visited`, `site_visit_date`, `address`, `city`, `state`, `description`, `pincode`, `created_at`, `updated_at`, `status`) VALUES
(596, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-25', 'Ambily', '', 'M', '7339414969', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Only land', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(597, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-25', 'Marlyn Rose', '', 'F', '8838558060', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Should share the details, budget-35-40L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(598, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-25', 'Sasikala', '', 'F', '8156914925', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in Podanur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(599, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-25', 'iPhonecellbuddy', '', 'M', '9487035371', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Budget 35L(Not interested)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(600, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-25', '', '', 'M', '7871005857', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call barred', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(601, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-25', 'Deep Yadav', '', 'M', '6369781690', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Number not in use', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(602, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-26', 'Siva Kumar', '', 'M', '9345965573', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(603, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-26', 'Karthi Keyan', '', 'M', '9865924954', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for plot', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(604, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-26', 'mohamed zulfar', '', 'M', '9944720202', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Twin villa details shared', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(605, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-26', 'vijayalakshmi', '', 'F', '9789489643', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not yet decided', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(606, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-26', 'Prawin Kumar', '', 'M', '9500026333', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'budget 35 to 45L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(607, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-26', 'Dinesh King', '', 'M', '9080184300', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget 25L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(608, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-26', 'Jeeva Ramasamy', '', 'M', '9003974303', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(609, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-26', 'Dinesh', '', 'M', '9900635584', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(610, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-26', 'Abilash', '', 'M', '8122122212', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget 35L(Individul villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(611, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-27', 'Fabiha Sultan', '', 'F', '9363220484', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in thudiyalur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(612, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-27', 'Avis Immanuel', '', 'M', '9894390409', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(613, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-27', 'Sharfu', '', 'M', '9171829900', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Budget is high. Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(614, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-27', 'crzz jery #', '', 'M', '7904450881', '', '', 'ig', 'Interested', '', 'Yes', '2021-01-25', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done , discussion gng on, Individual villa', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(615, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-27', 'Shyama Nair', '', 'F', '9445211865', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(616, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-27', 'Karthickeyan', '', 'M', '8754976208', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget is 40L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(617, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-28', 'gayatri', '', 'F', '9446749943', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(618, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-28', 'Ramesh vardhini', '', 'M', '9994874944', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plan dropped, budget problem', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(619, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-28', 'crzz jery #(Sulthan)', '', 'M', '7904450881', '', '', 'Google', 'Interested', '', 'Yes', '2021-01-25', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done, discussion gng on, individual villa', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(620, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-28', 'priya senthil', '', 'F', '9865505640', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(621, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-28', 'PÃ stor Mani', '', 'M', '9442368957', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He interested in thudiyalur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(622, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-28', 'Sudhy Sudheer', '', 'M', '8086346668', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back after a week', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(623, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-28', 'Jegan Amuthan', '', 'M', '8825635193', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(624, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-28', 'Rahamath Nisha', '', 'M', '8124933867', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(625, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-28', 'Kesavan Ramanujam', '', 'M', '9655733207', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Switched off', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(626, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-29', 'Philip Corlings', '', 'M', '8825849186', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(627, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-29', 'Angel', '', 'F', '8072738158', '', '', 'fb', 'Interested', '', 'Yes', '2021-01-25', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done, discussion gng on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(628, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-29', 'REYEESH', '', 'M', '9442217629', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Intersted in perur side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(629, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-29', 'Singaravel Srinivasan', '', 'M', '9942572720', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(630, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-29', 'Karthik Durairaj', '', 'M', '9500696840', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(631, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-29', 'Subash Chander', '', 'M', '8122924320', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'His budget is 30 L(Individual villa)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(632, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Vijay Mane', '', 'M', '', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared to him', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(633, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Antony Marshell', '', 'M', '8124490958', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will visit on 21/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(634, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Shakila Banu', '', 'F', '9944867048', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No idea for buying house', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(635, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Karan Singh', '', 'M', '8128976063', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(636, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Saravanan Rangarajan', '', 'M', '9943489897', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He will come and site visit sunday', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(637, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Soundar Tamil', '', 'M', '9003391032', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Switched off', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(638, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'P.M.M.Jawahar', '', 'M', '9894687005', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(639, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Vignesh Babu', '', 'M', '7373486732', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Old client', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(640, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Sirajudeen S', '', 'M', '9894999483', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(641, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Tamil Selvan', '', 'M', '9894829198', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He will come and site visit by sunday', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(642, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-31', 'Ravinder Kumar', '', 'M', '94422 39390', '', '', 'fb', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for another location', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(643, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-31', 'Naseema', '', 'F', '7402161247', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He will come and site visit by sunday', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(644, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-31', 'Rahul', '', 'M', '7010607036', '', '', 'ig', 'Not Interested', '', 'Yes', '2021-02-07', '', 'Coimbatore', 'Tamil Nadu', 'In discussion', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(645, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-31', 'Dina Dinesh', '', 'M', '9486266665', '', '', 'fb', 'Interested', '', 'Yes', '2021-02-02', '', 'Coimbatore', 'Tamil Nadu', 'Advance received on 4th Feb 2021', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(646, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Chandra Prakash', '', 'M', '9445236361', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He looking rental', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(647, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Suresh Srailway', '', 'M', '9095151986', '', '', 'fb', 'Interested', '', 'Yes', '2021-02-12', '', 'Coimbatore', 'Tamil Nadu', 'In discussion', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(648, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Shamna Barkath', '', 'F', '9677356640', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(649, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-01-30', 'Thinesh Ignatius', '', 'M', '7708455486', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared to him', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(650, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-01', 'Jery', '', 'M', '7904450881', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Old client', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(651, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-01', 'Shafeek', '', 'M', '9655428129', '', '', 'Google', 'Follow up', '', 'Yes', '2021-02-14', '', 'Coimbatore', 'Tamil Nadu', 'Interested in site no 19 & 18', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(652, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-01', 'Banu', '', 'F', '9367676555', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not Interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(653, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-01', 'Dinesh', '', 'M', '9840163384', '', '', 'Google', 'Interested', '', 'Yes', '2020-11-22', '', 'Coimbatore', 'Tamil Nadu', 'Interestec in 5 B apartment villa, in discussion', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(654, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-01', 'pradeep kumar', '', 'M', '9944965461', '', '', 'Google', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He will come any time for site visit', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(655, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-01', 'Jadhesh', '', 'M', '7418896495', '', '', 'Google', 'Not Interested', '', 'Yes', '2020-12-13', '', 'Coimbatore', 'Tamil Nadu', 'Plan Dropped because of marriage', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(656, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-01', 'Raja Gp', '', 'M', '9789111311', '', '', 'Google', 'Interested', '', 'Yes', '2020-12-27', '', 'Coimbatore', 'Tamil Nadu', 'Come back for discussion tomorrow 30/12', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(657, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-01', 'Ramanathan Iyer', '', 'M', '9820327235', '', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(658, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'SIVARAM', '', 'M', '7904623747', '', '', 'Chatbot', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Need individual villa, budget-40 L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(659, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Baby shalini', '', 'F', '9994430304', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She interested in udayar palayam', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(660, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Abdul Rahuman..', '', 'M', '9750035005', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He will come and site visit by sunday', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(661, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Nizar Ahmed', '', 'M', '9894937862', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(662, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Mani Kandan', '', 'M', '7845480589', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not seen the details yet, will call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(663, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Ravichandra Naganna', '', 'M', '9787617437', '', '', 'Google', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He intersted in singanallur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(664, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Joel.J', '', 'M', '9842030296', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He expect price 30L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(665, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Dr Manikandan Rangasamy', '', 'M', '9786693223', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back @ 6.10', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(666, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Varun Prabhu', '', 'M', '8056607103', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(667, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Shahir Peace', '', 'M', '9943315544', '', '', 'Google', 'Follow up', '', 'Yes', '2021-02-14', '', 'Coimbatore', 'Tamil Nadu', 'Discussion with family', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(668, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'hema', '', 'F', '9061484488', '', '', 'Chatbot', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(669, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Edhayamaran Kannan', '', 'M', '9487217671', '', '', 'Google', 'Not Interested', '', 'Yes', '2021-02-07', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(670, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'vishak', '', 'M', '8870484563', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(671, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Haridass Chandran', '', 'M', '9994484605', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(672, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Vijayakumar', '', 'M', '9677755042', '', '', 'fb', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(673, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-09', 'Sukumaran S', '', 'M', '8056060837', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(674, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-09', 'Joykumari chungkham', '', 'M', '7005347773', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(675, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-09', 'Pradeep Kumar MK', '', 'M', '9972558795', '', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong number', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(676, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-09', 'Ramesh', '', 'M', '9843180422', 'deeparamesh8679@gmail.com', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared to him', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(677, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-09', 'Balaprasath Anand', '', 'M', '9994331350', 'balaprasath2121@gmail.com', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(678, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-09', '', '', 'M', '9677755042', 'a.vijayakumar@email.com', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(679, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-09', 'Sai', '', 'M', '9751405791', 'saishank123@gmail.com', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Any time site visit', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(680, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Shobana', '', 'F', '9944195043', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', '35LIndivdual villa', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(681, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Saikumar kmc', '', 'M', '9940907355', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(682, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Simson', '', 'M', '9443400433', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(683, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Mohammed Rafi', '', 'M', '9894349696', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(684, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Shahir Peace', '', 'M', '9943315544', 'askshahir@gmail.com', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'In discussion', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(685, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Ravisankar', '', 'M', '9688135049', 's.sankarravi@gmail.com', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not interested in pothanur', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(686, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Ramesh', '', 'M', '9659427587', '', '', 'ig', 'Interested', '', 'Yes', '2021-02-14', '', 'Coimbatore', 'Tamil Nadu', 'Will inform status wednesday', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(687, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Anand Rtr', '', 'M', '9715336249', '', '', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(688, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Parameshwari Vasanthakumar', '', 'F', '7550127151', '', '', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for individual villa, budget - 30 L', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(689, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Happy aara', '', 'M', '9500371559', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(690, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Ganesh', '', 'M', '9489944848', 'ganeshramadas26@gmail.com', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Bought another property', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(691, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', '', '', 'M', '9566222026', '', 'Senior Engineer', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'sunday site visit', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(692, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'David Billa', '', 'M', '9952197998', '', 'Self employed', 'ig', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'He seaarching only land', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(693, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'mrs Venkat', '', 'M', '9900439126', '', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Busy', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(694, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Vignesh', '', 'M', '9843431043', '', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(695, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Raggavendhra', '', 'M', '8778733153', 'rag5kovai@gmail.com', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(696, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'R Pari', '', 'M', '9443515527', 'parinkl@gmail.com', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'In discussion', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(697, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Raadikaa', '', 'F', '9003528829', '', '', 'Google', 'Follow up', '', 'Yes', '2021-02-14', '', 'Coimbatore', 'Tamil Nadu', 'Site visit done, I will inform later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(698, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Nithya Priya', '', 'F', '9344075099', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not reachable', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(699, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Vignesh Balan', '', 'M', '9047281046', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back after 6', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(700, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Naveen Boopathy', '', 'M', '9500963564', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Will call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(701, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Brindha Subramanian', '', 'F', '8072838353', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not response', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(702, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-12', 'Sivadasan', '', 'M', '9585532439', 'rethinttf@gmail.com', '', 'Google', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(703, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-12', 'Vignesh', '', 'M', '9047281046', '', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn cl', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(704, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-12', 'Hemamalini', '', 'M', '9787808333', '', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(705, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-12', 'Logesh', '', 'M', '9566629886', '', '', 'Chatbot', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Clicked it by mistake', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(706, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-12', 'Jaya', '', 'M', '8072739057', '', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(707, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-12', 'Sivadasan', '', 'M', '9444985742', '', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call @ 6.15', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(708, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Bala', '', 'M', '9894211621', '', '', 'ig', 'Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in 3 BHK house, he may visit on 23/2/21', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(709, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Anbarasan Manickam', '', 'M', '9842237099', '', 'ceo', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'call back', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(710, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Vicky', '', 'M', '9688997470', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(711, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-15', 'Revathy', '', 'F', '9659908119', '', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'call back', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(712, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-15', 'SelvarajRN', '', 'M', '971504952233', 'selvainnet@gmail.com', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(713, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Anees Baig', '', 'M', '9500426888', '', 'Merchandising Manager', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared to him', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(714, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-10', 'Abhay', '', 'M', '9360513130', '', '', 'ig', 'Interested', '', 'Yes', '2021-02-15', '', 'Coimbatore', 'Tamil Nadu', 'Interested in site no 1 & 18, will come again with wife to confirm', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(715, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'R D Suresh Kumar', '', 'M', '9362246622', '', 'RSM', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Out off station', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(716, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Serin Sweety', '', 'F', '8754336162', '', 'HRBP', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No response', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(717, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Varun', '', 'M', '9494596656', '', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Out of service', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(718, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Aadhidhiya', '', 'M', '9566634009', '', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Call back later', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(719, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Saran Kumar', '', 'M', '8008152881', '', 'Business consultant', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Site visit scheduled on Monday', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(720, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Arvinth', '', 'M', '9894164014', '', '', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(721, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Karthick Subramaniam', '', 'M', '9597379813', '', 'Pastry Chef', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in the property will make the reminder call on Saturday', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(722, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Pasi Hina', '', 'F', '8870992822', '', 'Academician', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Site visit scheduled on Saturday', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(723, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Ilamparithi kanagasabapathy', '', 'M', '9894920567', '', 'Buisness', 'ig', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Interested in green fields have to make reminder call after 10 days (2.5C)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(724, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Ganesh', '', 'M', '8056308331', '', 'SE', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'No answer', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(725, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-18', 'Hari', '', 'M', '8667705974', '', '', 'Chatbot', 'Other', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Looking for a property at Thudiyalur ( Budget 75L)', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(726, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-18', 'Revathi', '', 'F', '9659908119', '', '', 'Chatbot', 'Not Interested', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She looking other side', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(727, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'CELEBRES', '', 'M', '6379933093', '', '', 'ig', '', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She will come and site visit today', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(728, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Dr.Melo-dy-îžèŒ¶', '', 'F', '37455265227', '', 'Doctor', 'ig', '', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Wrong no', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(729, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Gowtham Aravind', '', 'M', '9047557055', '', 'self employed', 'fb', '', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Line busy', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(730, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Sandeep Sandy', '', 'M', '9043891220', '', 'Student', 'ig', '', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(731, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Rajeshkannan Chockalingam', '', 'M', '9500475413', '', 'Assistant General Manager', 'fb', '', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Just click that ad', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(732, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Saravanan', '', 'M', '9629777002', '', 'Owner', 'fb', '', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'She will come and site visit by any time', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(733, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-19', 'Shanmugam', '', 'M', '8050852550', '', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the cl', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(734, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-19', 'Arasakumar', '', 'M', '9445067999', '', '', 'Chatbot', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared to him', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(735, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-19', 'RK VINOD', '', 'M', '9846846882', 'rkvinod47@gmail.com', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', ', He will come and site visit any time', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(736, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-19', 'Ram', '', 'M', '7550126434', 'ramkumar_1956@ymail.com', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Busy', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(737, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-19', 'Nishanth', '', 'M', '9600530370', '', '', 'Google', 'Follow up', '', 'Yes', '2021-02-18', '', 'Coimbatore', 'Tamil Nadu', 'Interested in greenfield, discussion going on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(738, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-19', 'Sudarsanan', '', 'M', '9994053933', '', '', 'Google', 'Follow up', '', 'Yes', '2021-02-17', '', 'Coimbatore', 'Tamil Nadu', 'Plot No. 19, again, he will come by sunday', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(739, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-19', 'Banu', '', 'F', '9786209051', '', '', 'Google', 'Follow up', '', 'Yes', '2021-02-17', '', 'Coimbatore', 'Tamil Nadu', 'Twin villa interested, Discussion gng on', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(740, 0, 'n7cZkv5FGeiv7zx', NULL, '2021-02-19', 'Pragash', '', 'M', '8903608314', '', '', 'Google', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the cl', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(741, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Chinnaian Namachivayam', '', 'M', '9943992994', '', 'Engineer Technician', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Plot No.18 confirmed,', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(742, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Ram', '', 'M', '7550126434', '', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the cl', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(743, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'srinivasan ramanan', '', 'M', '9841667968', 'srinigold@yahoo.co.in', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Cl back at 5 clk', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(744, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'pravin', '', 'M', '9099898283', 'pravin_dots@yahoo.com', '', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Cl not coonect', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(745, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Sri Rega Vellimalai', '', 'M', '9952405105', '', 'IT', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Details shared to her', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(746, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Karthik Raj', '', 'M', '9443319077', '', 'Junior Engineer', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Cl back at evg 4 clk', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(747, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Faizal Rahman', '', 'M', '9952777006', '', 'Air-conditioning service center', 'ig', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the cl', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0),
(748, 0, 'n7cZkv5FGeiv7zx', NULL, '1970-01-01', 'Gurpreet Singh Kahlon', '', 'M', '9655321380', '', 'manager', 'fb', 'Follow up', '', '', '1899-12-30', '', 'Coimbatore', 'Tamil Nadu', 'Not attn the cl', '', '2021-02-24 14:46:50', '2021-02-24 14:46:50', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vas_master_tbl`
--

CREATE TABLE `vas_master_tbl` (
  `id` int(11) NOT NULL,
  `token` varchar(250) NOT NULL,
  `item_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vas_master_tbl`
--

INSERT INTO `vas_master_tbl` (`id`, `token`, `item_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'insurance', 'Insurance', 1, 1, '2021-07-26 13:53:15', '2021-07-26 13:54:51'),
(2, 'finance', 'Finance', 1, 1, '2021-07-26 13:55:00', '2021-07-26 13:55:00');

-- --------------------------------------------------------

--
-- Table structure for table `virtualtour_menusettings_tbl`
--

CREATE TABLE `virtualtour_menusettings_tbl` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT 0,
  `menu` varchar(250) NOT NULL,
  `tour_code` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `virtualtour_menusettings_tbl`
--

INSERT INTO `virtualtour_menusettings_tbl` (`id`, `branch_id`, `menu`, `tour_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'hide', '', 1, '2020-10-06 15:13:33', '2020-10-06 15:13:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_type_tbl`
--
ALTER TABLE `activity_type_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adhoc_image_tbl`
--
ALTER TABLE `adhoc_image_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adhoc_log_tbl`
--
ALTER TABLE `adhoc_log_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adhoc_request_tbl`
--
ALTER TABLE `adhoc_request_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignlead_tbl`
--
ALTER TABLE `assignlead_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_tbl`
--
ALTER TABLE `block_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch_master_tbl`
--
ALTER TABLE `branch_master_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chit_master_tbl`
--
ALTER TABLE `chit_master_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_settings_tbl`
--
ALTER TABLE `company_settings_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactinfo_list_tbl`
--
ALTER TABLE `contactinfo_list_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactinfo_tbl`
--
ALTER TABLE `contactinfo_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customerdatasheet_tbl`
--
ALTER TABLE `customerdatasheet_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_profile_tbl`
--
ALTER TABLE `customer_profile_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_tbl`
--
ALTER TABLE `customer_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datasheet_tbl`
--
ALTER TABLE `datasheet_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposite_master_tbl`
--
ALTER TABLE `deposite_master_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_permission_tbl`
--
ALTER TABLE `employee_permission_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_role_master_tbl`
--
ALTER TABLE `employee_role_master_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_tbl`
--
ALTER TABLE `employee_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flatvilla_documenttype_list_tbl`
--
ALTER TABLE `flatvilla_documenttype_list_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flatvilla_documenttype_tbl`
--
ALTER TABLE `flatvilla_documenttype_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_type_master`
--
ALTER TABLE `flat_type_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_type_masterlist`
--
ALTER TABLE `flat_type_masterlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `floor_tbl`
--
ALTER TABLE `floor_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_image_tbl`
--
ALTER TABLE `gallery_image_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_tbl`
--
ALTER TABLE `gallery_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gendral_documents_tbl`
--
ALTER TABLE `gendral_documents_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_tbl`
--
ALTER TABLE `invoice_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kyc_list_tbl`
--
ALTER TABLE `kyc_list_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kyc_tbl`
--
ALTER TABLE `kyc_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kyc_type_tbl`
--
ALTER TABLE `kyc_type_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_activity_tbl`
--
ALTER TABLE `lead_activity_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_gallery_tbl`
--
ALTER TABLE `lead_gallery_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_logs_tbl`
--
ALTER TABLE `lead_logs_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_source_tbl`
--
ALTER TABLE `lead_source_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_tbl`
--
ALTER TABLE `lead_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_type_tbl`
--
ALTER TABLE `lead_type_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_tbl`
--
ALTER TABLE `news_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_email_tbl`
--
ALTER TABLE `notification_email_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paymentinfo_tbl`
--
ALTER TABLE `paymentinfo_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_type_tbl`
--
ALTER TABLE `project_type_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_broucher_tbl`
--
ALTER TABLE `property_broucher_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_document_list_items`
--
ALTER TABLE `property_document_list_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_document_list_tbl`
--
ALTER TABLE `property_document_list_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_gallery_tbl`
--
ALTER TABLE `property_gallery_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_gendral_documents_tbl`
--
ALTER TABLE `property_gendral_documents_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_rooms_list_tbl`
--
ALTER TABLE `property_rooms_list_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_tbl`
--
ALTER TABLE `property_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_type_tbl`
--
ALTER TABLE `request_type_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session_tbl`
--
ALTER TABLE `session_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_customer_excel_tbl`
--
ALTER TABLE `temp_customer_excel_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_excel_tbl`
--
ALTER TABLE `temp_excel_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vas_master_tbl`
--
ALTER TABLE `vas_master_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `virtualtour_menusettings_tbl`
--
ALTER TABLE `virtualtour_menusettings_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_type_tbl`
--
ALTER TABLE `activity_type_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `adhoc_image_tbl`
--
ALTER TABLE `adhoc_image_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adhoc_log_tbl`
--
ALTER TABLE `adhoc_log_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adhoc_request_tbl`
--
ALTER TABLE `adhoc_request_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `assignlead_tbl`
--
ALTER TABLE `assignlead_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `block_tbl`
--
ALTER TABLE `block_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `branch_master_tbl`
--
ALTER TABLE `branch_master_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `chit_master_tbl`
--
ALTER TABLE `chit_master_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `company_settings_tbl`
--
ALTER TABLE `company_settings_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contactinfo_list_tbl`
--
ALTER TABLE `contactinfo_list_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contactinfo_tbl`
--
ALTER TABLE `contactinfo_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customerdatasheet_tbl`
--
ALTER TABLE `customerdatasheet_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_profile_tbl`
--
ALTER TABLE `customer_profile_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=361;

--
-- AUTO_INCREMENT for table `customer_tbl`
--
ALTER TABLE `customer_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `datasheet_tbl`
--
ALTER TABLE `datasheet_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `deposite_master_tbl`
--
ALTER TABLE `deposite_master_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employee_permission_tbl`
--
ALTER TABLE `employee_permission_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `employee_role_master_tbl`
--
ALTER TABLE `employee_role_master_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `employee_tbl`
--
ALTER TABLE `employee_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `flatvilla_documenttype_list_tbl`
--
ALTER TABLE `flatvilla_documenttype_list_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `flatvilla_documenttype_tbl`
--
ALTER TABLE `flatvilla_documenttype_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `flat_type_master`
--
ALTER TABLE `flat_type_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `flat_type_masterlist`
--
ALTER TABLE `flat_type_masterlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `floor_tbl`
--
ALTER TABLE `floor_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `gallery_image_tbl`
--
ALTER TABLE `gallery_image_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gallery_tbl`
--
ALTER TABLE `gallery_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gendral_documents_tbl`
--
ALTER TABLE `gendral_documents_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_tbl`
--
ALTER TABLE `invoice_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kyc_list_tbl`
--
ALTER TABLE `kyc_list_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kyc_tbl`
--
ALTER TABLE `kyc_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kyc_type_tbl`
--
ALTER TABLE `kyc_type_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `lead_activity_tbl`
--
ALTER TABLE `lead_activity_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=579;

--
-- AUTO_INCREMENT for table `lead_gallery_tbl`
--
ALTER TABLE `lead_gallery_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `lead_logs_tbl`
--
ALTER TABLE `lead_logs_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=185;

--
-- AUTO_INCREMENT for table `lead_source_tbl`
--
ALTER TABLE `lead_source_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `lead_tbl`
--
ALTER TABLE `lead_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=865;

--
-- AUTO_INCREMENT for table `lead_type_tbl`
--
ALTER TABLE `lead_type_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `news_tbl`
--
ALTER TABLE `news_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_email_tbl`
--
ALTER TABLE `notification_email_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paymentinfo_tbl`
--
ALTER TABLE `paymentinfo_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_type_tbl`
--
ALTER TABLE `project_type_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_broucher_tbl`
--
ALTER TABLE `property_broucher_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_document_list_items`
--
ALTER TABLE `property_document_list_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_document_list_tbl`
--
ALTER TABLE `property_document_list_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_gallery_tbl`
--
ALTER TABLE `property_gallery_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_gendral_documents_tbl`
--
ALTER TABLE `property_gendral_documents_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_rooms_list_tbl`
--
ALTER TABLE `property_rooms_list_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_tbl`
--
ALTER TABLE `property_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_type_tbl`
--
ALTER TABLE `request_type_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `session_tbl`
--
ALTER TABLE `session_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=232;

--
-- AUTO_INCREMENT for table `temp_customer_excel_tbl`
--
ALTER TABLE `temp_customer_excel_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_excel_tbl`
--
ALTER TABLE `temp_excel_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=749;

--
-- AUTO_INCREMENT for table `vas_master_tbl`
--
ALTER TABLE `vas_master_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `virtualtour_menusettings_tbl`
--
ALTER TABLE `virtualtour_menusettings_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
