/*--------Anand 24/08/2021 ---------*/
ALTER TABLE `employee_permission_tbl`  ADD `branch_master` INT(1) NULL DEFAULT '0'  AFTER `vam_master`,  ADD `employee_role_master` INT(1) NULL DEFAULT '0'  AFTER `branch_master`;
ALTER TABLE `employee_tbl` CHANGE `role` `role` INT(1) NULL DEFAULT '0';

ALTER TABLE `activity_type_tbl` ADD `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `session_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `admin_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `customer_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `block_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `floor_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `flatvilla_documenttype_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `flatvilla_documenttype_list_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `property_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `contactinfo_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `contactinfo_list_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `paymentinfo_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `employee_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `employee_permission_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `gendral_documents_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `property_broucher_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `request_type_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `notification_email_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `gallery_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `gallery_image_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `flat_type_master` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `flat_type_masterlist` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `property_rooms_list_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `property_gallery_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `property_document_list_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `property_document_list_items` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `property_gendral_documents_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `adhoc_request_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `adhoc_log_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `adhoc_image_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `kyc_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `kyc_list_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `kyc_type_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `virtualtour_menusettings_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `invoice_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `company_settings_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `activity_type_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `lead_type_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `lead_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `assignlead_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `lead_logs_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `lead_activity_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `project_type_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `temp_excel_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `datasheet_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `temp_customer_excel_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `customerdatasheet_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `news_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `lead_source_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `customer_profile_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `chit_master_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `deposite_master_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `vas_master_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `branch_master_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `employee_role_master_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `lead_gallery_tbl` add `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;

/*--------Anand 28/08/2021 ---------*/
ALTER TABLE `employee_tbl` ADD `assigned_branch` VARCHAR(100) NULL DEFAULT NULL AFTER `status`;
ALTER TABLE `employee_tbl` ADD `assigned_bm` INT(11) NULL DEFAULT NULL AFTER `assigned_branch`;

/*--------Anand 30/08/2021 ---------*/
ALTER TABLE `employee_tbl` CHANGE `assigned_bm` `assigned_employee` INT(11) NULL DEFAULT NULL;
ALTER TABLE `temp_excel_tbl`  ADD `chit` VARCHAR(100) NULL DEFAULT NULL  AFTER `pincode`,  ADD `deposite` VARCHAR(100) NULL DEFAULT NULL  AFTER `chit`,  ADD `vas` VARCHAR(100) NULL DEFAULT NULL  AFTER `deposite`;

/*--------Anand 07/09/2021  ---------*/
ALTER TABLE `employee_tbl` CHANGE `branch_id` `branch_id` VARCHAR(50) NULL DEFAULT '0';
UPDATE `employee_tbl` SET `role` = NULL WHERE `employee_tbl`.`id` = 1;
