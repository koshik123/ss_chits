-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2021 at 08:20 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ss_chit_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee_role_master_tbl`
--

CREATE TABLE `employee_role_master_tbl` (
  `id` int(11) NOT NULL,
  `token` varchar(250) NOT NULL,
  `employee_role` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_role_master_tbl`
--

INSERT INTO `employee_role_master_tbl` (`id`, `token`, `employee_role`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'sr.executivesales', 'Sr. Executive Sales', 1, 1, '2021-08-24 11:20:33', '2021-08-24 11:20:33'),
(2, 'executiveoffice', 'Executive Office', 1, 1, '2021-08-24 11:21:09', '2021-08-24 11:21:09'),
(3, 'telecaller', 'Telecaller', 1, 1, '2021-08-24 11:23:01', '2021-08-24 11:23:01'),
(4, 'partner', 'Partner', 1, 1, '2021-08-24 11:23:41', '2021-08-24 11:23:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee_role_master_tbl`
--
ALTER TABLE `employee_role_master_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee_role_master_tbl`
--
ALTER TABLE `employee_role_master_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
