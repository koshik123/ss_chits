<?php
	

	// Website Base Path
	define('BASEPATH', HOST_NAME.'/');
	// Website Admin Path
	define('COREPATH', HOST_NAME.'/');

	// Define image folder path
	define('IMGPATH', HOST_NAME.'/lib/images/');
	// Define css folder path
	define('CSSPATH', HOST_NAME.'/lib/css/');
	// Define js folder path
	define('JSPATH', HOST_NAME.'/lib/js/');
	// Define js folder path
	define('FONTPATH', HOST_NAME.'/lib/fonts/');
	// Define Plugin folder path
	define('PLUGINS', HOST_NAME.'/lib/plugins/');
	// Define Uploads Image path
	define('UPLOADS', HOST_NAME.'/resource/uploads/');
	// Define Plugin folder path
	define('SRCIMG', HOST_NAME.'/resource/uploads/srcimg/');
	// Define Uploads Image path
	define('DOCUMENTS', HOST_NAME.'/resource/uploads/document/');

	// Define Plugin folder path
	define('FRONTSRCIMG', HOST_NAME.'/resource/uploads/srcimg/');
	

	//--------------------- DATABASE CONNECTION ---------------------//

	// Connect Mysql DB
	$connect = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);

	
?>